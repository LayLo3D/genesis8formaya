import maya.mel as mel
import maya.cmds as cmds


def Aiko8JCMs(character, blendShapeGroup):
    pyBlendShapesName = blendShapeGroup
    if not cmds.objExists('{}.pJCMAiko8AbdomenUpperFwd'.format(pyBlendShapesName)):
        error = character + ' specific JCMs were not created.\n\n' + character + ' JCMs were either not exported out properly or the wrong file was selected for import.'
        cmds.warning(error.replace('\n\n', ' '))
        return error

    # pJCMAiko8AbdomenUpperFwd
    mel.eval('setAttr "abdomenUpper.rotateX" 0;')
    mel.eval('setDrivenKeyframe -currentDriver abdomenUpper.rotateX {0}.pJCMAiko8AbdomenUpperFwd;'.format(
        pyBlendShapesName))
    mel.eval('setAttr "abdomenUpper.rotateX" 40;')
    mel.eval('setAttr "{0}.pJCMAiko8AbdomenUpperFwd" 1;'.format(pyBlendShapesName))
    mel.eval('setDrivenKeyframe -currentDriver abdomenUpper.rotateX {0}.pJCMAiko8AbdomenUpperFwd;'.format(
        pyBlendShapesName))
    mel.eval('setAttr "abdomenUpper.rotateX" 0;')

    # pJCMAiko8ChestLowerFwd
    mel.eval('setAttr "chestLower.rotateX" 0;')
    mel.eval('setDrivenKeyframe -currentDriver chestLower.rotateX {0}.pJCMAiko8ChestLowerFwd;'.format(
        pyBlendShapesName))
    mel.eval('setAttr "chestLower.rotateX" 35;')
    mel.eval('setAttr "{0}.pJCMAiko8ChestLowerFwd" 1;'.format(pyBlendShapesName))
    mel.eval('setDrivenKeyframe -currentDriver chestLower.rotateX {0}.pJCMAiko8ChestLowerFwd;'.format(
        pyBlendShapesName))
    mel.eval('setAttr "chestLower.rotateX" 0;')

    # pJCMAiko8CollarBndUpL
    mel.eval('setAttr "lCollar.rotateZ" 0;')
    mel.eval('setDrivenKeyframe -currentDriver lCollar.rotateZ {0}.pJCMAiko8CollarBndUpL;'.format(
        pyBlendShapesName))
    mel.eval('setAttr "lCollar.rotateZ" 55;')
    mel.eval('setAttr "{0}.pJCMAiko8CollarBndUpL" 1;'.format(pyBlendShapesName))
    mel.eval('setDrivenKeyframe -currentDriver lCollar.rotateZ {0}.pJCMAiko8CollarBndUpL;'.format(
        pyBlendShapesName))
    mel.eval('setAttr "lCollar.rotateZ" 0;')

    # pJCMAiko8CollarBndUpR
    mel.eval('setAttr "rCollar.rotateZ" 0;')
    mel.eval('setDrivenKeyframe -currentDriver rCollar.rotateZ {0}.pJCMAiko8CollarBndUpR;'.format(
        pyBlendShapesName))
    mel.eval('setAttr "rCollar.rotateZ" -55;')
    mel.eval('setAttr "{0}.pJCMAiko8CollarBndUpR" 1;'.format(pyBlendShapesName))
    mel.eval('setDrivenKeyframe -currentDriver rCollar.rotateZ {0}.pJCMAiko8CollarBndUpR;'.format(
        pyBlendShapesName))
    mel.eval('setAttr "rCollar.rotateZ" 0;')

    # pJCMAiko8FootBndUpL
    mel.eval('setAttr "lFoot.rotateX" 0;')
    mel.eval(
        'setDrivenKeyframe -currentDriver lFoot.rotateX {0}.pJCMAiko8FootBndUpL;'.format(pyBlendShapesName))
    mel.eval('setAttr "lFoot.rotateX" -45;')
    mel.eval('setAttr "{0}.pJCMAiko8FootBndUpL" 1;'.format(pyBlendShapesName))
    mel.eval(
        'setDrivenKeyframe -currentDriver lFoot.rotateX {0}.pJCMAiko8FootBndUpL;'.format(pyBlendShapesName))
    mel.eval('setAttr "lFoot.rotateX" 0;')

    # pJCMAiko8FootBndUpR
    mel.eval('setAttr "rFoot.rotateX" 0;')
    mel.eval(
        'setDrivenKeyframe -currentDriver rFoot.rotateX {0}.pJCMAiko8FootBndUpR;'.format(pyBlendShapesName))
    mel.eval('setAttr "rFoot.rotateX" -45;')
    mel.eval('setAttr "{0}.pJCMAiko8FootBndUpR" 1;'.format(pyBlendShapesName))
    mel.eval(
        'setDrivenKeyframe -currentDriver rFoot.rotateX {0}.pJCMAiko8FootBndUpR;'.format(pyBlendShapesName))
    mel.eval('setAttr "rFoot.rotateX" 0;')

    # pJCMAiko8ForearmBnd_n80L
    mel.eval('setAttr "lForearmBend.rotateY" 0;')
    mel.eval('setDrivenKeyframe -currentDriver lForearmBend.rotateY {0}.pJCMAiko8ForearmBnd_n80L;'.format(
        pyBlendShapesName))
    mel.eval('setAttr "lForearmBend.rotateY" -80;')
    mel.eval('setAttr "{0}.pJCMAiko8ForearmBnd_n80L" 1;'.format(pyBlendShapesName))
    mel.eval('setDrivenKeyframe -currentDriver lForearmBend.rotateY {0}.pJCMAiko8ForearmBnd_n80L;'.format(
        pyBlendShapesName))
    mel.eval('setAttr "lForearmBend.rotateY" 0;')

    # pJCMAiko8ForearmBnd_80R
    mel.eval('setAttr "rForearmBend.rotateY" 0;')
    mel.eval('setDrivenKeyframe -currentDriver rForearmBend.rotateY {0}.pJCMAiko8ForearmBnd_80R;'.format(
        pyBlendShapesName))
    mel.eval('setAttr "rForearmBend.rotateY" 80;')
    mel.eval('setAttr "{0}.pJCMAiko8ForearmBnd_80R" 1;'.format(pyBlendShapesName))
    mel.eval('setDrivenKeyframe -currentDriver rForearmBend.rotateY {0}.pJCMAiko8ForearmBnd_80R;'.format(
        pyBlendShapesName))
    mel.eval('setAttr "rForearmBend.rotateY" 0;')

    # pJCMAiko8ForearmBndL
    mel.eval('setAttr "lForearmBend.rotateY" 0;')
    mel.eval('setDrivenKeyframe -currentDriver lForearmBend.rotateY {0}.pJCMAiko8ForearmBndL;'.format(
        pyBlendShapesName))
    mel.eval('setAttr "lForearmBend.rotateY" -135;')
    mel.eval('setAttr "{0}.pJCMAiko8ForearmBndL" 1;'.format(pyBlendShapesName))
    mel.eval('setDrivenKeyframe -currentDriver lForearmBend.rotateY {0}.pJCMAiko8ForearmBndL;'.format(
        pyBlendShapesName))
    mel.eval('setAttr "lForearmBend.rotateY" 0;')

    # pJCMAiko8ForearmBndR
    mel.eval('setAttr "rForearmBend.rotateY" 0;')
    mel.eval('setDrivenKeyframe -currentDriver rForearmBend.rotateY {0}.pJCMAiko8ForearmBndR;'.format(
        pyBlendShapesName))
    mel.eval('setAttr "rForearmBend.rotateY" 135;')
    mel.eval('setAttr "{0}.pJCMAiko8ForearmBndR" 1;'.format(pyBlendShapesName))
    mel.eval('setDrivenKeyframe -currentDriver rForearmBend.rotateY {0}.pJCMAiko8ForearmBndR;'.format(
        pyBlendShapesName))
    mel.eval('setAttr "rForearmBend.rotateY" 0;')

    # pJCMAiko8HeadBndFwd
    mel.eval('setAttr "head.rotateX" 0;')
    mel.eval('setDrivenKeyframe -currentDriver head.rotateX {0}.pJCMAiko8HeadBndFwd;'.format(
        pyBlendShapesName))
    mel.eval('setAttr "head.rotateX" 25;')
    mel.eval('setAttr "{0}.pJCMAiko8HeadBndFwd" 1;'.format(pyBlendShapesName))
    mel.eval('setDrivenKeyframe -currentDriver head.rotateX {0}.pJCMAiko8HeadBndFwd;'.format(
        pyBlendShapesName))
    mel.eval('setAttr "head.rotateX" 0;')

    # pJCMAiko8HeadTwistL
    mel.eval('setAttr "head.rotateY" 0;')
    mel.eval('setDrivenKeyframe -currentDriver head.rotateY {0}.pJCMAiko8HeadTwistL;'.format(
        pyBlendShapesName))
    mel.eval('setAttr "head.rotateY" 22;')
    mel.eval('setAttr "{0}.pJCMAiko8HeadTwistL" 1;'.format(pyBlendShapesName))
    mel.eval('setDrivenKeyframe -currentDriver head.rotateY {0}.pJCMAiko8HeadTwistL;'.format(
        pyBlendShapesName))
    mel.eval('setAttr "head.rotateY" 0;')

    # pJCMAiko8HeadTwistR
    mel.eval('setAttr "head.rotateY" 0;')
    mel.eval('setDrivenKeyframe -currentDriver head.rotateY {0}.pJCMAiko8HeadTwistR;'.format(
        pyBlendShapesName))
    mel.eval('setAttr "head.rotateY" -22;')
    mel.eval('setAttr "{0}.pJCMAiko8HeadTwistR" 1;'.format(pyBlendShapesName))
    mel.eval('setDrivenKeyframe -currentDriver head.rotateY {0}.pJCMAiko8HeadTwistR;'.format(
        pyBlendShapesName))
    mel.eval('setAttr "head.rotateY" 0;')

    # pJCMAiko8ShinBndBck_90L
    mel.eval('setAttr "lShin.rotateX" 0;')
    mel.eval(
        'setDrivenKeyframe -currentDriver lShin.rotateX {0}.pJCMAiko8ShinBndBck_90L;'.format(
            pyBlendShapesName))
    mel.eval('setAttr "lShin.rotateX" 90;')
    mel.eval('setAttr "{0}.pJCMAiko8ShinBndBck_90L" 1;'.format(pyBlendShapesName))
    mel.eval(
        'setDrivenKeyframe -currentDriver lShin.rotateX {0}.pJCMAiko8ShinBndBck_90L;'.format(
            pyBlendShapesName))
    mel.eval('setAttr "lShin.rotateX" 0;')

    # pJCMAiko8ShinBndBck_90R
    mel.eval('setAttr "rShin.rotateX" 0;')
    mel.eval(
        'setDrivenKeyframe -currentDriver rShin.rotateX {0}.pJCMAiko8ShinBndBck_90R;'.format(
            pyBlendShapesName))
    mel.eval('setAttr "rShin.rotateX" 90;')
    mel.eval('setAttr "{0}.pJCMAiko8ShinBndBck_90R" 1;'.format(pyBlendShapesName))
    mel.eval(
        'setDrivenKeyframe -currentDriver rShin.rotateX {0}.pJCMAiko8ShinBndBck_90R;'.format(
            pyBlendShapesName))
    mel.eval('setAttr "rShin.rotateX" 0;')

    # pJCMAiko8ShinBndBckL
    mel.eval('setAttr "lShin.rotateX" 0;')
    mel.eval(
        'setDrivenKeyframe -currentDriver lShin.rotateX {0}.pJCMAiko8ShinBndBckL;'.format(pyBlendShapesName))
    mel.eval('setAttr "lShin.rotateX" 155;')
    mel.eval('setAttr "{0}.pJCMAiko8ShinBndBckL" 1;'.format(pyBlendShapesName))
    mel.eval(
        'setDrivenKeyframe -currentDriver lShin.rotateX {0}.pJCMAiko8ShinBndBckL;'.format(pyBlendShapesName))
    mel.eval('setAttr "lShin.rotateX" 0;')

    # pJCMAiko8ShinBndBckR
    mel.eval('setAttr "rShin.rotateX" 0;')
    mel.eval(
        'setDrivenKeyframe -currentDriver rShin.rotateX {0}.pJCMAiko8ShinBndBckR;'.format(pyBlendShapesName))
    mel.eval('setAttr "rShin.rotateX" 155;')
    mel.eval('setAttr "{0}.pJCMAiko8ShinBndBckR" 1;'.format(pyBlendShapesName))
    mel.eval(
        'setDrivenKeyframe -currentDriver rShin.rotateX {0}.pJCMAiko8ShinBndBckR;'.format(pyBlendShapesName))
    mel.eval('setAttr "rShin.rotateX" 0;')

    # pJCMAiko8ShoulderBndUpL
    mel.eval('setAttr "lShldrBend.rotateZ" 0;')
    mel.eval('setDrivenKeyframe -currentDriver lShldrBend.rotateZ {0}.pJCMAiko8ShoulderBndUpL;'.format(
        pyBlendShapesName))
    mel.eval('setAttr "lShldrBend.rotateZ" 90;')
    mel.eval('setAttr "{0}.pJCMAiko8ShoulderBndUpL" 1;'.format(pyBlendShapesName))
    mel.eval('setDrivenKeyframe -currentDriver lShldrBend.rotateZ {0}.pJCMAiko8ShoulderBndUpL;'.format(
        pyBlendShapesName))
    mel.eval('setAttr "lShldrBend.rotateZ" 0;')

    # pJCMAiko8ShoulderBndUpR
    mel.eval('setAttr "rShldrBend.rotateZ" 0;')
    mel.eval('setDrivenKeyframe -currentDriver rShldrBend.rotateZ {0}.pJCMAiko8ShoulderBndUpR;'.format(
        pyBlendShapesName))
    mel.eval('setAttr "rShldrBend.rotateZ" -90;')
    mel.eval('setAttr "{0}.pJCMAiko8ShoulderBndUpR" 1;'.format(pyBlendShapesName))
    mel.eval('setDrivenKeyframe -currentDriver rShldrBend.rotateZ {0}.pJCMAiko8ShoulderBndUpR;'.format(
        pyBlendShapesName))
    mel.eval('setAttr "rShldrBend.rotateZ" 0;')

    # pJCMAiko8ShoulderFwd_n95L
    mel.eval('setAttr "lShldrBend.rotateY" 0;')
    mel.eval('setDrivenKeyframe -currentDriver lShldrBend.rotateY {0}.pJCMAiko8ShoulderFwd_n95L;'.format(
        pyBlendShapesName))
    mel.eval('setAttr "lShldrBend.rotateY" -95;')
    mel.eval('setAttr "{0}.pJCMAiko8ShoulderFwd_n95L" 1;'.format(pyBlendShapesName))
    mel.eval('setDrivenKeyframe -currentDriver lShldrBend.rotateY {0}.pJCMAiko8ShoulderFwd_n95L;'.format(
        pyBlendShapesName))
    mel.eval('setAttr "lShldrBend.rotateY" 0;')

    # pJCMAiko8ShoulderFwd_95R
    mel.eval('setAttr "rShldrBend.rotateY" 0;')
    mel.eval('setDrivenKeyframe -currentDriver rShldrBend.rotateY {0}.pJCMAiko8ShoulderFwd_95R;'.format(
        pyBlendShapesName))
    mel.eval('setAttr "rShldrBend.rotateY" 95;')
    mel.eval('setAttr "{0}.pJCMAiko8ShoulderFwd_95R" 1;'.format(pyBlendShapesName))
    mel.eval('setDrivenKeyframe -currentDriver rShldrBend.rotateY {0}.pJCMAiko8ShoulderFwd_95R;'.format(
        pyBlendShapesName))
    mel.eval('setAttr "rShldrBend.rotateY" 0;')

    # pJCMAiko8ThighSideSideL
    mel.eval('setAttr "lThighBend.rotateZ" 0;')
    mel.eval('setDrivenKeyframe -currentDriver lThighBend.rotateZ {0}.pJCMAiko8ThighSideSideL;'.format(
        pyBlendShapesName))
    mel.eval('setAttr "lThighBend.rotateZ" 85;')
    mel.eval('setAttr "{0}.pJCMAiko8ThighSideSideL" 1;'.format(pyBlendShapesName))
    mel.eval('setDrivenKeyframe -currentDriver lThighBend.rotateZ {0}.pJCMAiko8ThighSideSideL;'.format(
        pyBlendShapesName))
    mel.eval('setAttr "lThighBend.rotateZ" 0;')

    # pJCMAiko8ThighSideSideR
    mel.eval('setAttr "rThighBend.rotateZ" 0;')
    mel.eval('setDrivenKeyframe -currentDriver rThighBend.rotateZ {0}.pJCMAiko8ThighSideSideR;'.format(
        pyBlendShapesName))
    mel.eval('setAttr "rThighBend.rotateZ" -85;')
    mel.eval('setAttr "{0}.pJCMAiko8ThighSideSideR" 1;'.format(pyBlendShapesName))
    mel.eval('setDrivenKeyframe -currentDriver rThighBend.rotateZ {0}.pJCMAiko8ThighSideSideR;'.format(
        pyBlendShapesName))
    mel.eval('setAttr "rThighBend.rotateZ" 0;')
