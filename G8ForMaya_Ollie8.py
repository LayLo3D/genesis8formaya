import maya.mel as mel
import maya.cmds as cmds


def Ollie8JCMs(character, blendShapeGroup):
    pyBlendShapesName = blendShapeGroup
    if not cmds.objExists('{}.pJCM_Ollie8_Abdomen2Fwd_40'.format(pyBlendShapesName)):
        error = character + ' specific JCMs were not created.\n\n' + character + ' JCMs were either not exported out properly or the wrong file was selected for import.'
        cmds.warning(error.replace('\n\n', ' '))
        return error

    # pJCM_Ollie8_Abdomen2Fwd_40
    mel.eval('setAttr "abdomenUpper.rotateX" 0;')
    mel.eval('setDrivenKeyframe -currentDriver abdomenUpper.rotateX {0}.pJCM_Ollie8_Abdomen2Fwd_40;'.format(
        pyBlendShapesName))
    mel.eval('setAttr "abdomenUpper.rotateX" 40;')
    mel.eval('setAttr "{0}.pJCM_Ollie8_Abdomen2Fwd_40" 1;'.format(pyBlendShapesName))
    mel.eval('setDrivenKeyframe -currentDriver abdomenUpper.rotateX {0}.pJCM_Ollie8_Abdomen2Fwd_40;'.format(
        pyBlendShapesName))
    mel.eval('setAttr "abdomenUpper.rotateX" 0;')

    # pJCM_Ollie8_ChestFwd_35
    mel.eval('setAttr "chestLower.rotateX" 0;')
    mel.eval('setDrivenKeyframe -currentDriver chestLower.rotateX {0}.pJCM_Ollie8_ChestFwd_35;'.format(
        pyBlendShapesName))
    mel.eval('setAttr "chestLower.rotateX" 35;')
    mel.eval('setAttr "{0}.pJCM_Ollie8_ChestFwd_35" 1;'.format(pyBlendShapesName))
    mel.eval('setDrivenKeyframe -currentDriver chestLower.rotateX {0}.pJCM_Ollie8_ChestFwd_35;'.format(
        pyBlendShapesName))
    mel.eval('setAttr "chestLower.rotateX" 0;')

    # pJCM_Ollie8_ForeArmFwd_75_L
    mel.eval('setAttr "lForearmBend.rotateY" 0;')
    mel.eval('setDrivenKeyframe -currentDriver lForearmBend.rotateY {0}.pJCM_Ollie8_ForeArmFwd_75_L;'.format(
        pyBlendShapesName))
    mel.eval('setAttr "lForearmBend.rotateY" -75;')
    mel.eval('setAttr "{0}.pJCM_Ollie8_ForeArmFwd_75_L" 1;'.format(pyBlendShapesName))
    mel.eval('setDrivenKeyframe -currentDriver lForearmBend.rotateY {0}.pJCM_Ollie8_ForeArmFwd_75_L;'.format(
        pyBlendShapesName))
    mel.eval('setAttr "lForearmBend.rotateY" 0;')

    # pJCM_Ollie8_ForeArmFwd_75_R
    mel.eval('setAttr "rForearmBend.rotateY" 0;')
    mel.eval('setDrivenKeyframe -currentDriver rForearmBend.rotateY {0}.pJCM_Ollie8_ForeArmFwd_75_R;'.format(
        pyBlendShapesName))
    mel.eval('setAttr "rForearmBend.rotateY" 75;')
    mel.eval('setAttr "{0}.pJCM_Ollie8_ForeArmFwd_75_R" 1;'.format(pyBlendShapesName))
    mel.eval('setDrivenKeyframe -currentDriver rForearmBend.rotateY {0}.pJCM_Ollie8_ForeArmFwd_75_R;'.format(
        pyBlendShapesName))
    mel.eval('setAttr "rForearmBend.rotateY" 0;')

    # pJCM_Ollie8_ShinBend_90_L
    mel.eval('setAttr "lShin.rotateX" 0;')
    mel.eval(
        'setDrivenKeyframe -currentDriver lShin.rotateX {0}.pJCM_Ollie8_ShinBend_90_L;'.format(pyBlendShapesName))
    mel.eval('setAttr "lShin.rotateX" 90;')
    mel.eval('setAttr "{0}.pJCM_Ollie8_ShinBend_90_L" 1;'.format(pyBlendShapesName))
    mel.eval(
        'setDrivenKeyframe -currentDriver lShin.rotateX {0}.pJCM_Ollie8_ShinBend_90_L;'.format(pyBlendShapesName))
    mel.eval('setAttr "lShin.rotateX" 0;')

    # pJCM_Ollie8_ShinBend_90_R
    mel.eval('setAttr "rShin.rotateX" 0;')
    mel.eval(
        'setDrivenKeyframe -currentDriver rShin.rotateX {0}.pJCM_Ollie8_ShinBend_90_R;'.format(pyBlendShapesName))
    mel.eval('setAttr "rShin.rotateX" 90;')
    mel.eval('setAttr "{0}.pJCM_Ollie8_ShinBend_90_R" 1;'.format(pyBlendShapesName))
    mel.eval(
        'setDrivenKeyframe -currentDriver rShin.rotateX {0}.pJCM_Ollie8_ShinBend_90_R;'.format(pyBlendShapesName))
    mel.eval('setAttr "rShin.rotateX" 0;')

    # pJCM_Ollie8_ShinBend_155_L
    mel.eval('setAttr "lShin.rotateX" 0;')
    mel.eval(
        'setDrivenKeyframe -currentDriver lShin.rotateX {0}.pJCM_Ollie8_ShinBend_155_L;'.format(pyBlendShapesName))
    mel.eval('setAttr "lShin.rotateX" 155;')
    mel.eval('setAttr "{0}.pJCM_Ollie8_ShinBend_155_L" 1;'.format(pyBlendShapesName))
    mel.eval(
        'setDrivenKeyframe -currentDriver lShin.rotateX {0}.pJCM_Ollie8_ShinBend_155_L;'.format(pyBlendShapesName))
    mel.eval('setAttr "lShin.rotateX" 0;')

    # pJCM_Ollie8_ShinBend_155_R
    mel.eval('setAttr "rShin.rotateX" 0;')
    mel.eval(
        'setDrivenKeyframe -currentDriver rShin.rotateX {0}.pJCM_Ollie8_ShinBend_155_R;'.format(pyBlendShapesName))
    mel.eval('setAttr "rShin.rotateX" 155;')
    mel.eval('setAttr "{0}.pJCM_Ollie8_ShinBend_155_R" 1;'.format(pyBlendShapesName))
    mel.eval(
        'setDrivenKeyframe -currentDriver rShin.rotateX {0}.pJCM_Ollie8_ShinBend_155_R;'.format(pyBlendShapesName))
    mel.eval('setAttr "rShin.rotateX" 0;')

    # pJCM_Ollie8_ShldrFwd_110_L
    mel.eval('setAttr "lShldrBend.rotateY" 0;')
    mel.eval('setDrivenKeyframe -currentDriver lShldrBend.rotateY {0}.pJCM_Ollie8_ShldrFwd_110_L;'.format(
        pyBlendShapesName))
    mel.eval('setAttr "lShldrBend.rotateY" -110;')
    mel.eval('setAttr "{0}.pJCM_Ollie8_ShldrFwd_110_L" 1;'.format(pyBlendShapesName))
    mel.eval('setDrivenKeyframe -currentDriver lShldrBend.rotateY {0}.pJCM_Ollie8_ShldrFwd_110_L;'.format(
        pyBlendShapesName))
    mel.eval('setAttr "lShldrBend.rotateY" 0;')

    # pJCM_Ollie8_ShldrFwd_110_R
    mel.eval('setAttr "rShldrBend.rotateY" 0;')
    mel.eval('setDrivenKeyframe -currentDriver rShldrBend.rotateY {0}.pJCM_Ollie8_ShldrFwd_110_R;'.format(pyBlendShapesName))
    mel.eval('setAttr "rShldrBend.rotateY" 110;')
    mel.eval('setAttr "{0}.pJCM_Ollie8_ShldrFwd_110_R" 1;'.format(pyBlendShapesName))
    mel.eval('setDrivenKeyframe -currentDriver rShldrBend.rotateY {0}.pJCM_Ollie8_ShldrFwd_110_R;'.format(pyBlendShapesName))
    mel.eval('setAttr "rShldrBend.rotateY" 0;')

    # pJCM_Ollie8_ThighFwd_115_L
    mel.eval('setAttr "lThighBend.rotateX" 0;')
    mel.eval('setDrivenKeyframe -currentDriver lThighBend.rotateX {0}.pJCM_Ollie8_ThighFwd_115_L;'.format(
        pyBlendShapesName))
    mel.eval('setAttr "lThighBend.rotateX" -115;')
    mel.eval('setAttr "{0}.pJCM_Ollie8_ThighFwd_115_L" 1;'.format(pyBlendShapesName))
    mel.eval('setDrivenKeyframe -currentDriver lThighBend.rotateX {0}.pJCM_Ollie8_ThighFwd_115_L;'.format(
        pyBlendShapesName))
    mel.eval('setAttr "lThighBend.rotateX" 0;')

    # pJCM_Ollie8_ThighFwd_115_R
    mel.eval('setAttr "rThighBend.rotateX" 0;')
    mel.eval('setDrivenKeyframe -currentDriver rThighBend.rotateX {0}.pJCM_Ollie8_ThighFwd_115_R;'.format(
        pyBlendShapesName))
    mel.eval('setAttr "rThighBend.rotateX" -115;')
    mel.eval('setAttr "{0}.pJCM_Ollie8_ThighFwd_115_R" 1;'.format(pyBlendShapesName))
    mel.eval('setDrivenKeyframe -currentDriver rThighBend.rotateX {0}.pJCM_Ollie8_ThighFwd_115_R;'.format(
        pyBlendShapesName))
    mel.eval('setAttr "rThighBend.rotateX" 0;')

    # pJCM_Ollie8_ThighFwd_57_L
    mel.eval('setAttr "lThighBend.rotateX" 0;')
    mel.eval('setDrivenKeyframe -currentDriver lThighBend.rotateX {0}.pJCM_Ollie8_ThighFwd_57_L;'.format(
        pyBlendShapesName))
    mel.eval('setAttr "lThighBend.rotateX" -57;')
    mel.eval('setAttr "{0}.pJCM_Ollie8_ThighFwd_57_L" 1;'.format(pyBlendShapesName))
    mel.eval('setDrivenKeyframe -currentDriver lThighBend.rotateX {0}.pJCM_Ollie8_ThighFwd_57_L;'.format(
        pyBlendShapesName))
    mel.eval('setAttr "lThighBend.rotateX" 0;')

    # pJCM_Ollie8_ThighFwd_57_R
    mel.eval('setAttr "rThighBend.rotateX" 0;')
    mel.eval('setDrivenKeyframe -currentDriver rThighBend.rotateX {0}.pJCM_Ollie8_ThighFwd_57_R;'.format(
        pyBlendShapesName))
    mel.eval('setAttr "rThighBend.rotateX" -57;')
    mel.eval('setAttr "{0}.pJCM_Ollie8_ThighFwd_57_R" 1;'.format(pyBlendShapesName))
    mel.eval('setDrivenKeyframe -currentDriver rThighBend.rotateX {0}.pJCM_Ollie8_ThighFwd_57_R;'.format(
        pyBlendShapesName))
    mel.eval('setAttr "rThighBend.rotateX" 0;')

    # pJCM_Ollie8_ThighSide_85_L
    mel.eval('setAttr "lThighBend.rotateZ" 0;')
    mel.eval('setDrivenKeyframe -currentDriver lThighBend.rotateZ {0}.pJCM_Ollie8_ThighSide_85_L;'.format(
        pyBlendShapesName))
    mel.eval('setAttr "lThighBend.rotateZ" 85;')
    mel.eval('setAttr "{0}.pJCM_Ollie8_ThighSide_85_L" 1;'.format(pyBlendShapesName))
    mel.eval('setDrivenKeyframe -currentDriver lThighBend.rotateZ {0}.pJCM_Ollie8_ThighSide_85_L;'.format(
        pyBlendShapesName))
    mel.eval('setAttr "lThighBend.rotateZ" 0;')

    # pJCM_Ollie8_ThighSide_85_R
    mel.eval('setAttr "rThighBend.rotateZ" 0;')
    mel.eval('setDrivenKeyframe -currentDriver rThighBend.rotateZ {0}.pJCM_Ollie8_ThighSide_85_R;'.format(
        pyBlendShapesName))
    mel.eval('setAttr "rThighBend.rotateZ" -85;')
    mel.eval('setAttr "{0}.pJCM_Ollie8_ThighSide_85_R" 1;'.format(pyBlendShapesName))
    mel.eval('setDrivenKeyframe -currentDriver rThighBend.rotateZ {0}.pJCM_Ollie8_ThighSide_85_R;'.format(
        pyBlendShapesName))
    mel.eval('setAttr "rThighBend.rotateZ" 0;')
