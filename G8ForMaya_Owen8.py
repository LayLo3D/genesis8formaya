import maya.mel as mel
import maya.cmds as cmds


def Owen8JCMs(character, blendShapeGroup):
    pyBlendShapesName = blendShapeGroup
    if not cmds.objExists('{}.pJCMOwen8_AbdomenLowerBnd'.format(pyBlendShapesName)):
        error = character + ' specific JCMs were not created.\n\n' + character + ' JCMs were either not exported out properly or the wrong file was selected for import.'
        cmds.warning(error.replace('\n\n', ' '))
        return error

    # pJCMOwen8_AbdomenLowerBnd
    mel.eval('setAttr "abdomenLower.rotateX" 0;')
    mel.eval('setDrivenKeyframe -currentDriver abdomenLower.rotateX {0}.pJCMOwen8_AbdomenLowerBnd;'.format(pyBlendShapesName))
    mel.eval('setAttr "abdomenLower.rotateX" 30;')
    mel.eval('setAttr "{0}.pJCMOwen8_AbdomenLowerBnd" 1;'.format(pyBlendShapesName))
    mel.eval('setDrivenKeyframe -currentDriver abdomenLower.rotateX {0}.pJCMOwen8_AbdomenLowerBnd;'.format(pyBlendShapesName))
    mel.eval('setAttr "abdomenLower.rotateX" 0;')

    # pJCMOwen8_AbdomenUpperBnd
    mel.eval('setAttr "abdomenUpper.rotateX" 0;')
    mel.eval('setDrivenKeyframe -currentDriver abdomenUpper.rotateX {0}.pJCMOwen8_AbdomenUpperBnd;'.format(
        pyBlendShapesName))
    mel.eval('setAttr "abdomenUpper.rotateX" 40;')
    mel.eval('setAttr "{0}.pJCMOwen8_AbdomenUpperBnd" 1;'.format(pyBlendShapesName))
    mel.eval('setDrivenKeyframe -currentDriver abdomenUpper.rotateX {0}.pJCMOwen8_AbdomenUpperBnd;'.format(
        pyBlendShapesName))
    mel.eval('setAttr "abdomenUpper.rotateX" 0;')

    # pJCMOwen8_CollarBndUpL
    mel.eval('setAttr "lCollar.rotateZ" 0;')
    mel.eval(
        'setDrivenKeyframe -currentDriver lCollar.rotateZ {0}.pJCMOwen8_CollarBndUpL;'.format(pyBlendShapesName))
    mel.eval('setAttr "lCollar.rotateZ" 55;')
    mel.eval('setAttr "{0}.pJCMOwen8_CollarBndUpL" 1;'.format(pyBlendShapesName))
    mel.eval(
        'setDrivenKeyframe -currentDriver lCollar.rotateZ {0}.pJCMOwen8_CollarBndUpL;'.format(pyBlendShapesName))
    mel.eval('setAttr "lCollar.rotateZ" 0;')

    # pJCMOwen8_CollarBndUpR
    mel.eval('setAttr "rCollar.rotateZ" 0;')
    mel.eval(
        'setDrivenKeyframe -currentDriver rCollar.rotateZ {0}.pJCMOwen8_CollarBndUpR;'.format(pyBlendShapesName))
    mel.eval('setAttr "rCollar.rotateZ" -55;')
    mel.eval('setAttr "{0}.pJCMOwen8_CollarBndUpR" 1;'.format(pyBlendShapesName))
    mel.eval(
        'setDrivenKeyframe -currentDriver rCollar.rotateZ {0}.pJCMOwen8_CollarBndUpR;'.format(pyBlendShapesName))
    mel.eval('setAttr "rCollar.rotateZ" 0;')

    # pJCMOwen8_CollarFwdL
    mel.eval('setAttr "lCollar.rotateY" 0;')
    mel.eval(
        'setDrivenKeyframe -currentDriver lCollar.rotateY {0}.pJCMOwen8_CollarFwdL;'.format(pyBlendShapesName))
    mel.eval('setAttr "lCollar.rotateY" -26;')
    mel.eval('setAttr "{0}.pJCMOwen8_CollarFwdL" 1;'.format(pyBlendShapesName))
    mel.eval(
        'setDrivenKeyframe -currentDriver lCollar.rotateY {0}.pJCMOwen8_CollarFwdL;'.format(pyBlendShapesName))
    mel.eval('setAttr "lCollar.rotateY" 0;')

    # pJCMOwen8_CollarFwdR
    mel.eval('setAttr "rCollar.rotateY" 0;')
    mel.eval(
        'setDrivenKeyframe -currentDriver rCollar.rotateY {0}.pJCMOwen8_CollarFwdR;'.format(pyBlendShapesName))
    mel.eval('setAttr "rCollar.rotateY" 26;')
    mel.eval('setAttr "{0}.pJCMOwen8_CollarFwdR" 1;'.format(pyBlendShapesName))
    mel.eval(
        'setDrivenKeyframe -currentDriver rCollar.rotateY {0}.pJCMOwen8_CollarFwdR;'.format(pyBlendShapesName))
    mel.eval('setAttr "rCollar.rotateY" 0;')

    # pJCMOwen8_FootBndBckL
    mel.eval('setAttr "lFoot.rotateX" 0;')
    mel.eval('setDrivenKeyframe -currentDriver lFoot.rotateX {0}.pJCMOwen8_FootBndBckL;'.format(pyBlendShapesName))
    mel.eval('setAttr "lFoot.rotateX" -45;')
    mel.eval('setAttr "{0}.pJCMOwen8_FootBndBckL" 1;'.format(pyBlendShapesName))
    mel.eval('setDrivenKeyframe -currentDriver lFoot.rotateX {0}.pJCMOwen8_FootBndBckL;'.format(pyBlendShapesName))
    mel.eval('setAttr "lFoot.rotateX" 0;')

    # pJCMOwen8_FootBndBckR
    mel.eval('setAttr "rFoot.rotateX" 0;')
    mel.eval('setDrivenKeyframe -currentDriver rFoot.rotateX {0}.pJCMOwen8_FootBndBckR;'.format(pyBlendShapesName))
    mel.eval('setAttr "rFoot.rotateX" 65;')
    mel.eval('setAttr "{0}.pJCMOwen8_FootBndBckR" 1;'.format(pyBlendShapesName))
    mel.eval('setDrivenKeyframe -currentDriver rFoot.rotateX {0}.pJCMOwen8_FootBndBckR;'.format(pyBlendShapesName))
    mel.eval('setAttr "rFoot.rotateX" 0;')

    # pJCMOwen8_FootBndFwdL
    mel.eval('setAttr "lFoot.rotateX" 0;')
    mel.eval('setDrivenKeyframe -currentDriver lFoot.rotateX {0}.pJCMOwen8_FootBndFwdL;'.format(pyBlendShapesName))
    mel.eval('setAttr "lFoot.rotateX" 65;')
    mel.eval('setAttr "{0}.pJCMOwen8_FootBndFwdL" 1;'.format(pyBlendShapesName))
    mel.eval('setDrivenKeyframe -currentDriver lFoot.rotateX {0}.pJCMOwen8_FootBndFwdL;'.format(pyBlendShapesName))
    mel.eval('setAttr "lFoot.rotateX" 0;')

    # pJCMOwen8_FootBndFwdR
    mel.eval('setAttr "rFoot.rotateX" 0;')
    mel.eval('setDrivenKeyframe -currentDriver rFoot.rotateX {0}.pJCMOwen8_FootBndFwdR;'.format(pyBlendShapesName))
    mel.eval('setAttr "rFoot.rotateX" -45;')
    mel.eval('setAttr "{0}.pJCMOwen8_FootBndFwdR" 1;'.format(pyBlendShapesName))
    mel.eval('setDrivenKeyframe -currentDriver rFoot.rotateX {0}.pJCMOwen8_FootBndFwdR;'.format(pyBlendShapesName))
    mel.eval('setAttr "rFoot.rotateX" 0;')

    # pJCMOwen8_ForearmBnd_n90L
    mel.eval('setAttr "lForearmBend.rotateY" 0;')
    mel.eval('setDrivenKeyframe -currentDriver lForearmBend.rotateY {0}.pJCMOwen8_ForearmBnd_n90L;'.format(
        pyBlendShapesName))
    mel.eval('setAttr "lForearmBend.rotateY" -90;')
    mel.eval('setAttr "{0}.pJCMOwen8_ForearmBnd_n90L" 1;'.format(pyBlendShapesName))
    mel.eval('setDrivenKeyframe -currentDriver lForearmBend.rotateY {0}.pJCMOwen8_ForearmBnd_n90L;'.format(
        pyBlendShapesName))
    mel.eval('setAttr "lForearmBend.rotateY" 0;')

    # pJCMOwen8_ForearmBnd_90R
    mel.eval('setAttr "rForearmBend.rotateY" 0;')
    mel.eval('setDrivenKeyframe -currentDriver rForearmBend.rotateY {0}.pJCMOwen8_ForearmBnd_90R;'.format(
        pyBlendShapesName))
    mel.eval('setAttr "rForearmBend.rotateY" 90;')
    mel.eval('setAttr "{0}.pJCMOwen8_ForearmBnd_90R" 1;'.format(pyBlendShapesName))
    mel.eval('setDrivenKeyframe -currentDriver rForearmBend.rotateY {0}.pJCMOwen8_ForearmBnd_90R;'.format(
        pyBlendShapesName))
    mel.eval('setAttr "rForearmBend.rotateY" 0;')

    # pJCMOwen8_ForearmBndL
    mel.eval('setAttr "lForearmBend.rotateY" 0;')
    mel.eval('setDrivenKeyframe -currentDriver lForearmBend.rotateY {0}.pJCMOwen8_ForearmBndL;'.format(
        pyBlendShapesName))
    mel.eval('setAttr "lForearmBend.rotateY" -135;')
    mel.eval('setAttr "{0}.pJCMOwen8_ForearmBndL" 1;'.format(pyBlendShapesName))
    mel.eval('setDrivenKeyframe -currentDriver lForearmBend.rotateY {0}.pJCMOwen8_ForearmBndL;'.format(
        pyBlendShapesName))
    mel.eval('setAttr "lForearmBend.rotateY" 0;')

    # pJCMOwen8_ForearmBndR
    mel.eval('setAttr "rForearmBend.rotateY" 0;')
    mel.eval('setDrivenKeyframe -currentDriver rForearmBend.rotateY {0}.pJCMOwen8_ForearmBndR;'.format(
        pyBlendShapesName))
    mel.eval('setAttr "rForearmBend.rotateY" 135;')
    mel.eval('setAttr "{0}.pJCMOwen8_ForearmBndR" 1;'.format(pyBlendShapesName))
    mel.eval('setDrivenKeyframe -currentDriver rForearmBend.rotateY {0}.pJCMOwen8_ForearmBndR;'.format(
        pyBlendShapesName))
    mel.eval('setAttr "rForearmBend.rotateY" 0;')

    # pJCMOwen8_HeadBndFwd_HDLv3
    mel.eval('setAttr "head.rotateX" 0;')
    mel.eval('setDrivenKeyframe -currentDriver head.rotateX {0}.pJCMOwen8_HeadBndFwd_HDLv3;'.format(pyBlendShapesName))
    mel.eval('setAttr "head.rotateX" 25;')
    mel.eval('setAttr "{0}.pJCMOwen8_HeadBndFwd_HDLv3" 1;'.format(pyBlendShapesName))
    mel.eval('setDrivenKeyframe -currentDriver head.rotateX {0}.pJCMOwen8_HeadBndFwd_HDLv3;'.format(pyBlendShapesName))
    mel.eval('setAttr "head.rotateX" 0;')

    # pJCMOwen8_HeadTwist_HDLv3L
    mel.eval('setAttr "head.rotateY" 0;')
    mel.eval('setDrivenKeyframe -currentDriver head.rotateY {0}.pJCMOwen8_HeadTwist_HDLv3L;'.format(pyBlendShapesName))
    mel.eval('setAttr "head.rotateY" 22;')
    mel.eval('setAttr "{0}.pJCMOwen8_HeadTwist_HDLv3L" 1;'.format(pyBlendShapesName))
    mel.eval('setDrivenKeyframe -currentDriver head.rotateY {0}.pJCMOwen8_HeadTwist_HDLv3L;'.format(pyBlendShapesName))
    mel.eval('setAttr "head.rotateY" 0;')

    # pJCMOwen8_HeadTwist_HDLv3R
    mel.eval('setAttr "head.rotateY" 0;')
    mel.eval('setDrivenKeyframe -currentDriver head.rotateY {0}.pJCMOwen8_HeadTwist_HDLv3R;'.format(
        pyBlendShapesName))
    mel.eval('setAttr "head.rotateY" -22;')
    mel.eval('setAttr "{0}.pJCMOwen8_HeadTwist_HDLv3R" 1;'.format(pyBlendShapesName))
    mel.eval('setDrivenKeyframe -currentDriver head.rotateY {0}.pJCMOwen8_HeadTwist_HDLv3R;'.format(
        pyBlendShapesName))
    mel.eval('setAttr "head.rotateY" 0;')

    # pJCMOwen8_ShinBndBckL
    mel.eval('setAttr "lShin.rotateX" 0;')
    mel.eval(
        'setDrivenKeyframe -currentDriver lShin.rotateX {0}.pJCMOwen8_ShinBndBckL;'.format(pyBlendShapesName))
    mel.eval('setAttr "lShin.rotateX" 155;')
    mel.eval('setAttr "{0}.pJCMOwen8_ShinBndBckL" 1;'.format(pyBlendShapesName))
    mel.eval(
        'setDrivenKeyframe -currentDriver lShin.rotateX {0}.pJCMOwen8_ShinBndBckL;'.format(pyBlendShapesName))
    mel.eval('setAttr "lShin.rotateX" 0;')

    # pJCMOwen8_ShinBndBckR
    mel.eval('setAttr "rShin.rotateX" 0;')
    mel.eval(
        'setDrivenKeyframe -currentDriver rShin.rotateX {0}.pJCMOwen8_ShinBndBckR;'.format(pyBlendShapesName))
    mel.eval('setAttr "rShin.rotateX" 155;')
    mel.eval('setAttr "{0}.pJCMOwen8_ShinBndBckR" 1;'.format(pyBlendShapesName))
    mel.eval(
        'setDrivenKeyframe -currentDriver rShin.rotateX {0}.pJCMOwen8_ShinBndBckR;'.format(pyBlendShapesName))
    mel.eval('setAttr "rShin.rotateX" 0;')

    # pJCMOwen8_ShoulderBndDn_n25_HDLv3L
    mel.eval('setAttr "lShldrBend.rotateZ" 0;')
    mel.eval('setDrivenKeyframe -currentDriver lShldrBend.rotateZ {0}.pJCMOwen8_ShoulderBndDn_n25_HDLv3L;'.format(
        pyBlendShapesName))
    mel.eval('setAttr "lShldrBend.rotateZ" -25;')
    mel.eval('setAttr "{0}.pJCMOwen8_ShoulderBndDn_n25_HDLv3L" 1;'.format(pyBlendShapesName))
    mel.eval('setDrivenKeyframe -currentDriver lShldrBend.rotateZ {0}.pJCMOwen8_ShoulderBndDn_n25_HDLv3L;'.format(
        pyBlendShapesName))
    mel.eval('setAttr "lShldrBend.rotateZ" 0;')

    # pJCMOwen8_ShoulderBndDn_25_HDLv3R
    mel.eval('setAttr "rShldrBend.rotateZ" 0;')
    mel.eval('setDrivenKeyframe -currentDriver rShldrBend.rotateZ {0}.pJCMOwen8_ShoulderBndDn_25_HDLv3R;'.format(
        pyBlendShapesName))
    mel.eval('setAttr "rShldrBend.rotateZ" 25;')
    mel.eval('setAttr "{0}.pJCMOwen8_ShoulderBndDn_25_HDLv3R" 1;'.format(pyBlendShapesName))
    mel.eval('setDrivenKeyframe -currentDriver rShldrBend.rotateZ {0}.pJCMOwen8_ShoulderBndDn_25_HDLv3R;'.format(
        pyBlendShapesName))
    mel.eval('setAttr "rShldrBend.rotateZ" 0;')

    # pJCMOwen8_ShoulderBndFwd_n90L
    mel.eval('setAttr "lShldrBend.rotateY" 0;')
    mel.eval('setDrivenKeyframe -currentDriver lShldrBend.rotateY {0}.pJCMOwen8_ShoulderBndFwd_n90L;'.format(
        pyBlendShapesName))
    mel.eval('setAttr "lShldrBend.rotateY" -90;')
    mel.eval('setAttr "{0}.pJCMOwen8_ShoulderBndFwd_n90L" 1;'.format(pyBlendShapesName))
    mel.eval('setDrivenKeyframe -currentDriver lShldrBend.rotateY {0}.pJCMOwen8_ShoulderBndFwd_n90L;'.format(
        pyBlendShapesName))
    mel.eval('setAttr "lShldrBend.rotateY" 0;')

    # pJCMOwen8_ShoulderBndFwd_90R
    mel.eval('setAttr "rShldrBend.rotateY" 0;')
    mel.eval('setDrivenKeyframe -currentDriver rShldrBend.rotateY {0}.pJCMOwen8_ShoulderBndFwd_90R;'.format(
        pyBlendShapesName))
    mel.eval('setAttr "rShldrBend.rotateY" 90;')
    mel.eval('setAttr "{0}.pJCMOwen8_ShoulderBndFwd_90R" 1;'.format(pyBlendShapesName))
    mel.eval('setDrivenKeyframe -currentDriver rShldrBend.rotateY {0}.pJCMOwen8_ShoulderBndFwd_90R;'.format(
        pyBlendShapesName))
    mel.eval('setAttr "rShldrBend.rotateY" 0;')

    # pJCMOwen8_ShoulderBndUpL
    mel.eval('setAttr "lShldrBend.rotateZ" 0;')
    mel.eval('setDrivenKeyframe -currentDriver lShldrBend.rotateZ {0}.pJCMOwen8_ShoulderBndUpL;'.format(
        pyBlendShapesName))
    mel.eval('setAttr "lShldrBend.rotateZ" 90;')
    mel.eval('setAttr "{0}.pJCMOwen8_ShoulderBndUpL" 1;'.format(pyBlendShapesName))
    mel.eval('setDrivenKeyframe -currentDriver lShldrBend.rotateZ {0}.pJCMOwen8_ShoulderBndUpL;'.format(
        pyBlendShapesName))
    mel.eval('setAttr "lShldrBend.rotateZ" 0;')

    # pJCMOwen8_ShoulderBndUpR
    mel.eval('setAttr "rShldrBend.rotateZ" 0;')
    mel.eval('setDrivenKeyframe -currentDriver rShldrBend.rotateZ {0}.pJCMOwen8_ShoulderBndUpR;'.format(
        pyBlendShapesName))
    mel.eval('setAttr "rShldrBend.rotateZ" -90;')
    mel.eval('setAttr "{0}.pJCMOwen8_ShoulderBndUpR" 1;'.format(pyBlendShapesName))
    mel.eval('setDrivenKeyframe -currentDriver rShldrBend.rotateZ {0}.pJCMOwen8_ShoulderBndUpR;'.format(
        pyBlendShapesName))
    mel.eval('setAttr "rShldrBend.rotateZ" 0;')

    # pJCMOwen8_ThighSideSideL
    mel.eval('setAttr "lThighBend.rotateZ" 0;')
    mel.eval('setDrivenKeyframe -currentDriver lThighBend.rotateZ {0}.pJCMOwen8_ThighSideSideL;'.format(
        pyBlendShapesName))
    mel.eval('setAttr "lThighBend.rotateZ" 85;')
    mel.eval('setAttr "{0}.pJCMOwen8_ThighSideSideL" 1;'.format(pyBlendShapesName))
    mel.eval('setDrivenKeyframe -currentDriver lThighBend.rotateZ {0}.pJCMOwen8_ThighSideSideL;'.format(
        pyBlendShapesName))
    mel.eval('setAttr "lThighBend.rotateZ" 0;')

    # pJCMOwen8_ThighSideSideR
    mel.eval('setAttr "rThighBend.rotateZ" 0;')
    mel.eval('setDrivenKeyframe -currentDriver rThighBend.rotateZ {0}.pJCMOwen8_ThighSideSideR;'.format(
        pyBlendShapesName))
    mel.eval('setAttr "rThighBend.rotateZ" -85;')
    mel.eval('setAttr "{0}.pJCMOwen8_ThighSideSideR" 1;'.format(pyBlendShapesName))
    mel.eval('setDrivenKeyframe -currentDriver rThighBend.rotateZ {0}.pJCMOwen8_ThighSideSideR;'.format(
        pyBlendShapesName))
    mel.eval('setAttr "rThighBend.rotateZ" 0;')
