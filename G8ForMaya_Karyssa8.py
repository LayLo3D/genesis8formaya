import maya.mel as mel
import maya.cmds as cmds


def Karyssa8JCMs(character, blendShapeGroup):
    pyBlendShapesName = blendShapeGroup
    if not cmds.objExists('{}.Karyssa_pJCMShldrDown_n40_L'.format(pyBlendShapesName)):
        error = character + ' specific JCMs were not created.\n\n' + character + ' JCMs were either not exported out properly or the wrong file was selected for import.'
        cmds.warning(error.replace('\n\n', ' '))
        return error

    # Karyssa_pJCMShldrDown_n40_L
    mel.eval('setAttr "lShldrBend.rotateZ" 0;')
    mel.eval('setDrivenKeyframe -currentDriver lShldrBend.rotateZ {0}.Karyssa_pJCMShldrDown_n40_L;'.format(
        pyBlendShapesName))
    mel.eval('setAttr "lShldrBend.rotateZ" -40;')
    mel.eval('setAttr "{0}.Karyssa_pJCMShldrDown_n40_L" 1;'.format(pyBlendShapesName))
    mel.eval('setDrivenKeyframe -currentDriver lShldrBend.rotateZ {0}.Karyssa_pJCMShldrDown_n40_L;'.format(
        pyBlendShapesName))
    mel.eval('setAttr "lShldrBend.rotateZ" 0;')

    # Karyssa_pJCMShldrDown_40_R
    mel.eval('setAttr "rShldrBend.rotateZ" 0;')
    mel.eval('setDrivenKeyframe -currentDriver rShldrBend.rotateZ {0}.Karyssa_pJCMShldrDown_40_R;'.format(
        pyBlendShapesName))
    mel.eval('setAttr "rShldrBend.rotateZ" 40;')
    mel.eval('setAttr "{0}.Karyssa_pJCMShldrDown_40_R" 1;'.format(pyBlendShapesName))
    mel.eval('setDrivenKeyframe -currentDriver rShldrBend.rotateZ {0}.Karyssa_pJCMShldrDown_40_R;'.format(
        pyBlendShapesName))
    mel.eval('setAttr "rShldrBend.rotateZ" 0;')

    # Karyssa_pJCMShldrFront_n110_L
    mel.eval('setAttr "lShldrBend.rotateY" 0;')
    mel.eval('setDrivenKeyframe -currentDriver lShldrBend.rotateY {0}.Karyssa_pJCMShldrFront_n110_L;'.format(
        pyBlendShapesName))
    mel.eval('setAttr "lShldrBend.rotateY" -110;')
    mel.eval('setAttr "{0}.Karyssa_pJCMShldrFront_n110_L" 1;'.format(pyBlendShapesName))
    mel.eval('setDrivenKeyframe -currentDriver lShldrBend.rotateY {0}.Karyssa_pJCMShldrFront_n110_L;'.format(
        pyBlendShapesName))
    mel.eval('setAttr "lShldrBend.rotateY" 0;')

    # Karyssa_pJCMShldrFront_110_R
    mel.eval('setAttr "rShldrBend.rotateY" 0;')
    mel.eval('setDrivenKeyframe -currentDriver rShldrBend.rotateY {0}.Karyssa_pJCMShldrFront_110_R;'.format(
        pyBlendShapesName))
    mel.eval('setAttr "rShldrBend.rotateY" 90;')
    mel.eval('setAttr "{0}.Karyssa_pJCMShldrFront_110_R" 1;'.format(pyBlendShapesName))
    mel.eval('setDrivenKeyframe -currentDriver rShldrBend.rotateY {0}.Karyssa_pJCMShldrFront_110_R;'.format(
        pyBlendShapesName))
    mel.eval('setAttr "rShldrBend.rotateY" 0;')

    # Karyssa_pJCMShldrFront_n110_Down_n40_L
    cmds.expression(
        s='if ((lShldrBend.rotateY * -0.0090909) * (lShldrBend.rotateZ * -0.0250) > 0)\n\t{0}.Karyssa_pJCMShldrFront_n110_Down_n40_L = (lShldrBend.rotateY * -0.0090909) * (lShldrBend.rotateZ * -0.0250);\nelse\n\t{0}.Karyssa_pJCMShldrFront_n110_Down_n40_L = 0'.format(
            pyBlendShapesName), o='{0}'.format(pyBlendShapesName), n='Karyssa_pJCMShldrFront_n110_Down_n40_L_exp', ae=1,
        uc='all')

    # Karyssa_pJCMShldrFront_110_Down_40_R
    cmds.expression(
        s='if ((rShldrBend.rotateY * 0.0090909) * (rShldrBend.rotateZ * 0.0250) > 0)\n\t{0}.Karyssa_pJCMShldrFront_110_Down_40_R = (rShldrBend.rotateY * -0.0090909) * (rShldrBend.rotateZ * -0.0250);\nelse\n\t{0}.Karyssa_pJCMShldrFront_110_Down_40_R = 0'.format(
            pyBlendShapesName), o='{0}'.format(pyBlendShapesName), n='Karyssa_pJCMShldrFront_110_Down_40_R_exp', ae=1,
        uc='all')

    # Karyssa_pJCMThighBend_n115_L
    mel.eval('setAttr "lThighBend.rotateX" -57;')
    mel.eval('setDrivenKeyframe -currentDriver lThighBend.rotateX {0}.Karyssa_pJCMThighBend_n115_L;'.format(pyBlendShapesName))
    mel.eval('setAttr "lThighBend.rotateX" -115;')
    mel.eval('setAttr "{0}.Karyssa_pJCMThighBend_n115_L" 1;'.format(pyBlendShapesName))
    mel.eval('setDrivenKeyframe -currentDriver lThighBend.rotateX {0}.Karyssa_pJCMThighBend_n115_L;'.format(pyBlendShapesName))
    mel.eval('setAttr "lThighBend.rotateX" 0;')

    # Karyssa_pJCMThighBend_n115_R
    mel.eval('setAttr "rThighBend.rotateX" -57;')
    mel.eval('setDrivenKeyframe -currentDriver rThighBend.rotateX {0}.Karyssa_pJCMThighBend_n115_R;'.format(pyBlendShapesName))
    mel.eval('setAttr "rThighBend.rotateX" -115;')
    mel.eval('setAttr "{0}.Karyssa_pJCMThighBend_n115_R" 1;'.format(pyBlendShapesName))
    mel.eval('setDrivenKeyframe -currentDriver rThighBend.rotateX {0}.Karyssa_pJCMThighBend_n115_R;'.format(pyBlendShapesName))
    mel.eval('setAttr "rThighBend.rotateX" 0;')
    
    # Karyssa_pJCMThighSide_85_L
    mel.eval('setAttr "lThighBend.rotateZ" 0;')
    mel.eval('setDrivenKeyframe -currentDriver lThighBend.rotateZ {0}.Karyssa_pJCMThighSide_85_L;'.format(
        pyBlendShapesName))
    mel.eval('setAttr "lThighBend.rotateZ" 85;')
    mel.eval('setAttr "{0}.Karyssa_pJCMThighSide_85_L" 1;'.format(pyBlendShapesName))
    mel.eval('setDrivenKeyframe -currentDriver lThighBend.rotateZ {0}.Karyssa_pJCMThighSide_85_L;'.format(
        pyBlendShapesName))
    mel.eval('setAttr "lThighBend.rotateZ" 0;')

    # Karyssa_pJCMThighSide_n85_R
    mel.eval('setAttr "rThighBend.rotateZ" 0;')
    mel.eval('setDrivenKeyframe -currentDriver rThighBend.rotateZ {0}.Karyssa_pJCMThighSide_n85_R;'.format(
        pyBlendShapesName))
    mel.eval('setAttr "rThighBend.rotateZ" -85;')
    mel.eval('setAttr "{0}.Karyssa_pJCMThighSide_n85_R" 1;'.format(pyBlendShapesName))
    mel.eval('setDrivenKeyframe -currentDriver rThighBend.rotateZ {0}.Karyssa_pJCMThighSide_n85_R;'.format(
        pyBlendShapesName))
    mel.eval('setAttr "rThighBend.rotateZ" 0;')
