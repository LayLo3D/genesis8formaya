import maya.mel as mel
import maya.cmds as cmds
import webbrowser
from functools import partial
import G8ForMaya


DNABLOCK_MODE = True


def run_script_on_selection():
    items = cmds.ls(selection=True)
    for item in items:
        if '_Morphs' in item:
            import G8ForMaya
            G8ForMaya.create_base_jcms(item)
            mel.eval('optionVar -intValue keyFullBody 1; hikSetKeyingMode( ); hikUpdateControlRigButtonState;')
            mel.eval('hikStancePose;')


def open_manual():
    webbrowser.open('https://www.laylo3d.com/genesis-8-for-maya-manual/')


def create_menu():
    if DNABLOCK_MODE:
        import dna_csv_entries
        dna_csv_entries.add_to_laylo3d_menu()

    VERSION = '3.0'
    G8ForMaya.VERSION = VERSION

    gMainWindow = mel.eval('$tmpVar=$gMainWindow')

    if not cmds.menu('LL3D_Tools_Menu', exists=True):
        cmds.menu('LL3D_Tools_Menu', label='LayLo3D Tools', tearOff=True, parent=gMainWindow)
        cmds.menuItem(
            'LL3D_G8ForMayaMI_1_TitleDivider',
            divider=True,
            dividerLabel='Genesis 8 for Maya ' + VERSION,
            parent='LL3D_Tools_Menu',
        )
        cmds.menuItem(
            'LL3D_G8ForMayaMI_2_Menu', subMenu=True, tearOff=True,
            label='Genesis 8 for Maya',
            parent='LL3D_Tools_Menu',
            insertAfter='LL3D_G8ForMayaMI_1_TitleDivider'
        )
    elif not cmds.menu('LL3D_G8ForMayaMI_2_Menu', exists=True):
        menu_items = cmds.menu('LL3D_Tools_Menu', query=True, itemArray=True)

        menu_items.append('LL3D_G8ForMayaMI_1_TitleDivider')
        menu_items.append('LL3D_G8ForMayaMI_2_Menu')
        menu_items.sort()

        for i, menu_item in enumerate(menu_items):
            if menu_item == 'LL3D_G8ForMayaMI_1_TitleDivider':
                if i == 0:
                    cmds.menuItem(
                        'LL3D_G8ForMayaMI_1_TitleDivider',
                        divider=True,
                        dividerLabel='Genesis 8 for Maya ' + VERSION,
                        parent='LL3D_Tools_Menu',
                        insertAfter=''
                    )
                else:
                    cmds.menuItem(
                        'LL3D_G8ForMayaMI_1_TitleDivider',
                        divider=True,
                        dividerLabel='Genesis 8 for Maya ' + VERSION,
                        parent='LL3D_Tools_Menu',
                        insertAfter=menu_items[i - 1]
                    )
            elif menu_item == 'LL3D_G8ForMayaMI_2_Menu':
                if i == 1:
                    cmds.menuItem(
                        'LL3D_G8ForMayaMI_2_Menu', subMenu=True, tearOff=True,
                        label='Genesis 8 for Maya',
                        parent='LL3D_Tools_Menu',
                        insertAfter='LL3D_G8ForMayaMI_1_TitleDivider'
                    )
                else:
                    cmds.menuItem(
                        'LL3D_G8ForMayaMI_2_Menu', subMenu=True, tearOff=True,
                        label='Genesis 8 for Maya',
                        parent='LL3D_Tools_Menu',
                        insertAfter=menu_items[i - 1]
                    )
    else:
        cmds.menuItem('LL3D_G8ForMayaMI_1_TitleDivider', edit=1, dividerLabel='Genesis 8 for Maya ' + VERSION, )

    cmds.setParent('LL3D_G8ForMayaMI_2_Menu', menu=1)
    if not cmds.menu('LL3D_G8ForMayaMI_2_Menu', query=True, itemArray=True):
        last_menu_item = cmds.menuItem(divider=True, dividerLabel='HumanIK Configuration')
    else:
        last_menu_item = cmds.menuItem(divider=True, dividerLabel='HumanIK Configuration', insertAfter='')

    if cmds.radioMenuItemCollection('LL3D_G8ForMaya_hikRadioCollection', exists=1):
        cmds.deleteUI('LL3D_G8ForMaya_hikRadioCollection')
    cmds.radioMenuItemCollection('LL3D_G8ForMaya_hikRadioCollection')
    last_menu_item = MBhikRadioButton = cmds.menuItem(
        label='Motion Builder Compatible HumanIK', insertAfter=last_menu_item,
        rb=bool(cmds.optionVar(query='LL3D_G8ForMaya_HIK')) if cmds.optionVar(exists='LL3D_G8ForMaya_HIK') else False
    )
    last_menu_item = cmds.menuItem(
        label='G8 for Maya Original HumanIK', insertAfter=last_menu_item,
        rb=not bool(cmds.optionVar(query='LL3D_G8ForMaya_HIK'))
        if cmds.optionVar(exists='LL3D_G8ForMaya_HIK') else True)

    last_menu_item = cmds.menuItem(
        divider=True, dividerLabel='Genesis 8 Base Male or Female', insertAfter=last_menu_item)

    last_menu_item = cmds.menuItem(label='Import Genesis 8 Base Male or Female', insertAfter=last_menu_item,
                                   command=partial(G8ForMaya.load_figure, 'base', MBhikRadioButton))

    last_menu_item = cmds.menuItem(
        divider=True, dividerLabel='Other Daz Original Core Characters', insertAfter=last_menu_item)
    last_menu_item = cmds.menuItem(subMenu=True, label='Female Characters', insertAfter=last_menu_item)
    cmds.menuItem(divider=True, dividerLabel='Genesis 8 Female Characters')
    cmds.menuItem(label='Import Aiko 8', command=partial(G8ForMaya.load_figure, 'Aiko8', MBhikRadioButton))
    cmds.menuItem(label='Import Alexandra 8', command=partial(G8ForMaya.load_figure, 'Alexandra8', MBhikRadioButton))
    cmds.menuItem(label='Import Charlotte 8', command=partial(G8ForMaya.load_figure, 'Charlotte8', MBhikRadioButton))
    cmds.menuItem(label='Import Edie 8', command=partial(G8ForMaya.load_figure, 'Edie8', MBhikRadioButton))
    cmds.menuItem(label='Import Gia 8', command=partial(G8ForMaya.load_figure, 'Gia8', MBhikRadioButton))
    cmds.menuItem(label='Import Girl 8', command=partial(G8ForMaya.load_figure, 'Girl8', MBhikRadioButton))
    cmds.menuItem(label='Import Karyssa 8', command=partial(G8ForMaya.load_figure, 'Karyssa8', MBhikRadioButton))
    cmds.menuItem(label='Import Latonya 8', command=partial(G8ForMaya.load_figure, 'Latonya8', MBhikRadioButton))
    cmds.menuItem(label='Import Mabel 8', command=partial(G8ForMaya.load_figure, 'Mabel8', MBhikRadioButton))
    cmds.menuItem(label='Import Mei Lin 8', command=partial(G8ForMaya.load_figure, 'MeiLin8', MBhikRadioButton))
    cmds.menuItem(label='Import Mika 8', command=partial(G8ForMaya.load_figure, 'Mika8', MBhikRadioButton))
    cmds.menuItem(label='Import Monique 8', command=partial(G8ForMaya.load_figure, 'Monique8', MBhikRadioButton))
    cmds.menuItem(label='Import Olympia 8', command=partial(G8ForMaya.load_figure, 'Olympia8', MBhikRadioButton))
    cmds.menuItem(label='Import Penny 8', command=partial(G8ForMaya.load_figure, 'Penny8', MBhikRadioButton))
    cmds.menuItem(label='Import Sakura 8', command=partial(G8ForMaya.load_figure, 'Sakura8', MBhikRadioButton))
    cmds.menuItem(label='Import Stephanie 8', command=partial(G8ForMaya.load_figure, 'Stephanie8', MBhikRadioButton))
    cmds.menuItem(label='Import Teen Josie 8', command=partial(G8ForMaya.load_figure, 'TeenJosie8', MBhikRadioButton))
    cmds.menuItem(label='Import Teen Kaylee 8', command=partial(G8ForMaya.load_figure, 'TeenKaylee8', MBhikRadioButton))
    cmds.menuItem(label='Import Victoria 8', command=partial(G8ForMaya.load_figure, 'Victoria8', MBhikRadioButton))
    cmds.menuItem(label='Import Zelara 8', command=partial(G8ForMaya.load_figure, 'Zelara8', MBhikRadioButton))

    cmds.setParent('LL3D_G8ForMayaMI_2_Menu', menu=True)
    last_menu_item = cmds.menuItem(subMenu=True, label='Male Characters', insertAfter=last_menu_item)
    cmds.menuItem(divider=True, dividerLabel='Genesis 8 Male Characters')
    cmds.menuItem(label='Import Darius 8', command=partial(G8ForMaya.load_figure, 'Darius8', MBhikRadioButton))
    cmds.menuItem(label='Import Edward 8', command=partial(G8ForMaya.load_figure, 'Edward8', MBhikRadioButton))
    cmds.menuItem(label='Import Floyd 8', command=partial(G8ForMaya.load_figure, 'Floyd8', MBhikRadioButton))
    cmds.menuItem(label='Import Lee 8', command=partial(G8ForMaya.load_figure, 'Lee8', MBhikRadioButton))
    cmds.menuItem(label='Import Lucas 8', command=partial(G8ForMaya.load_figure, 'Lucas8', MBhikRadioButton))
    cmds.menuItem(label='Import Michael 8', command=partial(G8ForMaya.load_figure, 'Michael8', MBhikRadioButton))
    cmds.menuItem(label='Import Ollie 8', command=partial(G8ForMaya.load_figure, 'Ollie8', MBhikRadioButton))
    cmds.menuItem(label='Import Owen 8', command=partial(G8ForMaya.load_figure, 'Owen8', MBhikRadioButton))
    cmds.menuItem(label='Import The Brute 8', command=partial(G8ForMaya.load_figure, 'TheBrute8', MBhikRadioButton))
    cmds.menuItem(label='Import Toon Dwayne 8', command=partial(G8ForMaya.load_figure, 'ToonDwayne8', MBhikRadioButton))
    cmds.menuItem(label='Import Vladimir 8', command=partial(G8ForMaya.load_figure, 'Vladimir8', MBhikRadioButton))

    cmds.setParent('LL3D_G8ForMayaMI_2_Menu', menu=True)
    last_menu_item = cmds.menuItem(divider=True, dividerLabel='Shading Networks', insertAfter=last_menu_item)
    last_menu_item = cmds.menuItem(label='Rename Shading Groups', insertAfter=last_menu_item,
                                   command=partial(G8ForMaya.rename_shading_groups, True))
    last_menu_item = cmds.menuItem(label='Rename Shaders', insertAfter=last_menu_item,
                                   command=partial(G8ForMaya.rename_shaders, True))
    # cmds.menuItem(divider=True, dividerLabel='Run Script on Selected')
    # cmds.menuItem(label='Configure JCMs for Selection', command=lambda x: runScriptOnSelection())
    # cmds.menuItem(label='Create JCMs for all',
    #               command='import G8ForMaya;G8ForMaya.createJCMsAllObjs("Genesis8Female", "Genesis8Female")')
    cmds.menuItem(divider=True)
    cmds.menuItem(label='Help', command=lambda *args: open_manual())


def delete_menu():
    if DNABLOCK_MODE:
        import dna_csv_entries
        dna_csv_entries.remove_from_laylo_3d_menu()

    menu_items = cmds.menu('LL3D_G8ForMayaMI_2_Menu', query=True, itemArray=True)
    for item in menu_items:
        if not item.endswith(('LL3D_GeoGraftFixes_divider', 'LL3D_GeoFixes_subMenu')):
            cmds.deleteUI(item)

    if not cmds.menu('LL3D_G8ForMayaMI_2_Menu', query=True, itemArray=True):
        if cmds.menuItem('LL3D_G8ForMayaMI_1_TitleDivider', exists=True):
            cmds.deleteUI('LL3D_G8ForMayaMI_1_TitleDivider')
        if cmds.menu('LL3D_G8ForMayaMI_2_Menu', exists=True):
            cmds.deleteUI('LL3D_G8ForMayaMI_2_Menu')

    if cmds.optionVar(exists='LL3D_G8ForMaya_HIK'):
        cmds.optionVar(remove='LL3D_G8ForMaya_HIK')

    if cmds.menu('LL3D_Tools_Menu', exists=True):
        if cmds.menu('LL3D_Tools_Menu', query=True, numberOfItems=True) == 0:
            cmds.deleteUI('LL3D_Tools_Menu')
