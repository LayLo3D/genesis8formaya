# Written by Landon Lopez Copyright (c) 2022 LayLo 3D, All Rights Reserved
# Created 6:42 AM, September 02, 2022
import maya.cmds as cmds
import maya.mel as mel
from functools import partial
from collections import OrderedDict
import re
import csv


FEMALE_CSV_FILE_PATH = 'T:/__Characters/Tables/Morphs/DT_Cloth_Morph_Female1.csv'
MALE_CSV_FILE_PATH = 'T:/__Characters/Tables/Morphs/DT_Cloth_Morph_Male.csv'

# SECTIONS = [
#     ['Row Name', ''],
#     ['Asset ID', ''],
#     ['Morph Targets', ''],
#     ['Morph Category', 'Body'],
#     ['Section', 'None'],
#     ['Min Value', -2],
#     ['Max Value', 2],
#     ['Supported Bodies', ['(Female0)', '']],
#     ['Friendly Name', ''],
#     ['Tab', 'None'],
#     ['Folder', 'None'],
#     ['Tags', ''],
#     ['Thumbnail Ref', 'None'],
# ]

FEMALE_ADJ_MORPHS = {
    'All_Expand': 'All Expand',
    'ArmpitLower': 'Armpit Lower',
    'Arms_Elbow': 'Arms Elbow',
    'Arms_Lower': 'Arms Lower',
    'Arms_Upper': 'Arms Upper',
    'Arms_Wrists': 'Arms Wrists',
    'Basic_Breast_Bridger': 'Large Breasts Clothing Fix',
    'BootsHigh': 'Boots High',
    'BootsLow': 'Boots Low',
    'BootsMedium': 'Boots Medium',
    'Breast_Helper_Default_Lower': 'Large Breasts Lower Adjust',
    'Breast_Helper_Super_Fixer': 'Large Breasts Center Adjust',
    'Buttocks': 'Buttocks',
    'Chest_Pectorals': 'Chest Pectorals',
    'Feet': 'Feet',
    'Gens_Area': 'Gens Area',
    'Hand_Expand_All': 'Hand Expand All',
    'Head_All': 'Head All',
    'Hip_Area': 'Hip Area',
    'HipLowLarger': 'Hip Low Larger',
    'JacketSupport': 'Jacket Support',
    'Legs_Knee_Area': 'Legs Knee Area',
    'Legs_Shin_Area': 'Legs Shin Area',
    'Legs_Upper': 'Legs Upper',
    'LegsRaisePants': 'Legs Raise Pants',
    'LowerTops': 'Lengthen Torso',
    'Neck': 'Neck',
    'Shoulder_Tops': 'Shoulder Tops',
    'Waist_01': 'Waist 01',
    'Waist_02': 'Waist 02',
    'Waist_03': 'Waist 03',
    'Waist_04': 'Waist 04',
}
MALE_ADJ_MORPHS = {
    'Arms_Elbow': 'Arms Elbow',
    'Arms_Lower': 'Arms Lower',
    'Arms_Upper': 'Arms Upper',
    'Arms_Wrists': 'Arms Wrists',
    'Back_Lower': 'Back Lower',
    'Back_Mid': 'Back Mid',
    'Back_Upper': 'Back Upper',
    'BootsHigh': 'Boots High',
    'BootsLow': 'Boots Low',
    'BootsMedium': 'Boots Medium',
    'Buttocks': 'Buttocks',
    'Chest_Pectorals': 'Chest Pectorals',
    'Chest_Upper': 'Chest Upper',
    'Feet': 'Feet',
    'Gens_Area': 'Gens Area',
    'Hip_Area': 'Hip Area',
    'Legs_Knee_Area': 'Legs Knee Area',
    'Legs_Shin_Area': 'Legs Shin Area',
    'Legs_Upper': 'Legs Upper',
    'LowWaistGeneral': 'Waist Low General',
    'Neck': 'Neck',
    'Shoulder_Tops': 'Shoulder Tops',
    'Waist_01': 'Waist 01',
    'Waist_02': 'Waist 02',
    'Waist_03': 'Waist 03',
    'Waist04': 'Waist 04',
    'Waist05': 'Waist 05',
}
MALE_IGNORE_FEMALE_MORPHS = ['Basic_Breast_Bridger', 'Breast_Helper_Default_Lower', 'Breast_Helper_Super_Fixer']

FEMALE_MORPHS = [
    'AA', 'Aging_Head_Controller', 'Aiko8', 'Alabama_Head', 'Alexandra8', 'Amelia2', 'AreolaDiameter',
    'AreolaDiameterL', 'AreolaDiameterR', 'BD_Legs_Volume', 'BF_FHM_CloneX', 'Basic_Weight_1', 'Basic_Weight_1_Assist',
    'Body_s___Children_Head', 'Body_s___Children_Head_Scale', 'Bodybuilder', 'BreastMoveHorizontalL',
    'BreastMoveHorizontalR', 'BreastPointy', 'BreastPointyL', 'BreastPointyR', 'BreastUnderCurve', 'BreastUnderCurveL',
    'BreastUnderCurveR', 'BreastUnderCurve_ClothingFix1', 'BreastUnderCurve_ClothingFix2', 'Breast_Height_Lower',
    'Breast_Height_Upper', 'Breast_Height_Upper_Assist', 'Breast_Pointed', 'Breast_Side_Smoother', 'Breast_Top_Curve1',
    'Breast_Top_Curve2', 'Breast_Under_Smoother1', 'Breast_Under_Smoother2', 'Breast_Under_Smoother3',
    'Breast_Under_Smoother4', 'Breast_Under_Smoother5', 'Breast_Width', 'Breasts08', 'BreastsDiameter', 'BreastsGone',
    'BreastsImplants', 'BreastsSize', 'BreastsSize_ClothingFix', 'BreastsSmall', 'BreastsUp_Down', 'CH', 'ChimpFemale',
    'Christine_Head', 'Crystal', 'DD222', 'E222', 'EJFM_Brows_Arched', 'EJFM_Brows_Arched_Out',
    'EJFM_Brows_Center_Down', 'EJFM_Brows_Definition', 'EJFM_Brows_Length_In', 'EJFM_Brows_Length_Out',
    'EJFM_Brows_Raise_All', 'EJFM_Brows_Raise_Center', 'EJFM_Cheekbones_Bottom', 'EJFM_Cheekbones_Front_01',
    'EJFM_Cheekbones_Front_02', 'EJFM_Cheekbones_Front_03', 'EJFM_Cheekbones_Front_Height', 'EJFM_Cheekbones_High_Fill',
    'EJFM_Cheekbones_Low_Fill_01', 'EJFM_Cheekbones_Low_Fill_02', 'EJFM_Cheekbones_Low_Fill_03',
    'EJFM_Cheekbones_Sides_Depression', 'EJFM_Cheekbones_Under_Depression', 'EJFM_Cheekbones_Upper_Lean',
    'EJFM_Cheekbones_Wide', 'EJFM_Cheeks_Angled', 'EJFM_Cheeks_Contour_01', 'EJFM_Cheeks_Contour_02',
    'EJFM_Cheeks_Contour_03', 'EJFM_Cheeks_Contour_04', 'EJFM_Cheeks_Full_01', 'EJFM_Cheeks_Full_02',
    'EJFM_Cheeks_Full_03', 'EJFM_Cheeks_Full_04', 'EJFM_Cheeks_Lean_Lower', 'EJFM_Cheeks_Lean_Upper',
    'EJFM_Cheeks_Low_01', 'EJFM_Cheeks_Low_02', 'EJFM_Cheeks_Mouth_Depth', 'EJFM_Cheeks_Remove', 'EJFM_Chin_Dimple',
    'EJFM_Chin_Height', 'EJFM_Chin_Lip_Crease', 'EJFM_Chin_Lip_Height', 'EJFM_Chin_Long_Down',
    'EJFM_Chin_Lower_Depression', 'EJFM_Chin_Lower_Soften', 'EJFM_Chin_Receding', 'EJFM_Chin_Sides_01',
    'EJFM_Chin_Sides_02', 'EJFM_Chin_Sides_Width', 'EJFM_Chin_Smooth', 'EJFM_Chin_Thin', 'EJFM_Chin_Volume',
    'EJFM_Chin_W_', 'EJFM_Chin__01', 'EJFM_Chin__02', 'EJFM_Chin__03', 'EJFM_Chin__04', 'EJFM_Chin__05',
    'EJFM_Chin__06', 'EJFM_Dimples_Style_01_Small', 'EJFM_Dimples_Style_02_Creased', 'EJFM_Dimples_Style_03_Low',
    'EJFM_Dimples_Style_04_Wide', 'EJFM_Eyelids_Lower_Height_Center', 'EJFM_Eyelids_Lower_Height_Inner',
    'EJFM_Eyelids_Lower_Height_Outer', 'EJFM_Eyelids_Lower_Sunken', 'EJFM_Eyelids_Lower_Volume_All',
    'EJFM_Eyelids_Lower_Volume_Inner', 'EJFM_Eyelids_Lower_Volume_Outer', 'EJFM_Eyelids_Lower_Volume_Wide',
    'EJFM_Eyelids_Upper_Heavy', 'EJFM_Eyelids_Upper_Inner_Style_01', 'EJFM_Eyelids_Upper_Inner_Style_02',
    'EJFM_Eyelids_Upper_Inner_Style_03', 'EJFM_Eyelids_Upper_Inner_Width', 'EJFM_Eyelids_Upper_Outer_Style_01',
    'EJFM_Eyelids_Upper_Outer_Style_02', 'EJFM_Eyelids_Upper_Outer_Style_03', 'EJFM_Eyelids_Upper_Smooth',
    'EJFM_Eyelids_Upper_Tension', 'EJFM_Eyes_Corner_Outer_Down', 'EJFM_Eyes_Corner_Outer_Longer_01',
    'EJFM_Eyes_Corner_Outer_Longer_02', 'EJFM_Eyes_Corner_Outer_Up', 'EJFM_Eyes_Cover_Fold_Inner', 'EJFM_Eyes_Distance',
    'EJFM_Eyes_Height', 'EJFM_Eyes_Iris_Size', 'EJFM_Eyes_Lower_Depression', 'EJFM_Eyes_Orbits_Lower_Size',
    'EJFM_Eyes_Orbits_Upper_Size', 'EJFM_Eyes_Orbits_Upper_Size_Outer', 'EJFM_Eyes_Pupil_Size', 'EJFM_Eyes_Rotation',
    'EJFM_Eyes_Round', 'EJFM_Eyes_Size', 'EJFM_Eyes_Style_Cat', 'EJFM_Eyes__Almond', 'EJFM_Eyes__Asian',
    'EJFM_Eyes__Close_Set', 'EJFM_Eyes__Deep_Set', 'EJFM_Eyes__Drooping', 'EJFM_Eyes__Romantic', 'EJFM_Eyes__Round',
    'EJFM_Eyes__Wide_Set', 'EJFM_Face_Chiseled', 'EJFM_Face_Concave', 'EJFM_Face_Convex', 'EJFM_Face_Diamond',
    'EJFM_Face_Heart', 'EJFM_Face_Long', 'EJFM_Face_Oval', 'EJFM_Face_Pear', 'EJFM_Face_Projection_Angle',
    'EJFM_Face_Projection_Center', 'EJFM_Face_Round', 'EJFM_Face_Square', 'EJFM_Forehead_Round', 'EJFM_Forehead_Slope',
    'EJFM_Forehead__01', 'EJFM_Forehead__02', 'EJFM_Forehead__03', 'EJFM_Frontal_Eminence', 'EJFM_Frontal_Lobes_',
    'EJFM_Frown_Double', 'EJFM_Frown_Low', 'EJFM_Frown_Single', 'EJFM_Glabella', 'EJFM_Glabella_Bumpy',
    'EJFM_Head_Receding_Upper', 'EJFM_Head_Top_Lower', 'EJFM_Head_Top_Narrow', 'EJFM_Head_Width',
    'EJFM_Infraorbital_Furrow_01', 'EJFM_Infraorbital_Furrow_02', 'EJFM_Jaw_Angle_High', 'EJFM_Jaw_Angle_Low',
    'EJFM_Jaw_Back_Depth', 'EJFM_Jaw_Back_Height', 'EJFM_Jaw_Bottom_Tension', 'EJFM_Jaw_Delicate', 'EJFM_Jaw_Diamond',
    'EJFM_Jaw_Full_Definition', 'EJFM_Jaw_Heart', 'EJFM_Jaw_Low', 'EJFM_Jaw_Lower_Definition', 'EJFM_Jaw_Lower_Width',
    'EJFM_Jaw_Middle_Depression', 'EJFM_Jaw_Middle_Width', 'EJFM_Jaw_Muscles', 'EJFM_Jaw_Narrow', 'EJFM_Jaw_Oval',
    'EJFM_Jaw_Regular', 'EJFM_Jaw_Round_Big', 'EJFM_Jaw_Round_Small', 'EJFM_Jaw_Sides_Definition',
    'EJFM_Jaw_Sides_Width', 'EJFM_Jaw_Small', 'EJFM_Jaw_Square', 'EJFM_Jaw_Strong', 'EJFM_Jaw_Style_01',
    'EJFM_Jaw_Style_02', 'EJFM_Jaw_Style_03', 'EJFM_Jaw_Style_04', 'EJFM_Jaw_Style_05', 'EJFM_Jaw_Top_Width',
    'EJFM_Lacrimals_Pointy_Down', 'EJFM_Lacrimals_Size', 'EJFM_Lacrimals_Up', 'EJFM_Lip_Corners_Style_01',
    'EJFM_Lip_Corners_Style_02', 'EJFM_Lip_Corners_Style_03', 'EJFM_Lip_Corners_Style_04', 'EJFM_Lip_Corners_Style_05',
    'EJFM_Lip_Corners_Style_06', 'EJFM_Lip_Gap_Soft', 'EJFM_Lip_Gap_Strong', 'EJFM_Lip_Lower_Bumpy',
    'EJFM_Lip_Lower_Definition', 'EJFM_Lip_Lower_Depth_Center', 'EJFM_Lip_Lower_Depth_Sides', 'EJFM_Lip_Lower_Diamond',
    'EJFM_Lip_Lower_Not_Occluded_01', 'EJFM_Lip_Lower_Not_Occluded_02', 'EJFM_Lip_Lower_Size', 'EJFM_Lip_Lower_Square',
    'EJFM_Lip_Lower_Thin', 'EJFM_Lip_Lower_Volume', 'EJFM_Lip_Lower_Width', 'EJFM_Lip_Peaks_Narrow',
    'EJFM_Lip_Peaks_Sharp', 'EJFM_Lip_Peaks_Smooth', 'EJFM_Lip_Upper_Bumpy', 'EJFM_Lip_Upper_Center_Definition',
    'EJFM_Lip_Upper_Center_Depth', 'EJFM_Lip_Upper_Definition', 'EJFM_Lip_Upper_Diamond', 'EJFM_Lip_Upper_Inner_Curves',
    'EJFM_Lip_Upper_Not_Occluded_01', 'EJFM_Lip_Upper_Not_Occluded_02', 'EJFM_Lip_Upper_Outer_Curves',
    'EJFM_Lip_Upper_Sides_Definition', 'EJFM_Lip_Upper_Sides_Depth', 'EJFM_Lip_Upper_Size', 'EJFM_Lip_Upper_Square',
    'EJFM_Lip_Upper_Thin', 'EJFM_Lip_Upper_Volume', 'EJFM_Lip_Upper_Width', 'EJFM_Lips_Bee_Bite_Lower',
    'EJFM_Lips_Bee_Bite_Upper', 'EJFM_Lips_Big_Not_Occluded', 'EJFM_Lips_Center_Height', 'EJFM_Lips_Protrusion',
    'EJFM_Lips_Small_Thin', 'EJFM_Lips_Style_Bee_Bite', 'EJFM_Lips_Style_Downward_Turned', 'EJFM_Lips_Style_Heart',
    'EJFM_Lips_Style_Model', 'EJFM_Lips_Style_Round', 'EJFM_Lips_Style_Silicone', 'EJFM_Lips_Style_Thin_01',
    'EJFM_Lips_Style_Thin_02', 'EJFM_Lips_Style_Thin_03', 'EJFM_Lips_Style_Thin_04', 'EJFM_Lips_Style_Thin_05',
    'EJFM_Lips_Style_Voluptuous', 'EJFM_Mouth_Downwards_Creases', 'EJFM_Mouth_Frown', 'EJFM_Mouth_Lower_Projection',
    'EJFM_Mouth_Narrow_Inner', 'EJFM_Mouth_Narrow_Outer', 'EJFM_Mouth_Projection_Full', 'EJFM_Mouth_Sides_Bump',
    'EJFM_Mouth_Sides_Depth', 'EJFM_Mouth_Sides_Muscle', 'EJFM_Mouth_Upper_Projection', 'EJFM_Nape_',
    'EJFM_Nape_Occipital_Bun', 'EJFM_Nape_Occipital_Smooth', 'EJFM_Nasal_Root_Depression_01',
    'EJFM_Nasal_Root_Depression_02', 'EJFM_Nasal_Root_Extra_Wide', 'EJFM_Nasal_Root_Height',
    'EJFM_Nasal_Root_Progressive', 'EJFM_Nasal_Root_Thin', 'EJFM_Nasal_Root_Width', 'EJFM_Nasolabial_Crease_Medium',
    'EJFM_Nasolabial_Crease_Smooth', 'EJFM_Nasolabial_Crease_Thin', 'EJFM_Neck_Short', 'EJFM_Neck_Width',
    'EJFM_Nose_Bridge_Depth', 'EJFM_Nose_Bridge_Small', 'EJFM_Nose_Bump_Concave', 'EJFM_Nose_Bump_Irregular',
    'EJFM_Nose_Bump_Lower', 'EJFM_Nose_Bump_Removal', 'EJFM_Nose_Bump_Size', 'EJFM_Nose_Bump_Upper',
    'EJFM_Nose_Cartilage_Side_Protrusion', 'EJFM_Nose_Flesh_Height', 'EJFM_Nose_Flesh_Small', 'EJFM_Nose_Lower_Width',
    'EJFM_Nose_Ridge_Angle', 'EJFM_Nose_Ridge_Greek', 'EJFM_Nose_Ridge_Hourglass', 'EJFM_Nose_Ridge_Irregular',
    'EJFM_Nose_Ridge_Middle_Width', 'EJFM_Nose_Ridge_Roman', 'EJFM_Nose_Ridge_Sides_Straight', 'EJFM_Nose_Ridge_Square',
    'EJFM_Nose_Ridge_Upper_Width', 'EJFM_Nose_Ridge_Wide', 'EJFM_Nose_Ridge_Width_Variation',
    'EJFM_Nose_Rige_Lower_Width', 'EJFM_Nose_Sides_Define', 'EJFM_Nose_Sides_Lower_Thin', 'EJFM_Nose_Sides_Thin',
    'EJFM_Nose_Style_01', 'EJFM_Nose_Style_02', 'EJFM_Nose_Style_03', 'EJFM_Nose_Style_04', 'EJFM_Nose_Style_05',
    'EJFM_Nose_Stylize', 'EJFM_Nose_Wide_Straight', 'EJFM_Nose_Width_Lower', 'EJFM_Nose_Wings_Depth',
    'EJFM_Nose_Wings_Front_Width', 'EJFM_Nose_Wings_Height', 'EJFM_Nose_Wings_Height_Upper',
    'EJFM_Nose_Wings_Lower_Crease', 'EJFM_Nose_Wings_Smooth', 'EJFM_Nose_Wings_Smooth_Shrink',
    'EJFM_Nose_Wings_Upper_Crease', 'EJFM_Nose_Wings_Upper_Smooth', 'EJFM_Nose_Wings_Width', 'EJFM_Nostrils_Depth',
    'EJFM_Nostrils_Front_Down', 'EJFM_Nostrils_Lower_Height', 'EJFM_Nostrils_Size', 'EJFM_Nostrils_Triangular',
    'EJFM_Nostrils_Width', 'EJFM_Orbital_Outer_Depression', 'EJFM_Philtrum_Depth_Lower', 'EJFM_Philtrum_Depth_Upper',
    'EJFM_Philtrum_Sides_Definition', 'EJFM_Piltrum_Emphasize', 'EJFM_Protruding_Ears', 'EJFM_Septum_Double',
    'EJFM_Septum_Height', 'EJFM_Septum_Inner_Planar', 'EJFM_Septum_Planar', 'EJFM_Septum_Top_Planar',
    'EJFM_Septum_Width_Inner', 'EJFM_Septum_Width_Middle', 'EJFM_Septum_Width_Outer', 'EJFM_Septum_Width_Uniform',
    'EJFM_Smooth_Tragus', 'EJFM_Superciliary_Arches_01', 'EJFM_Superciliary_Arches_02', 'EJFM_Superciliary_Arches_03',
    'EJFM_Superior_Crus_Depth', 'EJFM_Supraobital_Ridges', 'EJFM_Supraorbital_Arches', 'EJFM_Teeth_Canines_Big',
    'EJFM_Teeth_Gap', 'EJFM_Teeth_Incisives_Big', 'EJFM_Teeth_Irregular_01', 'EJFM_Teeth_Irregular_02',
    'EJFM_Teeth_Size', 'EJFM_Temples_Style_01', 'EJFM_Temples_Style_02', 'EJFM_Temples_Style_03',
    'EJFM_Temples_Style_04', 'EJFM_Temporal_Depth', 'EJFM_Temporal_Ridges', 'EJFM_Tip_Bulbous',
    'EJFM_Tip_Cartilage_Creases', 'EJFM_Tip_Depth', 'EJFM_Tip_Height', 'EJFM_Tip_Length', 'EJFM_Tip_Long_Pointy',
    'EJFM_Tip_Pinch', 'EJFM_Tip_Planar', 'EJFM_Tip_Sides_Bumps', 'EJFM_Tip_Sides_Square', 'EJFM_Tip_Thin',
    'EJFM_Tip_Triangular', 'EJFM_Tip_Turn_Up', 'EJFM_Tip_Width', 'EJFM_Tragus_Size', 'EJFM_Under_Eye_Bags',
    'EJFM_Zygomatic_Bone_Big', 'EJFM_Zygomatic_Bone_Thin', 'EarsElfLong', 'Edie8', 'Edie8HD', 'Ellithia8', 'Emaciated',
    'Emiko', 'Evelyn', 'Eyes_Scale', 'FF', 'Glute_Size_1', 'Glute_Size_2', 'HFS_Body_04_Zombi', 'HFS_Hands_05_Gecko',
    'HFS_Hands_08_Claws', 'HFS_Head_12_Goblin', 'HFS_Head_15_Hag', 'HFS_Head_16_Faun', 'HFS_Head_17_Cat',
    'HFS_Head_18_Dog', 'HFS_Head_19_Snake', 'HFS_Head_20_Deer', 'HFS_Head_21_Ogre', 'HFS_Head_22_Troll',
    'HFS_Head_23_Zombie', 'HannMei', 'HeadSize', 'Heavy', 'HipSize', 'I222', 'KK', 'Kala8', 'Kamala', 'Kanye07',
    'KaylaCollins', 'KimKardashian', 'LLF_PoseyHead', 'Melody_Head', 'Melody_Head_Eyes_01', 'Melody_Head_Lips_01',
    'Melody_Head_Lips_02', 'Melody_Head_Lips_03', 'Melody_Head_Nose_01', 'Melody_Head_Nose_02', 'Melody_Head_Nose_03',
    'Miquela', 'Monique8', 'NN', 'NikkiMinaj', 'O222', 'PBMNavel', 'PP', 'ParisHilton', 'PearFigure', 'RR',
    'Rynne8Head', 'SCLPropagatingHead', 'SS', 'Saya', 'TH', 'Teeth_Hide', 'Thin', 'U222', 'VYKMidgeHead', 'Voluptuous',
    'Weight', 'ZunaHead', 'browDownLeft', 'browDownRight', 'browInnerUp', 'browOuterUpLeft', 'browOuterUpRight',
    'cheekPuff', 'cheekSquintLeft', 'cheekSquintRight', 'duG8FAmberHead', 'duG8FBrynn_head', 'duG8FPaigeHead',
    'eyeBlinkLeft', 'eyeBlinkRight', 'eyeLookDownLeft', 'eyeLookDownRight', 'eyeLookInLeft', 'eyeLookInRight',
    'eyeLookOutLeft', 'eyeLookOutRight', 'eyeLookUpLeft', 'eyeLookUpRight', 'eyeSquintLeft', 'eyeSquintRight',
    'eyeWideLeft', 'eyeWideRight', 'jawForward', 'jawLeft', 'jawOpen', 'jawRight', 'mouthClose', 'mouthDimpleLeft',
    'mouthDimpleRight', 'mouthFrownLeft', 'mouthFrownRight', 'mouthFunnel', 'mouthLeft', 'mouthLowerDownLeft',
    'mouthLowerDownRight', 'mouthPressLeft', 'mouthPressRight', 'mouthPucker', 'mouthRight', 'mouthRollLower',
    'mouthRollUpper', 'mouthShrugLower', 'mouthShrugUpper', 'mouthSmileLeft', 'mouthSmileRight', 'mouthStretchLeft',
    'mouthStretchRight', 'mouthUpperUpLeft', 'mouthUpperUpRight', 'nasalFlareLeft', 'nasalFlareRight', 'noseSneerLeft',
    'noseSneerRight', 'pJCMAbdomen2Fwd_40', 'pJCMAbdomen2Side_24_L', 'pJCMAbdomen2Side_24_R', 'pJCMAbdomenFwd_35',
    'pJCMAbdomenLowerFwd_Navel', 'pJCMAbdomenUpperFwd_Navel', 'pJCMBigToeDown_45_L', 'pJCMBigToeDown_45_R',
    'pJCMChestFwd_35', 'pJCMChestSide_20_L', 'pJCMChestSide_20_R', 'pJCMCollarTwist_n30_L', 'pJCMCollarTwist_n30_R',
    'pJCMCollarTwist_p30_L', 'pJCMCollarTwist_p30_R', 'pJCMCollarUp_55_L', 'pJCMCollarUp_55_R', 'pJCMFlexBiceps_L',
    'pJCMFlexBiceps_R', 'pJCMFlexCalf_L', 'pJCMFlexCalf_R', 'pJCMFlexCollarUpperBack_L', 'pJCMFlexCollarUpperBack_R',
    'pJCMFlexGluteClench_L', 'pJCMFlexGluteClench_R', 'pJCMFlexHamstring_L', 'pJCMFlexHamstring_R', 'pJCMFlexQuad_L',
    'pJCMFlexQuad_R', 'pJCMFlexTriceps_L', 'pJCMFlexTriceps_R', 'pJCMFootDwn_75_L', 'pJCMFootDwn_75_R',
    'pJCMFootUp_40_L', 'pJCMFootUp_40_R', 'pJCMForeArmFwd_135_L', 'pJCMForeArmFwd_135_R', 'pJCMForeArmFwd_75_L',
    'pJCMForeArmFwd_75_R', 'pJCMHandDwn_70_L', 'pJCMHandDwn_70_R', 'pJCMHandUp_80_L', 'pJCMHandUp_80_R',
    'pJCMHeadBack_27', 'pJCMHeadFwd_25', 'pJCMIndex1Dwn_90_L', 'pJCMIndex1Dwn_90_R', 'pJCMIndex2Dwn_105_L',
    'pJCMIndex2Dwn_105_R', 'pJCMIndex3Dwn_90_L', 'pJCMIndex3Dwn_90_R', 'pJCMMid1Dwn_95_L', 'pJCMMid1Dwn_95_R',
    'pJCMMid2Dwn_105_L', 'pJCMMid2Dwn_105_R', 'pJCMMid3Dwn_90_L', 'pJCMMid3Dwn_90_R', 'pJCMNeckBack_27',
    'pJCMNeckFwd_35', 'pJCMNeckLowerSide_40_L', 'pJCMNeckLowerSide_40_R', 'pJCMNeckTwist_22_L', 'pJCMNeckTwist_22_R',
    'pJCMNeckTwist_Reverse', 'pJCMPelvisFwd_25', 'pJCMPinky1Dwn_95_L', 'pJCMPinky1Dwn_95_R', 'pJCMPinky2Dwn_105_L',
    'pJCMPinky2Dwn_105_R', 'pJCMPinky3Dwn_90_L', 'pJCMPinky3Dwn_90_R', 'pJCMRing1Dwn_95_L', 'pJCMRing1Dwn_95_R',
    'pJCMRing2Dwn_105_L', 'pJCMRing2Dwn_105_R', 'pJCMRing3Dwn_90_L', 'pJCMRing3Dwn_90_R', 'pJCMShinBend_155_L',
    'pJCMShinBend_155_R', 'pJCMShinBend_90_L', 'pJCMShinBend_90_R', 'pJCMShldrDown_40_L', 'pJCMShldrDown_40_R',
    'pJCMShldrFront_n110_Bend_n40_L', 'pJCMShldrFront_n110_Bend_p90_L', 'pJCMShldrFront_p110_Bend_n90_R',
    'pJCMShldrFront_p110_Bend_p40_R', 'pJCMShldrFwd_110_L', 'pJCMShldrFwd_110_R', 'pJCMShldrUp_90_L',
    'pJCMShldrUp_90_R', 'pJCMThighBack_35_L', 'pJCMThighBack_35_R', 'pJCMThighFwd_115_L', 'pJCMThighFwd_115_R',
    'pJCMThighFwd_57_L', 'pJCMThighFwd_57_R', 'pJCMThighSide_85_L', 'pJCMThighSide_85_R', 'pJCMThumb1Bend_50_L',
    'pJCMThumb1Bend_50_R', 'pJCMThumb1Up_20_L', 'pJCMThumb1Up_20_R', 'pJCMThumb2Bend_65_L', 'pJCMThumb2Bend_65_R',
    'pJCMThumb3Bend_90_L', 'pJCMThumb3Bend_90_R', 'pJCMToesUp_60_L', 'pJCMToesUp_60_R',
    'sfd_g8f_teethcontrol_MilkTeeth', 'tongueIn', 'tongueLeft', 'tongueOut', 'tongueRight', 'tongueUp', 'BF_FHM_CloneX'
]
OLD_FEMALE_MORPHS = [
    'Alawa8', 'Robyn8', 'Charlotte8', 'Stephanie8', 'MeiLin8', 'TeenJane8', 'Daisy8Head', 'Darcy8', 'Latonya8',
    'Zelara8', 'Mabel8', 'Leisa8', 'Babina8', 'Kayo8', 'CJ8', 'Kaylee_8_Head', 'Angharad8', 'Tika_8_Head', 'Olympia8',
    'TeenJosie8Head', 'Eva8', 'Nida8', 'Gabriela8', 'Sydney8', 'Aubrey8', 'Karyssa_8_Head', 'Sukai8Head', 'TeenRaven8',
    'Jenni8', 'Mika_8_Head', 'Sahira8', 'Bridget8', 'Penny8', 'Tasha8', 'Josephene8', 'Kanade8', 'Girl8', 'MrsChow8',
    'Sakura8Head', 'Victoria8'
]

MALE_MORPHS = [
    'AA', 'AllMuscles', 'Body_s___Children_Fat', 'Body_s___Children_Thin', 'Bodybuilder', 'CH', 'Cartoon_Jaw__01',
    'Cartoon_Jaw__02', 'Cartoon_Jaw__03', 'Cartoon_Jaw__04', 'Cartoon_Nose__01', 'Cartoon_Nose__02', 'Cartoon_Nose__03',
    'Cartoon_Nose__04', 'Cranium_Height', 'DD', 'DDStilettoNails', 'E333', 'EJFM_Brows_Arched', 'EJFM_Brows_Arched_Out',
    'EJFM_Brows_Center_Down', 'EJFM_Brows_Definition', 'EJFM_Brows_Length_In', 'EJFM_Brows_Length_Out',
    'EJFM_Brows_Pointed', 'EJFM_Brows_Raise_All', 'EJFM_Brows_Raise_Center', 'EJFM_Cheekbones_Bottom',
    'EJFM_Cheekbones_Front_01', 'EJFM_Cheekbones_Front_02', 'EJFM_Cheekbones_Front_03', 'EJFM_Cheekbones_Front_Height',
    'EJFM_Cheekbones_High_Fill', 'EJFM_Cheekbones_Low_Fill_01', 'EJFM_Cheekbones_Low_Fill_02',
    'EJFM_Cheekbones_Low_Fill_03', 'EJFM_Cheekbones_Sides_Depression', 'EJFM_Cheekbones_Under_Depression',
    'EJFM_Cheekbones_Upper_Lean', 'EJFM_Cheekbones_Wide', 'EJFM_Cheeks_Angled', 'EJFM_Cheeks_Contour_01',
    'EJFM_Cheeks_Contour_02', 'EJFM_Cheeks_Contour_03', 'EJFM_Cheeks_Contour_04', 'EJFM_Cheeks_Full_01',
    'EJFM_Cheeks_Full_02', 'EJFM_Cheeks_Full_03', 'EJFM_Cheeks_Full_04', 'EJFM_Cheeks_Lean_Lower',
    'EJFM_Cheeks_Lean_Upper', 'EJFM_Cheeks_Low_01', 'EJFM_Cheeks_Low_02', 'EJFM_Cheeks_Mouth_Depth',
    'EJFM_Cheeks_Remove', 'EJFM_Chin_Dimple', 'EJFM_Chin_Height', 'EJFM_Chin_Lip_Crease', 'EJFM_Chin_Lip_Height',
    'EJFM_Chin_Long_Down', 'EJFM_Chin_Lower_Depression', 'EJFM_Chin_Lower_Soften', 'EJFM_Chin_Receding',
    'EJFM_Chin_Sides_01', 'EJFM_Chin_Sides_02', 'EJFM_Chin_Sides_Width', 'EJFM_Chin_Smooth', 'EJFM_Chin_Thin',
    'EJFM_Chin_Volume', 'EJFM_Chin_W_', 'EJFM_Chin__01', 'EJFM_Chin__02', 'EJFM_Chin__03', 'EJFM_Chin__04',
    'EJFM_Chin__05', 'EJFM_Chin__06', 'EJFM_Dimples_Style_01_Small', 'EJFM_Dimples_Style_02_Creased',
    'EJFM_Dimples_Style_03_Low', 'EJFM_Dimples_Style_04_Wide', 'EJFM_Ears_Small', 'EJFM_Ears_Thin',
    'EJFM_Eyelids_Lower_Height_Center', 'EJFM_Eyelids_Lower_Height_Inner', 'EJFM_Eyelids_Lower_Height_Outer',
    'EJFM_Eyelids_Lower_Sunken', 'EJFM_Eyelids_Lower_Volume_All', 'EJFM_Eyelids_Lower_Volume_Inner',
    'EJFM_Eyelids_Lower_Volume_Outer', 'EJFM_Eyelids_Lower_Volume_Wide', 'EJFM_Eyelids_Upper_Heavy',
    'EJFM_Eyelids_Upper_Inner_Style_01', 'EJFM_Eyelids_Upper_Inner_Style_02', 'EJFM_Eyelids_Upper_Inner_Style_03',
    'EJFM_Eyelids_Upper_Inner_Width', 'EJFM_Eyelids_Upper_Outer_Style_01', 'EJFM_Eyelids_Upper_Outer_Style_02',
    'EJFM_Eyelids_Upper_Outer_Style_03', 'EJFM_Eyelids_Upper_Smooth', 'EJFM_Eyelids_Upper_Tension',
    'EJFM_Eyes_Corner_Outer_Down', 'EJFM_Eyes_Corner_Outer_Longer_01', 'EJFM_Eyes_Corner_Outer_Longer_02',
    'EJFM_Eyes_Corner_Outer_Up', 'EJFM_Eyes_Cover_Fold_Inner', 'EJFM_Eyes_Distance', 'EJFM_Eyes_Height',
    'EJFM_Eyes_Iris_Size', 'EJFM_Eyes_Lower_Depression', 'EJFM_Eyes_Orbits_Lower_Size', 'EJFM_Eyes_Orbits_Upper_Size',
    'EJFM_Eyes_Orbits_Upper_Size_Outer', 'EJFM_Eyes_Pupil_Size', 'EJFM_Eyes_Rotation', 'EJFM_Eyes_Round',
    'EJFM_Eyes_Size', 'EJFM_Eyes_Style_Almond', 'EJFM_Eyes_Style_Asian', 'EJFM_Eyes_Style_Close_Set',
    'EJFM_Eyes_Style_Drooping', 'EJFM_Eyes_Style_Hooded', 'EJFM_Eyes_Style_Romantic', 'EJFM_Eyes_Style_Round',
    'EJFM_Eyes_Style_Wide_Set', 'EJFM_Face_Chiseled', 'EJFM_Face_Concave', 'EJFM_Face_Convex', 'EJFM_Face_Diamond',
    'EJFM_Face_Heart', 'EJFM_Face_Long', 'EJFM_Face_Oval', 'EJFM_Face_Pear', 'EJFM_Face_Projection_Angle',
    'EJFM_Face_Projection_Center', 'EJFM_Face_Round', 'EJFM_Face_Square', 'EJFM_Forehead_Round', 'EJFM_Forehead_Slope',
    'EJFM_Forehead__01', 'EJFM_Forehead__02', 'EJFM_Forehead__03', 'EJFM_Frontal_Eminence', 'EJFM_Frontal_Lobes_',
    'EJFM_Frown_Double', 'EJFM_Frown_Low', 'EJFM_Frown_Single', 'EJFM_Glabella', 'EJFM_Glabella_Bumpy',
    'EJFM_Head_Receding_Upper', 'EJFM_Head_Top_Lower', 'EJFM_Head_Top_Narrow', 'EJFM_Head_Width',
    'EJFM_Infraorbital_Furrow_01', 'EJFM_Infraorbital_Furrow_02', 'EJFM_Jaw_Angle_High', 'EJFM_Jaw_Angle_Low',
    'EJFM_Jaw_Back_Depth', 'EJFM_Jaw_Back_Height', 'EJFM_Jaw_Bottom_Tension', 'EJFM_Jaw_Delicate', 'EJFM_Jaw_Diamond',
    'EJFM_Jaw_Full_Definition', 'EJFM_Jaw_Heart', 'EJFM_Jaw_Low', 'EJFM_Jaw_Lower_Definition', 'EJFM_Jaw_Lower_Width',
    'EJFM_Jaw_Middle_Depression', 'EJFM_Jaw_Middle_Width', 'EJFM_Jaw_Muscles', 'EJFM_Jaw_Narrow', 'EJFM_Jaw_Oval',
    'EJFM_Jaw_Regular', 'EJFM_Jaw_Round_Big', 'EJFM_Jaw_Round_Small', 'EJFM_Jaw_Sides_Definition',
    'EJFM_Jaw_Sides_Width', 'EJFM_Jaw_Small', 'EJFM_Jaw_Square', 'EJFM_Jaw_Strong', 'EJFM_Jaw_Style_01',
    'EJFM_Jaw_Style_02', 'EJFM_Jaw_Style_03', 'EJFM_Jaw_Style_04', 'EJFM_Jaw_Style_05', 'EJFM_Jaw_Top_Width',
    'EJFM_Lacrimals_Pointy_Down', 'EJFM_Lacrimals_Size', 'EJFM_Lacrimals_Up', 'EJFM_Lip_Corners_Style_01',
    'EJFM_Lip_Corners_Style_02', 'EJFM_Lip_Corners_Style_03', 'EJFM_Lip_Corners_Style_04', 'EJFM_Lip_Corners_Style_05',
    'EJFM_Lip_Corners_Style_06', 'EJFM_Lip_Gap_Soft', 'EJFM_Lip_Gap_Strong', 'EJFM_Lip_Lower_Bumpy',
    'EJFM_Lip_Lower_Definition', 'EJFM_Lip_Lower_Depth_Center', 'EJFM_Lip_Lower_Depth_Sides', 'EJFM_Lip_Lower_Diamond',
    'EJFM_Lip_Lower_Not_Occluded_01', 'EJFM_Lip_Lower_Not_Occluded_02', 'EJFM_Lip_Lower_Size', 'EJFM_Lip_Lower_Square',
    'EJFM_Lip_Lower_Thin', 'EJFM_Lip_Lower_Volume', 'EJFM_Lip_Lower_Width', 'EJFM_Lip_Peaks_Narrow',
    'EJFM_Lip_Peaks_Sharp', 'EJFM_Lip_Peaks_Smooth', 'EJFM_Lip_Upper_Bumpy', 'EJFM_Lip_Upper_Center_Definition',
    'EJFM_Lip_Upper_Center_Depth', 'EJFM_Lip_Upper_Definition', 'EJFM_Lip_Upper_Diamond', 'EJFM_Lip_Upper_Inner_Curves',
    'EJFM_Lip_Upper_Not_Occluded_01', 'EJFM_Lip_Upper_Not_Occluded_02', 'EJFM_Lip_Upper_Outer_Curves',
    'EJFM_Lip_Upper_Sides_Definition', 'EJFM_Lip_Upper_Sides_Depth', 'EJFM_Lip_Upper_Size', 'EJFM_Lip_Upper_Square',
    'EJFM_Lip_Upper_Thin', 'EJFM_Lip_Upper_Volume', 'EJFM_Lip_Upper_Width', 'EJFM_Lips_Bee_Bite_Lower',
    'EJFM_Lips_Bee_Bite_Upper', 'EJFM_Lips_Big_Not_Occluded', 'EJFM_Lips_Center_Height', 'EJFM_Lips_Protrusion',
    'EJFM_Lips_Small_Thin', 'EJFM_Lips_Style_Bee_Bite', 'EJFM_Lips_Style_Downward_Turned', 'EJFM_Lips_Style_Full_01',
    'EJFM_Lips_Style_Full_02', 'EJFM_Lips_Style_Heart', 'EJFM_Lips_Style_Round', 'EJFM_Lips_Style_Square',
    'EJFM_Lips_Style_Thin_01', 'EJFM_Lips_Style_Thin_02', 'EJFM_Lips_Style_Thin_03', 'EJFM_Lips_Style_Thin_04',
    'EJFM_Lips_Style_Thin_05', 'EJFM_Mouth_Downwards_Creases', 'EJFM_Mouth_Frown', 'EJFM_Mouth_Lower_Projection',
    'EJFM_Mouth_Narrow_Inner', 'EJFM_Mouth_Narrow_Outer', 'EJFM_Mouth_Projection_Full', 'EJFM_Mouth_Sides_Bump',
    'EJFM_Mouth_Sides_Depth', 'EJFM_Mouth_Sides_Muscle', 'EJFM_Mouth_Upper_Projection', 'EJFM_Nape_',
    'EJFM_Nape_Occipital_Bun', 'EJFM_Nape_Occipital_Smooth', 'EJFM_Nasal_Root_Depression_01',
    'EJFM_Nasal_Root_Depression_02', 'EJFM_Nasal_Root_Extra_Wide', 'EJFM_Nasal_Root_Height',
    'EJFM_Nasal_Root_Progressive', 'EJFM_Nasal_Root_Thin', 'EJFM_Nasal_Root_Width', 'EJFM_Nasolabial_Crease_Medium',
    'EJFM_Nasolabial_Crease_Smooth', 'EJFM_Nasolabial_Crease_Thin', 'EJFM_Neck_Short', 'EJFM_Neck_Width',
    'EJFM_Nose_Bridge_Depth', 'EJFM_Nose_Bridge_Small', 'EJFM_Nose_Bump_Concave', 'EJFM_Nose_Bump_Irregular',
    'EJFM_Nose_Bump_Lower', 'EJFM_Nose_Bump_Removal', 'EJFM_Nose_Bump_Size', 'EJFM_Nose_Bump_Upper',
    'EJFM_Nose_Cartilage_Side_Protrusion', 'EJFM_Nose_Flesh_Height', 'EJFM_Nose_Flesh_Small', 'EJFM_Nose_Lower_Width',
    'EJFM_Nose_Ridge_Angle', 'EJFM_Nose_Ridge_Greek', 'EJFM_Nose_Ridge_Hourglass', 'EJFM_Nose_Ridge_Irregular',
    'EJFM_Nose_Ridge_Middle_Width', 'EJFM_Nose_Ridge_Roman', 'EJFM_Nose_Ridge_Sides_Straight', 'EJFM_Nose_Ridge_Square',
    'EJFM_Nose_Ridge_Upper_Width', 'EJFM_Nose_Ridge_Wide', 'EJFM_Nose_Ridge_Width_Variation',
    'EJFM_Nose_Rige_Lower_Width', 'EJFM_Nose_Sides_Define', 'EJFM_Nose_Sides_Lower_Thin', 'EJFM_Nose_Sides_Thin',
    'EJFM_Nose_Style_01', 'EJFM_Nose_Style_02', 'EJFM_Nose_Style_03', 'EJFM_Nose_Style_04', 'EJFM_Nose_Style_05',
    'EJFM_Nose_Stylize', 'EJFM_Nose_Wide_Straight', 'EJFM_Nose_Width_Lower', 'EJFM_Nose_Wings_Depth',
    'EJFM_Nose_Wings_Front_Width', 'EJFM_Nose_Wings_Height', 'EJFM_Nose_Wings_Height_Upper',
    'EJFM_Nose_Wings_Lower_Crease', 'EJFM_Nose_Wings_Smooth', 'EJFM_Nose_Wings_Smooth_Shrink',
    'EJFM_Nose_Wings_Upper_Crease', 'EJFM_Nose_Wings_Upper_Smooth', 'EJFM_Nose_Wings_Width', 'EJFM_Nostrils_Depth',
    'EJFM_Nostrils_Front_Down', 'EJFM_Nostrils_Lower_Height', 'EJFM_Nostrils_Size', 'EJFM_Nostrils_Triangular',
    'EJFM_Nostrils_Width', 'EJFM_Orbital_Outer_Depression', 'EJFM_Philtrum_Depth_Lower', 'EJFM_Philtrum_Depth_Upper',
    'EJFM_Philtrum_Sides_Definition', 'EJFM_Piltrum_Emphasize', 'EJFM_Septum_Double', 'EJFM_Septum_Height',
    'EJFM_Septum_Inner_Planar', 'EJFM_Septum_Planar', 'EJFM_Septum_Top_Planar', 'EJFM_Septum_Width_Inner',
    'EJFM_Septum_Width_Middle', 'EJFM_Septum_Width_Outer', 'EJFM_Septum_Width_Uniform', 'EJFM_Superciliary_Arches_01',
    'EJFM_Superciliary_Arches_02', 'EJFM_Superciliary_Arches_03', 'EJFM_Supraobital_Ridges', 'EJFM_Supraorbital_Arches',
    'EJFM_Teeth_Canines_Big', 'EJFM_Teeth_Gap', 'EJFM_Teeth_Incisives_Big', 'EJFM_Teeth_Irregular_01',
    'EJFM_Teeth_Irregular_02', 'EJFM_Teeth_Size', 'EJFM_Temples_Style_01', 'EJFM_Temples_Style_02',
    'EJFM_Temples_Style_03', 'EJFM_Temples_Style_04', 'EJFM_Temporal_Depth', 'EJFM_Temporal_Ridges', 'EJFM_Tip_Bulbous',
    'EJFM_Tip_Cartilage_Creases', 'EJFM_Tip_Depth', 'EJFM_Tip_Height', 'EJFM_Tip_Length', 'EJFM_Tip_Long_Pointy',
    'EJFM_Tip_Pinch', 'EJFM_Tip_Planar', 'EJFM_Tip_Sides_Bumps', 'EJFM_Tip_Sides_Square', 'EJFM_Tip_Thin',
    'EJFM_Tip_Triangular', 'EJFM_Tip_Turn_Up', 'EJFM_Tip_Width', 'EJFM_Under_Eye_Bags', 'EJFM_Zygomatic_Bone_Big',
    'EJFM_Zygomatic_Bone_Thin', 'Ear_Age', 'Ear_Angle_A', 'Ear_Angle_B', 'Emaciated', 'EyelidsFoldDown', 'EyelidsHeavy',
    'EyelidsLowerCreaseHD', 'EyelidsLowerHeight', 'EyelidsSmooth', 'EyelidsUpperHeight', 'EyesAlmondInner',
    'EyesAlmondOuter', 'EyesAngled', 'EyesAngledInner', 'EyesAngledOuter', 'EyesCorneaBulge', 'EyesDepth', 'EyesHeight',
    'EyesInnerDepth', 'EyesIrisSize', 'EyesPuffyLower', 'EyesPuffyUpper', 'EyesPupilsDilate', 'EyesSize', 'EyesWidth',
    'FF', 'HFS_Body_04_Zombi', 'HFS_Body_05_Sharp', 'HFS_Body_14_Muscle', 'HFS_Hands_08_Claws', 'I333',
    'Jaw_Corner_Adjuster', 'Jaw_Side_Smoother', 'Jaw_Skin_Sag', 'KK', 'LipsHeart', 'LipsSquare', 'LipsThin',
    'NailsLength', 'Nose_Age', 'Nose_Angle_Down', 'Nose_Angle_Up', 'Nose_Bridge_Depth__Alt_', 'Nose_Bridge_Style_Low',
    'Nose_Bridge_Width__Alt_', 'Nose_Broken', 'Nose_Bulbus', 'Nose_Curve_In', 'Nose_Curve_Out', 'Nose_Depth__Alt_',
    'Nose_Lower_Height', 'Nose_Neutralizer', 'Nose_Scale', 'Nose_Size_Depth', 'Nose_Style_Angular_A',
    'Nose_Style_Angular_B', 'Nose_Style_Asian_A', 'Nose_Style_Asian_B', 'Nose_Style_Asian_C', 'Nose_Style_Germanic',
    'Nose_Style_Greek', 'Nose_Style_Indian_A', 'Nose_Style_Indian_B', 'Nose_Style_Nordic', 'Nose_Style_Nubian',
    'Nose_Style_Perky', 'Nose_Style_Roman', 'Nose_Style_Slopped', 'Nose_Style_Snub', 'Nose_Width_Center_Line',
    'Nose_Wing_Arc', 'Nose_Wing_Height', 'Nose_Wing_Upper_Height', 'Nose_Wing_Volume', 'Nose_Wing_Width', 'O333',
    'PBMNavel', 'PP', 'Portly', 'RR', 'SCLPropagatingHead', 'SS', 'Silas8', 'TH', 'TMC_TongueLonger', 'Teeth_Depth',
    'Teeth_Depth_Assist', 'Teeth_Front_Lower_Angle', 'Teeth_Height', 'Teeth_Lower_Caynines_Vampire', 'Teeth_Scale',
    'Teeth_Snaggle', 'Teeth_Upper_Angle_side_to_side', 'Teeth_Upper_Caynines_Angle', 'Teeth_Upper_Caynines_Depth',
    'Teeth_Upper_Caynines_Height', 'Teeth_Upper_Caynines_Scale', 'Teeth_Upper_Caynines_Vampire',
    'Teeth_Upper_Front_Gap', 'Teeth_Upper_Mid_Incissor_L_Height', 'Teeth_Upper_Mid_Incissor_R_Height',
    'Teeth_Upper_Middle_Incissors_Angle', 'Teeth_Upper_Middle_Incissors_Scale', 'Teeth_Upper_Outer_Incissor_L_Height',
    'Teeth_Upper_Outer_Incissor_R_Height', 'Teeth_Upper_Outer_Incissors_Angle', 'Teeth_Upper_Outer_Incissors_Scale',
    'Teeth_Width', 'U333', 'Waist_', 'Waist_Depth', 'Waist_Handles_2', 'Waist_Size', 'browDownLeft', 'browDownRight',
    'browInnerUp', 'browOuterUpLeft', 'browOuterUpRight', 'cheekPuff', 'cheekSquintLeft', 'cheekSquintRight',
    'd_Armand', 'd_Ashan', 'd_Asher', 'd_Briar', 'd_Cartoon_Man_B', 'd_Chimp', 'd_Christian', 'd_Dain', 'd_Darius',
    'd_Dasan', 'd_Diego', 'd_Edward', 'd_Elios', 'd_Esra', 'd_Floyd', 'd_Gorilla', 'd_HFSCat', 'd_HFSDeer', 'd_HFSDog',
    'd_HFSElf', 'd_HFSFaun', 'd_HFSGoblin', 'd_HFSMage', 'd_HFSOrc', 'd_HFSTroll', 'd_HFSZombie', 'd_Hal', 'd_Jared',
    'd_Jonathan', 'd_JuanCarlos', 'd_Kian', 'd_Kjaer', 'd_Kwan', 'd_M3DHero1', 'd_M3DHero4', 'd_M3DTeen1', 'd_M3DTeen7',
    'd_Michael', 'd_MrWoo', 'd_Niko', 'd_Nix', 'd_Owen', 'd_Pace', 'd_Rascal', 'd_Ricardo', 'd_Sanjay', 'd_Scar',
    'd_Silas', 'd_Steve', 'd_Stylized21', 'd_Tennesee', 'd_Theguy', 'd_Tobyn', 'd_ToonBoy', 'd_ToonDad', 'd_ToonHero',
    'd_Tristan', 'd_Tyrone', 'd_Valentino', 'd_Xander', 'd_Yuzur', 'eyeBlinkLeft', 'eyeBlinkRight', 'eyeLookDownLeft',
    'eyeLookDownRight', 'eyeLookInLeft', 'eyeLookInRight', 'eyeLookOutLeft', 'eyeLookOutRight', 'eyeLookUpLeft',
    'eyeLookUpRight', 'eyeSquintLeft', 'eyeSquintRight', 'eyeWideLeft', 'eyeWideRight', 'jawForward', 'jawLeft',
    'jawOpen', 'jawRight', 'm_Skull_Chimp', 'm_Skull_ToonBoy', 'mouthClose', 'mouthDimpleLeft', 'mouthDimpleRight',
    'mouthFrownLeft', 'mouthFrownRight', 'mouthFunnel', 'mouthLeft', 'mouthLowerDownLeft', 'mouthLowerDownRight',
    'mouthPressLeft', 'mouthPressRight', 'mouthPucker', 'mouthRight', 'mouthRollLower', 'mouthRollUpper',
    'mouthShrugLower', 'mouthShrugUpper', 'mouthSmileLeft', 'mouthSmileRight', 'mouthStretchLeft', 'mouthStretchRight',
    'mouthUpperUpLeft', 'mouthUpperUpRight', 'noseSneerLeft', 'noseSneerRight', 'pJCMAbdomen2Fwd_40',
    'pJCMAbdomen2Side_24_L', 'pJCMAbdomen2Side_24_R', 'pJCMAbdomenFwd_35', 'pJCMAbdomenLowerFwd_Navel',
    'pJCMAbdomenUpperFwd_Navel', 'pJCMBigToeDown_45_L', 'pJCMBigToeDown_45_R', 'pJCMChestFwd_35', 'pJCMChestSide_20_L',
    'pJCMChestSide_20_R', 'pJCMCollarTwist_n30_L', 'pJCMCollarTwist_n30_R', 'pJCMCollarTwist_p30_L',
    'pJCMCollarTwist_p30_R', 'pJCMCollarUp_55_L', 'pJCMCollarUp_55_R', 'pJCMFlexBiceps_L', 'pJCMFlexBiceps_R',
    'pJCMFlexCalf_L', 'pJCMFlexCalf_R', 'pJCMFlexCollarUpperBack_L', 'pJCMFlexCollarUpperBack_R',
    'pJCMFlexGluteClench_L', 'pJCMFlexGluteClench_R', 'pJCMFlexHamstring_L', 'pJCMFlexHamstring_R', 'pJCMFlexQuad_L',
    'pJCMFlexQuad_R', 'pJCMFlexTriceps_L', 'pJCMFlexTriceps_R', 'pJCMFootDwn_75_L', 'pJCMFootDwn_75_R',
    'pJCMFootUp_40_L', 'pJCMFootUp_40_R', 'pJCMForeArmFwd_135_L', 'pJCMForeArmFwd_135_R', 'pJCMForeArmFwd_75_L',
    'pJCMForeArmFwd_75_R', 'pJCMHandDwn_70_L', 'pJCMHandDwn_70_R', 'pJCMHandUp_80_L', 'pJCMHandUp_80_R',
    'pJCMHeadBack_27', 'pJCMHeadFwd_25', 'pJCMIndex1Dwn_90_L', 'pJCMIndex1Dwn_90_R', 'pJCMIndex2Dwn_105_L',
    'pJCMIndex2Dwn_105_R', 'pJCMIndex3Dwn_90_L', 'pJCMIndex3Dwn_90_R', 'pJCMMid1Dwn_95_L', 'pJCMMid1Dwn_95_R',
    'pJCMMid2Dwn_105_L', 'pJCMMid2Dwn_105_R', 'pJCMMid3Dwn_90_L', 'pJCMMid3Dwn_90_R', 'pJCMNeckBack_27',
    'pJCMNeckFwd_35', 'pJCMNeckLowerSide_40_L', 'pJCMNeckLowerSide_40_R', 'pJCMNeckTwist_22_L', 'pJCMNeckTwist_22_R',
    'pJCMNeckTwist_Reverse', 'pJCMPelvisFwd_25', 'pJCMPinky1Dwn_95_L', 'pJCMPinky1Dwn_95_R', 'pJCMPinky2Dwn_105_L',
    'pJCMPinky2Dwn_105_R', 'pJCMPinky3Dwn_90_L', 'pJCMPinky3Dwn_90_R', 'pJCMRing1Dwn_95_L', 'pJCMRing1Dwn_95_R',
    'pJCMRing2Dwn_105_L', 'pJCMRing2Dwn_105_R', 'pJCMRing3Dwn_90_L', 'pJCMRing3Dwn_90_R', 'pJCMShinBend_155_L',
    'pJCMShinBend_155_R', 'pJCMShinBend_90_L', 'pJCMShinBend_90_R', 'pJCMShldrDown_40_L', 'pJCMShldrDown_40_R',
    'pJCMShldrFront_n110_Bend_n40_L', 'pJCMShldrFront_n110_Bend_p90_L', 'pJCMShldrFront_p110_Bend_n90_R',
    'pJCMShldrFront_p110_Bend_p40_R', 'pJCMShldrFwd_110_L', 'pJCMShldrFwd_110_R', 'pJCMShldrUp_90_L',
    'pJCMShldrUp_90_R', 'pJCMThighBack_35_L', 'pJCMThighBack_35_R', 'pJCMThighFwd_115_L', 'pJCMThighFwd_115_R',
    'pJCMThighFwd_57_L', 'pJCMThighFwd_57_R', 'pJCMThighSide_85_L', 'pJCMThighSide_85_R', 'pJCMThumb1Bend_50_L',
    'pJCMThumb1Bend_50_R', 'pJCMThumb1Up_20_L', 'pJCMThumb1Up_20_R', 'pJCMThumb2Bend_65_L', 'pJCMThumb2Bend_65_R',
    'pJCMThumb3Bend_90_L', 'pJCMThumb3Bend_90_R', 'pJCMToesUp_60_L', 'pJCMToesUp_60_R', 'sfd_et_g8m_vamp_LR',
    'xAllArmsLowerAlt', 'xAllArmsUpper', 'xAllArmsUpperAlt', 'xAllBack', 'xAllBackAlt', 'xAllChest', 'xAllChestAlt',
    'xAllFeet', 'xAllHands', 'xAllHip', 'xAllHipAlt', 'xAllLegsLower', 'xAllLegsLowerAlt', 'xAllLegsUpperAlt',
    'xAllNeckAlt', 'xAllWaistAlt', 'x_Biden', 'x_BoredApe', 'x_Brock', 'x_ChiefKeef', 'x_Djzull', 'x_Dominic',
    'x_Kanye', 'x_Kelani', 'x_Khea', 'x_LilNasX', 'x_Luc', 'x_MacAfee', 'x_MartinLutherKing', 'x_Melly',
    'x_MichaelJackson', 'x_Mike', 'x_Pharrel', 'x_PretoShow', 'x_Racoon', 'x_RobertDJ', 'x_StevieWilliams', 'x_TheRock',
    'x_Trump', 'x_VinDiesel', 'x_Vitalik', 'x_WoodyAllen', 'x_Ziggy', 'x_Zuckerberg', 'd_Yuzuru', 'x_Djzullu',
    'BM_FHM_CloneX', 'BaseM', 'tongueOut', 'Soft'
]

csv_file_path = ''
morph_names = {}
sections = [
    ['Morph Targets', ''],  # 1
    ['Friendly Name', ''],  # 2
    ['Morph Category', 'Body'],
    ['Section', 'None'],
    ['Min Value', -2],
    ['Max Value', 2],
    ['Supported Bodies', ''],
    ['Tab', 'None'],
    ['Folder', 'None'],
    ['Tags', ''],
    ['bHidden', 'False'],
    ['Thumbnail Ref', 'None'],
    ['Pak', '(DN="""")']
]


def error_dialog(message):
    cmds.confirmDialog(
        title='Error', message=message,
        button=['Okay'], defaultButton='Okay', cancelButton='Okay', dismissString='Okay'
    )


def initialize_character():
    global csv_file_path
    global morph_names
    global sections

    try:
        if cmds.getAttr('GeFema_Group.StudioNodeName') == 'Genesis8Female':
            is_female = True
            csv_file_path = FEMALE_CSV_FILE_PATH
            morph_names = FEMALE_ADJ_MORPHS
            sections[6][1] = '(Female0)'
        else:
            is_female = False
            csv_file_path = MALE_CSV_FILE_PATH
            morph_names = MALE_ADJ_MORPHS
            sections[6][1] = '(Male0)'

        return is_female
    except ValueError:
        error_dialog("Please import a character with Genesis 8 for Maya first and then try again.")
        return


def add_to_laylo3d_menu():
    gMainWindow = mel.eval('$tmpVar=$gMainWindow')

    if not cmds.menu('LL3D_Tools_Menu', exists=True):
        cmds.menu('LL3D_Tools_Menu', label='LayLo3D Tools', tearOff=True, parent=gMainWindow)
        cmds.menuItem(
            'LL3D_DNA_DNATools_MI1_TitleDivider',
            divider=True,
            dividerLabel='DNABLOCK',
            parent='LL3D_Tools_Menu',
        )
        cmds.menuItem(
            'LL3D_DNA_DNATools_MI2_Menu', subMenu=True, tearOff=True,
            label='DNABLOCK Tools',
            parent='LL3D_Tools_Menu',
            insertAfter='LL3D_DNA_DNATools_MI1_TitleDivider'
        )
    elif not cmds.menu('LL3D_DNA_DNATools_MI2_Menu', exists=True):
        menu_items = cmds.menu('LL3D_Tools_Menu', query=True, itemArray=True)
        menu_items.append('LL3D_DNA_DNATools_MI1_TitleDivider')
        menu_items.append('LL3D_DNA_DNATools_MI2_Menu')
        menu_items.sort()

        for i, menu_item in enumerate(menu_items):
            if menu_item == 'LL3D_DNA_DNATools_MI1_TitleDivider':
                if i == 0:
                    insert_after = ''
                else:
                    insert_after = menu_items[i - 1]

                cmds.menuItem(
                    'LL3D_DNA_DNATools_MI1_TitleDivider',
                    divider=True,
                    dividerLabel='DNABLOCK',
                    parent='LL3D_Tools_Menu',
                    insertAfter=insert_after
                )
            elif menu_item == 'LL3D_DNA_DNATools_MI2_Menu':
                if i == 0:
                    insert_after = ''
                else:
                    insert_after = menu_items[i - 1]

                cmds.menuItem(
                    'LL3D_DNA_DNATools_MI2_Menu', subMenu=True, tearOff=True,
                    label='DNABLOCK Tools',
                    parent='LL3D_Tools_Menu',
                    insertAfter=insert_after
                )

    cmds.setParent('LL3D_DNA_DNATools_MI2_Menu', menu=True)

    menu_items = cmds.menu('LL3D_DNA_DNATools_MI2_Menu', query=True, itemArray=True)
    if not menu_items:
        menu_items = ['DNA_CsvEntries_MI1_Divider', 'DNA_CsvEntries_MI2_ShowWindow']
    else:
        menu_items.append('DNA_CsvEntries_MI1_Divider')
        menu_items.append('DNA_CsvEntries_MI2_ShowWindow')
    menu_items.sort()

    kwargs = {}
    if menu_items.index('DNA_CsvEntries_MI1_Divider') == 0 and len(menu_items) > 2:
        kwargs = {'insertAfter': ''}
    elif menu_items.index('DNA_CsvEntries_MI1_Divider') > 0:
        kwargs = {'insertAfter': menu_items[menu_items.index('DNA_CsvEntries_MI1_Divider') - 1]}

    last_menu_item = cmds.menuItem(
        'DNA_CsvEntries_MI1_Divider', divider=True,
        dividerLabel='DNABLOCK CSV Entries',
        **kwargs
    )
    cmds.menuItem(
        'DNA_CsvEntries_MI2_ShowWindow', label='Open CSV Entries', command=show_window, insertAfter=last_menu_item)


def remove_from_laylo_3d_menu():
    if cmds.menuItem('DNA_CsvEntries_MI1_Divider', exists=True):
        cmds.deleteUI('DNA_CsvEntries_MI1_Divider')
    if cmds.menuItem('DNA_CsvEntries_MI2_ShowWindow', exists=True):
        cmds.deleteUI('DNA_CsvEntries_MI2_ShowWindow')

    if cmds.optionVar(exists='DNA_DefaultCsvFile'):
        cmds.optionVar(remove='DNA_DefaultCsvFile')

    if not cmds.menu('LL3D_DNA_DNATools_MI2_Menu', query=True, itemArray=True):
        if cmds.menuItem('LL3D_DNA_DNATools_MI1_TitleDivider', exists=True):
            cmds.deleteUI('LL3D_DNA_DNATools_MI1_TitleDivider')
        if cmds.menu('LL3D_DNA_DNATools_MI2_Menu', exists=True):
            cmds.deleteUI('LL3D_DNA_DNATools_MI2_Menu')

    if cmds.menu('LL3D_Tools_Menu', exists=True):
        if cmds.menu('LL3D_Tools_Menu', query=True, numberOfItems=True) == 0:
            cmds.deleteUI('LL3D_Tools_Menu')


def get_adjustment_blend_shapes(is_female, obj_name):
    morph_list = list(
        set(cmds.aliasAttr('{}_Morphs'.format(obj_name), query=True)[::2]) -
        set(FEMALE_MORPHS + OLD_FEMALE_MORPHS) - set(MALE_MORPHS))

    if not is_female:
        morph_list = list(set(morph_list) - set(MALE_IGNORE_FEMALE_MORPHS))

    return morph_list


def file_browser(arg=None):
    source_file = cmds.fileDialog2(
        fileFilter='CSV (*.csv)', dialogStyle=2, fileMode=1,
        okCaption='Open', startingDirectory='T:/__Characters/Tables/Morphs',
        caption="Select the Data Table CSV File..."
    )
    if source_file:
        source_file = source_file[0]
        cmds.textFieldButtonGrp('DNA_CsvFile_textFieldButtonGrp', edit=True, text=source_file)
        cmds.optionVar(stringValue=('DNA_DefaultCsvFile', source_file))


def show_window(arg=None):
    is_female = initialize_character()
    if is_female is None:
        return

    if cmds.window('DNA_CsvWindow', exists=True):
        cmds.deleteUI('DNA_CsvWindow', window=True)
        cmds.windowPref('DNA_CsvWindow', remove=True)

    cmds.window(
        'DNA_CsvWindow',
        title="CSV Entries",
        widthHeight=(1450, 720)
    )
    cmds.scrollLayout(childResizable=True)
    cmds.formLayout('LL3D_DNA_RetargetAnimations_FormLayout')

    cmds.rowLayout(
        'DNA_CsvFile_rowLayout', numberOfColumns=1, adjustableColumn=1,
        # columnWidth=((1, 250), (2, 90), (3, 410)),
        # columnAttach=((1, 'left', 5), (2, 'left', 5), (3, 'left', 5))
    )
    cmds.textFieldButtonGrp(
        'DNA_CsvFile_textFieldButtonGrp', label='CSV File', adjustableColumn=2,
        columnWidth=((1, 50), (2, 90), (3, 80)),
        columnAttach=((1, 'left', 5), (2, 'left', 0), (3, 'left', 5)),
        # text=cmds.optionVar(query='DNA_DefaultCsvFile') if cmds.optionVar(exists='DNA_DefaultCsvFile') else '',
        text=csv_file_path,
        buttonLabel='Browse', buttonCommand=file_browser)
    cmds.setParent('..')

    cmds.rowLayout(
        'DNA_CsvFixFriendlyName_rowLayout', numberOfColumns=1, adjustableColumn=1,
        # columnWidth=((1, 250), (2, 90), (3, 410)),
        columnAttach=(1, 'left', 5)
    )
    cmds.checkBox('DNA_CsvFixFriendlyNames_checkbox', enable=True,
                  label='Update Existing Friendly Names and Sort Morphs Alphabetically', value=False)
    cmds.setParent('..')

    row = 0
    rows = []
    entries = []
    for obj in cmds.listRelatives('group1', children=True):
        if obj in ['GeFema']:
            continue

        rows.append(
            cmds.rowLayout(
                'DNA_CsvEntry_rowLayout{}'.format(row), numberOfColumns=3,  # adjustableColumn=2,
                columnWidth=((1, 250), (2, 90), (3, 410)),
                columnAttach=((1, 'left', 5), (2, 'left', 5), (3, 'left', 5))
            )
        )
        cmds.text('DNA_MayaAssetName{}_text'.format(obj), label=obj)
        cmds.text(label='Unreal Asset ID')
        entries.append([cmds.textField('DNA_UnrealAssetName{}_textFieldGrp'.format(row), width=400)])
        cmds.setParent('..')

        cmds.columnLayout('DNA_CsvAdjMorphs_columnLayout{}'.format(row))
        adj_blend_shapes = get_adjustment_blend_shapes(is_female, obj)
        columns = []
        if adj_blend_shapes:
            num_columns = len(sections) + 1
            cmds.rowLayout(
                'DNA_CsvMorphTitle{}_rowLayout'.format(obj), numberOfColumns=num_columns,  # adjustableColumn=3,
                columnWidth=(
                        [(1, 50), (2, 210), (3, 210)] + [(column, 110) for column in range(4, 6)] +
                        [(column, 70) for column in range(6, 8)] +
                        [(column, 110) for column in range(8, num_columns + 1)]),
                columnAttach=((column, 'left', 10) for column in range(1, num_columns + 1))
            )
            cmds.text(label='Include')
            for section in sections:
                cmds.text(label=section[0])

            cmds.setParent('..')

            for i, adjustment_morph in enumerate(adj_blend_shapes):
                entry = []
                cmds.rowLayout(
                    'DNA_CsvMorph{}_rowLayout{}'.format(obj, i), numberOfColumns=num_columns,  # adjustableColumn=3,
                    columnWidth=(
                            [(1, 50), (2, 210), (3, 210)] + [(column, 110) for column in range(4, 6)] +
                            [(column, 70) for column in range(6, 8)] +
                            [(column, 110) for column in range(8, num_columns + 1)]),
                    columnAttach=((column, 'left', 10) for column in range(1, num_columns + 1))
                )
                entry.append(cmds.checkBox('DNA_CsvEntry{}_checkBox{}'.format(obj, i), label='', value=True))
                entry.append(adjustment_morph)
                cmds.text('DNA_MayaAssetName{}_text{}'.format(obj, i), label=adjustment_morph)
                entry.append(cmds.textField(
                    'DNA_CsvFriendlyName{}_textfield{}'.format(obj, i),
                    text=morph_names[adjustment_morph] if adjustment_morph in morph_names
                    else create_friendly_name(adjustment_morph),
                    width=200
                ))
                for e, section in enumerate(sections[2:]):
                    if e in (2, 3):
                        entry.append(cmds.floatField(
                            'DNA_Csv{}{}_floatField{}'.format(section[0], obj, i),
                            value=float(section[1]), width=60, showTrailingZeros=False
                        ))
                    else:
                        entry.append(cmds.textField(
                            'DNA_Csv{}{}_textField{}'.format(section[0], obj, i),
                            text=section[1], width=100 if e not in (2, 3) else 60
                        ))

                cmds.setParent('..')
                columns.append(entry)
        else:
            cmds.text('DNA_MayaAssetName{}_text{}'.format(obj, 0), label='No Adjustment Morphs found...')

        entries[row].append(columns)
        cmds.setParent('..')

        if len(rows) == 1:
            cmds.formLayout(
                'LL3D_DNA_RetargetAnimations_FormLayout',
                edit=True,
                attachForm=(
                    ('DNA_CsvFile_rowLayout', 'top', 5),
                    ('DNA_CsvFile_rowLayout', 'left', 5),
                    ('DNA_CsvFile_rowLayout', 'right', 5),
                    ('DNA_CsvFixFriendlyName_rowLayout', 'left', 5),
                    ('DNA_CsvFixFriendlyName_rowLayout', 'right', 5),
                    (rows[row], 'left', 5),
                    (rows[row], 'right', 5),
                    ('DNA_CsvAdjMorphs_columnLayout{}'.format(row), 'left', 20),
                    ('DNA_CsvAdjMorphs_columnLayout{}'.format(row), 'right', 5),
                ),
                attachControl=(
                    ('DNA_CsvFixFriendlyName_rowLayout', 'top', 0, 'DNA_CsvFile_rowLayout'),
                    (rows[row], 'top', 10, 'DNA_CsvFixFriendlyName_rowLayout'),
                    ('DNA_CsvAdjMorphs_columnLayout{}'.format(row), 'top', 0, rows[row]),
                )
            )
            row += 1
            continue

        cmds.formLayout(
            'LL3D_DNA_RetargetAnimations_FormLayout',
            edit=True,
            attachForm=(
                (rows[row], 'left', 5),
                (rows[row], 'right', 5),
                ('DNA_CsvAdjMorphs_columnLayout{}'.format(row), 'left', 20),
                ('DNA_CsvAdjMorphs_columnLayout{}'.format(row), 'right', 5),
            ),
            attachControl=(
                (rows[row], 'top', 20, 'DNA_CsvAdjMorphs_columnLayout{}'.format(row - 1)),
                ('DNA_CsvAdjMorphs_columnLayout{}'.format(row), 'top', 0, rows[row]),
            )
        )

        row += 1

    cmds.button('DNA_CsvEntryDone_button', label='Done', command=partial(done, entries))
    try:
        cmds.formLayout(
            'LL3D_DNA_RetargetAnimations_FormLayout',
            edit=True,
            attachForm=(
                ('DNA_CsvEntryDone_button', 'left', 5),
                ('DNA_CsvEntryDone_button', 'right', 10),
                ('DNA_CsvEntryDone_button', 'bottom', 10),
            ),
            attachControl=(
                ('DNA_CsvEntryDone_button', 'top', 5, 'DNA_CsvAdjMorphs_columnLayout{}'.format(row-1)) if rows else
                ('DNA_CsvEntryDone_button', 'top', 5, 'DNA_CsvAdjMorphs_columnLayout{}'.format(row-1)),
            )
        )

        cmds.showWindow('DNA_CsvWindow')
    except RuntimeError:
        pass


def create_friendly_name(morph_name):
    if morph_name.startswith('ADJ_'):
        morph_name = morph_name[4:]

    return ' '.join(camel_case_split(morph_name.replace('_', ' ')))


def camel_case_split(string):
    return re.findall(r'[A-Z](?:[a-z]+|[A-Z]*(?=[A-Z]|$))', string)


def append_list_to_csv(source_file, data):
    with open(source_file, 'a') as csv_file:
        for line in data:
            csv_file.write(','.join(line) + '\n')


def delete_window():
    if cmds.window('DNA_CsvWindow', exists=True):
        cmds.deleteUI('DNA_CsvWindow', window=True)


def update_existing_friendly_names(source_file):
    with open(source_file, 'r') as csv_file:
        csv_reader = csv.reader(csv_file)
        csv_list = list(csv_reader)

    assets = OrderedDict()
    for row in csv_list[1:]:
        assets.setdefault(row[1], [])

        if row[2] in FEMALE_ADJ_MORPHS:
            if row[8] != FEMALE_ADJ_MORPHS[row[2]]:
                print("Changing {} to {}".format(row[8], FEMALE_ADJ_MORPHS[row[2]]))
                row[8] = FEMALE_ADJ_MORPHS[row[2]]
        elif row[2] in MALE_ADJ_MORPHS:
            if row[8] != MALE_ADJ_MORPHS[row[2]]:
                print("Changing {} to {}".format(row[8], MALE_ADJ_MORPHS[row[2]]))
                row[8] = MALE_ADJ_MORPHS[row[2]]

        assets[row[1]].append(row)

    with open('{}_organized.csv'.format(source_file[:-4]), 'w') as csv_file:
        # csv_writer = csv.writer(csv_file)
        # csv_writer.writerows(csv_list)

        csv_file.write(','.join(csv_list[0]) + '\n')
        for asset in assets:
            assets[asset].sort(key=lambda x: x[8])
            for row in assets[asset]:
                row = row[:1] + ['"{}"'.format(txt) for txt in row[1:]]
                csv_file.write(','.join(row) + '\n')


def done(entries, arg=None):
    source_file = cmds.textFieldButtonGrp('DNA_CsvFile_textFieldButtonGrp', query=True, text=True)
    if source_file:
        source_file = source_file.replace("\\", "/")

    if cmds.checkBox('DNA_CsvFixFriendlyNames_checkbox', query=True, value=True):
        update_existing_friendly_names(source_file)
        return

    asset_ids = {}

    for entry in entries:
        if not entry[1]:
            continue

        asset_id = cmds.textField(entry[0], query=True, text=True)
        cmds.textField(entry[0], edit=True, backgroundColor=(0.16863, 0.16863, 0.16863))
        if not asset_id:
            cmds.textField(entry[0], edit=True, backgroundColor=(0.7, 0, 0))
            error_dialog("If the item has morphs the Unreal Asset ID must be specified.")
            return

        asset_ids[asset_id] = []

    if source_file:
        with open(source_file, 'r') as csv_file:
            csvreader = csv.reader(csv_file)

            # extracting field names through first row
            # fields = next(csvreader)[2:]
            # fields = [f[:1].lower() + f[1:] for f in fields]

            for row in csvreader:
                if row[1] in asset_ids:
                    asset_ids[row[1]].append(row[2])

    append_to_csv = []
    for entry in entries:
        if not entry[1]:
            continue

        asset_id = cmds.textField(entry[0], query=True, text=True)
        obj_adj_morphs_data = []
        for adjustment_morph in entry[1]:
            checked = cmds.checkBox(adjustment_morph[0], query=True, value=True)
            if checked:
                morph_targets = adjustment_morph[1]
                if morph_targets in asset_ids[asset_id]:
                    print("Already in Data Table >", asset_id, morph_targets)
                    continue

                row_name = '{}_{}'.format(asset_id, morph_targets)
                friendly_name = cmds.textField(adjustment_morph[2], query=True, text=True)
                # morph_category = cmds.textField(adjustment_morph[3], query=True, text=True)
                # section = cmds.textField(adjustment_morph[4], query=True, text=True)
                # min_value = cmds.textField(adjustment_morph[5], query=True, text=True)
                # max_value = cmds.textField(adjustment_morph[6], query=True, text=True)
                # supported_bodies = cmds.textField(adjustment_morph[7], query=True, text=True)
                # tab = cmds.textField(adjustment_morph[8], query=True, text=True)
                # folder = cmds.textField(adjustment_morph[9], query=True, text=True)
                # tags = cmds.textField(adjustment_morph[10], query=True, text=True)
                # type = cmds.textField(adjustment_morph[11], query=True, text=True)
                # thumbnail = cmds.textField(adjustment_morph[12], query=True, text=True)
                min_value = cmds.floatField(adjustment_morph[5], query=True, value=True)
                max_value = cmds.floatField(adjustment_morph[6], query=True, value=True)
                data = [cmds.textField(
                    text, query=True, text=True) for text in adjustment_morph[3:5] + adjustment_morph[7:]]
                data.insert(0, row_name)
                data.insert(1, asset_id)
                data.insert(2, morph_targets)
                data.insert(5, str(min_value))
                data.insert(6, str(max_value))
                data.insert(8, friendly_name)
                data = data[:1] + ['"{}"'.format(txt) for txt in data[1:]]
                # print(','.join(data))
                obj_adj_morphs_data.append(data)

        obj_adj_morphs_data.sort(key=lambda x: x[8])  # Sort them alphabetically by Friendly Name
        append_to_csv = append_to_csv + obj_adj_morphs_data

    append_list_to_csv(source_file, append_to_csv)
    delete_window()
