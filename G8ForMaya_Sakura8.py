import maya.mel as mel
import maya.cmds as cmds


def Sakura8JCMs(character, blendShapeGroup):
    pyBlendShapesName = blendShapeGroup
    if not cmds.objExists('{}.pJCMSakura8lEyeDwna'.format(pyBlendShapesName)):
        error = character + ' specific JCMs were not created.\n\n' + character + ' JCMs were either not exported out properly or the wrong file was selected for import.'
        cmds.warning(error.replace('\n\n', ' '))
        return error

    # pJCMSakura8lEyeDwna
    mel.eval('setAttr "lEye.rotateX" 0;')
    mel.eval('setDrivenKeyframe -currentDriver lEye.rotateX {0}.pJCMSakura8lEyeDwna;'.format(
        pyBlendShapesName))
    mel.eval('setAttr "lEye.rotateX" -30;')
    mel.eval('setAttr "{0}.pJCMSakura8lEyeDwna" 1;'.format(pyBlendShapesName))
    mel.eval('setDrivenKeyframe -currentDriver lEye.rotateX {0}.pJCMSakura8lEyeDwna;'.format(
        pyBlendShapesName))
    mel.eval('setAttr "lEye.rotateX" 0;')

    # pJCMSakura8rEyeDwna
    mel.eval('setAttr "rEye.rotateX" 0;')
    mel.eval('setDrivenKeyframe -currentDriver rEye.rotateX {0}.pJCMSakura8rEyeDwna;'.format(
        pyBlendShapesName))
    mel.eval('setAttr "rEye.rotateX" -30;')
    mel.eval('setAttr "{0}.pJCMSakura8rEyeDwna" 1;'.format(pyBlendShapesName))
    mel.eval('setDrivenKeyframe -currentDriver rEye.rotateX {0}.pJCMSakura8rEyeDwna;'.format(
        pyBlendShapesName))
    mel.eval('setAttr "rEye.rotateX" 0;')
