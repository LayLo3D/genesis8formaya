import maya.mel as mel
import maya.cmds as cmds


def Lucas8JCMs(character, blendShapeGroup):
    pyBlendShapesName = blendShapeGroup
    if not cmds.objExists('{}.pJCMLucas8CollarUp_55_L'.format(pyBlendShapesName)):
        error = character + ' specific JCMs were not created.\n\n' + character + ' JCMs were either not exported out properly or the wrong file was selected for import.'
        cmds.warning(error.replace('\n\n', ' '))
        return error

    # pJCMLucas8CollarUp_55_L
    mel.eval('setAttr "lCollar.rotateZ" 0;')
    mel.eval(
        'setDrivenKeyframe -currentDriver lCollar.rotateZ {0}.pJCMLucas8CollarUp_55_L;'.format(pyBlendShapesName))
    mel.eval('setAttr "lCollar.rotateZ" 55;')
    mel.eval('setAttr "{0}.pJCMLucas8CollarUp_55_L" 1;'.format(pyBlendShapesName))
    mel.eval(
        'setDrivenKeyframe -currentDriver lCollar.rotateZ {0}.pJCMLucas8CollarUp_55_L;'.format(pyBlendShapesName))
    mel.eval('setAttr "lCollar.rotateZ" 0;')

    # pJCMLucas8CollarUp_55_R
    mel.eval('setAttr "rCollar.rotateZ" 0;')
    mel.eval(
        'setDrivenKeyframe -currentDriver rCollar.rotateZ {0}.pJCMLucas8CollarUp_55_R;'.format(pyBlendShapesName))
    mel.eval('setAttr "rCollar.rotateZ" -55;')
    mel.eval('setAttr "{0}.pJCMLucas8CollarUp_55_R" 1;'.format(pyBlendShapesName))
    mel.eval(
        'setDrivenKeyframe -currentDriver rCollar.rotateZ {0}.pJCMLucas8CollarUp_55_R;'.format(pyBlendShapesName))
    mel.eval('setAttr "rCollar.rotateZ" 0;')

    # pJCMLucas8FootUp_40_L
    mel.eval('setAttr "lFoot.rotateX" 0;')
    mel.eval('setDrivenKeyframe -currentDriver lFoot.rotateX {0}.pJCMLucas8FootUp_40_L;'.format(pyBlendShapesName))
    mel.eval('setAttr "lFoot.rotateX" -40;')
    mel.eval('setAttr "{0}.pJCMLucas8FootUp_40_L" 1;'.format(pyBlendShapesName))
    mel.eval('setDrivenKeyframe -currentDriver lFoot.rotateX {0}.pJCMLucas8FootUp_40_L;'.format(pyBlendShapesName))
    mel.eval('setAttr "lFoot.rotateX" 0;')

    # pJCMLucas8FootUp_40_R
    mel.eval('setAttr "rFoot.rotateX" 0;')
    mel.eval('setDrivenKeyframe -currentDriver rFoot.rotateX {0}.pJCMLucas8FootUp_40_R;'.format(pyBlendShapesName))
    mel.eval('setAttr "rFoot.rotateX" -40;')
    mel.eval('setAttr "{0}.pJCMLucas8FootUp_40_R" 1;'.format(pyBlendShapesName))
    mel.eval('setDrivenKeyframe -currentDriver rFoot.rotateX {0}.pJCMLucas8FootUp_40_R;'.format(pyBlendShapesName))
    mel.eval('setAttr "rFoot.rotateX" 0;')

    # pJCMLucas8ForeArmFwd_75_L
    mel.eval('setAttr "lForearmBend.rotateY" 0;')
    mel.eval('setDrivenKeyframe -currentDriver lForearmBend.rotateY {0}.pJCMLucas8ForeArmFwd_75_L;'.format(
        pyBlendShapesName))
    mel.eval('setAttr "lForearmBend.rotateY" -75;')
    mel.eval('setAttr "{0}.pJCMLucas8ForeArmFwd_75_L" 1;'.format(pyBlendShapesName))
    mel.eval('setDrivenKeyframe -currentDriver lForearmBend.rotateY {0}.pJCMLucas8ForeArmFwd_75_L;'.format(
        pyBlendShapesName))
    mel.eval('setAttr "lForearmBend.rotateY" 0;')

    # pJCMLucas8ForeArmFwd_75_R
    mel.eval('setAttr "rForearmBend.rotateY" 0;')
    mel.eval('setDrivenKeyframe -currentDriver rForearmBend.rotateY {0}.pJCMLucas8ForeArmFwd_75_R;'.format(
        pyBlendShapesName))
    mel.eval('setAttr "rForearmBend.rotateY" 75;')
    mel.eval('setAttr "{0}.pJCMLucas8ForeArmFwd_75_R" 1;'.format(pyBlendShapesName))
    mel.eval('setDrivenKeyframe -currentDriver rForearmBend.rotateY {0}.pJCMLucas8ForeArmFwd_75_R;'.format(
        pyBlendShapesName))
    mel.eval('setAttr "rForearmBend.rotateY" 0;')

    # pJCMLucas8HeadFwd_25
    mel.eval('setAttr "head.rotateX" 0;')
    mel.eval('setDrivenKeyframe -currentDriver head.rotateX {0}.pJCMLucas8HeadFwd_25;'.format(pyBlendShapesName))
    mel.eval('setAttr "head.rotateX" 25;')
    mel.eval('setAttr "{0}.pJCMLucas8HeadFwd_25" 1;'.format(pyBlendShapesName))
    mel.eval('setDrivenKeyframe -currentDriver head.rotateX {0}.pJCMLucas8HeadFwd_25;'.format(pyBlendShapesName))
    mel.eval('setAttr "head.rotateX" 0;')

    # pJCMLucas8ShinBend_155_L
    mel.eval('setAttr "lShin.rotateX" 0;')
    mel.eval(
        'setDrivenKeyframe -currentDriver lShin.rotateX {0}.pJCMLucas8ShinBend_155_L;'.format(pyBlendShapesName))
    mel.eval('setAttr "lShin.rotateX" 155;')
    mel.eval('setAttr "{0}.pJCMLucas8ShinBend_155_L" 1;'.format(pyBlendShapesName))
    mel.eval(
        'setDrivenKeyframe -currentDriver lShin.rotateX {0}.pJCMLucas8ShinBend_155_L;'.format(pyBlendShapesName))
    mel.eval('setAttr "lShin.rotateX" 0;')

    # pJCMLucas8ShinBend_155_R
    mel.eval('setAttr "rShin.rotateX" 0;')
    mel.eval(
        'setDrivenKeyframe -currentDriver rShin.rotateX {0}.pJCMLucas8ShinBend_155_R;'.format(pyBlendShapesName))
    mel.eval('setAttr "rShin.rotateX" 155;')
    mel.eval('setAttr "{0}.pJCMLucas8ShinBend_155_R" 1;'.format(pyBlendShapesName))
    mel.eval(
        'setDrivenKeyframe -currentDriver rShin.rotateX {0}.pJCMLucas8ShinBend_155_R;'.format(pyBlendShapesName))
    mel.eval('setAttr "rShin.rotateX" 0;')

    # pJCMLucas8ShinBend_90_L
    mel.eval('setAttr "lShin.rotateX" 0;')
    mel.eval(
        'setDrivenKeyframe -currentDriver lShin.rotateX {0}.pJCMLucas8ShinBend_90_L;'.format(pyBlendShapesName))
    mel.eval('setAttr "lShin.rotateX" 90;')
    mel.eval('setAttr "{0}.pJCMLucas8ShinBend_90_L" 1;'.format(pyBlendShapesName))
    mel.eval(
        'setDrivenKeyframe -currentDriver lShin.rotateX {0}.pJCMLucas8ShinBend_90_L;'.format(pyBlendShapesName))
    mel.eval('setAttr "lShin.rotateX" 0;')

    # pJCMLucas8ShinBend_90_R
    mel.eval('setAttr "rShin.rotateX" 0;')
    mel.eval(
        'setDrivenKeyframe -currentDriver rShin.rotateX {0}.pJCMLucas8ShinBend_90_R;'.format(pyBlendShapesName))
    mel.eval('setAttr "rShin.rotateX" 90;')
    mel.eval('setAttr "{0}.pJCMLucas8ShinBend_90_R" 1;'.format(pyBlendShapesName))
    mel.eval(
        'setDrivenKeyframe -currentDriver rShin.rotateX {0}.pJCMLucas8ShinBend_90_R;'.format(pyBlendShapesName))
    mel.eval('setAttr "rShin.rotateX" 0;')

    # pJCMLucas8ShldrUp_90_L
    mel.eval('setAttr "lShldrBend.rotateZ" 0;')
    mel.eval('setDrivenKeyframe -currentDriver lShldrBend.rotateZ {0}.pJCMLucas8ShldrUp_90_L;'.format(
        pyBlendShapesName))
    mel.eval('setAttr "lShldrBend.rotateZ" 90;')
    mel.eval('setAttr "{0}.pJCMLucas8ShldrUp_90_L" 1;'.format(pyBlendShapesName))
    mel.eval('setDrivenKeyframe -currentDriver lShldrBend.rotateZ {0}.pJCMLucas8ShldrUp_90_L;'.format(
        pyBlendShapesName))
    mel.eval('setAttr "lShldrBend.rotateZ" 0;')

    # pJCMLucas8ShldrUp_90_R
    mel.eval('setAttr "rShldrBend.rotateZ" 0;')
    mel.eval('setDrivenKeyframe -currentDriver rShldrBend.rotateZ {0}.pJCMLucas8ShldrUp_90_R;'.format(
        pyBlendShapesName))
    mel.eval('setAttr "rShldrBend.rotateZ" -90;')
    mel.eval('setAttr "{0}.pJCMLucas8ShldrUp_90_R" 1;'.format(pyBlendShapesName))
    mel.eval('setDrivenKeyframe -currentDriver rShldrBend.rotateZ {0}.pJCMLucas8ShldrUp_90_R;'.format(
        pyBlendShapesName))
    mel.eval('setAttr "rShldrBend.rotateZ" 0;')

    # pJCMLucas8ThighFwd_115_L
    mel.eval('setAttr "lThighBend.rotateX" 0;')
    mel.eval('setDrivenKeyframe -currentDriver lThighBend.rotateX {0}.pJCMLucas8ThighFwd_115_L;'.format(
        pyBlendShapesName))
    mel.eval('setAttr "lThighBend.rotateX" -115;')
    mel.eval('setAttr "{0}.pJCMLucas8ThighFwd_115_L" 1;'.format(pyBlendShapesName))
    mel.eval('setDrivenKeyframe -currentDriver lThighBend.rotateX {0}.pJCMLucas8ThighFwd_115_L;'.format(
        pyBlendShapesName))
    mel.eval('setAttr "lThighBend.rotateX" 0;')

    # pJCMLucas8ThighFwd_57_L
    mel.eval('setAttr "lThighBend.rotateX" 0;')
    mel.eval('setDrivenKeyframe -currentDriver lThighBend.rotateX {0}.pJCMLucas8ThighFwd_57_L;'.format(
        pyBlendShapesName))
    mel.eval('setAttr "lThighBend.rotateX" -57;')
    mel.eval('setAttr "{0}.pJCMLucas8ThighFwd_57_L" 1;'.format(pyBlendShapesName))
    mel.eval('setDrivenKeyframe -currentDriver lThighBend.rotateX {0}.pJCMLucas8ThighFwd_57_L;'.format(
        pyBlendShapesName))
    mel.eval('setAttr "lThighBend.rotateX" 0;')

    # pJCMLucas8ThighFwd_115_R
    mel.eval('setAttr "rThighBend.rotateX" 0;')
    mel.eval('setDrivenKeyframe -currentDriver rThighBend.rotateX {0}.pJCMLucas8ThighFwd_115_R;'.format(
        pyBlendShapesName))
    mel.eval('setAttr "rThighBend.rotateX" -115;')
    mel.eval('setAttr "{0}.pJCMLucas8ThighFwd_115_R" 1;'.format(pyBlendShapesName))
    mel.eval('setDrivenKeyframe -currentDriver rThighBend.rotateX {0}.pJCMLucas8ThighFwd_115_R;'.format(
        pyBlendShapesName))
    mel.eval('setAttr "rThighBend.rotateX" 0;')

    # pJCMLucas8ThighFwd_57_R
    mel.eval('setAttr "rThighBend.rotateX" 0;')
    mel.eval('setDrivenKeyframe -currentDriver rThighBend.rotateX {0}.pJCMLucas8ThighFwd_57_R;'.format(
        pyBlendShapesName))
    mel.eval('setAttr "rThighBend.rotateX" -57;')
    mel.eval('setAttr "{0}.pJCMLucas8ThighFwd_57_R" 1;'.format(pyBlendShapesName))
    mel.eval('setDrivenKeyframe -currentDriver rThighBend.rotateX {0}.pJCMLucas8ThighFwd_57_R;'.format(
        pyBlendShapesName))
    mel.eval('setAttr "rThighBend.rotateX" 0;')

    # pJCMLucas8ThighSide_85_L
    mel.eval('setAttr "lThighBend.rotateZ" 0;')
    mel.eval('setDrivenKeyframe -currentDriver lThighBend.rotateZ {0}.pJCMLucas8ThighSide_85_L;'.format(
        pyBlendShapesName))
    mel.eval('setAttr "lThighBend.rotateZ" 85;')
    mel.eval('setAttr "{0}.pJCMLucas8ThighSide_85_L" 1;'.format(pyBlendShapesName))
    mel.eval('setDrivenKeyframe -currentDriver lThighBend.rotateZ {0}.pJCMLucas8ThighSide_85_L;'.format(
        pyBlendShapesName))
    mel.eval('setAttr "lThighBend.rotateZ" 0;')

    # pJCMLucas8ThighSide_85_R
    mel.eval('setAttr "rThighBend.rotateZ" 0;')
    mel.eval('setDrivenKeyframe -currentDriver rThighBend.rotateZ {0}.pJCMLucas8ThighSide_85_R;'.format(
        pyBlendShapesName))
    mel.eval('setAttr "rThighBend.rotateZ" -85;')
    mel.eval('setAttr "{0}.pJCMLucas8ThighSide_85_R" 1;'.format(pyBlendShapesName))
    mel.eval('setDrivenKeyframe -currentDriver rThighBend.rotateZ {0}.pJCMLucas8ThighSide_85_R;'.format(
        pyBlendShapesName))
    mel.eval('setAttr "rThighBend.rotateZ" 0;')
