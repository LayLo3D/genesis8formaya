import maya.mel as mel
import maya.cmds as cmds


def stephanie8JCMs(character, blendShapeGroup):
    pyBlendShapesName = blendShapeGroup
    if not cmds.objExists('{}.pJCMStephanie8AbdomenLowerFwd'.format(pyBlendShapesName)):
        error = character + ' specific JCMs were not created.\n\n' + character + ' JCMs were either not exported out properly or the wrong file was selected for import.'
        cmds.warning(error.replace('\n\n', ' '))
        return error

    # pJCMStephanie8AbdomenLowerFwd
    mel.eval('setAttr "abdomenLower.rotateX" 0;')
    mel.eval('setDrivenKeyframe -currentDriver abdomenLower.rotateX {0}.pJCMStephanie8AbdomenLowerFwd;'.format(
        pyBlendShapesName))
    mel.eval('setAttr "abdomenLower.rotateX" 30;')
    mel.eval('setAttr "{0}.pJCMStephanie8AbdomenLowerFwd" 1;'.format(pyBlendShapesName))
    mel.eval('setDrivenKeyframe -currentDriver abdomenLower.rotateX {0}.pJCMStephanie8AbdomenLowerFwd;'.format(
        pyBlendShapesName))
    mel.eval('setAttr "abdomenLower.rotateX" 0;')

    # pJCMStephanie8AbdomenUpperFwd
    mel.eval('setAttr "abdomenUpper.rotateX" 0;')
    mel.eval('setDrivenKeyframe -currentDriver abdomenUpper.rotateX {0}.pJCMStephanie8AbdomenUpperFwd;'.format(
        pyBlendShapesName))
    mel.eval('setAttr "abdomenUpper.rotateX" 40;')
    mel.eval('setAttr "{0}.pJCMStephanie8AbdomenUpperFwd" 1;'.format(pyBlendShapesName))
    mel.eval('setDrivenKeyframe -currentDriver abdomenUpper.rotateX {0}.pJCMStephanie8AbdomenUpperFwd;'.format(
        pyBlendShapesName))
    mel.eval('setAttr "abdomenUpper.rotateX" 0;')

    # pJCMStephanie8ChestLowerFwd
    mel.eval('setAttr "chestLower.rotateX" 0;')
    mel.eval('setDrivenKeyframe -currentDriver chestLower.rotateX {0}.pJCMStephanie8ChestLowerFwd;'.format(pyBlendShapesName))
    mel.eval('setAttr "chestLower.rotateX" 35;')
    mel.eval('setAttr "{0}.pJCMStephanie8ChestLowerFwd" 1;'.format(pyBlendShapesName))
    mel.eval('setDrivenKeyframe -currentDriver chestLower.rotateX {0}.pJCMStephanie8ChestLowerFwd;'.format(pyBlendShapesName))
    mel.eval('setAttr "chestLower.rotateX" 0;')

    # pJCMStephanie8CollarBndUpL
    mel.eval('setAttr "lCollar.rotateZ" 0;')
    mel.eval('setDrivenKeyframe -currentDriver lCollar.rotateZ {0}.pJCMStephanie8CollarBndUpL;'.format(
        pyBlendShapesName))
    mel.eval('setAttr "lCollar.rotateZ" 55;')
    mel.eval('setAttr "{0}.pJCMStephanie8CollarBndUpL" 1;'.format(pyBlendShapesName))
    mel.eval('setDrivenKeyframe -currentDriver lCollar.rotateZ {0}.pJCMStephanie8CollarBndUpL;'.format(
        pyBlendShapesName))
    mel.eval('setAttr "lCollar.rotateZ" 0;')

    # pJCMStephanie8CollarBndUpR
    mel.eval('setAttr "rCollar.rotateZ" 0;')
    mel.eval('setDrivenKeyframe -currentDriver rCollar.rotateZ {0}.pJCMStephanie8CollarBndUpR;'.format(
        pyBlendShapesName))
    mel.eval('setAttr "rCollar.rotateZ" -55;')
    mel.eval('setAttr "{0}.pJCMStephanie8CollarBndUpR" 1;'.format(pyBlendShapesName))
    mel.eval('setDrivenKeyframe -currentDriver rCollar.rotateZ {0}.pJCMStephanie8CollarBndUpR;'.format(
        pyBlendShapesName))
    mel.eval('setAttr "rCollar.rotateZ" 0;')

    # pJCMStephanie8FootBndBckL
    mel.eval('setAttr "lFoot.rotateX" 0;')
    mel.eval('setDrivenKeyframe -currentDriver lFoot.rotateX {0}.pJCMStephanie8FootBndBckL;'.format(pyBlendShapesName))
    mel.eval('setAttr "lFoot.rotateX" 75;')
    mel.eval('setAttr "{0}.pJCMStephanie8FootBndBckL" 1;'.format(pyBlendShapesName))
    mel.eval('setDrivenKeyframe -currentDriver lFoot.rotateX {0}.pJCMStephanie8FootBndBckL;'.format(pyBlendShapesName))
    mel.eval('setAttr "lFoot.rotateX" 0;')

    # pJCMStephanie8FootBndBckR
    mel.eval('setAttr "rFoot.rotateX" 0;')
    mel.eval('setDrivenKeyframe -currentDriver rFoot.rotateX {0}.pJCMStephanie8FootBndBckR;'.format(pyBlendShapesName))
    mel.eval('setAttr "rFoot.rotateX" 75;')
    mel.eval('setAttr "{0}.pJCMStephanie8FootBndBckR" 1;'.format(pyBlendShapesName))
    mel.eval('setDrivenKeyframe -currentDriver rFoot.rotateX {0}.pJCMStephanie8FootBndBckR;'.format(pyBlendShapesName))
    mel.eval('setAttr "rFoot.rotateX" 0;')

    # pJCMStephanie8FootBndUpL
    mel.eval('setAttr "lFoot.rotateX" 0;')
    mel.eval('setDrivenKeyframe -currentDriver lFoot.rotateX {0}.pJCMStephanie8FootBndUpL;'.format(pyBlendShapesName))
    mel.eval('setAttr "lFoot.rotateX" -45;')
    mel.eval('setAttr "{0}.pJCMStephanie8FootBndUpL" 1;'.format(pyBlendShapesName))
    mel.eval('setDrivenKeyframe -currentDriver lFoot.rotateX {0}.pJCMStephanie8FootBndUpL;'.format(pyBlendShapesName))
    mel.eval('setAttr "lFoot.rotateX" 0;')

    # pJCMStephanie8FootBndUpR
    mel.eval('setAttr "rFoot.rotateX" 0;')
    mel.eval('setDrivenKeyframe -currentDriver rFoot.rotateX {0}.pJCMStephanie8FootBndUpR;'.format(pyBlendShapesName))
    mel.eval('setAttr "rFoot.rotateX" -45;')
    mel.eval('setAttr "{0}.pJCMStephanie8FootBndUpR" 1;'.format(pyBlendShapesName))
    mel.eval('setDrivenKeyframe -currentDriver rFoot.rotateX {0}.pJCMStephanie8FootBndUpR;'.format(pyBlendShapesName))
    mel.eval('setAttr "rFoot.rotateX" 0;')

    # pJCMStephanie8ForearmBndL_HDLv1
    mel.eval('setAttr "lForearmBend.rotateY" 0;')
    mel.eval('setDrivenKeyframe -currentDriver lForearmBend.rotateY {0}.pJCMStephanie8ForearmBndL_HDLv1;'.format(
        pyBlendShapesName))
    mel.eval('setAttr "lForearmBend.rotateY" -135;')
    mel.eval('setAttr "{0}.pJCMStephanie8ForearmBndL_HDLv1" 1;'.format(pyBlendShapesName))
    mel.eval('setDrivenKeyframe -currentDriver lForearmBend.rotateY {0}.pJCMStephanie8ForearmBndL_HDLv1;'.format(
        pyBlendShapesName))
    mel.eval('setAttr "lForearmBend.rotateY" 0;')

    # pJCMStephanie8ForearmBndR_HDLv1
    mel.eval('setAttr "rForearmBend.rotateY" 0;')
    mel.eval('setDrivenKeyframe -currentDriver rForearmBend.rotateY {0}.pJCMStephanie8ForearmBndR_HDLv1;'.format(
        pyBlendShapesName))
    mel.eval('setAttr "rForearmBend.rotateY" 135;')
    mel.eval('setAttr "{0}.pJCMStephanie8ForearmBndR_HDLv1" 1;'.format(pyBlendShapesName))
    mel.eval('setDrivenKeyframe -currentDriver rForearmBend.rotateY {0}.pJCMStephanie8ForearmBndR_HDLv1;'.format(
        pyBlendShapesName))
    mel.eval('setAttr "rForearmBend.rotateY" 0;')

    # pJCMStephanie8HeadTwistL_HDLv2
    mel.eval('setAttr "head.rotateY" 0;')
    mel.eval('setDrivenKeyframe -currentDriver head.rotateY {0}.pJCMStephanie8HeadTwistL_HDLv2;'.format(pyBlendShapesName))
    mel.eval('setAttr "head.rotateY" 22;')
    mel.eval('setAttr "{0}.pJCMStephanie8HeadTwistL_HDLv2" 1;'.format(pyBlendShapesName))
    mel.eval('setDrivenKeyframe -currentDriver head.rotateY {0}.pJCMStephanie8HeadTwistL_HDLv2;'.format(pyBlendShapesName))
    mel.eval('setAttr "head.rotateY" 0;')

    # pJCMStephanie8HeadTwistR_HDLv2
    mel.eval('setAttr "head.rotateY" 0;')
    mel.eval('setDrivenKeyframe -currentDriver head.rotateY {0}.pJCMStephanie8HeadTwistR_HDLv2;'.format(pyBlendShapesName))
    mel.eval('setAttr "head.rotateY" -22;')
    mel.eval('setAttr "{0}.pJCMStephanie8HeadTwistR_HDLv2" 1;'.format(pyBlendShapesName))
    mel.eval('setDrivenKeyframe -currentDriver head.rotateY {0}.pJCMStephanie8HeadTwistR_HDLv2;'.format(pyBlendShapesName))
    mel.eval('setAttr "head.rotateY" 0;')
    
    # pJCMStephanie8ShinBndBckL
    mel.eval('setAttr "lShin.rotateX" 0;')
    mel.eval(
        'setDrivenKeyframe -currentDriver lShin.rotateX {0}.pJCMStephanie8ShinBndBckL;'.format(pyBlendShapesName))
    mel.eval('setAttr "lShin.rotateX" 155;')
    mel.eval('setAttr "{0}.pJCMStephanie8ShinBndBckL" 1;'.format(pyBlendShapesName))
    mel.eval(
        'setDrivenKeyframe -currentDriver lShin.rotateX {0}.pJCMStephanie8ShinBndBckL;'.format(pyBlendShapesName))
    mel.eval('setAttr "lShin.rotateX" 0;')

    # pJCMStephanie8ShinBndBckR
    mel.eval('setAttr "rShin.rotateX" 0;')
    mel.eval(
        'setDrivenKeyframe -currentDriver rShin.rotateX {0}.pJCMStephanie8ShinBndBckR;'.format(pyBlendShapesName))
    mel.eval('setAttr "rShin.rotateX" 155;')
    mel.eval('setAttr "{0}.pJCMStephanie8ShinBndBckR" 1;'.format(pyBlendShapesName))
    mel.eval(
        'setDrivenKeyframe -currentDriver rShin.rotateX {0}.pJCMStephanie8ShinBndBckR;'.format(pyBlendShapesName))
    mel.eval('setAttr "rShin.rotateX" 0;')

    # pJCMStephanie8ShoulderBndUpL
    mel.eval('setAttr "lShldrBend.rotateZ" 0;')
    mel.eval('setDrivenKeyframe -currentDriver lShldrBend.rotateZ {0}.pJCMStephanie8ShoulderBndUpL;'.format(
        pyBlendShapesName))
    mel.eval('setAttr "lShldrBend.rotateZ" 90;')
    mel.eval('setAttr "{0}.pJCMStephanie8ShoulderBndUpL" 1;'.format(pyBlendShapesName))
    mel.eval('setDrivenKeyframe -currentDriver lShldrBend.rotateZ {0}.pJCMStephanie8ShoulderBndUpL;'.format(
        pyBlendShapesName))
    mel.eval('setAttr "lShldrBend.rotateZ" 0;')

    # pJCMStephanie8ShoulderBndUpR
    mel.eval('setAttr "rShldrBend.rotateZ" 0;')
    mel.eval('setDrivenKeyframe -currentDriver rShldrBend.rotateZ {0}.pJCMStephanie8ShoulderBndUpR;'.format(
        pyBlendShapesName))
    mel.eval('setAttr "rShldrBend.rotateZ" -90;')
    mel.eval('setAttr "{0}.pJCMStephanie8ShoulderBndUpR" 1;'.format(pyBlendShapesName))
    mel.eval('setDrivenKeyframe -currentDriver rShldrBend.rotateZ {0}.pJCMStephanie8ShoulderBndUpR;'.format(
        pyBlendShapesName))
    mel.eval('setAttr "rShldrBend.rotateZ" 0;')

    # pJCMStephanie8ShoulderBndFwdL
    mel.eval('setAttr "lShldrBend.rotateY" 0;')
    mel.eval('setDrivenKeyframe -currentDriver lShldrBend.rotateY {0}.pJCMStephanie8ShoulderBndFwdL;'.format(
        pyBlendShapesName))
    mel.eval('setAttr "lShldrBend.rotateY" -110;')
    mel.eval('setAttr "{0}.pJCMStephanie8ShoulderBndFwdL" 1;'.format(pyBlendShapesName))
    mel.eval('setDrivenKeyframe -currentDriver lShldrBend.rotateY {0}.pJCMStephanie8ShoulderBndFwdL;'.format(
        pyBlendShapesName))
    mel.eval('setAttr "lShldrBend.rotateY" 0;')

    # pJCMStephanie8ShoulderBndFwdR
    mel.eval('setAttr "rShldrBend.rotateY" 0;')
    mel.eval('setDrivenKeyframe -currentDriver rShldrBend.rotateY {0}.pJCMStephanie8ShoulderBndFwdR;'.format(
        pyBlendShapesName))
    mel.eval('setAttr "rShldrBend.rotateY" 110;')
    mel.eval('setAttr "{0}.pJCMStephanie8ShoulderBndFwdR" 1;'.format(pyBlendShapesName))
    mel.eval('setDrivenKeyframe -currentDriver rShldrBend.rotateY {0}.pJCMStephanie8ShoulderBndFwdR;'.format(
        pyBlendShapesName))
    mel.eval('setAttr "rShldrBend.rotateY" 0;')

    # pJCMStephanie8ThighSideSideL
    mel.eval('setAttr "lThighBend.rotateZ" 0;')
    mel.eval('setDrivenKeyframe -currentDriver lThighBend.rotateZ {0}.pJCMStephanie8ThighSideSideL;'.format(
        pyBlendShapesName))
    mel.eval('setAttr "lThighBend.rotateZ" 85;')
    mel.eval('setAttr "{0}.pJCMStephanie8ThighSideSideL" 1;'.format(pyBlendShapesName))
    mel.eval('setDrivenKeyframe -currentDriver lThighBend.rotateZ {0}.pJCMStephanie8ThighSideSideL;'.format(
        pyBlendShapesName))
    mel.eval('setAttr "lThighBend.rotateZ" 0;')

    # pJCMStephanie8ThighSideSideR
    mel.eval('setAttr "rThighBend.rotateZ" 0;')
    mel.eval('setDrivenKeyframe -currentDriver rThighBend.rotateZ {0}.pJCMStephanie8ThighSideSideR;'.format(
        pyBlendShapesName))
    mel.eval('setAttr "rThighBend.rotateZ" -85;')
    mel.eval('setAttr "{0}.pJCMStephanie8ThighSideSideR" 1;'.format(pyBlendShapesName))
    mel.eval('setDrivenKeyframe -currentDriver rThighBend.rotateZ {0}.pJCMStephanie8ThighSideSideR;'.format(
        pyBlendShapesName))
    mel.eval('setAttr "rThighBend.rotateZ" 0;')
