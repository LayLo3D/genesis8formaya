import maya.mel as mel
import maya.cmds as cmds

def ult_natural_bend_g8f_jcms(blend_shape_group):
    cmds.setDrivenKeyframe(blend_shape_group, currentDriver='lCollar.rotateY', driverValue=0,
                           attribute='pJCMkhColrBack17_L', value=0)
    cmds.setDrivenKeyframe(blend_shape_group, currentDriver='lCollar.rotateY', driverValue=15,
                           attribute='pJCMkhColrBack17_L', value=1)
    cmds.setDrivenKeyframe(blend_shape_group, currentDriver='lThighTwist.rotateY', driverValue=0,
                           attribute='pJCMkhThiBnd_85Twst_75_L', value=0)
    cmds.setDrivenKeyframe(blend_shape_group, currentDriver='lThighTwist.rotateY', driverValue=0,
                           attribute='pJCMkhThiBnd_85Twst_75_L', value=0)
    cmds.setDrivenKeyframe(blend_shape_group, inTangentType='linear', outTangentType='linear',
                           currentDriver='rThighBend.rotateX', driverValue=0, attribute='pJCMkhThiBnd35_R', value=0)
    cmds.setDrivenKeyframe(blend_shape_group, inTangentType='linear', outTangentType='linear',
                           currentDriver='rThighBend.rotateX', driverValue=35.0, attribute='pJCMkhThiBnd35_R', value=1)
    cmds.setDrivenKeyframe(blend_shape_group, currentDriver='rThighBend.rotateX', driverValue=0,
                           attribute='pJCMkhThighBnd_85_R', value=0)
    cmds.setDrivenKeyframe(blend_shape_group, currentDriver='rThighBend.rotateX', driverValue=0,
                           attribute='pJCMkhThighBnd_85_R', value=0)
    cmds.setDrivenKeyframe(blend_shape_group, currentDriver='lThighTwist.rotateY', driverValue=0,
                           attribute='pJCMkhThiBnd_85Side40Twst45_L', value=0)
    cmds.setDrivenKeyframe(blend_shape_group, currentDriver='lThighTwist.rotateY', driverValue=45,
                           attribute='pJCMkhThiBnd_85Side40Twst45_L', value=1)
    cmds.setDrivenKeyframe(blend_shape_group, currentDriver='rThighTwist.rotateY', driverValue=0,
                           attribute='pJCMkhThiTwst55_R', value=0)
    cmds.setDrivenKeyframe(blend_shape_group, currentDriver='rThighTwist.rotateY', driverValue=55,
                           attribute='pJCMkhThiTwst55_R', value=1)
    cmds.setDrivenKeyframe(blend_shape_group, currentDriver='lThighTwist.rotateY', driverValue=0,
                           attribute='pJCMkhThiBnd_85Twst45_L', value=0)
    cmds.setDrivenKeyframe(blend_shape_group, currentDriver='lThighTwist.rotateY', driverValue=45,
                           attribute='pJCMkhThiBnd_85Twst45_L', value=1)
    cmds.setDrivenKeyframe(blend_shape_group, inTangentType='linear', outTangentType='linear',
                           currentDriver='rCollar.rotateZ', driverValue=0, attribute='pJCMkhColrBnd_45_R', value=0)
    cmds.setDrivenKeyframe(blend_shape_group, inTangentType='linear', outTangentType='linear',
                           currentDriver='rCollar.rotateZ', driverValue=-45.0, attribute='pJCMkhColrBnd_45_R', value=1)
    cmds.setDrivenKeyframe(blend_shape_group, currentDriver='lCollar.rotateX', driverValue=0,
                           attribute='pJCMkhColrTwst30_L', value=0)
    cmds.setDrivenKeyframe(blend_shape_group, currentDriver='lCollar.rotateX', driverValue=30,
                           attribute='pJCMkhColrTwst30_L', value=1)
    cmds.setDrivenKeyframe(blend_shape_group, currentDriver='rThighBend.rotateZ', driverValue=0,
                           attribute='pJCMkhThiBnd_115Side_40_R', value=0)
    cmds.setDrivenKeyframe(blend_shape_group, currentDriver='rThighBend.rotateZ', driverValue=0,
                           attribute='pJCMkhThiBnd_115Side_40_R', value=0)
    cmds.setDrivenKeyframe(blend_shape_group, currentDriver='rCollar.rotateY', driverValue=15,
                           attribute='pJCMkhColrFwd26_R', value=0)
    cmds.setDrivenKeyframe(blend_shape_group, currentDriver='rCollar.rotateY', driverValue=26,
                           attribute='pJCMkhColrFwd26_R', value=1)
    cmds.setDrivenKeyframe(blend_shape_group, currentDriver='lCollar.rotateZ', driverValue=0,
                           attribute='pJCMkhColrFwd_26Bnd45_L', value=0)
    cmds.setDrivenKeyframe(blend_shape_group, currentDriver='lCollar.rotateZ', driverValue=45,
                           attribute='pJCMkhColrFwd_26Bnd45_L', value=1)
    cmds.setDrivenKeyframe(blend_shape_group, currentDriver='lThighBend.rotateX', driverValue=0,
                           attribute='pJCMkhThighBnd_85_L', value=0)
    cmds.setDrivenKeyframe(blend_shape_group, currentDriver='lThighBend.rotateX', driverValue=0,
                           attribute='pJCMkhThighBnd_85_L', value=0)
    cmds.setDrivenKeyframe(blend_shape_group, inTangentType='linear', outTangentType='linear',
                           currentDriver='lCollar.rotateZ', driverValue=0, attribute='pJCMkhColrBnd45_L', value=0)
    cmds.setDrivenKeyframe(blend_shape_group, inTangentType='linear', outTangentType='linear',
                           currentDriver='lCollar.rotateZ', driverValue=45.0, attribute='pJCMkhColrBnd45_L', value=1)
    cmds.setDrivenKeyframe(blend_shape_group, inTangentType='linear', outTangentType='linear',
                           currentDriver='lThighBend.rotateX', driverValue=0, attribute='pJCMkhThiBnd35_L', value=0)
    cmds.setDrivenKeyframe(blend_shape_group, inTangentType='linear', outTangentType='linear',
                           currentDriver='lThighBend.rotateX', driverValue=35.0, attribute='pJCMkhThiBnd35_L', value=1)
    cmds.setDrivenKeyframe(blend_shape_group, inTangentType='linear', outTangentType='linear',
                           currentDriver='rForearmTwist.rotateX', driverValue=0, attribute='pJCMkhFarmBnd135Twst80_R',
                           value=0)
    cmds.setDrivenKeyframe(blend_shape_group, inTangentType='linear', outTangentType='linear',
                           currentDriver='rForearmTwist.rotateX', driverValue=80.0,
                           attribute='pJCMkhFarmBnd135Twst80_R', value=1)
    cmds.setDrivenKeyframe(blend_shape_group, currentDriver='rCollar.rotateX', driverValue=0,
                           attribute='pJCMkhColrFwd26Twst30_R', value=0)
    cmds.setDrivenKeyframe(blend_shape_group, currentDriver='rCollar.rotateX', driverValue=30,
                           attribute='pJCMkhColrFwd26Twst30_R', value=1)
    cmds.setDrivenKeyframe(blend_shape_group, currentDriver='rThighTwist.rotateY', driverValue=0,
                           attribute='pJCMkhThiBnd_115Side_40Twst45_R', value=0)
    cmds.setDrivenKeyframe(blend_shape_group, currentDriver='rThighTwist.rotateY', driverValue=45,
                           attribute='pJCMkhThiBnd_115Side_40Twst45_R', value=1)
    cmds.setDrivenKeyframe(blend_shape_group, currentDriver='rShin.rotateX', driverValue=90,
                           attribute='pJCMkhShinBnd135_R', value=0)
    cmds.setDrivenKeyframe(blend_shape_group, currentDriver='rShin.rotateX', driverValue=135,
                           attribute='pJCMkhShinBnd135_R', value=1)
    cmds.setDrivenKeyframe(blend_shape_group, currentDriver='rThighBend.rotateZ', driverValue=0,
                           attribute='pJCMkhThiSide_R', value=0)
    cmds.setDrivenKeyframe(blend_shape_group, currentDriver='rThighBend.rotateZ', driverValue=0,
                           attribute='pJCMkhThiSide_R', value=0)
    cmds.setDrivenKeyframe(blend_shape_group, currentDriver='rThighTwist.rotateY', driverValue=0,
                           attribute='pJCMkhThiBnd_85Twst75_R', value=0)
    cmds.setDrivenKeyframe(blend_shape_group, currentDriver='rThighTwist.rotateY', driverValue=75,
                           attribute='pJCMkhThiBnd_85Twst75_R', value=1)
    cmds.setDrivenKeyframe(blend_shape_group, currentDriver='rThighTwist.rotateY', driverValue=0,
                           attribute='pJCMkhThiTwst_45_R', value=0)
    cmds.setDrivenKeyframe(blend_shape_group, currentDriver='rThighTwist.rotateY', driverValue=0,
                           attribute='pJCMkhThiTwst_45_R', value=0)
    cmds.setDrivenKeyframe(blend_shape_group, currentDriver='lCollar.rotateY', driverValue=-15,
                           attribute='pJCMkhColrFwd_26_L', value=0)
    cmds.setDrivenKeyframe(blend_shape_group, currentDriver='lCollar.rotateY', driverValue=-15,
                           attribute='pJCMkhColrFwd_26_L', value=0)
    cmds.setDrivenKeyframe(blend_shape_group, currentDriver='rThighTwist.rotateY', driverValue=-45,
                           attribute='pJCMkhThiBnd_85Side_40Twst_75_R', value=0)
    cmds.setDrivenKeyframe(blend_shape_group, currentDriver='rThighTwist.rotateY', driverValue=-45,
                           attribute='pJCMkhThiBnd_85Side_40Twst_75_R', value=0)
    cmds.setDrivenKeyframe(blend_shape_group, inTangentType='linear', outTangentType='linear',
                           currentDriver='lCollar.rotateY', driverValue=0, attribute='pJCMkhColrFwd_15_L', value=0)
    cmds.setDrivenKeyframe(blend_shape_group, inTangentType='linear', outTangentType='linear',
                           currentDriver='lCollar.rotateY', driverValue=-15.0, attribute='pJCMkhColrFwd_15_L', value=1)
    cmds.setDrivenKeyframe(blend_shape_group, currentDriver='lThighTwist.rotateY', driverValue=45,
                           attribute='pJCMkhThiBnd_85Side40Twst75_L', value=0)
    cmds.setDrivenKeyframe(blend_shape_group, currentDriver='lThighTwist.rotateY', driverValue=75,
                           attribute='pJCMkhThiBnd_85Side40Twst75_L', value=1)
    cmds.setDrivenKeyframe(blend_shape_group, currentDriver='lThighBend.rotateZ', driverValue=0,
                           attribute='pJCMkhThighSide_L', value=0)
    cmds.setDrivenKeyframe(blend_shape_group, currentDriver='lThighBend.rotateZ', driverValue=65,
                           attribute='pJCMkhThighSide_L', value=1)
    cmds.setDrivenKeyframe(blend_shape_group, currentDriver='lThighBend.rotateZ', driverValue=40,
                           attribute='pJCMkhThiBnd_115Side60_L', value=0)
    cmds.setDrivenKeyframe(blend_shape_group, currentDriver='lThighBend.rotateZ', driverValue=60,
                           attribute='pJCMkhThiBnd_115Side60_L', value=1)
    cmds.setDrivenKeyframe(blend_shape_group, currentDriver='lShin.rotateX', driverValue=90,
                           attribute='pJCMkhShinBnd135_L', value=0)
    cmds.setDrivenKeyframe(blend_shape_group, currentDriver='lShin.rotateX', driverValue=135,
                           attribute='pJCMkhShinBnd135_L', value=1)
    cmds.setDrivenKeyframe(blend_shape_group, currentDriver='rThighBend.rotateZ', driverValue=-40,
                           attribute='pJCMkhThiBnd_85Side_60_R', value=0)
    cmds.setDrivenKeyframe(blend_shape_group, currentDriver='rThighBend.rotateZ', driverValue=-40,
                           attribute='pJCMkhThiBnd_85Side_60_R', value=0)
    cmds.setDrivenKeyframe(blend_shape_group, currentDriver='rShin.rotateX', driverValue=0,
                           attribute='pJCMkhShinBnd35_R', value=0)
    cmds.setDrivenKeyframe(blend_shape_group, currentDriver='rShin.rotateX', driverValue=35,
                           attribute='pJCMkhShinBnd35_R', value=1)
    cmds.setDrivenKeyframe(blend_shape_group, currentDriver='rCollar.rotateZ', driverValue=0,
                           attribute='pJCMkhColrFwd26Bnd_45_R', value=0)
    cmds.setDrivenKeyframe(blend_shape_group, currentDriver='rCollar.rotateZ', driverValue=0,
                           attribute='pJCMkhColrFwd26Bnd_45_R', value=0)
    cmds.setDrivenKeyframe(blend_shape_group, currentDriver='lThighBend.rotateZ', driverValue=0,
                           attribute='pJCMkhThiBnd_85Side40_L', value=0)
    cmds.setDrivenKeyframe(blend_shape_group, currentDriver='lThighBend.rotateZ', driverValue=40,
                           attribute='pJCMkhThiBnd_85Side40_L', value=1)
    cmds.setDrivenKeyframe(blend_shape_group, currentDriver='lCollar.rotateZ', driverValue=0,
                           attribute='pJCMkhColrBack17Bnd45_L', value=0)
    cmds.setDrivenKeyframe(blend_shape_group, currentDriver='lCollar.rotateZ', driverValue=45,
                           attribute='pJCMkhColrBack17Bnd45_L', value=1)
    cmds.setDrivenKeyframe(blend_shape_group, currentDriver='lThighTwist.rotateY', driverValue=0,
                           attribute='pJCMkhThiTwst45_L', value=0)
    cmds.setDrivenKeyframe(blend_shape_group, currentDriver='lThighTwist.rotateY', driverValue=45,
                           attribute='pJCMkhThiTwst45_L', value=1)
    cmds.setDrivenKeyframe(blend_shape_group, currentDriver='lShin.rotateX', driverValue=35,
                           attribute='pJCMkhShinBnd90_L', value=0)
    cmds.setDrivenKeyframe(blend_shape_group, currentDriver='lShin.rotateX', driverValue=90,
                           attribute='pJCMkhShinBnd90_L', value=1)
    cmds.setDrivenKeyframe(blend_shape_group, currentDriver='lThighTwist.rotateY', driverValue=45,
                           attribute='pJCMkhThiBnd_115Side40Twst75_L', value=0)
    cmds.setDrivenKeyframe(blend_shape_group, currentDriver='lThighTwist.rotateY', driverValue=75,
                           attribute='pJCMkhThiBnd_115Side40Twst75_L', value=1)
    cmds.setDrivenKeyframe(blend_shape_group, currentDriver='lShin.rotateX', driverValue=135,
                           attribute='pJCMkhShinBnd155_L', value=0)
    cmds.setDrivenKeyframe(blend_shape_group, currentDriver='lShin.rotateX', driverValue=155,
                           attribute='pJCMkhShinBnd155_L', value=1)
    cmds.setDrivenKeyframe(blend_shape_group, currentDriver='rThighBend.rotateZ', driverValue=0,
                           attribute='pJCMkhThiBnd_85Side_40_R', value=0)
    cmds.setDrivenKeyframe(blend_shape_group, currentDriver='rThighBend.rotateZ', driverValue=0,
                           attribute='pJCMkhThiBnd_85Side_40_R', value=0)
    cmds.setDrivenKeyframe(blend_shape_group, currentDriver='rShin.rotateX', driverValue=135,
                           attribute='pJCMkhShinBnd155_R', value=0)
    cmds.setDrivenKeyframe(blend_shape_group, currentDriver='rShin.rotateX', driverValue=155,
                           attribute='pJCMkhShinBnd155_R', value=1)
    cmds.setDrivenKeyframe(blend_shape_group, currentDriver='lThighBend.rotateX', driverValue=-85,
                           attribute='pJCMkhThiBnd_115_L', value=0)
    cmds.setDrivenKeyframe(blend_shape_group, currentDriver='lThighBend.rotateX', driverValue=-85,
                           attribute='pJCMkhThiBnd_115_L', value=0)
    cmds.setDrivenKeyframe(blend_shape_group, currentDriver='rThighTwist.rotateY', driverValue=0,
                           attribute='pJCMkhThiBnd_85Side_40Twst_45_R', value=0)
    cmds.setDrivenKeyframe(blend_shape_group, currentDriver='rThighTwist.rotateY', driverValue=0,
                           attribute='pJCMkhThiBnd_85Side_40Twst_45_R', value=0)
    cmds.setDrivenKeyframe(blend_shape_group, inTangentType='linear', outTangentType='linear',
                           currentDriver='lForearmTwist.rotateX', driverValue=0, attribute='pJCMkhFarmBnd_135Twst80_L',
                           value=0)
    cmds.setDrivenKeyframe(blend_shape_group, inTangentType='linear', outTangentType='linear',
                           currentDriver='lForearmTwist.rotateX', driverValue=80.0,
                           attribute='pJCMkhFarmBnd_135Twst80_L', value=1)
    cmds.setDrivenKeyframe(blend_shape_group, currentDriver='lThighBend.rotateZ', driverValue=40,
                           attribute='pJCMkhThiBnd_85Side60_L', value=0)
    cmds.setDrivenKeyframe(blend_shape_group, currentDriver='lThighBend.rotateZ', driverValue=60,
                           attribute='pJCMkhThiBnd_85Side60_L', value=1)
    cmds.setDrivenKeyframe(blend_shape_group, currentDriver='rThighTwist.rotateY', driverValue=0,
                           attribute='pJCMkhThiBnd_85Side_40Twst45_R', value=0)
    cmds.setDrivenKeyframe(blend_shape_group, currentDriver='rThighTwist.rotateY', driverValue=45,
                           attribute='pJCMkhThiBnd_85Side_40Twst45_R', value=1)
    cmds.setDrivenKeyframe(blend_shape_group, currentDriver='lCollar.rotateX', driverValue=0,
                           attribute='pJCMkhColrBnd45Twst_30_L', value=0)
    cmds.setDrivenKeyframe(blend_shape_group, currentDriver='lCollar.rotateX', driverValue=0,
                           attribute='pJCMkhColrBnd45Twst_30_L', value=0)
    cmds.setDrivenKeyframe(blend_shape_group, currentDriver='lShin.rotateX', driverValue=0,
                           attribute='pJCMkhShinBnd35_L', value=0)
    cmds.setDrivenKeyframe(blend_shape_group, currentDriver='lShin.rotateX', driverValue=35,
                           attribute='pJCMkhShinBnd35_L', value=1)
    cmds.setDrivenKeyframe(blend_shape_group, currentDriver='rCollar.rotateY', driverValue=0,
                           attribute='pJCMkhColrBack_17_R', value=0)
    cmds.setDrivenKeyframe(blend_shape_group, currentDriver='rCollar.rotateY', driverValue=0,
                           attribute='pJCMkhColrBack_17_R', value=0)
    cmds.setDrivenKeyframe(blend_shape_group, currentDriver='lCollar.rotateX', driverValue=0,
                           attribute='pJCMkhColrFwd_26Twst30_L', value=0)
    cmds.setDrivenKeyframe(blend_shape_group, currentDriver='lCollar.rotateX', driverValue=30,
                           attribute='pJCMkhColrFwd_26Twst30_L', value=1)
    cmds.setDrivenKeyframe(blend_shape_group, inTangentType='linear', outTangentType='linear',
                           currentDriver='rForearmTwist.rotateX', driverValue=0, attribute='pJCMkhFarmBndTwst_90_R',
                           value=0)
    cmds.setDrivenKeyframe(blend_shape_group, inTangentType='linear', outTangentType='linear',
                           currentDriver='rForearmTwist.rotateX', driverValue=-90.0, attribute='pJCMkhFarmBndTwst_90_R',
                           value=1)
    cmds.setDrivenKeyframe(blend_shape_group, currentDriver='lForearmBend.rotateY', driverValue=0,
                           attribute='pJCMkhElbwBnd_L', value=0)
    cmds.setDrivenKeyframe(blend_shape_group, currentDriver='lForearmBend.rotateY', driverValue=0,
                           attribute='pJCMkhElbwBnd_L', value=0)
    cmds.setDrivenKeyframe(blend_shape_group, inTangentType='linear', outTangentType='linear',
                           currentDriver='rCollar.rotateY', driverValue=0, attribute='pJCMkhColrFwd15_R', value=0)
    cmds.setDrivenKeyframe(blend_shape_group, inTangentType='linear', outTangentType='linear',
                           currentDriver='rCollar.rotateY', driverValue=15.0, attribute='pJCMkhColrFwd15_R', value=1)
    cmds.setDrivenKeyframe(blend_shape_group, currentDriver='rCollar.rotateX', driverValue=0,
                           attribute='pJCMkhColrTwst30_R', value=0)
    cmds.setDrivenKeyframe(blend_shape_group, currentDriver='rCollar.rotateX', driverValue=30,
                           attribute='pJCMkhColrTwst30_R', value=1)
    cmds.setDrivenKeyframe(blend_shape_group, currentDriver='rCollar.rotateZ', driverValue=0,
                           attribute='pJCMkhColrBack_17Bnd_45_R', value=0)
    cmds.setDrivenKeyframe(blend_shape_group, currentDriver='rCollar.rotateZ', driverValue=0,
                           attribute='pJCMkhColrBack_17Bnd_45_R', value=0)
    cmds.setDrivenKeyframe(blend_shape_group, currentDriver='lThighTwist.rotateY', driverValue=0,
                           attribute='pJCMkhThiTwst_55_L', value=0)
    cmds.setDrivenKeyframe(blend_shape_group, currentDriver='lThighTwist.rotateY', driverValue=0,
                           attribute='pJCMkhThiTwst_55_L', value=0)
    cmds.setDrivenKeyframe(blend_shape_group, currentDriver='lThighTwist.rotateY', driverValue=0,
                           attribute='pJCMkhThiBnd_85Side40Twst_45_L', value=0)
    cmds.setDrivenKeyframe(blend_shape_group, currentDriver='lThighTwist.rotateY', driverValue=0,
                           attribute='pJCMkhThiBnd_85Side40Twst_45_L', value=0)
    cmds.setDrivenKeyframe(blend_shape_group, currentDriver='lThighBend.rotateZ', driverValue=0,
                           attribute='pJCMkhThiBnd_115Side40_L', value=0)
    cmds.setDrivenKeyframe(blend_shape_group, currentDriver='lThighBend.rotateZ', driverValue=40,
                           attribute='pJCMkhThiBnd_115Side40_L', value=1)
    cmds.setDrivenKeyframe(blend_shape_group, currentDriver='rShin.rotateX', driverValue=0,
                           attribute='pJCMkhShinBnd_11_R', value=0)
    cmds.setDrivenKeyframe(blend_shape_group, currentDriver='rShin.rotateX', driverValue=0,
                           attribute='pJCMkhShinBnd_11_R', value=0)
    cmds.setDrivenKeyframe(blend_shape_group, currentDriver='rThighTwist.rotateY', driverValue=-45,
                           attribute='pJCMkhThiBnd_115Side_40Twst_75_R', value=0)
    cmds.setDrivenKeyframe(blend_shape_group, currentDriver='rThighTwist.rotateY', driverValue=-45,
                           attribute='pJCMkhThiBnd_115Side_40Twst_75_R', value=0)
    cmds.setDrivenKeyframe(blend_shape_group, currentDriver='lThighTwist.rotateY', driverValue=0,
                           attribute='pJCMkhThiBnd_115Side40Twst45_L', value=0)
    cmds.setDrivenKeyframe(blend_shape_group, currentDriver='lThighTwist.rotateY', driverValue=45,
                           attribute='pJCMkhThiBnd_115Side40Twst45_L', value=1)
    cmds.setDrivenKeyframe(blend_shape_group, currentDriver='lShin.rotateX', driverValue=0,
                           attribute='pJCMkhShinBnd_11_L', value=0)
    cmds.setDrivenKeyframe(blend_shape_group, currentDriver='lShin.rotateX', driverValue=0,
                           attribute='pJCMkhShinBnd_11_L', value=0)
    cmds.setDrivenKeyframe(blend_shape_group, inTangentType='linear', outTangentType='linear',
                           currentDriver='lForearmTwist.rotateX', driverValue=0, attribute='pJCMkhFarmBndTwst90_L',
                           value=0)
    cmds.setDrivenKeyframe(blend_shape_group, inTangentType='linear', outTangentType='linear',
                           currentDriver='lForearmTwist.rotateX', driverValue=90.0, attribute='pJCMkhFarmBndTwst90_L',
                           value=1)
    cmds.setDrivenKeyframe(blend_shape_group, currentDriver='rThighBend.rotateX', driverValue=-85,
                           attribute='pJCMkhThiBnd_115_R', value=0)
    cmds.setDrivenKeyframe(blend_shape_group, currentDriver='rThighBend.rotateX', driverValue=-85,
                           attribute='pJCMkhThiBnd_115_R', value=0)
    cmds.setDrivenKeyframe(blend_shape_group, currentDriver='rCollar.rotateX', driverValue=0,
                           attribute='pJCMkhColrBnd_45Twist_30_R', value=0)
    cmds.setDrivenKeyframe(blend_shape_group, currentDriver='rCollar.rotateX', driverValue=0,
                           attribute='pJCMkhColrBnd_45Twist_30_R', value=0)
    cmds.setDrivenKeyframe(blend_shape_group, currentDriver='rThighBend.rotateZ', driverValue=-40,
                           attribute='pJCMkhThiBnd_115Side_60_R', value=0)
    cmds.setDrivenKeyframe(blend_shape_group, currentDriver='rThighBend.rotateZ', driverValue=-40,
                           attribute='pJCMkhThiBnd_115Side_60_R', value=0)
    cmds.setDrivenKeyframe(blend_shape_group, currentDriver='rForearmBend.rotateY', driverValue=0,
                           attribute='pJCMkhElbwBnd_R', value=0)
    cmds.setDrivenKeyframe(blend_shape_group, currentDriver='rForearmBend.rotateY', driverValue=135,
                           attribute='pJCMkhElbwBnd_R', value=1)
    cmds.setDrivenKeyframe(blend_shape_group, currentDriver='rShin.rotateX', driverValue=35,
                           attribute='pJCMkhShinBnd90_R', value=0)
    cmds.setDrivenKeyframe(blend_shape_group, currentDriver='rShin.rotateX', driverValue=90,
                           attribute='pJCMkhShinBnd90_R', value=1)
    cmds.setDrivenKeyframe(blend_shape_group, currentDriver='rThighTwist.rotateY', driverValue=0,
                           attribute='pJCMkhThiBnd_85Twst_45_R', value=0)
    cmds.setDrivenKeyframe(blend_shape_group, currentDriver='rThighTwist.rotateY', driverValue=0,
                           attribute='pJCMkhThiBnd_85Twst_45_R', value=0)
    cmds.setDrivenKeyframe(blend_shape_group, currentDriver='rThighTwist.rotateY', driverValue=0,
                           attribute='pJCMkhThiBnd_115Side_40Twst_45_R', value=0)
    cmds.setDrivenKeyframe(blend_shape_group, currentDriver='rThighTwist.rotateY', driverValue=0,
                           attribute='pJCMkhThiBnd_115Side_40Twst_45_R', value=0)
    cmds.setDrivenKeyframe(blend_shape_group, currentDriver='lThighTwist.rotateY', driverValue=0,
                           attribute='pJCMkhThiBnd_115Side40Twst_45_L', value=0)
    cmds.setDrivenKeyframe(blend_shape_group, currentDriver='lThighTwist.rotateY', driverValue=0,
                           attribute='pJCMkhThiBnd_115Side40Twst_45_L', value=0)

    # mel.eval('select {};'.format(blend_shape_group))
    # mel.eval('setDrivenKeyframe -at pJCMkhThiTwst55_R -v 0 -cd Genesis8Female_Group|hip|pelvis|rThighBend|rThighTwist.rotateY -dv 0;')
    # mel.eval('setDrivenKeyframe -at pJCMkhThiTwst55_R -v 1 -cd Genesis8Female_Group|hip|pelvis|rThighBend|rThighTwist.rotateY -dv 55;')
    # mel.eval('select {};'.format(blend_shape_group))
    # mel.eval('setDrivenKeyframe -at pJCMkhThiTwst45_L -v 0 -cd Genesis8Female_Group|hip|pelvis|lThighBend|lThighTwist.rotateY -dv 0;')
    # mel.eval('setDrivenKeyframe -at pJCMkhThiTwst45_L -v 1 -cd Genesis8Female_Group|hip|pelvis|lThighBend|lThighTwist.rotateY -dv 45;')
    # mel.eval('select {};'.format(blend_shape_group))
    # mel.eval('setDrivenKeyframe -at pJCMkhThiTwst-55_L -v 1 -cd Genesis8Female_Group|hip|pelvis|lThighBend|lThighTwist.rotateY -dv -55;')
    # mel.eval('setDrivenKeyframe -at pJCMkhThiTwst-55_L -v 0 -cd Genesis8Female_Group|hip|pelvis|lThighBend|lThighTwist.rotateY -dv 0;')
    # mel.eval('select {};'.format(blend_shape_group))
    # mel.eval('setDrivenKeyframe -at pJCMkhThiTwst-45_R -v 1 -cd Genesis8Female_Group|hip|pelvis|rThighBend|rThighTwist.rotateY -dv -45;')
    # mel.eval('setDrivenKeyframe -at pJCMkhThiTwst-45_R -v 0 -cd Genesis8Female_Group|hip|pelvis|rThighBend|rThighTwist.rotateY -dv 0;')
    # mel.eval('select {};'.format(blend_shape_group))
    # mel.eval('setDrivenKeyframe -at pJCMkhThiSide_R -v 1 -cd Genesis8Female_Group|hip|pelvis|rThighBend.rotateZ -dv -65;')
    # mel.eval('setDrivenKeyframe -at pJCMkhThiSide_R -v 0 -cd Genesis8Female_Group|hip|pelvis|rThighBend.rotateZ -dv 0;')
    # mel.eval('select {};'.format(blend_shape_group))
    # mel.eval('setDrivenKeyframe -at pJCMkhThighSide_L -v 0 -cd Genesis8Female_Group|hip|pelvis|lThighBend.rotateZ -dv 0;')
    # mel.eval('setDrivenKeyframe -at pJCMkhThighSide_L -v 1 -cd Genesis8Female_Group|hip|pelvis|lThighBend.rotateZ -dv 65;')
    # mel.eval('select {};'.format(blend_shape_group))
    # mel.eval('setDrivenKeyframe -at pJCMkhThighBnd-85_R -v 1 -cd Genesis8Female_Group|hip|pelvis|rThighBend.rotateX -dv -85;')
    # mel.eval('setDrivenKeyframe -at pJCMkhThighBnd-85_R -v 0 -cd Genesis8Female_Group|hip|pelvis|rThighBend.rotateX -dv 0;')
    # mel.eval('select {};'.format(blend_shape_group))
    # mel.eval('setDrivenKeyframe -at pJCMkhThighBnd-85_L -v 1 -cd Genesis8Female_Group|hip|pelvis|lThighBend.rotateX -dv -85;')
    # mel.eval('setDrivenKeyframe -at pJCMkhThighBnd-85_L -v 0 -cd Genesis8Female_Group|hip|pelvis|lThighBend.rotateX -dv 0;')
    # mel.eval('select {};'.format(blend_shape_group))
    # mel.eval('setDrivenKeyframe -itt linear -ott linear -at pJCMkhThiBnd35_R -v 0 -cd Genesis8Female_Group|hip|pelvis|rThighBend.rotateX -dv 0;')
    # mel.eval('setDrivenKeyframe -itt linear -ott linear -at pJCMkhThiBnd35_R -v 1 -cd Genesis8Female_Group|hip|pelvis|rThighBend.rotateX -dv 35;')
    # mel.eval('select {};'.format(blend_shape_group))
    # mel.eval('setDrivenKeyframe -itt linear -ott linear -at pJCMkhThiBnd35_L -v 0 -cd Genesis8Female_Group|hip|pelvis|lThighBend.rotateX -dv 0;')
    # mel.eval('setDrivenKeyframe -itt linear -ott linear -at pJCMkhThiBnd35_L -v 1 -cd Genesis8Female_Group|hip|pelvis|lThighBend.rotateX -dv 35;')
    # mel.eval('select {};'.format(blend_shape_group))
    # mel.eval('setDrivenKeyframe -at pJCMkhThiBnd-85Twst75_R -v 0 -cd Genesis8Female_Group|hip|pelvis|rThighBend|rThighTwist.rotateY -dv 0;')
    # mel.eval('setDrivenKeyframe -at pJCMkhThiBnd-85Twst75_R -v 1 -cd Genesis8Female_Group|hip|pelvis|rThighBend|rThighTwist.rotateY -dv 75;')
    # mel.eval('select {};'.format(blend_shape_group))
    # mel.eval('setDrivenKeyframe -at pJCMkhThiBnd-85Twst45_L -v 0 -cd Genesis8Female_Group|hip|pelvis|lThighBend|lThighTwist.rotateY -dv 0;')
    # mel.eval('setDrivenKeyframe -at pJCMkhThiBnd-85Twst45_L -v 1 -cd Genesis8Female_Group|hip|pelvis|lThighBend|lThighTwist.rotateY -dv 45;')
    # mel.eval('select {};'.format(blend_shape_group))
    # mel.eval('setDrivenKeyframe -at pJCMkhThiBnd-85Twst-75_L -v 1 -cd Genesis8Female_Group|hip|pelvis|lThighBend|lThighTwist.rotateY -dv -75;')
    # mel.eval('setDrivenKeyframe -at pJCMkhThiBnd-85Twst-75_L -v 0 -cd Genesis8Female_Group|hip|pelvis|lThighBend|lThighTwist.rotateY -dv 0;')
    # mel.eval('select {};'.format(blend_shape_group))
    # mel.eval('setDrivenKeyframe -at pJCMkhThiBnd-85Twst-45_R -v 1 -cd Genesis8Female_Group|hip|pelvis|rThighBend|rThighTwist.rotateY -dv -45;')
    # mel.eval('setDrivenKeyframe -at pJCMkhThiBnd-85Twst-45_R -v 0 -cd Genesis8Female_Group|hip|pelvis|rThighBend|rThighTwist.rotateY -dv 0;')
    # mel.eval('select {};'.format(blend_shape_group))
    # mel.eval('setDrivenKeyframe -at pJCMkhThiBnd-85Side60_L -v 0 -cd Genesis8Female_Group|hip|pelvis|lThighBend.rotateZ -dv 40;')
    # mel.eval('setDrivenKeyframe -at pJCMkhThiBnd-85Side60_L -v 1 -cd Genesis8Female_Group|hip|pelvis|lThighBend.rotateZ -dv 60;')
    # mel.eval('select {};'.format(blend_shape_group))
    # mel.eval('setDrivenKeyframe -at pJCMkhThiBnd-85Side40_L -v 0 -cd Genesis8Female_Group|hip|pelvis|lThighBend.rotateZ -dv 0;')
    # mel.eval('setDrivenKeyframe -at pJCMkhThiBnd-85Side40_L -v 1 -cd Genesis8Female_Group|hip|pelvis|lThighBend.rotateZ -dv 40;')
    # mel.eval('select {};'.format(blend_shape_group))
    # mel.eval('setDrivenKeyframe -at pJCMkhThiBnd-85Side40Twst75_L -v 0 -cd Genesis8Female_Group|hip|pelvis|lThighBend|lThighTwist.rotateY -dv 45;')
    # mel.eval('setDrivenKeyframe -at pJCMkhThiBnd-85Side40Twst75_L -v 1 -cd Genesis8Female_Group|hip|pelvis|lThighBend|lThighTwist.rotateY -dv 75;')
    # mel.eval('select {};'.format(blend_shape_group))
    # mel.eval('setDrivenKeyframe -at pJCMkhThiBnd-85Side40Twst45_L -v 0 -cd Genesis8Female_Group|hip|pelvis|lThighBend|lThighTwist.rotateY -dv 0;')
    # mel.eval('setDrivenKeyframe -at pJCMkhThiBnd-85Side40Twst45_L -v 1 -cd Genesis8Female_Group|hip|pelvis|lThighBend|lThighTwist.rotateY -dv 45;')
    # mel.eval('select {};'.format(blend_shape_group))
    # mel.eval('setDrivenKeyframe -at pJCMkhThiBnd-85Side40Twst-45_L -v 1 -cd Genesis8Female_Group|hip|pelvis|lThighBend|lThighTwist.rotateY -dv -45;')
    # mel.eval('setDrivenKeyframe -at pJCMkhThiBnd-85Side40Twst-45_L -v 0 -cd Genesis8Female_Group|hip|pelvis|lThighBend|lThighTwist.rotateY -dv 0;')
    # mel.eval('select {};'.format(blend_shape_group))
    # mel.eval('setDrivenKeyframe -at pJCMkhThiBnd-85Side-60_R -v 1 -cd Genesis8Female_Group|hip|pelvis|rThighBend.rotateZ -dv -60;')
    # mel.eval('setDrivenKeyframe -at pJCMkhThiBnd-85Side-60_R -v 0 -cd Genesis8Female_Group|hip|pelvis|rThighBend.rotateZ -dv -40;')
    # mel.eval('select {};'.format(blend_shape_group))
    # mel.eval('setDrivenKeyframe -at pJCMkhThiBnd-85Side-40_R -v 1 -cd Genesis8Female_Group|hip|pelvis|rThighBend.rotateZ -dv -40;')
    # mel.eval('setDrivenKeyframe -at pJCMkhThiBnd-85Side-40_R -v 0 -cd Genesis8Female_Group|hip|pelvis|rThighBend.rotateZ -dv 0;')
    # mel.eval('select {};'.format(blend_shape_group))
    # mel.eval('setDrivenKeyframe -at pJCMkhThiBnd-85Side-40Twst45_R -v 0 -cd Genesis8Female_Group|hip|pelvis|rThighBend|rThighTwist.rotateY -dv 0;')
    # mel.eval('setDrivenKeyframe -at pJCMkhThiBnd-85Side-40Twst45_R -v 1 -cd Genesis8Female_Group|hip|pelvis|rThighBend|rThighTwist.rotateY -dv 45;')
    # mel.eval('select {};'.format(blend_shape_group))
    # mel.eval('setDrivenKeyframe -at pJCMkhThiBnd-85Side-40Twst-75_R -v 1 -cd Genesis8Female_Group|hip|pelvis|rThighBend|rThighTwist.rotateY -dv -75;')
    # mel.eval('setDrivenKeyframe -at pJCMkhThiBnd-85Side-40Twst-75_R -v 0 -cd Genesis8Female_Group|hip|pelvis|rThighBend|rThighTwist.rotateY -dv -45;')
    # mel.eval('select {};'.format(blend_shape_group))
    # mel.eval('setDrivenKeyframe -at pJCMkhThiBnd-85Side-40Twst-45_R -v 1 -cd Genesis8Female_Group|hip|pelvis|rThighBend|rThighTwist.rotateY -dv -45;')
    # mel.eval('setDrivenKeyframe -at pJCMkhThiBnd-85Side-40Twst-45_R -v 0 -cd Genesis8Female_Group|hip|pelvis|rThighBend|rThighTwist.rotateY -dv 0;')
    # mel.eval('select {};'.format(blend_shape_group))
    # mel.eval('setDrivenKeyframe -at pJCMkhThiBnd-115_R -v 1 -cd Genesis8Female_Group|hip|pelvis|rThighBend.rotateX -dv -115;')
    # mel.eval('setDrivenKeyframe -at pJCMkhThiBnd-115_R -v 0 -cd Genesis8Female_Group|hip|pelvis|rThighBend.rotateX -dv -85;')
    # mel.eval('select {};'.format(blend_shape_group))
    # mel.eval('setDrivenKeyframe -at pJCMkhThiBnd-115_L -v 1 -cd Genesis8Female_Group|hip|pelvis|lThighBend.rotateX -dv -115;')
    # mel.eval('setDrivenKeyframe -at pJCMkhThiBnd-115_L -v 0 -cd Genesis8Female_Group|hip|pelvis|lThighBend.rotateX -dv -85;')
    # mel.eval('select {};'.format(blend_shape_group))
    # mel.eval('setDrivenKeyframe -at pJCMkhThiBnd-115Side60_L -v 0 -cd Genesis8Female_Group|hip|pelvis|lThighBend.rotateZ -dv 40;')
    # mel.eval('setDrivenKeyframe -at pJCMkhThiBnd-115Side60_L -v 1 -cd Genesis8Female_Group|hip|pelvis|lThighBend.rotateZ -dv 60;')
    # mel.eval('select {};'.format(blend_shape_group))
    # mel.eval('setDrivenKeyframe -at pJCMkhThiBnd-115Side40_L -v 0 -cd Genesis8Female_Group|hip|pelvis|lThighBend.rotateZ -dv 0;')
    # mel.eval('setDrivenKeyframe -at pJCMkhThiBnd-115Side40_L -v 1 -cd Genesis8Female_Group|hip|pelvis|lThighBend.rotateZ -dv 40;')
    # mel.eval('select {};'.format(blend_shape_group))
    # mel.eval('setDrivenKeyframe -at pJCMkhThiBnd-115Side40Twst75_L -v 0 -cd Genesis8Female_Group|hip|pelvis|lThighBend|lThighTwist.rotateY -dv 45;')
    # mel.eval('setDrivenKeyframe -at pJCMkhThiBnd-115Side40Twst75_L -v 1 -cd Genesis8Female_Group|hip|pelvis|lThighBend|lThighTwist.rotateY -dv 75;')
    # mel.eval('select {};'.format(blend_shape_group))
    # mel.eval('setDrivenKeyframe -at pJCMkhThiBnd-115Side40Twst45_L -v 0 -cd Genesis8Female_Group|hip|pelvis|lThighBend|lThighTwist.rotateY -dv 0;')
    # mel.eval('setDrivenKeyframe -at pJCMkhThiBnd-115Side40Twst45_L -v 1 -cd Genesis8Female_Group|hip|pelvis|lThighBend|lThighTwist.rotateY -dv 45;')
    # mel.eval('select {};'.format(blend_shape_group))
    # mel.eval('setDrivenKeyframe -at pJCMkhThiBnd-115Side40Twst-45_L -v 1 -cd Genesis8Female_Group|hip|pelvis|lThighBend|lThighTwist.rotateY -dv -45;')
    # mel.eval('setDrivenKeyframe -at pJCMkhThiBnd-115Side40Twst-45_L -v 0 -cd Genesis8Female_Group|hip|pelvis|lThighBend|lThighTwist.rotateY -dv 0;')
    # mel.eval('select {};'.format(blend_shape_group))
    # mel.eval('setDrivenKeyframe -at pJCMkhThiBnd-115Side-60_R -v 1 -cd Genesis8Female_Group|hip|pelvis|rThighBend.rotateZ -dv -60;')
    # mel.eval('setDrivenKeyframe -at pJCMkhThiBnd-115Side-60_R -v 0 -cd Genesis8Female_Group|hip|pelvis|rThighBend.rotateZ -dv -40;')
    # mel.eval('select {};'.format(blend_shape_group))
    # mel.eval('setDrivenKeyframe -at pJCMkhThiBnd-115Side-40_R -v 1 -cd Genesis8Female_Group|hip|pelvis|rThighBend.rotateZ -dv -40;')
    # mel.eval('setDrivenKeyframe -at pJCMkhThiBnd-115Side-40_R -v 0 -cd Genesis8Female_Group|hip|pelvis|rThighBend.rotateZ -dv 0;')
    # mel.eval('select {};'.format(blend_shape_group))
    # mel.eval('setDrivenKeyframe -at pJCMkhThiBnd-115Side-40Twst45_R -v 0 -cd Genesis8Female_Group|hip|pelvis|rThighBend|rThighTwist.rotateY -dv 0;')
    # mel.eval('setDrivenKeyframe -at pJCMkhThiBnd-115Side-40Twst45_R -v 1 -cd Genesis8Female_Group|hip|pelvis|rThighBend|rThighTwist.rotateY -dv 45;')
    # mel.eval('select {};'.format(blend_shape_group))
    # mel.eval('setDrivenKeyframe -at pJCMkhThiBnd-115Side-40Twst-75_R -v 1 -cd Genesis8Female_Group|hip|pelvis|rThighBend|rThighTwist.rotateY -dv -75;')
    # mel.eval('setDrivenKeyframe -at pJCMkhThiBnd-115Side-40Twst-75_R -v 0 -cd Genesis8Female_Group|hip|pelvis|rThighBend|rThighTwist.rotateY -dv -45;')
    # mel.eval('select {};'.format(blend_shape_group))
    # mel.eval('setDrivenKeyframe -at pJCMkhThiBnd-115Side-40Twst-45_R -v 1 -cd Genesis8Female_Group|hip|pelvis|rThighBend|rThighTwist.rotateY -dv -45;')
    # mel.eval('setDrivenKeyframe -at pJCMkhThiBnd-115Side-40Twst-45_R -v 0 -cd Genesis8Female_Group|hip|pelvis|rThighBend|rThighTwist.rotateY -dv 0;')
    # mel.eval('select {};'.format(blend_shape_group))
    # mel.eval('setDrivenKeyframe -at pJCMkhShinBnd90_R -v 0 -cd Genesis8Female_Group|hip|pelvis|rThighBend|rThighTwist|rShin.rotateX -dv 35;')
    # mel.eval('setDrivenKeyframe -at pJCMkhShinBnd90_R -v 1 -cd Genesis8Female_Group|hip|pelvis|rThighBend|rThighTwist|rShin.rotateX -dv 90;')
    # mel.eval('select {};'.format(blend_shape_group))
    # mel.eval('setDrivenKeyframe -at pJCMkhShinBnd90_L -v 0 -cd Genesis8Female_Group|hip|pelvis|lThighBend|lThighTwist|lShin.rotateX -dv 35;')
    # mel.eval('setDrivenKeyframe -at pJCMkhShinBnd90_L -v 1 -cd Genesis8Female_Group|hip|pelvis|lThighBend|lThighTwist|lShin.rotateX -dv 90;')
    # mel.eval('select {};'.format(blend_shape_group))
    # mel.eval('setDrivenKeyframe -at pJCMkhShinBnd35_R -v 0 -cd Genesis8Female_Group|hip|pelvis|rThighBend|rThighTwist|rShin.rotateX -dv 0;')
    # mel.eval('setDrivenKeyframe -at pJCMkhShinBnd35_R -v 1 -cd Genesis8Female_Group|hip|pelvis|rThighBend|rThighTwist|rShin.rotateX -dv 35;')
    # mel.eval('select {};'.format(blend_shape_group))
    # mel.eval('setDrivenKeyframe -at pJCMkhShinBnd35_L -v 0 -cd Genesis8Female_Group|hip|pelvis|lThighBend|lThighTwist|lShin.rotateX -dv 0;')
    # mel.eval('setDrivenKeyframe -at pJCMkhShinBnd35_L -v 1 -cd Genesis8Female_Group|hip|pelvis|lThighBend|lThighTwist|lShin.rotateX -dv 35;')
    # mel.eval('select {};'.format(blend_shape_group))
    # mel.eval('setDrivenKeyframe -at pJCMkhShinBnd155_R -v 0 -cd Genesis8Female_Group|hip|pelvis|rThighBend|rThighTwist|rShin.rotateX -dv 135;')
    # mel.eval('setDrivenKeyframe -at pJCMkhShinBnd155_R -v 1 -cd Genesis8Female_Group|hip|pelvis|rThighBend|rThighTwist|rShin.rotateX -dv 155;')
    # mel.eval('select {};'.format(blend_shape_group))
    # mel.eval('setDrivenKeyframe -at pJCMkhShinBnd155_L -v 0 -cd Genesis8Female_Group|hip|pelvis|lThighBend|lThighTwist|lShin.rotateX -dv 135;')
    # mel.eval('setDrivenKeyframe -at pJCMkhShinBnd155_L -v 1 -cd Genesis8Female_Group|hip|pelvis|lThighBend|lThighTwist|lShin.rotateX -dv 155;')
    # mel.eval('select {};'.format(blend_shape_group))
    # mel.eval('setDrivenKeyframe -at pJCMkhShinBnd135_R -v 0 -cd Genesis8Female_Group|hip|pelvis|rThighBend|rThighTwist|rShin.rotateX -dv 90;')
    # mel.eval('setDrivenKeyframe -at pJCMkhShinBnd135_R -v 1 -cd Genesis8Female_Group|hip|pelvis|rThighBend|rThighTwist|rShin.rotateX -dv 135;')
    # mel.eval('select {};'.format(blend_shape_group))
    # mel.eval('setDrivenKeyframe -at pJCMkhShinBnd135_L -v 0 -cd Genesis8Female_Group|hip|pelvis|lThighBend|lThighTwist|lShin.rotateX -dv 90;')
    # mel.eval('setDrivenKeyframe -at pJCMkhShinBnd135_L -v 1 -cd Genesis8Female_Group|hip|pelvis|lThighBend|lThighTwist|lShin.rotateX -dv 135;')
    # mel.eval('select {};'.format(blend_shape_group))
    # mel.eval('setDrivenKeyframe -at pJCMkhShinBnd-11_R -v 1 -cd Genesis8Female_Group|hip|pelvis|rThighBend|rThighTwist|rShin.rotateX -dv -11;')
    # mel.eval('setDrivenKeyframe -at pJCMkhShinBnd-11_R -v 0 -cd Genesis8Female_Group|hip|pelvis|rThighBend|rThighTwist|rShin.rotateX -dv 0;')
    # mel.eval('select {};'.format(blend_shape_group))
    # mel.eval('setDrivenKeyframe -at pJCMkhShinBnd-11_L -v 1 -cd Genesis8Female_Group|hip|pelvis|lThighBend|lThighTwist|lShin.rotateX -dv -11;')
    # mel.eval('setDrivenKeyframe -at pJCMkhShinBnd-11_L -v 0 -cd Genesis8Female_Group|hip|pelvis|lThighBend|lThighTwist|lShin.rotateX -dv 0;')
    # mel.eval('select {};'.format(blend_shape_group))
    # mel.eval('setDrivenKeyframe -itt linear -ott linear -at pJCMkhFarmBndTwst90_L -v 0 -cd Genesis8Female_Group|hip|abdomenLower|abdomenUpper|chestLower|chestUpper|lCollar|lShldrBend|lShldrTwist|lForearmBend|lForearmTwist.rotateX -dv 0;')
    # mel.eval('setDrivenKeyframe -itt linear -ott linear -at pJCMkhFarmBndTwst90_L -v 1 -cd Genesis8Female_Group|hip|abdomenLower|abdomenUpper|chestLower|chestUpper|lCollar|lShldrBend|lShldrTwist|lForearmBend|lForearmTwist.rotateX -dv 90;')
    # mel.eval('select {};'.format(blend_shape_group))
    # mel.eval('setDrivenKeyframe -itt linear -ott linear -at pJCMkhFarmBndTwst-90_R -v 0 -cd Genesis8Female_Group|hip|abdomenLower|abdomenUpper|chestLower|chestUpper|rCollar|rShldrBend|rShldrTwist|rForearmBend|rForearmTwist.rotateX -dv 0;')
    # mel.eval('setDrivenKeyframe -itt linear -ott linear -at pJCMkhFarmBndTwst-90_R -v 1 -cd Genesis8Female_Group|hip|abdomenLower|abdomenUpper|chestLower|chestUpper|rCollar|rShldrBend|rShldrTwist|rForearmBend|rForearmTwist.rotateX -dv -90;')
    # mel.eval('select {};'.format(blend_shape_group))
    # mel.eval('setDrivenKeyframe -itt linear -ott linear -at pJCMkhFarmBnd135Twst80_R -v 0 -cd Genesis8Female_Group|hip|abdomenLower|abdomenUpper|chestLower|chestUpper|rCollar|rShldrBend|rShldrTwist|rForearmBend|rForearmTwist.rotateX -dv 0;')
    # mel.eval('setDrivenKeyframe -itt linear -ott linear -at pJCMkhFarmBnd135Twst80_R -v 1 -cd Genesis8Female_Group|hip|abdomenLower|abdomenUpper|chestLower|chestUpper|rCollar|rShldrBend|rShldrTwist|rForearmBend|rForearmTwist.rotateX -dv 80;')
    # mel.eval('select {};'.format(blend_shape_group))
    # mel.eval('setDrivenKeyframe -itt linear -ott linear -at pJCMkhFarmBnd-135Twst80_L -v 0 -cd Genesis8Female_Group|hip|abdomenLower|abdomenUpper|chestLower|chestUpper|lCollar|lShldrBend|lShldrTwist|lForearmBend|lForearmTwist.rotateX -dv 0;')
    # mel.eval('setDrivenKeyframe -itt linear -ott linear -at pJCMkhFarmBnd-135Twst80_L -v 1 -cd Genesis8Female_Group|hip|abdomenLower|abdomenUpper|chestLower|chestUpper|lCollar|lShldrBend|lShldrTwist|lForearmBend|lForearmTwist.rotateX -dv 80;')
    # mel.eval('select {};'.format(blend_shape_group))
    # mel.eval('setDrivenKeyframe -at pJCMkhElbwBnd_R -v 0 -cd Genesis8Female_Group|hip|abdomenLower|abdomenUpper|chestLower|chestUpper|rCollar|rShldrBend|rShldrTwist|rForearmBend.rotateY -dv 0;')
    # mel.eval('setDrivenKeyframe -at pJCMkhElbwBnd_R -v 1 -cd Genesis8Female_Group|hip|abdomenLower|abdomenUpper|chestLower|chestUpper|rCollar|rShldrBend|rShldrTwist|rForearmBend.rotateY -dv 135;')
    # mel.eval('select {};'.format(blend_shape_group))
    # mel.eval('setDrivenKeyframe -at pJCMkhElbwBnd_L -v 1 -cd Genesis8Female_Group|hip|abdomenLower|abdomenUpper|chestLower|chestUpper|lCollar|lShldrBend|lShldrTwist|lForearmBend.rotateY -dv -135;')
    # mel.eval('setDrivenKeyframe -at pJCMkhElbwBnd_L -v 0 -cd Genesis8Female_Group|hip|abdomenLower|abdomenUpper|chestLower|chestUpper|lCollar|lShldrBend|lShldrTwist|lForearmBend.rotateY -dv 0;')
    # mel.eval('select {};'.format(blend_shape_group))
    # mel.eval('setDrivenKeyframe -at pJCMkhColrTwst30_R -v 0 -cd Genesis8Female_Group|hip|abdomenLower|abdomenUpper|chestLower|chestUpper|rCollar.rotateX -dv 0;')
    # mel.eval('setDrivenKeyframe -at pJCMkhColrTwst30_R -v 1 -cd Genesis8Female_Group|hip|abdomenLower|abdomenUpper|chestLower|chestUpper|rCollar.rotateX -dv 30;')
    # mel.eval('select {};'.format(blend_shape_group))
    # mel.eval('setDrivenKeyframe -at pJCMkhColrTwst30_L -v 0 -cd Genesis8Female_Group|hip|abdomenLower|abdomenUpper|chestLower|chestUpper|lCollar.rotateX -dv 0;')
    # mel.eval('setDrivenKeyframe -at pJCMkhColrTwst30_L -v 1 -cd Genesis8Female_Group|hip|abdomenLower|abdomenUpper|chestLower|chestUpper|lCollar.rotateX -dv 30;')
    # mel.eval('select {};'.format(blend_shape_group))
    # mel.eval('setDrivenKeyframe -at pJCMkhColrFwd26_R -v 0 -cd Genesis8Female_Group|hip|abdomenLower|abdomenUpper|chestLower|chestUpper|rCollar.rotateY -dv 15;')
    # mel.eval('setDrivenKeyframe -at pJCMkhColrFwd26_R -v 1 -cd Genesis8Female_Group|hip|abdomenLower|abdomenUpper|chestLower|chestUpper|rCollar.rotateY -dv 26;')
    # mel.eval('select {};'.format(blend_shape_group))
    # mel.eval('setDrivenKeyframe -at pJCMkhColrFwd26Twst30_R -v 0 -cd Genesis8Female_Group|hip|abdomenLower|abdomenUpper|chestLower|chestUpper|rCollar.rotateX -dv 0;')
    # mel.eval('setDrivenKeyframe -at pJCMkhColrFwd26Twst30_R -v 1 -cd Genesis8Female_Group|hip|abdomenLower|abdomenUpper|chestLower|chestUpper|rCollar.rotateX -dv 30;')
    # mel.eval('select {};'.format(blend_shape_group))
    # mel.eval('setDrivenKeyframe -at pJCMkhColrFwd26Bnd-45_R -v 1 -cd Genesis8Female_Group|hip|abdomenLower|abdomenUpper|chestLower|chestUpper|rCollar.rotateZ -dv -45;')
    # mel.eval('setDrivenKeyframe -at pJCMkhColrFwd26Bnd-45_R -v 0 -cd Genesis8Female_Group|hip|abdomenLower|abdomenUpper|chestLower|chestUpper|rCollar.rotateZ -dv 0;')
    # mel.eval('select {};'.format(blend_shape_group))
    # mel.eval('setDrivenKeyframe -itt linear -ott linear -at pJCMkhColrFwd15_R -v 0 -cd Genesis8Female_Group|hip|abdomenLower|abdomenUpper|chestLower|chestUpper|rCollar.rotateY -dv 0;')
    # mel.eval('setDrivenKeyframe -itt linear -ott linear -at pJCMkhColrFwd15_R -v 1 -cd Genesis8Female_Group|hip|abdomenLower|abdomenUpper|chestLower|chestUpper|rCollar.rotateY -dv 15;')
    # mel.eval('select {};'.format(blend_shape_group))
    # mel.eval('setDrivenKeyframe -at pJCMkhColrFwd-26_L -v 1 -cd Genesis8Female_Group|hip|abdomenLower|abdomenUpper|chestLower|chestUpper|lCollar.rotateY -dv -26;')
    # mel.eval('setDrivenKeyframe -at pJCMkhColrFwd-26_L -v 0 -cd Genesis8Female_Group|hip|abdomenLower|abdomenUpper|chestLower|chestUpper|lCollar.rotateY -dv -15;')
    # mel.eval('select {};'.format(blend_shape_group))
    # mel.eval('setDrivenKeyframe -at pJCMkhColrFwd-26Twst30_L -v 0 -cd Genesis8Female_Group|hip|abdomenLower|abdomenUpper|chestLower|chestUpper|lCollar.rotateX -dv 0;')
    # mel.eval('setDrivenKeyframe -at pJCMkhColrFwd-26Twst30_L -v 1 -cd Genesis8Female_Group|hip|abdomenLower|abdomenUpper|chestLower|chestUpper|lCollar.rotateX -dv 30;')
    # mel.eval('select {};'.format(blend_shape_group))
    # mel.eval('setDrivenKeyframe -at pJCMkhColrFwd-26Bnd45_L -v 0 -cd Genesis8Female_Group|hip|abdomenLower|abdomenUpper|chestLower|chestUpper|lCollar.rotateZ -dv 0;')
    # mel.eval('setDrivenKeyframe -at pJCMkhColrFwd-26Bnd45_L -v 1 -cd Genesis8Female_Group|hip|abdomenLower|abdomenUpper|chestLower|chestUpper|lCollar.rotateZ -dv 45;')
    # mel.eval('select {};'.format(blend_shape_group))
    # mel.eval('setDrivenKeyframe -itt linear -ott linear -at pJCMkhColrFwd-15_L -v 0 -cd Genesis8Female_Group|hip|abdomenLower|abdomenUpper|chestLower|chestUpper|lCollar.rotateY -dv 0;')
    # mel.eval('setDrivenKeyframe -itt linear -ott linear -at pJCMkhColrFwd-15_L -v 1 -cd Genesis8Female_Group|hip|abdomenLower|abdomenUpper|chestLower|chestUpper|lCollar.rotateY -dv -15;')
    # mel.eval('select {};'.format(blend_shape_group))
    # mel.eval('setDrivenKeyframe -itt linear -ott linear -at pJCMkhColrBnd45_L -v 0 -cd Genesis8Female_Group|hip|abdomenLower|abdomenUpper|chestLower|chestUpper|lCollar.rotateZ -dv 0;')
    # mel.eval('setDrivenKeyframe -itt linear -ott linear -at pJCMkhColrBnd45_L -v 1 -cd Genesis8Female_Group|hip|abdomenLower|abdomenUpper|chestLower|chestUpper|lCollar.rotateZ -dv 45;')
    # mel.eval('select {};'.format(blend_shape_group))
    # mel.eval('setDrivenKeyframe -at pJCMkhColrBnd45Twst-30_L -v 1 -cd Genesis8Female_Group|hip|abdomenLower|abdomenUpper|chestLower|chestUpper|lCollar.rotateX -dv -30;')
    # mel.eval('setDrivenKeyframe -at pJCMkhColrBnd45Twst-30_L -v 0 -cd Genesis8Female_Group|hip|abdomenLower|abdomenUpper|chestLower|chestUpper|lCollar.rotateX -dv 0;')
    # mel.eval('select {};'.format(blend_shape_group))
    # mel.eval('setDrivenKeyframe -itt linear -ott linear -at pJCMkhColrBnd-45_R -v 0 -cd Genesis8Female_Group|hip|abdomenLower|abdomenUpper|chestLower|chestUpper|rCollar.rotateZ -dv 0;')
    # mel.eval('setDrivenKeyframe -itt linear -ott linear -at pJCMkhColrBnd-45_R -v 1 -cd Genesis8Female_Group|hip|abdomenLower|abdomenUpper|chestLower|chestUpper|rCollar.rotateZ -dv -45;')
    # mel.eval('select {};'.format(blend_shape_group))
    # mel.eval('setDrivenKeyframe -at pJCMkhColrBnd-45Twist-30_R -v 1 -cd Genesis8Female_Group|hip|abdomenLower|abdomenUpper|chestLower|chestUpper|rCollar.rotateX -dv -30;')
    # mel.eval('setDrivenKeyframe -at pJCMkhColrBnd-45Twist-30_R -v 0 -cd Genesis8Female_Group|hip|abdomenLower|abdomenUpper|chestLower|chestUpper|rCollar.rotateX -dv 0;')
    # mel.eval('select {};'.format(blend_shape_group))
    # mel.eval('setDrivenKeyframe -at pJCMkhColrBack17_L -v 0 -cd Genesis8Female_Group|hip|abdomenLower|abdomenUpper|chestLower|chestUpper|lCollar.rotateY -dv 0;')
    # mel.eval('setDrivenKeyframe -at pJCMkhColrBack17_L -v 1 -cd Genesis8Female_Group|hip|abdomenLower|abdomenUpper|chestLower|chestUpper|lCollar.rotateY -dv 15;')
    # mel.eval('select {};'.format(blend_shape_group))
    # mel.eval('setDrivenKeyframe -at pJCMkhColrBack17Bnd45_L -v 0 -cd Genesis8Female_Group|hip|abdomenLower|abdomenUpper|chestLower|chestUpper|lCollar.rotateZ -dv 0;')
    # mel.eval('setDrivenKeyframe -at pJCMkhColrBack17Bnd45_L -v 1 -cd Genesis8Female_Group|hip|abdomenLower|abdomenUpper|chestLower|chestUpper|lCollar.rotateZ -dv 45;')
    # mel.eval('select {};'.format(blend_shape_group))
    # mel.eval('setDrivenKeyframe -at pJCMkhColrBack-17_R -v 1 -cd Genesis8Female_Group|hip|abdomenLower|abdomenUpper|chestLower|chestUpper|rCollar.rotateY -dv -15;')
    # mel.eval('setDrivenKeyframe -at pJCMkhColrBack-17_R -v 0 -cd Genesis8Female_Group|hip|abdomenLower|abdomenUpper|chestLower|chestUpper|rCollar.rotateY -dv 0;')
    # mel.eval('select {};'.format(blend_shape_group))
    # mel.eval('setDrivenKeyframe -at pJCMkhColrBack-17Bnd-45_R -v 1 -cd Genesis8Female_Group|hip|abdomenLower|abdomenUpper|chestLower|chestUpper|rCollar.rotateZ -dv -45;')
    # mel.eval('setDrivenKeyframe -at pJCMkhColrBack-17Bnd-45_R -v 0 -cd Genesis8Female_Group|hip|abdomenLower|abdomenUpper|chestLower|chestUpper|rCollar.rotateZ -dv 0;')
