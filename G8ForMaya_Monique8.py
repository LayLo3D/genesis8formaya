import maya.mel as mel
import maya.cmds as cmds


def monique8JCMs(character, blendShapeGroup):
    pyBlendShapesName = blendShapeGroup
    if not cmds.objExists('{}.pJCMMonique8AbdomenUpperFwd'.format(pyBlendShapesName)):
        error = character + ' specific JCMs were not created.\n\n' + character + ' JCMs were either not exported out properly or the wrong file was selected for import.'
        cmds.warning(error.replace('\n\n', ' '))
        return error

    # pJCMMonique8AbdomenUpperFwd
    mel.eval('setAttr "abdomenUpper.rotateX" 0;')
    mel.eval('setDrivenKeyframe -currentDriver abdomenUpper.rotateX {0}.pJCMMonique8AbdomenUpperFwd;'.format(
        pyBlendShapesName))
    mel.eval('setAttr "abdomenUpper.rotateX" 40;')
    mel.eval('setAttr "{0}.pJCMMonique8AbdomenUpperFwd" 1;'.format(pyBlendShapesName))
    mel.eval('setDrivenKeyframe -currentDriver abdomenUpper.rotateX {0}.pJCMMonique8AbdomenUpperFwd;'.format(
        pyBlendShapesName))
    mel.eval('setAttr "abdomenUpper.rotateX" 0;')

    # pJCMMonique8CollarBndUpL
    mel.eval('setAttr "lCollar.rotateZ" 0;')
    mel.eval('setDrivenKeyframe -currentDriver lCollar.rotateZ {0}.pJCMMonique8CollarBndUpL;'.format(
        pyBlendShapesName))
    mel.eval('setAttr "lCollar.rotateZ" 55;')
    mel.eval('setAttr "{0}.pJCMMonique8CollarBndUpL" 1;'.format(pyBlendShapesName))
    mel.eval('setDrivenKeyframe -currentDriver lCollar.rotateZ {0}.pJCMMonique8CollarBndUpL;'.format(
        pyBlendShapesName))
    mel.eval('setAttr "lCollar.rotateZ" 0;')

    # pJCMMonique8CollarBndUpR
    mel.eval('setAttr "rCollar.rotateZ" 0;')
    mel.eval('setDrivenKeyframe -currentDriver rCollar.rotateZ {0}.pJCMMonique8CollarBndUpR;'.format(
        pyBlendShapesName))
    mel.eval('setAttr "rCollar.rotateZ" -55;')
    mel.eval('setAttr "{0}.pJCMMonique8CollarBndUpR" 1;'.format(pyBlendShapesName))
    mel.eval('setDrivenKeyframe -currentDriver rCollar.rotateZ {0}.pJCMMonique8CollarBndUpR;'.format(
        pyBlendShapesName))
    mel.eval('setAttr "rCollar.rotateZ" 0;')

    # pJCMMonique8FootBndUpL
    mel.eval('setAttr "lFoot.rotateX" 0;')
    mel.eval('setDrivenKeyframe -currentDriver lFoot.rotateX {0}.pJCMMonique8FootBndUpL;'.format(pyBlendShapesName))
    mel.eval('setAttr "lFoot.rotateX" -45;')
    mel.eval('setAttr "{0}.pJCMMonique8FootBndUpL" 1;'.format(pyBlendShapesName))
    mel.eval('setDrivenKeyframe -currentDriver lFoot.rotateX {0}.pJCMMonique8FootBndUpL;'.format(pyBlendShapesName))
    mel.eval('setAttr "lFoot.rotateX" 0;')

    # pJCMMonique8FootBndUpR
    mel.eval('setAttr "rFoot.rotateX" 0;')
    mel.eval('setDrivenKeyframe -currentDriver rFoot.rotateX {0}.pJCMMonique8FootBndUpR;'.format(pyBlendShapesName))
    mel.eval('setAttr "rFoot.rotateX" -45;')
    mel.eval('setAttr "{0}.pJCMMonique8FootBndUpR" 1;'.format(pyBlendShapesName))
    mel.eval('setDrivenKeyframe -currentDriver rFoot.rotateX {0}.pJCMMonique8FootBndUpR;'.format(pyBlendShapesName))
    mel.eval('setAttr "rFoot.rotateX" 0;')

    # pJCMMonique8ForearmBndL
    mel.eval('setAttr "lForearmBend.rotateY" 0;')
    mel.eval('setDrivenKeyframe -currentDriver lForearmBend.rotateY {0}.pJCMMonique8ForearmBndL;'.format(
        pyBlendShapesName))
    mel.eval('setAttr "lForearmBend.rotateY" -135;')
    mel.eval('setAttr "{0}.pJCMMonique8ForearmBndL" 1;'.format(pyBlendShapesName))
    mel.eval('setDrivenKeyframe -currentDriver lForearmBend.rotateY {0}.pJCMMonique8ForearmBndL;'.format(
        pyBlendShapesName))
    mel.eval('setAttr "lForearmBend.rotateY" 0;')

    # pJCMMonique8ForearmBndR
    mel.eval('setAttr "rForearmBend.rotateY" 0;')
    mel.eval('setDrivenKeyframe -currentDriver rForearmBend.rotateY {0}.pJCMMonique8ForearmBndR;'.format(
        pyBlendShapesName))
    mel.eval('setAttr "rForearmBend.rotateY" 135;')
    mel.eval('setAttr "{0}.pJCMMonique8ForearmBndR" 1;'.format(pyBlendShapesName))
    mel.eval('setDrivenKeyframe -currentDriver rForearmBend.rotateY {0}.pJCMMonique8ForearmBndR;'.format(
        pyBlendShapesName))
    mel.eval('setAttr "rForearmBend.rotateY" 0;')

    # pJCMMonique8HeadTwistL_HDLv3
    mel.eval('setAttr "head.rotateY" 0;')
    mel.eval('setDrivenKeyframe -currentDriver head.rotateY {0}.pJCMMonique8HeadTwistL_HDLv3;'.format(pyBlendShapesName))
    mel.eval('setAttr "head.rotateY" 22;')
    mel.eval('setAttr "{0}.pJCMMonique8HeadTwistL_HDLv3" 1;'.format(pyBlendShapesName))
    mel.eval('setDrivenKeyframe -currentDriver head.rotateY {0}.pJCMMonique8HeadTwistL_HDLv3;'.format(pyBlendShapesName))
    mel.eval('setAttr "head.rotateY" 0;')

    # pJCMMonique8HeadTwistR_HDLv3
    mel.eval('setAttr "head.rotateY" 0;')
    mel.eval('setDrivenKeyframe -currentDriver head.rotateY {0}.pJCMMonique8HeadTwistR_HDLv3;'.format(pyBlendShapesName))
    mel.eval('setAttr "head.rotateY" -22;')
    mel.eval('setAttr "{0}.pJCMMonique8HeadTwistR_HDLv3" 1;'.format(pyBlendShapesName))
    mel.eval('setDrivenKeyframe -currentDriver head.rotateY {0}.pJCMMonique8HeadTwistR_HDLv3;'.format(pyBlendShapesName))
    mel.eval('setAttr "head.rotateY" 0;')
    
    # pJCMMonique8ShinBndBck_85L
    mel.eval('setAttr "lShin.rotateX" 0;')
    mel.eval(
        'setDrivenKeyframe -currentDriver lShin.rotateX {0}.pJCMMonique8ShinBndBck_85L;'.format(pyBlendShapesName))
    mel.eval('setAttr "lShin.rotateX" 85;')
    mel.eval('setAttr "{0}.pJCMMonique8ShinBndBck_85L" 1;'.format(pyBlendShapesName))
    mel.eval(
        'setDrivenKeyframe -currentDriver lShin.rotateX {0}.pJCMMonique8ShinBndBck_85L;'.format(pyBlendShapesName))
    mel.eval('setAttr "lShin.rotateX" 0;')

    # pJCMMonique8ShinBndBck_85R
    mel.eval('setAttr "rShin.rotateX" 0;')
    mel.eval(
        'setDrivenKeyframe -currentDriver rShin.rotateX {0}.pJCMMonique8ShinBndBck_85R;'.format(pyBlendShapesName))
    mel.eval('setAttr "rShin.rotateX" 85;')
    mel.eval('setAttr "{0}.pJCMMonique8ShinBndBck_85R" 1;'.format(pyBlendShapesName))
    mel.eval(
        'setDrivenKeyframe -currentDriver rShin.rotateX {0}.pJCMMonique8ShinBndBck_85R;'.format(pyBlendShapesName))
    mel.eval('setAttr "rShin.rotateX" 0;')

    # pJCMMonique8ShoulderBndUpL_HDLv2
    mel.eval('setAttr "lShldrBend.rotateZ" 0;')
    mel.eval('setDrivenKeyframe -currentDriver lShldrBend.rotateZ {0}.pJCMMonique8ShoulderBndUpL_HDLv2;'.format(
        pyBlendShapesName))
    mel.eval('setAttr "lShldrBend.rotateZ" 90;')
    mel.eval('setAttr "{0}.pJCMMonique8ShoulderBndUpL_HDLv2" 1;'.format(pyBlendShapesName))
    mel.eval('setDrivenKeyframe -currentDriver lShldrBend.rotateZ {0}.pJCMMonique8ShoulderBndUpL_HDLv2;'.format(
        pyBlendShapesName))
    mel.eval('setAttr "lShldrBend.rotateZ" 0;')

    # pJCMMonique8ShoulderBndUpR_HDLv2
    mel.eval('setAttr "rShldrBend.rotateZ" 0;')
    mel.eval('setDrivenKeyframe -currentDriver rShldrBend.rotateZ {0}.pJCMMonique8ShoulderBndUpR_HDLv2;'.format(
        pyBlendShapesName))
    mel.eval('setAttr "rShldrBend.rotateZ" -90;')
    mel.eval('setAttr "{0}.pJCMMonique8ShoulderBndUpR_HDLv2" 1;'.format(pyBlendShapesName))
    mel.eval('setDrivenKeyframe -currentDriver rShldrBend.rotateZ {0}.pJCMMonique8ShoulderBndUpR_HDLv2;'.format(
        pyBlendShapesName))
    mel.eval('setAttr "rShldrBend.rotateZ" 0;')

    # pJCMMonique8ShoulderDn_n35L_HDLv4
    mel.eval('setAttr "lShldrBend.rotateZ" 0;')
    mel.eval('setDrivenKeyframe -currentDriver lShldrBend.rotateZ {0}.pJCMMonique8ShoulderDn_n35L_HDLv4;'.format(
        pyBlendShapesName))
    mel.eval('setAttr "lShldrBend.rotateZ" -35;')
    mel.eval('setAttr "{0}.pJCMMonique8ShoulderDn_n35L_HDLv4" 1;'.format(pyBlendShapesName))
    mel.eval('setDrivenKeyframe -currentDriver lShldrBend.rotateZ {0}.pJCMMonique8ShoulderDn_n35L_HDLv4;'.format(
        pyBlendShapesName))
    mel.eval('setAttr "lShldrBend.rotateZ" 0;')

    # pJCMMonique8ShoulderDn_35R_HDLv4
    mel.eval('setAttr "rShldrBend.rotateZ" 0;')
    mel.eval('setDrivenKeyframe -currentDriver rShldrBend.rotateZ {0}.pJCMMonique8ShoulderDn_35R_HDLv4;'.format(
        pyBlendShapesName))
    mel.eval('setAttr "rShldrBend.rotateZ" 35;')
    mel.eval('setAttr "{0}.pJCMMonique8ShoulderDn_35R_HDLv4" 1;'.format(pyBlendShapesName))
    mel.eval('setDrivenKeyframe -currentDriver rShldrBend.rotateZ {0}.pJCMMonique8ShoulderDn_35R_HDLv4;'.format(
        pyBlendShapesName))
    mel.eval('setAttr "rShldrBend.rotateZ" 0;')

    # pJCMMonique8ShoulderFwd_n80L
    mel.eval('setAttr "lShldrBend.rotateY" 0;')
    mel.eval('setDrivenKeyframe -currentDriver lShldrBend.rotateY {0}.pJCMMonique8ShoulderFwd_n80L;'.format(
        pyBlendShapesName))
    mel.eval('setAttr "lShldrBend.rotateY" -80;')
    mel.eval('setAttr "{0}.pJCMMonique8ShoulderFwd_n80L" 1;'.format(pyBlendShapesName))
    mel.eval('setDrivenKeyframe -currentDriver lShldrBend.rotateY {0}.pJCMMonique8ShoulderFwd_n80L;'.format(
        pyBlendShapesName))
    mel.eval('setAttr "lShldrBend.rotateY" 0;')

    # pJCMMonique8ShoulderFwd_80R
    mel.eval('setAttr "rShldrBend.rotateY" 0;')
    mel.eval('setDrivenKeyframe -currentDriver rShldrBend.rotateY {0}.pJCMMonique8ShoulderFwd_80R;'.format(
        pyBlendShapesName))
    mel.eval('setAttr "rShldrBend.rotateY" 80;')
    mel.eval('setAttr "{0}.pJCMMonique8ShoulderFwd_80R" 1;'.format(pyBlendShapesName))
    mel.eval('setDrivenKeyframe -currentDriver rShldrBend.rotateY {0}.pJCMMonique8ShoulderFwd_80R;'.format(
        pyBlendShapesName))
    mel.eval('setAttr "rShldrBend.rotateY" 0;')

    # pJCMMonique8ThighBndFwd_n65L
    mel.eval('setAttr "lThighBend.rotateX" 0;')
    mel.eval('setDrivenKeyframe -currentDriver lThighBend.rotateX {0}.pJCMMonique8ThighBndFwd_n65L;'.format(
        pyBlendShapesName))
    mel.eval('setAttr "lThighBend.rotateX" -65;')
    mel.eval('setAttr "{0}.pJCMMonique8ThighBndFwd_n65L" 1;'.format(pyBlendShapesName))
    mel.eval('setDrivenKeyframe -currentDriver lThighBend.rotateX {0}.pJCMMonique8ThighBndFwd_n65L;'.format(
        pyBlendShapesName))
    mel.eval('setAttr "lThighBend.rotateX" 0;')

    # pJCMMonique8ThighBndFwd_n65R
    mel.eval('setAttr "lThighBend.rotateX" 0;')
    mel.eval('setDrivenKeyframe -currentDriver lThighBend.rotateX {0}.pJCMMonique8ThighBndFwd_n65R;'.format(
        pyBlendShapesName))
    mel.eval('setAttr "lThighBend.rotateX" -65;')
    mel.eval('setAttr "{0}.pJCMMonique8ThighBndFwd_n65R" 1;'.format(pyBlendShapesName))
    mel.eval('setDrivenKeyframe -currentDriver lThighBend.rotateX {0}.pJCMMonique8ThighBndFwd_n65R;'.format(
        pyBlendShapesName))
    mel.eval('setAttr "lThighBend.rotateX" 0;')
    
    # pJCMMonique8ThighSideSideL
    mel.eval('setAttr "lThighBend.rotateZ" 0;')
    mel.eval('setDrivenKeyframe -currentDriver lThighBend.rotateZ {0}.pJCMMonique8ThighSideSideL;'.format(
        pyBlendShapesName))
    mel.eval('setAttr "lThighBend.rotateZ" 85;')
    mel.eval('setAttr "{0}.pJCMMonique8ThighSideSideL" 1;'.format(pyBlendShapesName))
    mel.eval('setDrivenKeyframe -currentDriver lThighBend.rotateZ {0}.pJCMMonique8ThighSideSideL;'.format(
        pyBlendShapesName))
    mel.eval('setAttr "lThighBend.rotateZ" 0;')

    # pJCMMonique8ThighSideSideR
    mel.eval('setAttr "rThighBend.rotateZ" 0;')
    mel.eval('setDrivenKeyframe -currentDriver rThighBend.rotateZ {0}.pJCMMonique8ThighSideSideR;'.format(
        pyBlendShapesName))
    mel.eval('setAttr "rThighBend.rotateZ" -85;')
    mel.eval('setAttr "{0}.pJCMMonique8ThighSideSideR" 1;'.format(pyBlendShapesName))
    mel.eval('setDrivenKeyframe -currentDriver rThighBend.rotateZ {0}.pJCMMonique8ThighSideSideR;'.format(
        pyBlendShapesName))
    mel.eval('setAttr "rThighBend.rotateZ" 0;')
