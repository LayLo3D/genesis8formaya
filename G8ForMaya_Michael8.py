import maya.mel as mel
import maya.cmds as cmds


def michael8JCMs(character, blendShapeGroup):
    pyBlendShapesName = blendShapeGroup
    if not cmds.objExists('{}.pJCMMichael8AbdomenUpperBndFwd'.format(pyBlendShapesName)):
        error = character + ' specific JCMs were not created.\n\n' + character + ' JCMs were either not exported out properly or the wrong file was selected for import.'
        cmds.warning(error.replace('\n\n', ' '))
        return error

    # pJCMMichael8AbdomenUpperBndFwd
    mel.eval('setAttr "abdomenUpper.rotateX" 0;')
    mel.eval('setDrivenKeyframe -currentDriver abdomenUpper.rotateX {0}.pJCMMichael8AbdomenUpperBndFwd;'.format(
        pyBlendShapesName))
    mel.eval('setAttr "abdomenUpper.rotateX" 40;')
    mel.eval('setAttr "{0}.pJCMMichael8AbdomenUpperBndFwd" 1;'.format(pyBlendShapesName))
    mel.eval('setDrivenKeyframe -currentDriver abdomenUpper.rotateX {0}.pJCMMichael8AbdomenUpperBndFwd;'.format(
        pyBlendShapesName))
    mel.eval('setAttr "abdomenUpper.rotateX" 0;')

    # pJCMMichael8ChestLowerBndFwd
    mel.eval('setAttr "chestLower.rotateX" 0;')
    mel.eval('setDrivenKeyframe -currentDriver chestLower.rotateX {0}.pJCMMichael8ChestLowerBndFwd;'.format(
        pyBlendShapesName))
    mel.eval('setAttr "chestLower.rotateX" 35;')
    mel.eval('setAttr "{0}.pJCMMichael8ChestLowerBndFwd" 1;'.format(pyBlendShapesName))
    mel.eval('setDrivenKeyframe -currentDriver chestLower.rotateX {0}.pJCMMichael8ChestLowerBndFwd;'.format(
        pyBlendShapesName))
    mel.eval('setAttr "chestLower.rotateX" 0;')

    # pJCMMichael8CollarBndUpL
    mel.eval('setAttr "lCollar.rotateZ" 0;')
    mel.eval(
        'setDrivenKeyframe -currentDriver lCollar.rotateZ {0}.pJCMMichael8CollarBndUpL;'.format(pyBlendShapesName))
    mel.eval('setAttr "lCollar.rotateZ" 55;')
    mel.eval('setAttr "{0}.pJCMMichael8CollarBndUpL" 1;'.format(pyBlendShapesName))
    mel.eval(
        'setDrivenKeyframe -currentDriver lCollar.rotateZ {0}.pJCMMichael8CollarBndUpL;'.format(pyBlendShapesName))
    mel.eval('setAttr "lCollar.rotateZ" 0;')

    # pJCMMichael8CollarBndUpR
    mel.eval('setAttr "rCollar.rotateZ" 0;')
    mel.eval(
        'setDrivenKeyframe -currentDriver rCollar.rotateZ {0}.pJCMMichael8CollarBndUpR;'.format(pyBlendShapesName))
    mel.eval('setAttr "rCollar.rotateZ" -55;')
    mel.eval('setAttr "{0}.pJCMMichael8CollarBndUpR" 1;'.format(pyBlendShapesName))
    mel.eval(
        'setDrivenKeyframe -currentDriver rCollar.rotateZ {0}.pJCMMichael8CollarBndUpR;'.format(pyBlendShapesName))
    mel.eval('setAttr "rCollar.rotateZ" 0;')

    # pJCMMichael8FootBndFwdL
    mel.eval('setAttr "lFoot.rotateX" 0;')
    mel.eval('setDrivenKeyframe -currentDriver lFoot.rotateX {0}.pJCMMichael8FootBndFwdL;'.format(pyBlendShapesName))
    mel.eval('setAttr "lFoot.rotateX" -45;')
    mel.eval('setAttr "{0}.pJCMMichael8FootBndFwdL" 1;'.format(pyBlendShapesName))
    mel.eval('setDrivenKeyframe -currentDriver lFoot.rotateX {0}.pJCMMichael8FootBndFwdL;'.format(pyBlendShapesName))
    mel.eval('setAttr "lFoot.rotateX" 0;')

    # pJCMMichael8FootBndFwdR
    mel.eval('setAttr "rFoot.rotateX" 0;')
    mel.eval('setDrivenKeyframe -currentDriver rFoot.rotateX {0}.pJCMMichael8FootBndFwdR;'.format(pyBlendShapesName))
    mel.eval('setAttr "rFoot.rotateX" -45;')
    mel.eval('setAttr "{0}.pJCMMichael8FootBndFwdR" 1;'.format(pyBlendShapesName))
    mel.eval('setDrivenKeyframe -currentDriver rFoot.rotateX {0}.pJCMMichael8FootBndFwdR;'.format(pyBlendShapesName))
    mel.eval('setAttr "rFoot.rotateX" 0;')

    # pJCMMichael8ForearmBndL_HDLv2
    mel.eval('setAttr "lForearmBend.rotateY" 0;')
    mel.eval('setDrivenKeyframe -currentDriver lForearmBend.rotateY {0}.pJCMMichael8ForearmBndL_HDLv2;'.format(
        pyBlendShapesName))
    mel.eval('setAttr "lForearmBend.rotateY" -135;')
    mel.eval('setAttr "{0}.pJCMMichael8ForearmBndL_HDLv2" 1;'.format(pyBlendShapesName))
    mel.eval('setDrivenKeyframe -currentDriver lForearmBend.rotateY {0}.pJCMMichael8ForearmBndL_HDLv2;'.format(
        pyBlendShapesName))
    mel.eval('setAttr "lForearmBend.rotateY" 0;')

    # pJCMMichael8ForearmBndR_HDLv2
    mel.eval('setAttr "rForearmBend.rotateY" 0;')
    mel.eval('setDrivenKeyframe -currentDriver rForearmBend.rotateY {0}.pJCMMichael8ForearmBndR_HDLv2;'.format(
        pyBlendShapesName))
    mel.eval('setAttr "rForearmBend.rotateY" 135;')
    mel.eval('setAttr "{0}.pJCMMichael8ForearmBndR_HDLv2" 1;'.format(pyBlendShapesName))
    mel.eval('setDrivenKeyframe -currentDriver rForearmBend.rotateY {0}.pJCMMichael8ForearmBndR_HDLv2;'.format(
        pyBlendShapesName))
    mel.eval('setAttr "rForearmBend.rotateY" 0;')

    # pJCMMichael8ShinBndBckL_105
    mel.eval('setAttr "lShin.rotateX" 0;')
    mel.eval(
        'setDrivenKeyframe -currentDriver lShin.rotateX {0}.pJCMMichael8ShinBndBckL_105;'.format(pyBlendShapesName))
    mel.eval('setAttr "lShin.rotateX" 105;')
    mel.eval('setAttr "{0}.pJCMMichael8ShinBndBckL_105" 1;'.format(pyBlendShapesName))
    mel.eval(
        'setDrivenKeyframe -currentDriver lShin.rotateX {0}.pJCMMichael8ShinBndBckL_105;'.format(pyBlendShapesName))
    mel.eval('setAttr "lShin.rotateX" 0;')

    # pJCMMichael8ShinBndBckR_105
    mel.eval('setAttr "rShin.rotateX" 0;')
    mel.eval(
        'setDrivenKeyframe -currentDriver rShin.rotateX {0}.pJCMMichael8ShinBndBckR_105;'.format(pyBlendShapesName))
    mel.eval('setAttr "rShin.rotateX" 105;')
    mel.eval('setAttr "{0}.pJCMMichael8ShinBndBckR_105" 1;'.format(pyBlendShapesName))
    mel.eval(
        'setDrivenKeyframe -currentDriver rShin.rotateX {0}.pJCMMichael8ShinBndBckR_105;'.format(pyBlendShapesName))
    mel.eval('setAttr "rShin.rotateX" 0;')

    # pJCMMichael8ShinBndBckL_HDLv2
    mel.eval('setAttr "lShin.rotateX" 0;')
    mel.eval(
        'setDrivenKeyframe -currentDriver lShin.rotateX {0}.pJCMMichael8ShinBndBckL_HDLv2;'.format(pyBlendShapesName))
    mel.eval('setAttr "lShin.rotateX" 155;')
    mel.eval('setAttr "{0}.pJCMMichael8ShinBndBckL_HDLv2" 1;'.format(pyBlendShapesName))
    mel.eval(
        'setDrivenKeyframe -currentDriver lShin.rotateX {0}.pJCMMichael8ShinBndBckL_HDLv2;'.format(pyBlendShapesName))
    mel.eval('setAttr "lShin.rotateX" 0;')

    # pJCMMichael8ShinBndBckR_HDLv2
    mel.eval('setAttr "rShin.rotateX" 0;')
    mel.eval(
        'setDrivenKeyframe -currentDriver rShin.rotateX {0}.pJCMMichael8ShinBndBckR_HDLv2;'.format(pyBlendShapesName))
    mel.eval('setAttr "rShin.rotateX" 155;')
    mel.eval('setAttr "{0}.pJCMMichael8ShinBndBckR_HDLv2" 1;'.format(pyBlendShapesName))
    mel.eval(
        'setDrivenKeyframe -currentDriver rShin.rotateX {0}.pJCMMichael8ShinBndBckR_HDLv2;'.format(pyBlendShapesName))
    mel.eval('setAttr "rShin.rotateX" 0;')

    # pJCMMichael8ShoulderBndDownL_n35_HDLv3
    mel.eval('setAttr "lShldrBend.rotateZ" 0;')
    mel.eval('setDrivenKeyframe -currentDriver lShldrBend.rotateZ {0}.pJCMMichael8ShoulderBndDownL_n35_HDLv3;'.format(
        pyBlendShapesName))
    mel.eval('setAttr "lShldrBend.rotateZ" -35;')
    mel.eval('setAttr "{0}.pJCMMichael8ShoulderBndDownL_n35_HDLv3" 1;'.format(pyBlendShapesName))
    mel.eval('setDrivenKeyframe -currentDriver lShldrBend.rotateZ {0}.pJCMMichael8ShoulderBndDownL_n35_HDLv3;'.format(
        pyBlendShapesName))
    mel.eval('setAttr "lShldrBend.rotateZ" 0;')

    # pJCMMichael8ShoulderBndDownR_35_HDLv3
    mel.eval('setAttr "rShldrBend.rotateZ" 0;')
    mel.eval('setDrivenKeyframe -currentDriver rShldrBend.rotateZ {0}.pJCMMichael8ShoulderBndDownR_35_HDLv3;'.format(
        pyBlendShapesName))
    mel.eval('setAttr "rShldrBend.rotateZ" 35;')
    mel.eval('setAttr "{0}.pJCMMichael8ShoulderBndDownR_35_HDLv3" 1;'.format(pyBlendShapesName))
    mel.eval('setDrivenKeyframe -currentDriver rShldrBend.rotateZ {0}.pJCMMichael8ShoulderBndDownR_35_HDLv3;'.format(
        pyBlendShapesName))
    mel.eval('setAttr "rShldrBend.rotateZ" 0;')

    # pJCMMichael8ShoulderBndUpL_HDLv2
    mel.eval('setAttr "lShldrBend.rotateZ" 0;')
    mel.eval('setDrivenKeyframe -currentDriver lShldrBend.rotateZ {0}.pJCMMichael8ShoulderBndUpL_HDLv2;'.format(
        pyBlendShapesName))
    mel.eval('setAttr "lShldrBend.rotateZ" 90;')
    mel.eval('setAttr "{0}.pJCMMichael8ShoulderBndUpL_HDLv2" 1;'.format(pyBlendShapesName))
    mel.eval('setDrivenKeyframe -currentDriver lShldrBend.rotateZ {0}.pJCMMichael8ShoulderBndUpL_HDLv2;'.format(
        pyBlendShapesName))
    mel.eval('setAttr "lShldrBend.rotateZ" 0;')

    # pJCMMichael8ShoulderBndUpR_HDLv2
    mel.eval('setAttr "rShldrBend.rotateZ" 0;')
    mel.eval('setDrivenKeyframe -currentDriver rShldrBend.rotateZ {0}.pJCMMichael8ShoulderBndUpR_HDLv2;'.format(
        pyBlendShapesName))
    mel.eval('setAttr "rShldrBend.rotateZ" -90;')
    mel.eval('setAttr "{0}.pJCMMichael8ShoulderBndUpR_HDLv2" 1;'.format(pyBlendShapesName))
    mel.eval('setDrivenKeyframe -currentDriver rShldrBend.rotateZ {0}.pJCMMichael8ShoulderBndUpR_HDLv2;'.format(
        pyBlendShapesName))
    mel.eval('setAttr "rShldrBend.rotateZ" 0;')

    # pJCMMichael8ShoulderFwdL_n75
    mel.eval('setAttr "lShldrBend.rotateY" 0;')
    mel.eval('setDrivenKeyframe -currentDriver lShldrBend.rotateY {0}.pJCMMichael8ShoulderFwdL_n75;'.format(
        pyBlendShapesName))
    mel.eval('setAttr "lShldrBend.rotateY" -75;')
    mel.eval('setAttr "{0}.pJCMMichael8ShoulderFwdL_n75" 1;'.format(pyBlendShapesName))
    mel.eval('setDrivenKeyframe -currentDriver lShldrBend.rotateY {0}.pJCMMichael8ShoulderFwdL_n75;'.format(
        pyBlendShapesName))
    mel.eval('setAttr "lShldrBend.rotateY" 0;')

    # pJCMMichael8ShoulderFwdR_75
    mel.eval('setAttr "rShldrBend.rotateY" 0;')
    mel.eval('setDrivenKeyframe -currentDriver rShldrBend.rotateY {0}.pJCMMichael8ShoulderFwdR_75;'.format(pyBlendShapesName))
    mel.eval('setAttr "rShldrBend.rotateY" 75;')
    mel.eval('setAttr "{0}.pJCMMichael8ShoulderFwdR_75" 1;'.format(pyBlendShapesName))
    mel.eval('setDrivenKeyframe -currentDriver rShldrBend.rotateY {0}.pJCMMichael8ShoulderFwdR_75;'.format(pyBlendShapesName))
    mel.eval('setAttr "rShldrBend.rotateY" 0;')

    # pJCMMichael8ThighSideSideL
    mel.eval('setAttr "lThighBend.rotateZ" 0;')
    mel.eval('setDrivenKeyframe -currentDriver lThighBend.rotateZ {0}.pJCMMichael8ThighSideSideL;'.format(
        pyBlendShapesName))
    mel.eval('setAttr "lThighBend.rotateZ" 85;')
    mel.eval('setAttr "{0}.pJCMMichael8ThighSideSideL" 1;'.format(pyBlendShapesName))
    mel.eval('setDrivenKeyframe -currentDriver lThighBend.rotateZ {0}.pJCMMichael8ThighSideSideL;'.format(
        pyBlendShapesName))
    mel.eval('setAttr "lThighBend.rotateZ" 0;')

    # pJCMMichael8ThighSideSideR
    mel.eval('setAttr "rThighBend.rotateZ" 0;')
    mel.eval('setDrivenKeyframe -currentDriver rThighBend.rotateZ {0}.pJCMMichael8ThighSideSideR;'.format(
        pyBlendShapesName))
    mel.eval('setAttr "rThighBend.rotateZ" -85;')
    mel.eval('setAttr "{0}.pJCMMichael8ThighSideSideR" 1;'.format(pyBlendShapesName))
    mel.eval('setDrivenKeyframe -currentDriver rThighBend.rotateZ {0}.pJCMMichael8ThighSideSideR;'.format(
        pyBlendShapesName))
    mel.eval('setAttr "rThighBend.rotateZ" 0;')
