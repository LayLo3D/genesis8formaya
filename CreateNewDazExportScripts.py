character_names = [
        "Aiko8",
        "Alexandra8",
        "Charlotte8",
        "Darius8",
        "Edie8",
        "Edward8",
        "Eva8",
        "Floyd8",
        "Genesis8Female",
        "Genesis8Male",
        "Gia8",
        "Girl8",
        "Karyssa8",
        "Latonya8",
        "Lee8",
        "Lucas8",
        "Mabel8",
        "MeiLin8",
        "Michael8",
        "Mika8",
        "Monique8",
        "Nix8",
        "Ollie8",
        "Olympia8",
        "Owen8",
        "Penny8",
        "Sakura8",
        "Stephanie8",
        "TeenJosie8",
        "TeenKaylee8",
        "TheBrute8",
        "ToonDwayne8",
        "Victoria8",
        "Vladimir8",
        "Zelara8",
    ]


def update_files():
    print('Updating files...')
    character_jcms_folder = [
        "Hidden/People/Aiko 8/pJCMs",
        "Hidden/People/Alexandra 8/pJCMs",
        "Hidden/People/Charlotte 8/pJCMs",
        "Hidden/People/Darius 8/pJCMs",
        "Hidden/People/Edie 8/pJCMs",
        "Hidden/People/Edward 8/pJCMs",
        "Hidden/People/Eva 8/pJCMs",
        "Hidden/People/Floyd 8/pJCMs",
        "",
        "",
        "Hidden/People/Gia 8/pJCMs",
        "Hidden/People/Girl8/pJCMs",
        "Hidden/People/Karyssa 8/JCMs",
        "Hidden/People/Latonya 8/pJCMs",
        "Hidden/People/Lee 8/pJCMs",
        "Hidden/People/Lucas 8/pJCMs",
        "Hidden/People/Mabel 8/pJCMs",
        "Hidden/People/Mei Lin 8/pJCMs",
        "Hidden/People/Michael 8/pJCMs",
        "",
        "Hidden/People/Monique 8/pJCMs",
        "Hidden/People/Nix 8/pJCMs",
        "Hidden/People/Ollie 8/pJCMs",
        "Hidden/People/Olympia8/pJCMs",
        "Hidden/People/Owen 8/pJCMs",
        "Hidden/People/Penny 8/pJCMs",
        "Hidden/People/Sakura 8",
        "Hidden/People/Stephanie 8/pJCMs",
        "",
        "Hidden/People/Teen Kaylee 8/JCMs",
        "Hidden/People/The Brute 8/pJCMs",
        "Hidden/People/Toon Dwayne 8/pJCMs",
        "Hidden/People/Victoria 8/pJCMs",
        "Hidden/People/Vladimir 8/pJCMs",
        "Hidden/People/Zelara 8/pJCMs",
    ]

    extra_pjcms = {
        'Alexandra8': [
            'pJCMAlexandra8_HeadBndDn_HDLv3',
            'pJCMAlexandra8_NeckUpperTwist_HDLv3L',
            'pJCMAlexandra8_NeckUpperTwist_HDLv3R',
            'pJCMAlexandra8_ShoulderBndDn_15_HDLv3R',
            'pJCMAlexandra8_ShoulderBndDn_n15_HDLv3L'
        ]
    }

    in_file = "D:/Users/LayLo/Documents/maya/plug-ins/Genesis8ForMaya/Genesis8_ExportScript.dsa"

    export_folder = "D:/Scripts/G8 Encrypted/New/"

    for i, character in enumerate(character_names):
        lines_of_text = []

        with open(in_file) as fp:
            for line in fp:
                if 'var exportRules = "head.\\n2\\nCTRLMD\\n2";' in line:
                    if character in extra_pjcms:
                        extra_morph_rules = ''
                        for jcm in extra_pjcms[character]:
                            extra_morph_rules += '\\n' + jcm + '\\n1'

                        lines_of_text.append(
                            line.replace(
                                'var exportRules = "head.\\n2\\nCTRLMD\\n2";',
                                'var exportRules = "head.\\n2\\nCTRLMD\\n2' + extra_morph_rules + '";'
                            ).rstrip('\n')
                        )
                    else:
                        lines_of_text.append(line.rstrip('\n'))

                elif 'var pJCMsPath = "";' in line:
                    # print(
                    #     line.replace(
                    #         'var pJCMsPath = "";', 'var pJCMsPath = "' + character_jcms_folder[i] + '";'
                    #     ).rstrip('\n'))
                    lines_of_text.append(
                        line.replace(
                            'var pJCMsPath = "";', 'var pJCMsPath = "' + character_jcms_folder[i] + '";'
                        ).rstrip('\n')
                    )
                elif 'oSettings.setBoolValue("doCopyTextures", false);' in line:
                    lines_of_text.append(line.replace('false', 'true').rstrip('\n'))
                else:
                    # print(line.rstrip('\n'))
                    lines_of_text.append(line.rstrip('\n'))

        with open(export_folder + character + '_Export.dsa', 'a') as the_file:
            the_file.truncate()
            for line in lines_of_text:
                the_file.write(line + '\n')


def get_jcm_folder_names():
    for file_name in character_names:
        file_name = 'D:\Scripts\G8 Encrypted\\' + file_name
        file_name += '_Export.dsa'

        with open(file_name) as fp:
            for line in fp:
                if 'var pJCMsPath = ' in line:
                    print(line.strip())

update_files()
