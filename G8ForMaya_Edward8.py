import maya.mel as mel
import maya.cmds as cmds


def Edward8JCMs(character, blendShapeGroup):
    pyBlendShapesName = blendShapeGroup
    if not cmds.objExists('{}.pJCM_Edward8_Abdomen2Side_24_L'.format(pyBlendShapesName)):
        error = character + ' specific JCMs were not created.\n\n' + character + ' JCMs were either not exported out properly or the wrong file was selected for import.'
        cmds.warning(error.replace('\n\n', ' '))
        return error

    # pJCM_Edward8_Abdomen2Side_24_L
    mel.eval('setAttr "abdomenLower.rotateZ" 0;')
    mel.eval(
        'setDrivenKeyframe -currentDriver abdomenLower.rotateZ {0}.pJCM_Edward8_Abdomen2Side_24_L;'.format(pyBlendShapesName))
    mel.eval('setAttr "abdomenLower.rotateZ" -24;')
    mel.eval('setAttr "{0}.pJCM_Edward8_Abdomen2Side_24_L" 1;'.format(pyBlendShapesName))
    mel.eval(
        'setDrivenKeyframe -currentDriver abdomenLower.rotateZ {0}.pJCM_Edward8_Abdomen2Side_24_L;'.format(pyBlendShapesName))
    mel.eval('setAttr "abdomenLower.rotateZ" 0;')

    # pJCM_Edward8_Abdomen2Side_24_R
    mel.eval('setAttr "abdomenLower.rotateZ" 0;')
    mel.eval(
        'setDrivenKeyframe -currentDriver abdomenLower.rotateZ {0}.pJCM_Edward8_Abdomen2Side_24_R;'.format(pyBlendShapesName))
    mel.eval('setAttr "abdomenLower.rotateZ" 24;')
    mel.eval('setAttr "{0}.pJCM_Edward8_Abdomen2Side_24_R" 1;'.format(pyBlendShapesName))
    mel.eval(
        'setDrivenKeyframe -currentDriver abdomenLower.rotateZ {0}.pJCM_Edward8_Abdomen2Side_24_R;'.format(pyBlendShapesName))
    mel.eval('setAttr "abdomenLower.rotateZ" 0;')
    
    # pJCM_Edward8_ChestSide_20_L
    mel.eval('setAttr "chestLower.rotateZ" 0;')
    mel.eval('setDrivenKeyframe -currentDriver chestLower.rotateZ {0}.pJCM_Edward8_ChestSide_20_L;'.format(pyBlendShapesName))
    mel.eval('setAttr "chestLower.rotateZ" -20;')
    mel.eval('setAttr "{0}.pJCM_Edward8_ChestSide_20_L" 1;'.format(pyBlendShapesName))
    mel.eval('setDrivenKeyframe -currentDriver chestLower.rotateZ {0}.pJCM_Edward8_ChestSide_20_L;'.format(pyBlendShapesName))
    mel.eval('setAttr "chestLower.rotateZ" 0;')

    # pJCM_Edward8_ChestSide_20_R
    mel.eval('setAttr "chestLower.rotateZ" 0;')
    mel.eval('setDrivenKeyframe -currentDriver chestLower.rotateZ {0}.pJCM_Edward8_ChestSide_20_R;'.format(pyBlendShapesName))
    mel.eval('setAttr "chestLower.rotateZ" 20;')
    mel.eval('setAttr "{0}.pJCM_Edward8_ChestSide_20_R" 1;'.format(pyBlendShapesName))
    mel.eval('setDrivenKeyframe -currentDriver chestLower.rotateZ {0}.pJCM_Edward8_ChestSide_20_R;'.format(pyBlendShapesName))
    mel.eval('setAttr "chestLower.rotateZ" 0;')
