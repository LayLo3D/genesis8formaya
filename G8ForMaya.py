import maya.mel as mel
import maya.cmds as cmds
import os
import re
import sys
import json

GAME_ENGINE_ENABLED = False
DNABLOCK_MODE = False
REMOVE_FACE_JOINTS = False

VERSION = ''
SUPPORTED_FIGURES = ['Genesis8Female', 'Genesis8Male', 'Genesis8_1Female', 'Genesis8_1Male']

plugin_path = os.path.expandvars("$HOME/maya/plug-ins/Genesis8ForMaya")
plugin_path_mac = "/users/Shared/Autodesk/maya/plug-ins/Genesis8ForMaya"
if os.path.exists(plugin_path_mac):
    plugin_path = plugin_path_mac

G8_FOR_MAYA_IMG = plugin_path + '/g8_for_maya.png'
LL3D_LOGO = plugin_path + '/LL3D_Logo.png'
LOG_FILE = plugin_path + '/log.txt'
MAYA_VERSION = cmds.about(version=True)
FBX_ASCII_CODE = re.compile(r'FBXASC\d{3}')
PYTHON_VERSION = sys.version_info.major

errors = []
num_errors = 0
pose_controls_list = []
report_bug_msg = 'An error occurred. If you believe this error occurred due to a bug ' \
               'please email the log file located at : ' + LOG_FILE + ' to LayLo3D@gmail.com.'

config_hik_for_mb = False


def show_invalid_character_dialog():
    cmds.confirmDialog(
        title='Error',
        icon='information',
        message='Unable to find a character supported by Genesis 8 for Maya.'
                '\nBoth Genesis 8 and 8.1 characters are supported.'
                '\n'
                '\nPlease make sure you are trying to import a'
                '\nGenesis 8 for Maya FBX file and try again.',
        button='Ok',
        defaultButton='Ok',
        cancelButton='Ok',
        dismissString='Ok'
    )


def zero_joint_rotate_axes():
    print("Fixing joint rotations for game engines...")
    temp_folder = '{}/temp'.format(plugin_path)
    if not os.path.exists(temp_folder):
        os.makedirs(temp_folder)

    def _export_skin_weights():
        return cmds.deformerWeights(
            mesh_name + '.xml', path='{}/'.format(temp_folder), export=True, deformer=skin_cluster, format='XML')

    def _zero_joint_rotate_axes():
        for joint in cmds.ls(type='joint'):
            cmds.joint(joint, edit=True, zeroScaleOrient=True)
            cmds.makeIdentity(joint, apply=True)

    def _rebind_skin(skin_cluster_influences):
        for influence in skin_cluster_influences:
            new_skin_cluster = cmds.skinCluster(
                influence[1], influence[0], name=influence[0] + 'SkinCluster', toSelectedBones=True, skinMethod=0)
            # skinMethod=1 dual quaternion
            # skinMethod=0 classic linear

            influence[1] = new_skin_cluster[0]

        return skin_cluster_influences

    existing_skin_cluster_influences = []
    for skin_cluster in cmds.ls(type='skinCluster'):
        mesh_name = cmds.skinCluster(skin_cluster, query=True, geometry=True)[0]
        joint_names = cmds.skinCluster(skin_cluster, query=1, influence=1)

        if (mesh_name == 'Genesis8FemaleFBXASC046ShapeShape'
                and cmds.polyEvaluate('Genesis8FemaleFBXASC046ShapeShape', vertex=True) == 65806):
            file_path = plugin_path + '/SubD1-G8F.xml'
        elif (mesh_name == 'Genesis8FemaleEyelashesFBXASC046ShapeShape'
              and cmds.polyEvaluate('Genesis8FemaleEyelashesFBXASC046ShapeShape', vertex=True) == 1620):
            file_path = plugin_path + '/SubD1-G8F-Eyelashes.xml'
        else:
            file_path = _export_skin_weights()

        existing_skin_cluster_influences.append([mesh_name, joint_names, file_path])
        cmds.skinCluster(skin_cluster, edit=True, unbind=True)

    _zero_joint_rotate_axes()
    new_skin_clusters = _rebind_skin(existing_skin_cluster_influences)
    for cluster in new_skin_clusters:
        path, file_name = cluster[2].rsplit('/', 1)
        print(cluster[0], path, file_name)
        cmds.deformerWeights(file_name, path=path, deformer=cluster[1], im=True, method='index')
        cmds.skinCluster(cluster[1], edit=True, forceNormalizeWeights=True)
        # mel.eval(
        #     'deformerWeights -import -method "index" -deformer "{}" -path "{}" "{}"; '
        #     'skinCluster -e -forceNormalizeWeights "{}";'.format(
        #         cluster[1], path, file_name, cluster[1])
        # )
        if not cluster[2].endswith(('SubD1-G8F.xml', 'SubD1-G8F-Eyelashes.xml')):
            os.remove(cluster[2])

    # os.rmdir(temp_folder)


def remove_empty_blend_shapes():
    def _remove_blend_shape():
        print("Removing useless blend shape: {}".format(alias))
        if '2016 Extension 2' in MAYA_VERSION or int(MAYA_VERSION[:4]) >= 2017:
            mel.eval('blendShapeDeleteTargetGroup "{}" "{}";'.format(blend_shape_node, target_index))
        else:
            cmds.removeMultiInstance('{}.weight[{}]'.format(blend_shape_node, target_index), b=True)
            cmds.aliasAttr(blend_shape_node + '.' + alias, remove=True)

    for blend_shape_node in cmds.ls(type='blendShape'):
        input_target_group = cmds.getAttr(blend_shape_node + ".inputTarget[0].inputTargetGroup", mi=True)
        for target_index in input_target_group:
            target_index = str(target_index)
            alias = cmds.listAttr('{}.weight[{}]'.format(blend_shape_node, target_index), m=True)[0]
            delta_offsets = cmds.getAttr(
                '{}.inputTarget[0].inputTargetGroup[{}].inputTargetItem[6000].inputPointsTarget'.format(
                    blend_shape_node, target_index))
            # print(target_index, alias, delta_offsets)
            # print cmds.getAttr(
            #     '{}.inputTarget[0].inputTargetGroup[{}].inputTargetItem[6000].inputComponentsTarget'.format(
            #         blend_shape_node, target_index))

            if not delta_offsets:
                _remove_blend_shape()
            else:
                keep = False
                for offset in delta_offsets:
                    if offset[0] < 0.0001 and offset[1] < 0.0001 and offset[2] < 0.0001:
                        continue
                    else:
                        keep = True
                        break

                if not keep:
                    _remove_blend_shape()

        if not cmds.getAttr(blend_shape_node + ".inputTarget[0].inputTargetGroup", mi=True):
            cmds.delete(blend_shape_node)


def get_blend_shape_target_index(blend_shape, target):
    """
    Get the target index of the specified blendShape and target name
    @param blend_shape: Name of blendShape to get target index for
    @type blend_shape: str
    @param target: BlendShape target to get the index for
    @type target: str
    """

    # Get attribute alias
    alias_list = cmds.aliasAttr(blend_shape, q=True)
    alias_index = alias_list.index(target)
    alias_attr = alias_list[alias_index + 1]

    # Get index
    target_index = int(alias_attr.split('[')[-1].split(']')[0])

    # Return result
    return target_index


def clean_blend_shapes_names(figure):
    global pose_controls_list
    blend_shapes_list = cmds.aliasAttr(figure, query=True)
    if not blend_shapes_list:
        return

    blend_shapes_list = blend_shapes_list[::2]
    for alias in blend_shapes_list:
        expression = False
        if 'eCTRL' in alias or 'pCTRL' in alias or 'ePHM' in alias:
            expression = True

        fixed_alias = alias
        if 'CTRLMD' not in fixed_alias:
            fixed_alias = fixed_alias.split('__', 1)[-1]
        fixed_alias = FBX_ASCII_CODE.sub('_', fixed_alias)
        fixed_alias = fixed_alias.replace('eCTRL', '')
        fixed_alias = fixed_alias.replace('pCTRL', '')
        fixed_alias = fixed_alias.replace('CTRL_', '')
        fixed_alias = fixed_alias.replace('CTRL', '')  # Comment out if needing proper names for pose controllers
        fixed_alias = fixed_alias.replace('eJCM', '')
        # fixed_alias = fixed_alias.replace('FHM', '')
        if fixed_alias.startswith('FHM'):
            fixed_alias = fixed_alias[3:]
        fixed_alias = fixed_alias.replace('ePHM', '')
        fixed_alias = fixed_alias.replace('PHM', '')
        fixed_alias = fixed_alias.replace('FBM', '')
        fixed_alias = fixed_alias.replace('_div2', '')  # Comment out if needing proper names for pose controllers
        fixed_alias = fixed_alias.replace('Shape', '')
        fixed_alias = fixed_alias.lstrip('0123456789.- _')
        if 'PBMNavel' not in alias:
            fixed_alias = fixed_alias.replace('PBM', '')
        if '_HDL' not in alias:  # Comment out if needing proper names for pose controllers, maybe?
            fixed_alias = fixed_alias.replace('_HD', '')
        if alias == fixed_alias:
            continue

        original_alias = figure + '.' + alias
        try:
            cmds.aliasAttr(fixed_alias, original_alias)
            if expression and fixed_alias not in pose_controls_list:
                pose_controls_list.append(fixed_alias)
        except RuntimeError:
            print(alias, "couldn't be renamed to", fixed_alias)
            pass


def remove_blend_targets():
    camera_list = cmds.ls(cameras=True)
    camera_list = camera_list + cmds.listRelatives(camera_list, parent=True)
    objList = cmds.ls(invisible=True, type=['transform', 'geometryShape'])
    for obj in objList:
        if obj not in camera_list:
            try:
                cmds.delete(obj)
            except:
                pass


def merge_namespace():
    joints_list = cmds.ls(type='joint')
    remove_prefix = False
    i = 0
    for joint in joints_list:
        if '_Genesis8Female' in joint or '_Genesis8Male' in joint:
            if not remove_prefix:
                prefix = joint.split('_')[0] + '_'
                remove_prefix = True
        if ':' in joint and i < 1:
            try:
                print('Merging namespace...')
                remove_name_space = joint.split(':')[0]
                cmds.namespace(mergeNamespaceWithRoot=True, removeNamespace='{}'.format(remove_name_space))
                i += 1
            except:
                pass

    if remove_prefix:
        objList = cmds.ls()
        for obj in objList:
            obj_new_name = obj.replace(prefix, '')
            try:
                cmds.rename(obj, obj_new_name)
            except RuntimeError:
                pass


def re_parent_and_delete_extra_transform_node(child, parent):
    grand_parent = cmds.listRelatives(parent, parent=True)[0]
    cmds.parent(child, grand_parent)

    cmds.delete(parent)
    if child.endswith('1'):
        cmds.rename(child, child[:-1])


def create_character_group():
    axis = ['x', 'y', 'z']
    attrs = ['t', 'r', 's']
    hipParent = cmds.listRelatives('hip', parent=True, path=True)[0]
    hipParent = cmds.rename(hipParent, hipParent + '_Group')
    cmds.setAttr(hipParent + '.drawStyle', 2)
    hipChildren = cmds.listRelatives(hipParent, children=True)

    if 'Genesis8FemaleGenitalia' in hipChildren:
        genitaliaTransform = cmds.ls('Genesis8FemaleGenitalia', typ='transform')[0]
        genitalia = cmds.listRelatives(genitaliaTransform, children=True)[0]
        cmds.parent(genitalia, hipParent)
        cmds.setAttr(genitalia + '.visibility', 0)
        cmds.delete(genitaliaTransform)
    elif 'GoldenPalace_2254' in hipChildren:
        genitaliaTransform = cmds.ls('GoldenPalace_2254', typ='transform')[0]
        genitalia = cmds.listRelatives(genitaliaTransform, children=True)[0]
        cmds.parent(genitalia, hipParent)
        cmds.setAttr(genitalia + '.visibility', 0)
        cmds.delete(genitaliaTransform)

    if 'G8MGenitalia' in hipChildren:
        genitaliaTransform = cmds.ls('G8MGenitalia', typ='transform')[0]
        genitalia = cmds.listRelatives(genitaliaTransform, children=True)[0]
        cmds.parent(genitalia, hipParent)
        cmds.setAttr(genitalia + '.visibility', 0)
        cmds.delete(genitaliaTransform)
    elif 'Dicktator_Genitalia_G8M' in hipChildren:
        genitaliaTransform = cmds.ls('Dicktator_Genitalia_G8M', typ='transform')[0]
        genitalia = cmds.listRelatives(genitaliaTransform, children=True)[0]
        cmds.parent(genitalia, hipParent)
        cmds.setAttr(genitalia + '.visibility', 0)
        cmds.delete(genitaliaTransform)

    for child in cmds.listRelatives(hipParent, allDescendents=True, fullPath=True, type='transform'):
        if cmds.objExists(child):
            if cmds.objectType(child, isType='transform'):
                cmds.setAttr(child.rsplit('|', 1)[1] + '.inheritsTransform', 0)
                if cmds.listRelatives(child, parent=True)[0] != hipParent:
                    shape_node = cmds.listRelatives(child, allDescendents=True, type='mesh')
                    if shape_node:
                        shape_node = shape_node[0]
                        if not cmds.listConnections(shape_node, type='skinCluster'):
                            cmds.setAttr(child.rsplit('|', 1)[1] + '.inheritsTransform', 1)
                            parent = cmds.listRelatives(child, parent=True)[0]
                            if not cmds.objectType(parent, isType='joint'):
                                re_parent_and_delete_extra_transform_node(child, parent)

                            continue

                    for ax in axis:
                        for attr in attrs:
                            cmds.setAttr(child.rsplit('|', 1)[1] + '.' + attr + ax, lock=0)
                    cmds.parent(child, hipParent)
                    for ax in axis:
                        for attr in attrs:
                            cmds.setAttr(child.rsplit('|', 1)[1] + '.' + attr + ax, lock=1)

                    # parent = cmds.listRelatives(child, parent=True)[0]
                    # reparent_and_delete_extra_transform_node(child, parent)


def obj_renamer():
    objects = cmds.ls()
    for obj_old_name in objects:
        obj_new_name = FBX_ASCII_CODE.sub('_', obj_old_name)
        obj_new_name = obj_new_name.replace('BlendShapes', '_Morphs')
        obj_new_name = obj_new_name.replace('ShapeShapeOrig', 'Shape')
        obj_new_name = obj_new_name.replace('ShapeShape', 'Shape')
        obj_new_name = obj_new_name.replace('_Shape', '')
        obj_new_name = obj_new_name.replace('Shape', '')
        obj_new_name = obj_new_name.lstrip('0123456789.- _')
        if obj_old_name == obj_new_name:
            continue

        try:
            cmds.rename(obj_old_name, obj_new_name)
        except RuntimeError:
            pass


def rid_duplicate_joint_names(gender):
    jointsList = cmds.ls(type='joint', l=True)

    with open(os.path.join(plugin_path, 'g8_joint_list.json')) as json_file:
        g8Joints = ['|{}{}'.format(gender, j) for j in json.load(json_file)]

    nonG8Joints = set(jointsList) - set(g8Joints)
    nonG8Joints = list(sorted(nonG8Joints, key=len, reverse=True))
    for joint in nonG8Joints:
        cmds.rename(joint, '{}_nonG8Joint'.format(joint.rsplit('|', 1)[1]))
    return nonG8Joints


def fix_non_g8_joints(non_g8_joints):
    non_g8_joints = list(sorted(non_g8_joints, key=len))
    for joint in non_g8_joints:
        cmds.rename(joint + '_nonG8Joint', joint.rsplit('|', 1)[1])


def get_gender():
    jointsList = cmds.ls(type='joint')
    for figure in SUPPORTED_FIGURES:
        if figure in jointsList:
            cmds.setAttr('{}.drawStyle'.format(figure), 2)
            print('A {} character detected...'.format(
                re.sub(r"(\w)([A-Z])", r"\1 \2", figure).replace('8', ' 8').replace('_', '.')))
            cmds.rename('{}BlendShapes'.format(figure), '{}_Morphs'.format(figure))
            return figure  # .replace('_1', '')

    if DNABLOCK_MODE:
        root_nodes = cmds.ls(assemblies=True, visible=True)
        if len(root_nodes) == 1:
            return root_nodes[0]  # cmds.rename(root_nodes[0], 'GeFema')

    print('Unexpected figure or naming convention.')
    return


def create_base_jcms(blend_shape_node):
    import G8ForMaya_BaseJCMs
    G8ForMaya_BaseJCMs.createBaseCharacterJCMs(blend_shape_node)


def create_character_jcm_creation_error(character):
    global errors
    global num_errors

    error = '{0} specific JCMs were not created.\n\n' \
            '{0} JCMs were either not exported out properly ' \
            'or the wrong file was selected for import.'.format(character)
    cmds.warning(error.replace('\n\n', ' '))
    if error not in errors:
        errors.append(error)
    num_errors += 1


def create_character_jcms(character, blend_shape_node):
    global errors
    global num_errors

    if character == 'Mika8' or character == 'TeenJosie8':
        # No Mika specific JCMs
        return

    try:
        character_jcm_creator = getattr(__import__('G8ForMaya_' + character), character + 'JCMS')
        error = character_jcm_creator(character, blend_shape_node)
        if error:
            errors.append(error)
            num_errors += 1
    except ImportError:
        print('Something went wrong while trying to create the character specific JCMs.'
              'Character specific JCM file not found.')
        create_character_jcm_creation_error(character)
        pass


def auto_follow_morphs(gender, blend_shape_node):
    character_blend_shape_node = gender + '_Morphs'
    blend_shapes = cmds.aliasAttr(character_blend_shape_node, query=True)
    if blend_shapes:
        blend_shapes = blend_shapes[::2]
        for blend_shape in blend_shapes:
            try:
                cmds.setDrivenKeyframe(
                    blend_shape_node, inTangentType='linear', outTangentType='linear',
                    currentDriver='{}.{}'.format(character_blend_shape_node, blend_shape), driverValue=0,
                    attribute=blend_shape, value=0
                )
                cmds.setDrivenKeyframe(
                    blend_shape_node, inTangentType='linear', outTangentType='linear',
                    currentDriver='{}.{}'.format(character_blend_shape_node, blend_shape), driverValue=1,
                    attribute=blend_shape, value=1
                )
            except ValueError:
                print("{} doesn't have the morph: {}".format(blend_shape_node.replace('_Morphs', ''), blend_shape))
                pass


def group_blend_shapes(blend_shape_node, blend_shape_alias, group_name):
    if not len(blend_shape_alias) > 0:
        return

    def _get_blend_shape_target_indices():
        alias_indices = []
        aliases = cmds.aliasAttr(blend_shape_node, query=True)
        for i, alias in enumerate(aliases):
            if alias in blend_shape_alias:
                alias_indices.append(aliases[i + 1].split('[')[-1].split(']')[0])

        return alias_indices

    def _convert_list_to_mel_string_array(py_list):
        py_list = str([str(x) for x in py_list])
        py_list = py_list.replace("'", "\"")
        py_list = py_list.replace("[", "{")
        py_list = py_list.replace("]", "}")
        return py_list

    def _get_shape_editor_group_index():
        size = cmds.getAttr(blend_shape_node + '.targetDirectory', size=True)
        return str(size - 1)

    blend_shape_targets = ['{}.{}'.format(blend_shape_node, index) for index in _get_blend_shape_target_indices()]
    mel.eval('optionVar -rm blendShapeEditorTreeViewSelection;')
    mel.eval('blendShapeCreateTargetGroup({});'.format(_convert_list_to_mel_string_array(blend_shape_targets)))
    cmds.setAttr('{}.targetDirectory[{}].directoryName'.format(blend_shape_node, _get_shape_editor_group_index()),
                 group_name, type='string')


def determine_blend_shape_grouping(blend_shape_group):
    global pose_controls_list
    pJCMGroupList = []
    flexGroupList = []
    poseControlsGroupList = []
    blendShapes = cmds.aliasAttr(blend_shape_group, query=True)[::2]
    if not blendShapes:
        return

    for blendShape in blendShapes:
        if 'pJCM' in blendShape and 'pJCMFlex' not in blendShape:
            pJCMGroupList.append(blendShape)
        elif 'pJCMFlex' in blendShape and cmds.connectionInfo('{}.{}'.format(blend_shape_group, blendShape),
                                                              isDestination=True):
            flexGroupList.append(blendShape)
        elif blendShape in pose_controls_list:
            poseControlsGroupList.append(blendShape)

    if pJCMGroupList:
        group_blend_shapes(blend_shape_group, pJCMGroupList, 'Joint Corrective Morphs')

    if flexGroupList:
        group_blend_shapes(blend_shape_group, flexGroupList, 'Joint Flex Morphs')

    if poseControlsGroupList:
        group_blend_shapes(blend_shape_group, poseControlsGroupList, 'Expressions and Pose Controls')


def create_jcms(gender, character):
    blend_shape_nodes = cmds.ls(type='blendShape')
    character_blend_shape_node = ''
    for blend_shape_node in blend_shape_nodes:
        if blend_shape_node != gender + '_Morphs':
            print('Setting {} JCMs and flexion morphs to auto-follow the main figure\'s morphs...'.format(
                blend_shape_node.replace('_Morphs', '')))
            auto_follow_morphs(gender, blend_shape_node)
            if '2016 Extension 2' in MAYA_VERSION or int(MAYA_VERSION[:4]) >= 2017:
                determine_blend_shape_grouping(blend_shape_node)
        elif blend_shape_node == gender + '_Morphs':
            character_blend_shape_node = blend_shape_node

    if gender in SUPPORTED_FIGURES:
        print('Creating {} base JCMs and flexion morphs for {}...'.format(
            gender, character_blend_shape_node.replace('_Morphs', '')))
        if DNABLOCK_MODE:
            try:
                create_base_jcms(character_blend_shape_node)
            except RuntimeError:
                print('{} JCM blend shapes not found...'.format(gender))
        else:
            create_base_jcms(character_blend_shape_node)

    if gender != character:
        print('Creating {} specific JCMs for {}...'.format(
            character.replace('8', ' 8'), character_blend_shape_node.replace('_Morphs', '')))
        if DNABLOCK_MODE:
            try:
                create_character_jcms(character, character_blend_shape_node)
            except RuntimeError:
                print('Character specific JCM blend shapes not found...')
        else:
            create_character_jcms(character, character_blend_shape_node)

    if '2016 Extension 2' in MAYA_VERSION or int(MAYA_VERSION[:4]) >= 2017:
        determine_blend_shape_grouping(character_blend_shape_node)

    try:
        set_key_tangents_jcms(character_blend_shape_node)
    except ValueError:
        pass


def set_key_tangents_jcms(blend_shape_group):
    cmds.keyTangent(blend_shape_group, itt='linear', ott='linear')
    cmds.keyTangent('animCurveUU1', itt='linear', ott='linear')
    cmds.keyTangent(blend_shape_group + '_pJCMFlexBiceps_L', itt='linear', ott='linear')
    cmds.keyTangent('animCurveUU2', itt='linear', ott='linear')
    cmds.keyTangent(blend_shape_group + '_pJCMFlexBiceps_R', itt='linear', ott='linear')


def disable_limits():
    joints_list = mel.eval('ls -type joint')
    for j in joints_list:
        mel.eval('transformLimits -erx 0 0 {}'.format(j))
        mel.eval('transformLimits -ery 0 0 {}'.format(j))
        mel.eval('transformLimits -erz 0 0 {}'.format(j))
        mel.eval('transformLimits -etx 0 0 {}'.format(j))
        mel.eval('transformLimits -ety 0 0 {}'.format(j))
        mel.eval('transformLimits -etz 0 0 {}'.format(j))
    print('Joint limits disabled.')


def set_eyelid_driven_keys():
    def _set_set_driven_key_y_l():
        mel.eval('setDrivenKeyframe -itt linear -ott linear-currentDriver lEye.rotateY lEyelidInner.rotateY')
        mel.eval('setDrivenKeyframe -itt linear -ott linear-currentDriver lEye.rotateY lEyelidUpperInner.rotateY')
        mel.eval('setDrivenKeyframe -itt linear -ott linear-currentDriver lEye.rotateY lEyelidUpper.rotateY')
        mel.eval('setDrivenKeyframe -itt linear -ott linear-currentDriver lEye.rotateY lEyelidUpperOuter.rotateY')
        mel.eval('setDrivenKeyframe -itt linear -ott linear-currentDriver lEye.rotateY lEyelidOuter.rotateY')
        mel.eval('setDrivenKeyframe -itt linear -ott linear-currentDriver lEye.rotateY lEyelidLowerOuter.rotateY')
        mel.eval('setDrivenKeyframe -itt linear -ott linear-currentDriver lEye.rotateY lEyelidLower.rotateY')
        mel.eval('setDrivenKeyframe -itt linear -ott linear-currentDriver lEye.rotateY lEyelidLowerInner.rotateY')

    def _set_set_driven_key_y_r():
        mel.eval('setDrivenKeyframe -itt linear -ott linear-currentDriver rEye.rotateY rEyelidInner.rotateY')
        mel.eval('setDrivenKeyframe -itt linear -ott linear-currentDriver rEye.rotateY rEyelidUpperInner.rotateY')
        mel.eval('setDrivenKeyframe -itt linear -ott linear-currentDriver rEye.rotateY rEyelidUpper.rotateY')
        mel.eval('setDrivenKeyframe -itt linear -ott linear-currentDriver rEye.rotateY rEyelidUpperOuter.rotateY')
        mel.eval('setDrivenKeyframe -itt linear -ott linear-currentDriver rEye.rotateY rEyelidOuter.rotateY')
        mel.eval('setDrivenKeyframe -itt linear -ott linear-currentDriver rEye.rotateY rEyelidLowerOuter.rotateY')
        mel.eval('setDrivenKeyframe -itt linear -ott linear-currentDriver rEye.rotateY rEyelidLower.rotateY')
        mel.eval('setDrivenKeyframe -itt linear -ott linear-currentDriver rEye.rotateY rEyelidLowerInner.rotateY')

    def _set_set_driven_key_x_l():
        mel.eval('setDrivenKeyframe -itt linear -ott linear-currentDriver lEye.rotateX lEyelidInner.rotateX')
        mel.eval('setDrivenKeyframe -itt linear -ott linear-currentDriver lEye.rotateX lEyelidUpperInner.rotateX')
        mel.eval('setDrivenKeyframe -itt linear -ott linear-currentDriver lEye.rotateX lEyelidUpper.rotateX')
        mel.eval('setDrivenKeyframe -itt linear -ott linear-currentDriver lEye.rotateX lEyelidUpperOuter.rotateX')
        mel.eval('setDrivenKeyframe -itt linear -ott linear-currentDriver lEye.rotateX lEyelidOuter.rotateX')
        mel.eval('setDrivenKeyframe -itt linear -ott linear-currentDriver lEye.rotateX lEyelidLowerOuter.rotateX')
        mel.eval('setDrivenKeyframe -itt linear -ott linear-currentDriver lEye.rotateX lEyelidLower.rotateX')
        mel.eval('setDrivenKeyframe -itt linear -ott linear-currentDriver lEye.rotateX lEyelidLowerInner.rotateX')

    def _set_set_driven_key_x_r():
        mel.eval('setDrivenKeyframe -itt linear -ott linear-currentDriver rEye.rotateX rEyelidInner.rotateX')
        mel.eval('setDrivenKeyframe -itt linear -ott linear-currentDriver rEye.rotateX rEyelidUpperInner.rotateX')
        mel.eval('setDrivenKeyframe -itt linear -ott linear-currentDriver rEye.rotateX rEyelidUpper.rotateX')
        mel.eval('setDrivenKeyframe -itt linear -ott linear-currentDriver rEye.rotateX rEyelidUpperOuter.rotateX')
        mel.eval('setDrivenKeyframe -itt linear -ott linear-currentDriver rEye.rotateX rEyelidOuter.rotateX')
        mel.eval('setDrivenKeyframe -itt linear -ott linear-currentDriver rEye.rotateX rEyelidLowerOuter.rotateX')
        mel.eval('setDrivenKeyframe -itt linear -ott linear-currentDriver rEye.rotateX rEyelidLower.rotateX')
        mel.eval('setDrivenKeyframe -itt linear -ott linear-currentDriver rEye.rotateX rEyelidLowerInner.rotateX')

    # Eye side-side
    # Left eye 40
    _set_set_driven_key_y_l()
    mel.eval('setAttr "lEye.rotateY" 40')
    mel.eval('setAttr "lEyelidInner.rotateY" 1')
    mel.eval('setAttr "lEyelidUpperInner.rotateY" -5')
    mel.eval('setAttr "lEyelidUpper.rotateY" -5')
    mel.eval('setAttr "lEyelidUpperOuter.rotateY" -5')
    mel.eval('setAttr "lEyelidOuter.rotateY" 5')
    mel.eval('setAttr "lEyelidLowerOuter.rotateY" 5')
    mel.eval('setAttr "lEyelidLower.rotateY" 5')
    mel.eval('setAttr "lEyelidLowerInner.rotateY" 5')
    _set_set_driven_key_y_l()
    mel.eval('setAttr "lEye.rotateY" 0')
    mel.eval('setAttr "lEyelidInner.rotateY" 0')
    mel.eval('setAttr "lEyelidUpperInner.rotateY" 0')
    mel.eval('setAttr "lEyelidUpper.rotateY" 0')
    mel.eval('setAttr "lEyelidUpperOuter.rotateY" 0')
    mel.eval('setAttr "lEyelidOuter.rotateY" 0')
    mel.eval('setAttr "lEyelidLowerOuter.rotateY" 0')
    mel.eval('setAttr "lEyelidLower.rotateY" 0')
    mel.eval('setAttr "lEyelidLowerInner.rotateY" 0')

    # Left eye -30
    _set_set_driven_key_y_l()
    mel.eval('setAttr "lEye.rotateY" -30')
    mel.eval('setAttr "lEyelidInner.rotateY" -0.75')
    mel.eval('setAttr "lEyelidUpperInner.rotateY" 3.75')
    mel.eval('setAttr "lEyelidUpper.rotateY" 3.75')
    mel.eval('setAttr "lEyelidUpperOuter.rotateY" 3.75')
    mel.eval('setAttr "lEyelidOuter.rotateY" -3.75')
    mel.eval('setAttr "lEyelidLowerOuter.rotateY" -3.75')
    mel.eval('setAttr "lEyelidLower.rotateY" -3.75')
    mel.eval('setAttr "lEyelidLowerInner.rotateY" -3.75')
    _set_set_driven_key_y_l()
    mel.eval('setAttr "lEye.rotateY" 0')
    mel.eval('setAttr "lEyelidInner.rotateY" 0')
    mel.eval('setAttr "lEyelidUpperInner.rotateY" 0')
    mel.eval('setAttr "lEyelidUpper.rotateY" 0')
    mel.eval('setAttr "lEyelidUpperOuter.rotateY" 0')
    mel.eval('setAttr "lEyelidOuter.rotateY" 0')
    mel.eval('setAttr "lEyelidLowerOuter.rotateY" 0')
    mel.eval('setAttr "lEyelidLower.rotateY" 0')
    mel.eval('setAttr "lEyelidLowerInner.rotateY" 0')

    # Right eye -40
    _set_set_driven_key_y_r()
    mel.eval('setAttr "rEye.rotateY" -40')
    mel.eval('setAttr "rEyelidInner.rotateY" -1')
    mel.eval('setAttr "rEyelidUpperInner.rotateY" 5')
    mel.eval('setAttr "rEyelidUpper.rotateY" 5')
    mel.eval('setAttr "rEyelidUpperOuter.rotateY" 5')
    mel.eval('setAttr "rEyelidOuter.rotateY" -5')
    mel.eval('setAttr "rEyelidLowerOuter.rotateY" -5')
    mel.eval('setAttr "rEyelidLower.rotateY" -5')
    mel.eval('setAttr "rEyelidLowerInner.rotateY" -5')
    _set_set_driven_key_y_r()
    mel.eval('setAttr "rEye.rotateY" 0')
    mel.eval('setAttr "rEyelidInner.rotateY" 0')
    mel.eval('setAttr "rEyelidUpperInner.rotateY" 0')
    mel.eval('setAttr "rEyelidUpper.rotateY" 0')
    mel.eval('setAttr "rEyelidUpperOuter.rotateY" 0')
    mel.eval('setAttr "rEyelidOuter.rotateY" 0')
    mel.eval('setAttr "rEyelidLowerOuter.rotateY" 0')
    mel.eval('setAttr "rEyelidLower.rotateY" 0')
    mel.eval('setAttr "rEyelidLowerInner.rotateY" 0')

    # Right eye -30
    _set_set_driven_key_y_r()
    mel.eval('setAttr "rEye.rotateY" 30')
    mel.eval('setAttr "rEyelidInner.rotateY" 0.75')
    mel.eval('setAttr "rEyelidUpperInner.rotateY" -3.75')
    mel.eval('setAttr "rEyelidUpper.rotateY" -3.75')
    mel.eval('setAttr "rEyelidUpperOuter.rotateY" -3.75')
    mel.eval('setAttr "rEyelidOuter.rotateY" 3.75')
    mel.eval('setAttr "rEyelidLowerOuter.rotateY" 3.75')
    mel.eval('setAttr "rEyelidLower.rotateY" 3.75')
    mel.eval('setAttr "rEyelidLowerInner.rotateY" 3.75')
    _set_set_driven_key_y_r()
    mel.eval('setAttr "rEye.rotateY" 0')
    mel.eval('setAttr "rEyelidInner.rotateY" 0')
    mel.eval('setAttr "rEyelidUpperInner.rotateY" 0')
    mel.eval('setAttr "rEyelidUpper.rotateY" 0')
    mel.eval('setAttr "rEyelidUpperOuter.rotateY" 0')
    mel.eval('setAttr "rEyelidOuter.rotateY" 0')
    mel.eval('setAttr "rEyelidLowerOuter.rotateY" 0')
    mel.eval('setAttr "rEyelidLower.rotateY" 0')
    mel.eval('setAttr "rEyelidLowerInner.rotateY" 0')

    # Eyes Up-Down
    # Left eye 30
    _set_set_driven_key_x_l()
    mel.eval('setAttr "lEye.rotateX" 30')
    mel.eval('setAttr "lEyelidInner.rotateX" 2')
    mel.eval('setAttr "lEyelidUpperInner.rotateX" -5')
    mel.eval('setAttr "lEyelidUpper.rotateX" -5')
    mel.eval('setAttr "lEyelidUpperOuter.rotateX" -5')
    mel.eval('setAttr "lEyelidOuter.rotateX" 2')
    mel.eval('setAttr "lEyelidLowerOuter.rotateX" 6')
    mel.eval('setAttr "lEyelidLower.rotateX" 6')
    mel.eval('setAttr "lEyelidLowerInner.rotateX" 6')
    _set_set_driven_key_x_l()
    mel.eval('setAttr "lEye.rotateX" 0')
    mel.eval('setAttr "lEyelidInner.rotateX" 0')
    mel.eval('setAttr "lEyelidUpperInner.rotateX" 0')
    mel.eval('setAttr "lEyelidUpper.rotateX" 0')
    mel.eval('setAttr "lEyelidUpperOuter.rotateX" 0')
    mel.eval('setAttr "lEyelidOuter.rotateX" 0')
    mel.eval('setAttr "lEyelidLowerOuter.rotateX" 0')
    mel.eval('setAttr "lEyelidLower.rotateX" 0')
    mel.eval('setAttr "lEyelidLowerInner.rotateX" 0')

    # Left eye -30 # Eyes Up-Down
    _set_set_driven_key_x_l()
    mel.eval('setAttr "lEye.rotateX" -30')
    mel.eval('setAttr "lEyelidInner.rotateX" -2')
    mel.eval('setAttr "lEyelidUpperInner.rotateX" 5')
    mel.eval('setAttr "lEyelidUpper.rotateX" 5')
    mel.eval('setAttr "lEyelidUpperOuter.rotateX" 5')
    mel.eval('setAttr "lEyelidOuter.rotateX" -2')
    mel.eval('setAttr "lEyelidLowerOuter.rotateX" -5')
    mel.eval('setAttr "lEyelidLower.rotateX" -5')
    mel.eval('setAttr "lEyelidLowerInner.rotateX" -5')
    _set_set_driven_key_x_l()
    mel.eval('setAttr "lEye.rotateX" 0')
    mel.eval('setAttr "lEyelidInner.rotateX" 0')
    mel.eval('setAttr "lEyelidUpperInner.rotateX" 0')
    mel.eval('setAttr "lEyelidUpper.rotateX" 0')
    mel.eval('setAttr "lEyelidUpperOuter.rotateX" 0')
    mel.eval('setAttr "lEyelidOuter.rotateX" 0')
    mel.eval('setAttr "lEyelidLowerOuter.rotateX" 0')
    mel.eval('setAttr "lEyelidLower.rotateX" 0')
    mel.eval('setAttr "lEyelidLowerInner.rotateX" 0')

    # Right eye 30 # Eyes Up-Down
    _set_set_driven_key_x_r()
    mel.eval('setAttr "rEye.rotateX" 30')
    mel.eval('setAttr "rEyelidInner.rotateX" 2')
    mel.eval('setAttr "rEyelidUpperInner.rotateX" -5')
    mel.eval('setAttr "rEyelidUpper.rotateX" -5')
    mel.eval('setAttr "rEyelidUpperOuter.rotateX" -5')
    mel.eval('setAttr "rEyelidOuter.rotateX" 2')
    mel.eval('setAttr "rEyelidLowerOuter.rotateX" 6')
    mel.eval('setAttr "rEyelidLower.rotateX" 6')
    mel.eval('setAttr "rEyelidLowerInner.rotateX" 6')
    _set_set_driven_key_x_r()
    mel.eval('setAttr "rEye.rotateX" 0')
    mel.eval('setAttr "rEyelidInner.rotateX" 0')
    mel.eval('setAttr "rEyelidUpperInner.rotateX" 0')
    mel.eval('setAttr "rEyelidUpper.rotateX" 0')
    mel.eval('setAttr "rEyelidUpperOuter.rotateX" 0')
    mel.eval('setAttr "rEyelidOuter.rotateX" 0')
    mel.eval('setAttr "rEyelidLowerOuter.rotateX" 0')
    mel.eval('setAttr "rEyelidLower.rotateX" 0')
    mel.eval('setAttr "rEyelidLowerInner.rotateX" 0')

    # Right eye -30 # Eyes Up-Down
    _set_set_driven_key_x_r()
    mel.eval('setAttr "rEye.rotateX" -30')
    mel.eval('setAttr "rEyelidInner.rotateX" -2')
    mel.eval('setAttr "rEyelidUpperInner.rotateX" 5')
    mel.eval('setAttr "rEyelidUpper.rotateX" 5')
    mel.eval('setAttr "rEyelidUpperOuter.rotateX" 5')
    mel.eval('setAttr "rEyelidOuter.rotateX" -2')
    mel.eval('setAttr "rEyelidLowerOuter.rotateX" -5')
    mel.eval('setAttr "rEyelidLower.rotateX" -5')
    mel.eval('setAttr "rEyelidLowerInner.rotateX" -5')
    _set_set_driven_key_x_r()
    mel.eval('setAttr "rEye.rotateX" 0')
    mel.eval('setAttr "rEyelidInner.rotateX" 0')
    mel.eval('setAttr "rEyelidUpperInner.rotateX" 0')
    mel.eval('setAttr "rEyelidUpper.rotateX" 0')
    mel.eval('setAttr "rEyelidUpperOuter.rotateX" 0')
    mel.eval('setAttr "rEyelidOuter.rotateX" 0')
    mel.eval('setAttr "rEyelidLowerOuter.rotateX" 0')
    mel.eval('setAttr "rEyelidLower.rotateX" 0')
    mel.eval('setAttr "rEyelidLowerInner.rotateX" 0')

    # Left eye rotation limits enable
    mel.eval('transformLimits -rx -30 30 -erx 1 1 lEye')
    mel.eval('transformLimits -ry -40 30 -ery 1 1 lEye')
    mel.eval('transformLimits -rz -180 180 -erz 1 1 lEye')

    # Right eye rotation limits enable
    mel.eval('transformLimits -rx -30 30 -erx 1 1 rEye')
    mel.eval('transformLimits -ry -40 30 -ery 1 1 rEye')
    mel.eval('transformLimits -rz -180 180 -erz 1 1 rEye')


def add_tip_joint(base_joint, end_joint, new_tip_joint):
    if cmds.objExists(base_joint):
        cmds.xform(base_joint, scale=(2, 2, 2))
        location = cmds.xform(end_joint, query=True, translation=True, worldSpace=True)
        cmds.xform(base_joint, scale=(1, 1, 1))
        cmds.joint(end_joint, name=new_tip_joint, p=location)


def add_finger_tip_joints():
    new_joints = (
        ('lThumb2', 'lThumb3', 'lThumb4'),
        ('lIndex2', 'lIndex3', 'lIndex4'),
        ('lMid2', 'lMid3', 'lMid4'),
        ('lRing2', 'lRing3', 'lRing4'),
        ('lPinky2', 'lPinky3', 'lPinky4'),
        ('rThumb2', 'rThumb3', 'rThumb4'),
        ('rIndex2', 'rIndex3', 'rIndex4'),
        ('rMid2', 'rMid3', 'rMid4'),
        ('rRing2', 'rRing3', 'rRing4'),
        ('rPinky2', 'rPinky3', 'rPinky4'),
    )

    for joint in new_joints:
        try:
            add_tip_joint(*joint)
        except RuntimeError:
            pass


def add_toe_tip_joints():
    new_joints = (
        ('lBigToe', 'lBigToe_2', 'lBigToe_3'),
        ('lSmallToe1', 'lSmallToe1_2', 'lSmallToe1_3'),
        ('lSmallToe2', 'lSmallToe2_2', 'lSmallToe2_3'),
        ('lSmallToe3', 'lSmallToe3_2', 'lSmallToe3_3'),
        ('lSmallToe4', 'lSmallToe4_2', 'lSmallToe4_3'),
        ('rBigToe', 'rBigToe_2', 'rBigToe_3'),
        ('rSmallToe1', 'rSmallToe1_2', 'rSmallToe1_3'),
        ('rSmallToe2', 'rSmallToe2_2', 'rSmallToe2_3'),
        ('rSmallToe3', 'rSmallToe3_2', 'rSmallToe3_3'),
        ('rSmallToe4', 'rSmallToe4_2', 'rSmallToe4_3'),
    )

    for joint in new_joints:
        try:
            add_tip_joint(*joint)
        except RuntimeError:
            pass


def zero_joint_orients_and_axes():
    joints = cmds.ls(type="joint")
    for joint in joints:
        cmds.setAttr(joint + '.jointOrientX', 0)
        cmds.setAttr(joint + '.jointOrientY', 0)
        cmds.setAttr(joint + '.jointOrientZ', 0)
        cmds.setAttr(joint + '.rotateAxisX', 0)
        cmds.setAttr(joint + '.rotateAxisY', 0)
        cmds.setAttr(joint + '.rotateAxisZ', 0)


def t_pose(gender, character):
    arm_prefix = '_Group|hip|abdomenLower|abdomenUpper|chestLower|chestUpper|'
    leg_rotations = {
        '_Group|hip|pelvis|lThighBend|lThighTwist|lShin.rotateY': -9.66,
        '_Group|hip|pelvis|rThighBend|rThighTwist|rShin.rotateY': 9.66,
    }
    if config_hik_for_mb:
        arm_rotations = {
            'lCollar|lShldrBend.rotateZ': 47,
            'rCollar|rShldrBend.rotateZ': -47,
            'lCollar|lShldrBend|lShldrTwist|lForearmBend.rotateY': 15,
            'rCollar|rShldrBend|rShldrTwist|rForearmBend.rotateY': -15,
            'lCollar|lShldrBend|lShldrTwist|lForearmBend|lForearmTwist|lHand.rotateY': -12.75,
            'rCollar|rShldrBend|rShldrTwist|rForearmBend|rForearmTwist|rHand.rotateY': 12.75,

        }
        leg_rotations.update({
            '_Group|hip|pelvis|lThighBend.rotateZ': -6,  # -7.5 Daz Video
            '_Group|hip|pelvis|rThighBend.rotateZ': 6  # 7.5 Daz Video
        })
        # cmds.setAttr(gender + 'lCollar|lShldrBend|lShldrTwist|lForearmBend.rotateX', 1.16)
        # cmds.setAttr(gender + 'lCollar|lShldrBend|lShldrTwist|lForearmBend.rotateZ', -4.2)

        # cmds.setAttr(gender + 'rCollar|rShldrBend|rShldrTwist|rForearmBend.rotateX', 1.16)
        # cmds.setAttr(gender + 'rCollar|rShldrBend|rShldrTwist|rForearmBend.rotateZ', 4.2)

        # cmds.setAttr(gender + '_Group|hip|pelvis|lThighBend.rotateX', 0.54)
        # cmds.setAttr(gender + '_Group|hip|pelvis|lThighBend.rotateY', 14)  # Daz Video,
        # but in Daz Studio this joint doesn't twist on this axis
        # cmds.setAttr(gender + '_Group|hip|pelvis|lThighBend|lThighTwist|lShin|lFoot.rotateZ', 5)  # Daz Video

        # cmds.setAttr(gender + '_Group|hip|pelvis|rThighBend.rotateX', 0.54)
        # cmds.setAttr(gender + '_Group|hip|pelvis|rThighBend.rotateY', -14)  # Daz Video,
        # but in Daz Studio this joint doesn't twist on this axis
        # cmds.setAttr(gender + '_Group|hip|pelvis|rThighBend|rThighTwist|rShin|rFoot.rotateZ', -5)  # Daz Video
    else:
        arm_rotations = {
            'lCollar|lShldrBend.rotateZ': 48.24,
            'rCollar|rShldrBend.rotateZ': -48.24,
            'lCollar|lShldrBend|lShldrTwist|lForearmBend.rotateX': 1.16,
            'lCollar|lShldrBend|lShldrTwist|lForearmBend.rotateY': 15.49,
            'lCollar|lShldrBend|lShldrTwist|lForearmBend.rotateZ': -4.2,
            'rCollar|rShldrBend|rShldrTwist|rForearmBend.rotateX': 1.16,
            'rCollar|rShldrBend|rShldrTwist|rForearmBend.rotateY': -15.49,
            'rCollar|rShldrBend|rShldrTwist|rForearmBend.rotateZ': 4.2,
            'lCollar|lShldrBend|lShldrTwist|lForearmBend|lForearmTwist|lHand.rotateY': -13.76,
            'rCollar|rShldrBend|rShldrTwist|rForearmBend|rForearmTwist|rHand.rotateY': 13.76,
        }
        leg_rotations.update({
            '_Group|hip|pelvis|lThighBend.rotateX': 0.54,
            '_Group|hip|pelvis|lThighBend.rotateY': -0.16,
            '_Group|hip|pelvis|lThighBend.rotateZ': -6.23,
            '_Group|hip|pelvis|rThighBend.rotateX': 0.54,
            '_Group|hip|pelvis|rThighBend.rotateY': 0.16,
            '_Group|hip|pelvis|rThighBend.rotateZ': 6.23,
        })

    if 'ToonDwayne8' == character:
        arm_rotations['lCollar|lShldrBend.rotateZ'] = 48.244
        arm_rotations['rCollar|rShldrBend.rotateZ'] = -48.244
        arm_rotations['lCollar|lShldrBend.rotateY'] = 9.361
        arm_rotations['rCollar|rShldrBend.rotateY'] = -9.361

    if PYTHON_VERSION == 3:
        for joint, rotation in arm_rotations.items():
            try:
                cmds.setAttr('{}{}{}'.format(gender, arm_prefix, joint), rotation)
            except RuntimeError:
                pass

        for joint, rotation in leg_rotations.items():
            try:
                cmds.setAttr('{}{}'.format(gender, joint), rotation)
            except RuntimeError:
                pass
    else:
        for joint, rotation in arm_rotations.iteritems():
            try:
                cmds.setAttr('{}{}{}'.format(gender, arm_prefix, joint), rotation)
            except RuntimeError:
                pass

        for joint, rotation in leg_rotations.iteritems():
            try:
                cmds.setAttr('{}{}'.format(gender, joint), rotation)
            except RuntimeError:
                pass


def define_ik(gender):
    if config_hik_for_mb:
        print('Defining HumanIK Skeleton for MotionBuilder...')
    else:
        print('Defining HumanIK Skeleton to G8 for Maya Original...')
    mel.eval('HIKCharacterControlsTool')
    mel.eval('hikCreateDefinition()')
    mel.eval('setCharacterObject("{}_Group", "Character1", 0, 0)'.format(gender))
    mel.eval('setCharacterObject("hip","Character1",1,0)')
    mel.eval('setCharacterObject("abdomenLower","Character1",8,0)')
    mel.eval('setCharacterObject("abdomenUpper","Character1",23,0)')
    mel.eval('setCharacterObject("chestLower","Character1",24,0)')
    mel.eval('setCharacterObject("chestUpper","Character1",25,0)')
    # mel.eval('setCharacterObject("chestUpper","Character1",26,0)')  # if pelvis defined to spine
    mel.eval('setCharacterObject("neckLower","Character1",20,0)')
    mel.eval('setCharacterObject("neckUpper","Character1",32,0)')
    mel.eval('setCharacterObject("head","Character1",15,0)')

    mel.eval('setCharacterObject("lThighBend","Character1",2,0)')
    if config_hik_for_mb:
        mel.eval('setCharacterObject("lThighTwist","Character1",41,0)')
    elif '2016 Extension 2' in MAYA_VERSION or int(MAYA_VERSION[:4]) >= 2017:
        mel.eval('setCharacterObject("lThighTwist","Character1",172,0)')
    else:
        mel.eval('setCharacterObject("lThighTwist","Character1",41,0)')
    mel.eval('setCharacterObject("lShin","Character1",3,0)')
    mel.eval('setCharacterObject("lFoot","Character1",4,0)')
    mel.eval('setCharacterObject("lToe","Character1",16,0)')
    mel.eval('setCharacterObject("lBigToe", "Character1", 118, 0)')
    mel.eval('setCharacterObject("lBigToe_2", "Character1", 119, 0)')
    mel.eval('setCharacterObject("lBigToe_3", "Character1", 120, 0)')
    mel.eval('setCharacterObject("lSmallToe1", "Character1", 102, 0)')
    mel.eval('setCharacterObject("lSmallToe1_2", "Character1", 103, 0)')
    mel.eval('setCharacterObject("lSmallToe1_3", "Character1", 104, 0)')
    mel.eval('setCharacterObject("lSmallToe2", "Character1", 106, 0)')
    mel.eval('setCharacterObject("lSmallToe2_2", "Character1", 107, 0)')
    mel.eval('setCharacterObject("lSmallToe2_3", "Character1", 108, 0)')
    mel.eval('setCharacterObject("lSmallToe3", "Character1", 110, 0)')
    mel.eval('setCharacterObject("lSmallToe3_2", "Character1", 111, 0)')
    mel.eval('setCharacterObject("lSmallToe3_3", "Character1", 112, 0)')
    mel.eval('setCharacterObject("lSmallToe4", "Character1", 114, 0)')
    mel.eval('setCharacterObject("lSmallToe4_2", "Character1", 115, 0)')
    mel.eval('setCharacterObject("lSmallToe4_3", "Character1", 116, 0)')

    mel.eval('setCharacterObject("lCollar","Character1",18,0)')
    mel.eval('setCharacterObject("lShldrBend","Character1",9,0)')
    if config_hik_for_mb:
        mel.eval('setCharacterObject("lShldrTwist", "Character1", 45, 0)')
    elif '2016 Extension 2' in MAYA_VERSION or int(MAYA_VERSION[:4]) >= 2017:
        mel.eval('setCharacterObject("lShldrTwist","Character1",176,0)')
    else:
        mel.eval('setCharacterObject("lShldrTwist", "Character1", 45, 0)')
    mel.eval('setCharacterObject("lForearmBend","Character1",10,0)')
    if config_hik_for_mb:
        mel.eval('setCharacterObject("lForearmTwist", "Character1", 46, 0)')
    elif '2016 Extension 2' in MAYA_VERSION or int(MAYA_VERSION[:4]) >= 2017:
        mel.eval('setCharacterObject("lForearmTwist","Character1",177,0)')
    else:
        mel.eval('setCharacterObject("lForearmTwist", "Character1", 46, 0)')
    mel.eval('setCharacterObject("lHand","Character1",11,0)')
    mel.eval('setCharacterObject("lThumb1","Character1",50,0)')
    mel.eval('setCharacterObject("lThumb2","Character1",51,0)')
    mel.eval('setCharacterObject("lThumb3","Character1",52,0)')
    mel.eval('setCharacterObject("lThumb4","Character1",53,0)')
    mel.eval('setCharacterObject("lIndex1","Character1",54,0)')
    mel.eval('setCharacterObject("lIndex2","Character1",55,0)')
    mel.eval('setCharacterObject("lIndex3","Character1",56,0)')
    mel.eval('setCharacterObject("lIndex4","Character1",57,0)')
    mel.eval('setCharacterObject("lMid1","Character1",58,0)')
    mel.eval('setCharacterObject("lMid2","Character1",59,0)')
    mel.eval('setCharacterObject("lMid3","Character1",60,0)')
    mel.eval('setCharacterObject("lMid4","Character1",61,0)')
    mel.eval('setCharacterObject("lRing1","Character1",62,0)')
    mel.eval('setCharacterObject("lRing2","Character1",63,0)')
    mel.eval('setCharacterObject("lRing3","Character1",64,0)')
    mel.eval('setCharacterObject("lRing4","Character1",65,0)')
    mel.eval('setCharacterObject("lPinky1","Character1",66,0)')
    mel.eval('setCharacterObject("lPinky2","Character1",67,0)')
    mel.eval('setCharacterObject("lPinky3","Character1",68,0)')
    mel.eval('setCharacterObject("lPinky4","Character1",69,0)')

    mel.eval('setCharacterObject("rThighBend","Character1",5,0)')
    if config_hik_for_mb:
        mel.eval('setCharacterObject("rThighTwist", "Character1", 43, 0)')
    elif '2016 Extension 2' in MAYA_VERSION or int(MAYA_VERSION[:4]) >= 2017:
        mel.eval('setCharacterObject("rThighTwist","Character1",174,0)')
    else:
        mel.eval('setCharacterObject("rThighTwist", "Character1", 43, 0)')
    mel.eval('setCharacterObject("rShin","Character1",6,0)')
    mel.eval('setCharacterObject("rFoot","Character1",7,0)')
    mel.eval('setCharacterObject("rToe","Character1",17,0)')
    mel.eval('setCharacterObject("rBigToe", "Character1", 142, 0)')
    mel.eval('setCharacterObject("rBigToe_2", "Character1", 143, 0)')
    mel.eval('setCharacterObject("rBigToe_3", "Character1", 144, 0)')
    mel.eval('setCharacterObject("rSmallToe1", "Character1", 126, 0)')
    mel.eval('setCharacterObject("rSmallToe1_2", "Character1", 127, 0)')
    mel.eval('setCharacterObject("rSmallToe1_3", "Character1", 128, 0)')
    mel.eval('setCharacterObject("rSmallToe2", "Character1", 130, 0)')
    mel.eval('setCharacterObject("rSmallToe2_2", "Character1", 131, 0)')
    mel.eval('setCharacterObject("rSmallToe2_3", "Character1", 132, 0)')
    mel.eval('setCharacterObject("rSmallToe3", "Character1", 134, 0)')
    mel.eval('setCharacterObject("rSmallToe3_2", "Character1", 135, 0)')
    mel.eval('setCharacterObject("rSmallToe3_3", "Character1", 136, 0)')
    mel.eval('setCharacterObject("rSmallToe4", "Character1", 138, 0)')
    mel.eval('setCharacterObject("rSmallToe4_2", "Character1", 139, 0)')
    mel.eval('setCharacterObject("rSmallToe4_3", "Character1", 140, 0)')

    mel.eval('setCharacterObject("rCollar","Character1",19,0)')
    mel.eval('setCharacterObject("rShldrBend","Character1",12,0)')
    if config_hik_for_mb:
        mel.eval('setCharacterObject("rShldrTwist", "Character1", 47, 0)')
    elif '2016 Extension 2' in MAYA_VERSION or int(MAYA_VERSION[:4]) >= 2017:
        mel.eval('setCharacterObject("rShldrTwist","Character1",178,0)')
    else:
        mel.eval('setCharacterObject("rShldrTwist", "Character1", 47, 0)')
    mel.eval('setCharacterObject("rForearmBend","Character1",13,0)')
    if config_hik_for_mb:
        mel.eval('setCharacterObject("rForearmTwist","Character1",48,0)')
    elif '2016 Extension 2' in MAYA_VERSION or int(MAYA_VERSION[:4]) >= 2017:
        mel.eval('setCharacterObject("rForearmTwist","Character1",179,0)')
    else:
        mel.eval('setCharacterObject("rForearmTwist","Character1",48,0)')
    mel.eval('setCharacterObject("rHand","Character1",14,0)')
    mel.eval('setCharacterObject("rThumb1","Character1",74,0)')
    mel.eval('setCharacterObject("rThumb2","Character1",75,0)')
    mel.eval('setCharacterObject("rThumb3","Character1",76,0)')
    mel.eval('setCharacterObject("rThumb4","Character1",77,0)')
    mel.eval('setCharacterObject("rIndex1","Character1",78,0)')
    mel.eval('setCharacterObject("rIndex2","Character1",79,0)')
    mel.eval('setCharacterObject("rIndex3","Character1",80,0)')
    mel.eval('setCharacterObject("rIndex4","Character1",81,0)')
    mel.eval('setCharacterObject("rMid1","Character1",82,0)')
    mel.eval('setCharacterObject("rMid2","Character1",83,0)')
    mel.eval('setCharacterObject("rMid3","Character1",84,0)')
    mel.eval('setCharacterObject("rMid4","Character1",85,0)')
    mel.eval('setCharacterObject("rRing1","Character1",86,0)')
    mel.eval('setCharacterObject("rRing2","Character1",87,0)')
    mel.eval('setCharacterObject("rRing3","Character1",88,0)')
    mel.eval('setCharacterObject("rRing4","Character1",89,0)')
    mel.eval('setCharacterObject("rPinky1","Character1",90,0)')
    mel.eval('setCharacterObject("rPinky2","Character1",91,0)')
    mel.eval('setCharacterObject("rPinky3","Character1",92,0)')
    mel.eval('setCharacterObject("rPinky4","Character1",93,0)')
    mel.eval('hikToggleLockDefinition()')
    mel.eval('hikCreateControlRig')


def rename_character(gender, character):
    print('Renaming transform and shape nodes...')
    if cmds.ls('G8MGenitalia'):
        cmds.rename('G8MGenitalia', gender + 'Genitalia')
    obj_list = [gender + '_Group']
    obj_list = obj_list + cmds.listRelatives(obj_list, allDescendents=True)
    for obj in obj_list:
        if gender in obj:
            objNewName = obj.replace(gender, character)
            cmds.rename(obj, objNewName)
    print('Renaming blend shape node to ' + character + '_Morphs...')
    mel.eval('rename "{}_Morphs" "{}_Morphs"'.format(gender, character))
    try:
        cmds.rename("{}Eyelashes_Morphs".format(gender), "{}Eyelashes_Morphs".format(character))
    except RuntimeError:
        print("No eyelashes found...")
        pass


def rename_ik(character):
    print('Renaming IK and FK control handles...')
    mel.eval('rename "Character1_Ctrl_Reference" "{0}"'.format(character + '_Ctrl_Reference'))
    controlsList = cmds.ls(type=['hikIKEffector', 'hikFKJoint', 'locator', 'HIKControlSetNode', 'keyingGroup'])
    for ctrl in controlsList:
        if 'Character1' in ctrl:
            ctrlNewName = ctrl.replace('Character1', character)
            cmds.rename(ctrl, ctrlNewName)
    print('Renaming HumanIK character rig...')
    rigName = character + 'Rig'
    cmds.rename('Character1', rigName)


def rename_shading_groups(notify_in_view=False):
    in_view_message = ''
    figures = cmds.ls(dag=1, o=1, s=1)
    camera_list = cmds.ls(cameras=True)
    camera_list = camera_list + cmds.listRelatives(camera_list, parent=True)
    for figure in figures:
        if figure not in camera_list and 'Orig' not in figure and 'Ctrl' not in figure:
            print('Renaming shading groups for ' + figure.replace('Shape', '') + '...')
            in_view_message += 'Renaming shading groups for ' + figure.replace('Shape', '') + '...\n'
            shaders_list = cmds.listConnections(figure, type='shadingEngine')
            i = 0
            for sg in shaders_list:
                if i % 2 == 0:
                    shader_name = cmds.listConnections(sg, d=0, s=1, c=1, p=0)[1]
                    shader_name = shader_name.splitlines()[0]
                    new_name = figure.replace('Shape', '')
                    # if 'G8' or 'G8F' or 'G8M' or 'G8_' or 'G8F_' or 'G8M_' not in new_name:
                        # new_name = new_name.rstrip('0123456789_')
                    new_name = new_name + '_' + shader_name + 'SG'
                    try:
                        cmds.rename(shaders_list[i], new_name)
                    except RuntimeError:
                        print("Cannot rename {} to {}, it's a read only node.".format(shaders_list[i], new_name))
                i += 1
    in_view_message += 'Done.'
    if notify_in_view:
        cmds.inViewMessage(amg=in_view_message, pos='midCenterTop', bkc=0x00000000, fade=True)


def rename_shaders(notify_in_view=False):
    in_view_message = ''
    figures = cmds.ls(dag=1, o=1, s=1)
    camera_list = cmds.ls(cameras=True)
    camera_list = camera_list + cmds.listRelatives(camera_list, parent=True)
    for figure in figures:
        if figure not in camera_list and 'Orig' not in figure and 'Ctrl' not in figure:
            print('Renaming shaders for ' + figure.replace('Shape', '') + '...')
            in_view_message += 'Renaming shaders for ' + figure.replace('Shape', '') + '...\n'
            shaders_list = cmds.listConnections(figure, type='shadingEngine')
            i = 0
            for sg in shaders_list:
                if i % 2 == 0:
                    shader_name = cmds.listConnections(sg, d=0, s=1, c=1, p=0)[1]
                    shader_name = shader_name.splitlines()[0]
                    new_name = figure.replace('Shape', '')
                    # if 'G8' or 'G8F' or 'G8M' or 'G8_' or 'G8F_' or 'G8M_'not in new_name:
                        # new_name = new_name.rstrip('0123456789_')
                    if cmds.checkBox('checkBoxAbbreviateG8', query=True, exists=True):
                        if cmds.checkBox('checkBoxAbbreviateG8', query=True, value=True) and cmds.checkBox(
                                'checkBoxAbbreviateG8', query=True, enable=True):
                            if 'Genesis8Female' in new_name:
                                new_name = new_name.replace('Genesis8Female', 'G8F')
                            if 'Genesis8Male' in new_name:
                                new_name = new_name.replace('Genesis8Male', 'G8M')
                    new_name = new_name + '_' + shader_name.replace('_ncl1_1', '')
                    cmds.rename(shader_name, new_name)
                i += 1
    in_view_message += 'Done.'
    if notify_in_view:
        cmds.inViewMessage(amg=in_view_message, pos='midCenterTop', bkc=0x00000000, fade=True)


def rename_animation_curves(gender, character):
    if gender != character:
        animation_curves = cmds.ls(et='animCurveUU')
        for curve_name in animation_curves:
            if gender in curve_name:
                new_name = curve_name.replace(gender, character)
                cmds.rename(curve_name, new_name)


def please_wait_dialog_delete():
    if cmds.window('LL3D_G8ForMaya_WaitWindow', exists=True):
        cmds.deleteUI('LL3D_G8ForMaya_WaitWindow')
        cmds.windowPref('LL3D_G8ForMaya_WaitWindow', remove=True)


def please_wait_dialog_show():
    please_wait_dialog_delete()
    cmds.window(
        'LL3D_G8ForMaya_WaitWindow', toolbox=True, title='Genesis 8 for Maya', width=300,
        maximizeButton=False, minimizeButton=False, sizeable=False)
    cmds.columnLayout(adjustableColumn=True)
    cmds.separator(height=20, style='out')
    cmds.text(label='Importing please wait...', font='boldLabelFont')
    cmds.separator(height=20, style='in')
    cmds.showWindow('LL3D_G8ForMaya_WaitWindow')


def rename_dialog_answer(gender, character_name, default=False):
    cmds.scriptEditorInfo(hfn=LOG_FILE, writeHistory=True)
    if default:
        print('Naming to character defaults...')
    else:
        character_name = cmds.textField('LL3D_G8ForMaya_CharName_txtField', query=True, text=True)

    rename_character(gender, character_name)
    if gender in SUPPORTED_FIGURES:
        rename_ik(character_name)

    if not DNABLOCK_MODE:
        if cmds.checkBox('checkBoxRenameShadingGroups', query=True, value=True):
            rename_shading_groups()
        if cmds.checkBox('checkBoxRenameShaders', query=True, value=True):
            rename_shaders()

    delete_rename_window()
    rename_animation_curves(gender, character_name)
    cmds.scriptEditorInfo(writeHistory=False)


def delete_rename_window():
    if cmds.window('LL3D_G8ForMaya_RenameCharacterWindow', exists=True):
        cmds.deleteUI('LL3D_G8ForMaya_RenameCharacterWindow')
        cmds.windowPref('LL3D_G8ForMaya_RenameCharacterWindow', remove=True)


def toggle_abbreviate_check_box():
    if cmds.checkBox('checkBoxRenameShaders', query=True, value=True):
        cmds.checkBox('checkBoxAbbreviateG8', edit=True, enable=True)
    else:
        cmds.checkBox('checkBoxAbbreviateG8', edit=True, enable=False)


def rename_dialog(gender, character):
    global num_errors
    delete_rename_window()
    cmds.window(
        'LL3D_G8ForMaya_RenameCharacterWindow', toolbox=True, title='Rename Character?', width=400,
        titleBar=False, sizeable=False)
    cmds.rowColumnLayout('mainLayout', numberOfColumns=1)
    cmds.image(image=G8_FOR_MAYA_IMG, width=400)
    cmds.separator(height=10, style='in')
    # cmds.separator(height=5, style='none')
    cmds.text(label='Current Character Name: ' + character, align='center', font='boldLabelFont')
    cmds.separator(height=10, style='in')
    cmds.text(label='Specify a Different Character Name?', align='center')
    cmds.separator(height=10, style='out')
    cmds.textField(
        'LL3D_G8ForMaya_CharName_txtField',
        text=character,
        enterCommand=lambda x: rename_dialog_answer(gender, character),
        alwaysInvokeEnterCommandOnReturn=True)
    cmds.separator(height=3, style='none')
    cmds.rowColumnLayout(numberOfColumns=2, columnWidth=[(1, 199), (2, 199)], columnSpacing=[(2, 2)])
    cmds.button(label='Yes', command=lambda x: rename_dialog_answer(gender, character))
    cmds.button(label='No', command=lambda x: rename_dialog_answer(gender, character, True))
    cmds.setParent('..')
    cmds.separator(height=2, style='none')
    cmds.separator(height=10, style='out')
    cmds.text(label='Shading Networks Renaming Options', align='center', font='boldLabelFont')
    cmds.separator(height=10, style='out')
    cmds.rowColumnLayout(
        numberOfColumns=2, columnWidth=[(1, 374), (2, 19)],
        columnSpacing=[(1, 5), (2, 2), (3, 5), (4, 2), (5, 15), (6, 2)])
    cmds.text(label='Rename Shading Groups', align='left')
    cmds.checkBox('checkBoxRenameShadingGroups', label=' ', v=True)
    cmds.text(label='Rename Shaders', align='left')
    cmds.checkBox('checkBoxRenameShaders', label=' ', v=True, cc=lambda *args: toggle_abbreviate_check_box())
    cmds.text(label='Abbreviate Genesis 8 as G8F or G8M in Shader Names', align='left')
    cmds.checkBox('checkBoxAbbreviateG8', label=' ', v=True)
    cmds.setParent('..')
    cmds.separator(height=10, style='in')

    if num_errors > 0:
        if num_errors == 1:
            cmds.text(label='Error:', align='left')
        else:
            cmds.text(label='Errors:', align='left')
        cmds.separator(height=10, style='out')
        cmds.columnLayout(columnAlign='center', width=400, adjustableColumn=True, columnOffset=('both', 5))
        for error in errors:
            cmds.scrollField(editable=False, wordWrap=True, text=error.replace('8', ' 8', 2),
                             backgroundColor=(1.0, 1.0, 1.0), height=140)
            # cmds.text(label=error, backgroundColor=(1.0, 1.0, 1.0), align='center', wordWrap=True)
            cmds.separator(height=10, style='in')
    cmds.showWindow('LL3D_G8ForMaya_RenameCharacterWindow')
    del errors[:]
    num_errors = 0
    cmds.rowColumnLayout(nc=2, cw=[(1, 200), (2, 200)], co=(2, 'left', 100))
    cmds.rowColumnLayout(nr=1)
    cmds.columnLayout(cw=200, co=('left', 5), rs=5)
    cmds.text(label='Version ' + VERSION)
    cmds.text(label='Copyright 2018 LayLo 3D')
    cmds.setParent('..')
    cmds.setParent('..')
    cmds.image(image=LL3D_LOGO, width=100)


def use_mel_script(file_name):
    mel_script_path = file_name.rsplit('.', 1)[0] + '.mel'
    if os.path.isfile(mel_script_path):
        # mel.eval('source "{}"'.format(mel_script_path))
        with open(mel_script_path) as mel_script:
            for line in mel_script.readlines():
                if 'Eyelashes' in line:
                    break

                line = line.rstrip()
                if line.startswith('select '):
                    blend_shape_node_name = line.replace('select ', '').replace(';', '.')
                    print(line)
                words = line.split(' ')
                if len(words) > 3:
                    if not words[2] == 'linear':
                        object_name = mel.eval(
                            'formValidObjectName("{}");'.format(words[2].rsplit('__', 1)[-1])
                        )
                        if not cmds.objExists(blend_shape_node_name + object_name):
                            print("Mel script name:", words[2], "converted to >", object_name, "doesn't exist.")
                        words[2] = object_name
                        print(' '.join(words))
                    else:
                        object_name = mel.eval(
                            'formValidObjectName("{}");'.format(words[6].rsplit('__', 1)[-1])
                        )
                        if not cmds.objExists(blend_shape_node_name + object_name):
                            print("Mel script name:", words[6], "converted to >", object_name, "doesn't exist.")
                        words[6] = object_name
                mel.eval(' '.join(words))

        cmds.select(clear=True)
        return True

    return False


def end_with_error(traceback=True):
    please_wait_dialog_delete()
    if traceback:
        import traceback
        traceback.print_exc()
    cmds.warning(report_bug_msg)
    cmds.scriptEditorInfo(writeHistory=False)


def prompt_for_game_fix():
    response = cmds.confirmDialog(
        title='Game Engines', message='Set up character for game engines?', button=['Yes', 'No'],
        defaultButton='No', cancelButton='No', dismissString='No'
    )
    if response == 'Yes':
        return True

    return False


def file_browser():
    file_name = cmds.fileDialog2(
        fileFilter='*.fbx',
        dialogStyle=2,
        fileMode=1,
        startingDirectory=str(cmds.workspace(query=True, directory=True)),
        caption="Select your Genesis 8 for Maya FBX File..."
    )
    return file_name


def load_figure(character, mb_hik_radio_button, arg):
    global config_hik_for_mb

    cmds.NewScene()
    config_hik_for_mb = cmds.menuItem(mb_hik_radio_button, q=True, rb=True)
    cmds.optionVar(intValue=('LL3D_G8ForMaya_HIK', int(config_hik_for_mb)))
    cmds.scriptEditorInfo(hfn=LOG_FILE, chf=True, wh=True)
    print("Genesis 8 for Maya wants your Genesis 8 .fbx file...")
    please_wait_dialog_show()
    try:
        mel.eval('FBXImportMode -v Add')
        mel.eval('FBXImportMergeAnimationLayers -v false')
        mel.eval('FBXImportProtectDrivenKeys -v true')
        mel.eval('FBXImportConvertDeformingNullsToJoint -v true')
        mel.eval('FBXImportMergeBackNullPivots -v false')
        mel.eval('FBXImportSetLockedAttribute -v true')
        mel.eval('FBXImportConstraints -v false')
        # mel.eval('Import')
        file_name = file_browser()
        if file_name:
            file_name = file_name[0]
            if int(MAYA_VERSION[:4]) >= 2017:
                cmds.file(
                    file_name, i=True, type="FBX", groupReference=False,
                    ignoreVersion=True, mergeNamespacesOnClash=False, options="fbx",
                    preserveReferences=True, importFrameRate=True, importTimeRange="override"
                )
            else:
                cmds.file(
                    file_name, i=True, type="FBX", groupReference=False,
                    ignoreVersion=True, mergeNamespacesOnClash=False, options="fbx",
                    preserveReferences=True
                )
        else:
            please_wait_dialog_delete()
            return

        remove_blend_targets()
        merge_namespace()
        meshList = cmds.ls(type='blendShape')
        for mesh in meshList:
            clean_blend_shapes_names(mesh)

        used_mel_script = False  # use_mel_script(file_name)
        gender = get_gender()
        if not gender:
            end_with_error(False)
            show_invalid_character_dialog()
            return

        if not DNABLOCK_MODE:
            game_engine_setup = False
            if GAME_ENGINE_ENABLED:
                please_wait_dialog_delete()
                game_engine_setup = prompt_for_game_fix()
        else:
            game_engine_setup = True

        if game_engine_setup:
            please_wait_dialog_show()
            zero_joint_rotate_axes()

        create_character_group()
        obj_renamer()
        nonG8Joints = rid_duplicate_joint_names(gender)
        if character == 'base':
            character = gender

        if not used_mel_script:
            if not DNABLOCK_MODE:
                create_jcms(gender, character)

        if gender in ['Genesis8Male', 'Genesis8_1Male']:
            if cmds.objExists(character + '_Morphs.MD_JCM_R_Arm_UP'):
                import G8ForMaya_AutoShapeEnhancerG8M
                G8ForMaya_AutoShapeEnhancerG8M.auto_shape_enhancer_g8m(character + '_Morphs')
            if cmds.objExists(character + '_Morphs.pJCMkhThTw75_L'):
                import G8ForMaya_UltNaturalBendG8M
                G8ForMaya_UltNaturalBendG8M.ult_natural_bend_g8m_jcms(character + '_Morphs')
        if gender in ['Genesis8Female', 'Genesis8_1Female']:
            if cmds.objExists(character + '_Morphs.DM_JCM_LForearmBendTwistKEYED'):
                import G8ForMaya_AutoShapeEnhancerG8F
                G8ForMaya_AutoShapeEnhancerG8F.auto_shape_enhancer_g8f(character + '_Morphs')
            if cmds.objExists(character + '_Morphs.pJCMkhThiTwst55_R'):
                import G8ForMaya_UltNaturalBendG8F
                G8ForMaya_UltNaturalBendG8F.ult_natural_bend_g8f_jcms(character + '_Morphs')

        # remove_empty_blend_shapes()
        disable_limits()
        # if not game_engine_setup:
        add_finger_tip_joints()
        add_toe_tip_joints()
        t_pose(gender, character)
        if gender in SUPPORTED_FIGURES:
            set_eyelid_driven_keys()
            define_ik(gender)

        fix_non_g8_joints(nonG8Joints)
        please_wait_dialog_delete()

        if not DNABLOCK_MODE:
            cmds.setAttr(gender + '.displaySmoothMesh', 2)
            cmds.setAttr(gender + '.useSmoothPreviewForRender', 1)

        try:
            cmds.setAttr(gender + 'Eyelashes.displaySmoothMesh', 2)
            cmds.setAttr(gender + 'Eyelashes.useSmoothPreviewForRender', 1)
        except RuntimeError:
            pass

        # if not game_engine_setup:
        if not DNABLOCK_MODE:
            rename_dialog(gender, character)
        else:
            rename_dialog_answer(gender, 'GeFema', True)
            parent = [n for n in cmds.ls(assemblies=True, visible=True) if cmds.objectType(n) == 'joint'][0]
            meshes = [n for n in cmds.listRelatives(parent, type='transform') if cmds.objectType(n) != 'joint']
            cmds.group(meshes, world=True)
            cmds.select(clear=True)

            if REMOVE_FACE_JOINTS:
                import dna_remove_face_joints
                dna_remove_face_joints.remove_face_joints()

        mel.eval('FrameAllInAllViews')
    except:
        end_with_error()
