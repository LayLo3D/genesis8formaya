# Written by Landon Lopez Copyright (c) 2022 LayLo 3D, All Rights Reserved
# Created 6:10 AM, June 24, 2022
import maya.cmds as cmds
import maya.mel as mel

FACE_JOINTS = [
    'BelowJaw', 'CenterBrow', 'Chin', 'LipBelow', 'LipLowerMiddle', 'LipUpperMiddle', 'MidNoseBridge', 'Nose',
    'lBrowInner', 'lBrowMid', 'lBrowOuter', 'lCheekLower', 'lCheekUpper', 'lEar', 'lEye', 'lEyelidInner',
    'lEyelidLower', 'lEyelidLowerInner', 'lEyelidLowerOuter', 'lEyelidOuter', 'lEyelidUpper', 'lEyelidUpperInner',
    'lEyelidUpperOuter', 'lJawClench', 'lLipBelowNose', 'lLipCorner', 'lLipLowerInner', 'lLipLowerOuter',
    'lLipNasolabialCrease', 'lLipUpperInner', 'lLipUpperOuter', 'lNasolabialLower', 'lNasolabialMiddle',
    'lNasolabialMouthCorner', 'lNasolabialUpper', 'lNostril', 'lSquintInner', 'lSquintOuter', 'lowerFaceRig',
    'lowerJaw', 'lowerTeeth', 'rBrowInner', 'rBrowMid', 'rBrowOuter', 'rCheekLower', 'rCheekUpper', 'rEar', 'rEye',
    'rEyelidInner', 'rEyelidLower', 'rEyelidLowerInner', 'rEyelidLowerOuter', 'rEyelidOuter', 'rEyelidUpper',
    'rEyelidUpperInner', 'rEyelidUpperOuter', 'rJawClench', 'rLipBelowNose', 'rLipCorner', 'rLipLowerInner',
    'rLipLowerOuter', 'rLipNasolabialCrease', 'rLipUpperInner', 'rLipUpperOuter', 'rNasolabialLower',
    'rNasolabialMiddle', 'rNasolabialMouthCorner', 'rNasolabialUpper', 'rNostril', 'rSquintInner', 'rSquintOuter',
    'tongue01', 'tongue02', 'tongue03', 'tongue04', 'upperFaceRig', 'upperTeeth']


def flood_selection(flood_influence, influences, value=0, operation="absolute"):
    if len(cmds.ls(sl=1)) == 0:
        return

    # if we're not currently in the paint skin weights tool context, get us into it
    if cmds.currentCtx() != "artAttrSkinContext":
        mel.eval("ArtPaintSkinWeightsTool;")

    # select the joint
    for joint in influences:
        mel.eval('artSkinInflListChanging "{}" 0;'.format(joint))

    mel.eval('artSkinInflListChanging "{}" 1;'.format(flood_influence))
    mel.eval('artSkinInflListChanged artAttrSkinPaintCtx;')

    # first get the current settings so that the user doesn't have to switch back
    original_operation = cmds.artAttrSkinPaintCtx(cmds.currentCtx(), query=True, selectedattroper=1)
    original_value = cmds.artAttrSkinPaintCtx(cmds.currentCtx(), query=True, value=1)

    # flood the current selection to zero

    # first set our tool to the selected operation
    cmds.artAttrSkinPaintCtx(cmds.currentCtx(), edit=True, selectedattroper=operation)
    # change the tool value to the selected value
    cmds.artAttrSkinPaintCtx(cmds.currentCtx(), edit=True, value=value)
    # flood the tool
    cmds.artAttrSkinPaintCtx(cmds.currentCtx(), edit=True, clear=1)

    # set the tools back to the way you found them
    cmds.artAttrSkinPaintCtx(cmds.currentCtx(), edit=True, selectedattroper=original_operation)
    cmds.artAttrSkinPaintCtx(cmds.currentCtx(), edit=True, value=original_value)


def remove_influences(skin_cluster):
    influences = cmds.skinCluster(skin_cluster, query=True, influence=True)
    influences_to_remove = [j for j in influences if j in FACE_JOINTS]
    for joint in influences:
        cmds.setAttr('{}.liw'.format(joint), 0)

    cmds.skinCluster(skin_cluster, edit=True, removeInfluence=influences_to_remove)
    for joint in influences:
        cmds.setAttr('{}.liw'.format(joint), 1)


def remove_face_joints():
    for joint in cmds.listRelatives('GeFema_Group', allDescendents=True, type='joint'):
        try:
            cmds.setAttr('{}.liw'.format(joint), 1)
        except RuntimeError:
            pass

    for joint in FACE_JOINTS:
        cmds.setAttr('head.liw', 0)
        try:
            cmds.setAttr('{}.liw'.format(joint), 0)
        except RuntimeError:
            pass

    skin_clusters = cmds.ls(type='skinCluster')
    for skin_cluster in skin_clusters:
        mesh_name = cmds.listRelatives(cmds.skinCluster(skin_cluster, query=True, geometry=True)[0], parent=True)[0]
        cmds.select(mesh_name)
        influences = cmds.skinCluster(skin_cluster, query=True, influence=True)
        flood_selection('head', influences, 1)
        cmds.select(clear=True)

    for skin_cluster in skin_clusters:
        remove_influences(skin_cluster)

    cmds.delete(FACE_JOINTS)
    cmds.setToolTo('selectSuperContext')
