import maya.cmds as cmds


def auto_shape_enhancer_g8f(blend_shape_group):
    cmds.setDrivenKeyframe(blend_shape_group, inTangentType='linear', outTangentType='linear', currentDriver='lForearmTwist.rotateX', driverValue=0, attribute='DM_JCM_LForearmBendTwistKEYED', value=0)
    cmds.setDrivenKeyframe(blend_shape_group, inTangentType='linear', outTangentType='linear', currentDriver='lForearmTwist.rotateX', driverValue=-90.0, attribute='DM_JCM_LForearmBendTwistKEYED', value=1)
    cmds.setDrivenKeyframe(blend_shape_group, inTangentType='linear', outTangentType='linear', currentDriver='rFoot.rotateX', driverValue=0, attribute='DM_JCM_R_Foot_Down', value=0)
    cmds.setDrivenKeyframe(blend_shape_group, inTangentType='linear', outTangentType='linear', currentDriver='rFoot.rotateX', driverValue=65.0, attribute='DM_JCM_R_Foot_Down', value=1)
    cmds.setDrivenKeyframe(blend_shape_group, inTangentType='linear', outTangentType='linear', currentDriver=blend_shape_group + '.DM_JCM_Back_Bend01', driverValue=0, attribute='DM_JCM_Back_Bend02', value=0)
    cmds.setDrivenKeyframe(blend_shape_group, inTangentType='linear', outTangentType='linear', currentDriver=blend_shape_group + '.DM_JCM_Back_Bend01', driverValue=1.0, attribute='DM_JCM_Back_Bend02', value=-1)
    cmds.setDrivenKeyframe(blend_shape_group, inTangentType='linear', outTangentType='linear', currentDriver=blend_shape_group + '.pelvis__CTRLMD_N_XRotate_n92_9545', driverValue=0, attribute='DM_JCM_Back_Bend01', value=0)
    cmds.setDrivenKeyframe(blend_shape_group, inTangentType='linear', outTangentType='linear', currentDriver=blend_shape_group + '.pelvis__CTRLMD_N_XRotate_n92_9545', driverValue=1.0, attribute='DM_JCM_Back_Bend01', value=1)
    cmds.setDrivenKeyframe(blend_shape_group, inTangentType='linear', outTangentType='linear', currentDriver='lShin.rotateX', driverValue=0, attribute='DM_JCM_L_Shin_UP', value=0)
    cmds.setDrivenKeyframe(blend_shape_group, inTangentType='linear', outTangentType='linear', currentDriver='lShin.rotateX', driverValue=-11.0, attribute='DM_JCM_L_Shin_UP', value=1)
    cmds.setDrivenKeyframe(blend_shape_group, inTangentType='linear', outTangentType='linear', currentDriver='lCollar.rotateX', driverValue=0, attribute='lCollar__CTRLMD_N_XRotate_n15', value=0)
    cmds.setDrivenKeyframe(blend_shape_group, inTangentType='linear', outTangentType='linear', currentDriver='lCollar.rotateX', driverValue=-15.0, attribute='lCollar__CTRLMD_N_XRotate_n15', value=1)
    cmds.setDrivenKeyframe(blend_shape_group, inTangentType='linear', outTangentType='linear', currentDriver=blend_shape_group + '.DM_JCM_R_Thigh_UP_KEYED', driverValue=0, attribute='DM_JCM_R_G8F_Abs', value=0)
    cmds.setDrivenKeyframe(blend_shape_group, inTangentType='linear', outTangentType='linear', currentDriver=blend_shape_group + '.DM_JCM_R_Thigh_UP_KEYED', driverValue=1.0, attribute='DM_JCM_R_G8F_Abs', value=1)
    cmds.setDrivenKeyframe(blend_shape_group, inTangentType='linear', outTangentType='linear', currentDriver='lShin.rotateY', driverValue=0, attribute='DM_JCM_LKneeTwist__25_L1_KEYED', value=0)
    cmds.setDrivenKeyframe(blend_shape_group, inTangentType='linear', outTangentType='linear', currentDriver='lShin.rotateY', driverValue=-25.0, attribute='DM_JCM_LKneeTwist__25_L1_KEYED', value=1)
    cmds.setDrivenKeyframe(blend_shape_group, currentDriver=blend_shape_group + '.DM_JCM_R_Knee_Bend_KEYED', driverValue=0.5806452, attribute='DM_JCM_R_Knee_Bend', value=0)
    cmds.setDrivenKeyframe(blend_shape_group, currentDriver=blend_shape_group + '.DM_JCM_R_Knee_Bend_KEYED', driverValue=1, attribute='DM_JCM_R_Knee_Bend', value=1)
    cmds.setDrivenKeyframe(blend_shape_group, currentDriver=blend_shape_group + '.DM_JCM_R_Front_110_Bend_40_KEYED', driverValue=0.5454545, attribute='DM_JCM_R_Front_110_Bend_40', value=0)
    cmds.setDrivenKeyframe(blend_shape_group, currentDriver=blend_shape_group + '.DM_JCM_R_Front_110_Bend_40_KEYED', driverValue=1, attribute='DM_JCM_R_Front_110_Bend_40', value=1)
    cmds.setDrivenKeyframe(blend_shape_group, inTangentType='linear', outTangentType='linear', currentDriver=blend_shape_group + '.lThighBend__CTRLMD_N_XRotate_n115', driverValue=0, attribute='DM_JCM_L_Thigh_Bend_115_Side_85_KEYED', value=0)
    cmds.setDrivenKeyframe(blend_shape_group, inTangentType='linear', outTangentType='linear', currentDriver=blend_shape_group + '.lThighBend__CTRLMD_N_XRotate_n115', driverValue=1.0, attribute='DM_JCM_L_Thigh_Bend_115_Side_85_KEYED', value=1)
    cmds.setDrivenKeyframe(blend_shape_group, currentDriver=blend_shape_group + '.DM_JCM_LForearmBendTwistKEYED', driverValue=0.5555556, attribute='DM_JCM_DM_JCM_LForearmBendTwist', value=0)
    cmds.setDrivenKeyframe(blend_shape_group, currentDriver=blend_shape_group + '.DM_JCM_LForearmBendTwistKEYED', driverValue=1, attribute='DM_JCM_DM_JCM_LForearmBendTwist', value=1)
    cmds.setDrivenKeyframe(blend_shape_group, currentDriver=blend_shape_group + '.DM_JCM_L_Forearm_Bend_KEYED', driverValue=0.3703704, attribute='DM_JCM_L_ForearmBone135', value=0)
    cmds.setDrivenKeyframe(blend_shape_group, currentDriver=blend_shape_group + '.DM_JCM_L_Forearm_Bend_KEYED', driverValue=1, attribute='DM_JCM_L_ForearmBone135', value=1)
    cmds.setDrivenKeyframe(blend_shape_group, inTangentType='linear', outTangentType='linear', currentDriver=blend_shape_group + '.lShldrBend__CTRLMD_N_ZRotate_48', driverValue=0, attribute='DM_JCM_L_Bicep', value=0)
    cmds.setDrivenKeyframe(blend_shape_group, inTangentType='linear', outTangentType='linear', currentDriver=blend_shape_group + '.lShldrBend__CTRLMD_N_ZRotate_48', driverValue=1.0, attribute='DM_JCM_L_Bicep', value=1)
    cmds.setDrivenKeyframe(blend_shape_group, inTangentType='linear', outTangentType='linear', currentDriver='rShin.rotateX', driverValue=0, attribute='DM_JCM_R_Shin_UP', value=0)
    cmds.setDrivenKeyframe(blend_shape_group, inTangentType='linear', outTangentType='linear', currentDriver='rShin.rotateX', driverValue=-11.0, attribute='DM_JCM_R_Shin_UP', value=1)
    cmds.setDrivenKeyframe(blend_shape_group, currentDriver=blend_shape_group + '.DM_JCM_L_Knee_Bend_KEYED', driverValue=0.7741935, attribute='DM_JCM_L_Knee_Bend_140', value=0)
    cmds.setDrivenKeyframe(blend_shape_group, currentDriver=blend_shape_group + '.DM_JCM_L_Knee_Bend_KEYED', driverValue=1, attribute='DM_JCM_L_Knee_Bend_140', value=0)
    cmds.setDrivenKeyframe(blend_shape_group, currentDriver=blend_shape_group + '.DM_JCM_L_Knee_Bend_KEYED', driverValue=0.5806452, attribute='DM_JCM_L_Knee_Bend', value=0)
    cmds.setDrivenKeyframe(blend_shape_group, currentDriver=blend_shape_group + '.DM_JCM_L_Knee_Bend_KEYED', driverValue=1, attribute='DM_JCM_L_Knee_Bend', value=1)
    cmds.setDrivenKeyframe(blend_shape_group, inTangentType='linear', outTangentType='linear', currentDriver=blend_shape_group + '.DM_JCM_L_Thigh_Bend_115_Side_85_KEYED', driverValue=0, attribute='DM_JCM_L_Thigh_Side_85_KEYED', value=0)
    cmds.setDrivenKeyframe(blend_shape_group, inTangentType='linear', outTangentType='linear', currentDriver=blend_shape_group + '.DM_JCM_L_Thigh_Bend_115_Side_85_KEYED', driverValue=1.0, attribute='DM_JCM_L_Thigh_Side_85_KEYED', value=-1)
    cmds.setDrivenKeyframe(blend_shape_group, currentDriver=blend_shape_group + '.DM_JCM_L_Front_110_Bend_40_KEYED', driverValue=0.5454545, attribute='DM_JCM_L_Front_110_Bend_40', value=0)
    cmds.setDrivenKeyframe(blend_shape_group, currentDriver=blend_shape_group + '.DM_JCM_L_Front_110_Bend_40_KEYED', driverValue=1, attribute='DM_JCM_L_Front_110_Bend_40', value=1)
    cmds.setDrivenKeyframe(blend_shape_group, currentDriver=blend_shape_group + '.DM_JCM_L_Thigh_UP_KEYED', driverValue=0, attribute='DM_JCM_L_Leg_Muscle', value=0)
    cmds.setDrivenKeyframe(blend_shape_group, currentDriver=blend_shape_group + '.DM_JCM_L_Thigh_UP_KEYED', driverValue=1, attribute='DM_JCM_L_Leg_Muscle', value=0.5)
    cmds.setDrivenKeyframe(blend_shape_group, currentDriver=blend_shape_group + '.DM_JCM_R_Knee_Bend_KEYED', driverValue=0.7741935, attribute='DM_JCM_R_Knee_Bend_140', value=0)
    cmds.setDrivenKeyframe(blend_shape_group, currentDriver=blend_shape_group + '.DM_JCM_R_Knee_Bend_KEYED', driverValue=1, attribute='DM_JCM_R_Knee_Bend_140', value=0)
    cmds.setDrivenKeyframe(blend_shape_group, inTangentType='linear', outTangentType='linear', currentDriver='rShin.rotateX', driverValue=0, attribute='DM_JCM_R_Knee_Bend_KEYED', value=0)
    cmds.setDrivenKeyframe(blend_shape_group, inTangentType='linear', outTangentType='linear', currentDriver='rShin.rotateX', driverValue=155.0, attribute='DM_JCM_R_Knee_Bend_KEYED', value=1)
    cmds.setDrivenKeyframe(blend_shape_group, inTangentType='linear', outTangentType='linear', currentDriver='rShldrTwist.rotateX', driverValue=0, attribute='DM_JCM_R_Shoulder_bend_40_twist__95', value=0)
    cmds.setDrivenKeyframe(blend_shape_group, inTangentType='linear', outTangentType='linear', currentDriver='rShldrTwist.rotateX', driverValue=-95.0, attribute='DM_JCM_R_Shoulder_bend_40_twist__95', value=1)
    cmds.setDrivenKeyframe(blend_shape_group, inTangentType='linear', outTangentType='linear', currentDriver='pelvis.rotateX', driverValue=0, attribute='pelvis__CTRLMD_N_XRotate_n92_9545', value=0)
    cmds.setDrivenKeyframe(blend_shape_group, inTangentType='linear', outTangentType='linear', currentDriver='pelvis.rotateX', driverValue=-92.955, attribute='pelvis__CTRLMD_N_XRotate_n92_9545', value=1)
    cmds.setDrivenKeyframe(blend_shape_group, inTangentType='linear', outTangentType='linear', currentDriver='rCollar.rotateZ', driverValue=0, attribute='rCollar__CTRLMD_N_ZRotate_n55', value=0)
    cmds.setDrivenKeyframe(blend_shape_group, inTangentType='linear', outTangentType='linear', currentDriver='rCollar.rotateZ', driverValue=-55.0, attribute='rCollar__CTRLMD_N_ZRotate_n55', value=1)
    cmds.setDrivenKeyframe(blend_shape_group, currentDriver=blend_shape_group + '.DM_JCM_RForearmBendTwistKEYED', driverValue=0.5555556, attribute='DM_JCM_RForearmBendTwist', value=0)
    cmds.setDrivenKeyframe(blend_shape_group, currentDriver=blend_shape_group + '.DM_JCM_RForearmBendTwistKEYED', driverValue=1, attribute='DM_JCM_RForearmBendTwist', value=1)
    cmds.setDrivenKeyframe(blend_shape_group, inTangentType='linear', outTangentType='linear', currentDriver='rShin.rotateX', driverValue=0, attribute='rShin__CTRLMD_N_XRotate_155', value=0)
    cmds.setDrivenKeyframe(blend_shape_group, inTangentType='linear', outTangentType='linear', currentDriver='rShin.rotateX', driverValue=155.0, attribute='rShin__CTRLMD_N_XRotate_155', value=1)
    cmds.setDrivenKeyframe(blend_shape_group, currentDriver=blend_shape_group + '.DM_JCM_L_Forearm_Bend_KEYED', driverValue=0.5970149, attribute='DM_JCM_L_Forearm_Bend_135', value=0)
    cmds.setDrivenKeyframe(blend_shape_group, currentDriver=blend_shape_group + '.DM_JCM_L_Forearm_Bend_KEYED', driverValue=1, attribute='DM_JCM_L_Forearm_Bend_135', value=1)
    cmds.setDrivenKeyframe(blend_shape_group, inTangentType='linear', outTangentType='linear', currentDriver='lShldrBend.rotateY', driverValue=0, attribute='lShldrBend__CTRLMD_N_YRotate_40', value=0)
    cmds.setDrivenKeyframe(blend_shape_group, inTangentType='linear', outTangentType='linear', currentDriver='lShldrBend.rotateY', driverValue=40.0, attribute='lShldrBend__CTRLMD_N_YRotate_40', value=1)
    cmds.setDrivenKeyframe(blend_shape_group, inTangentType='linear', outTangentType='linear', currentDriver='lShldrTwist.rotateX', driverValue=0, attribute='DM_JCM_L_Shoulder_bend_40_twist__95', value=0)
    cmds.setDrivenKeyframe(blend_shape_group, inTangentType='linear', outTangentType='linear', currentDriver='lShldrTwist.rotateX', driverValue=-95.0, attribute='DM_JCM_L_Shoulder_bend_40_twist__95', value=1)
    cmds.setDrivenKeyframe(blend_shape_group, inTangentType='linear', outTangentType='linear', currentDriver=blend_shape_group + '.DM_JCM_R_Thigh_UP_KEYED', driverValue=0, attribute='DM_JCM_R_Thigh_UPWrinkle_HD', value=0)
    cmds.setDrivenKeyframe(blend_shape_group, inTangentType='linear', outTangentType='linear', currentDriver=blend_shape_group + '.DM_JCM_R_Thigh_UP_KEYED', driverValue=1.0, attribute='DM_JCM_R_Thigh_UPWrinkle_HD', value=1)
    cmds.setDrivenKeyframe(blend_shape_group, inTangentType='linear', outTangentType='linear', currentDriver='lShin.rotateX', driverValue=0, attribute='DM_JCM_LKneeTwist__25_KEYED', value=0)
    cmds.setDrivenKeyframe(blend_shape_group, inTangentType='linear', outTangentType='linear', currentDriver='lShin.rotateX', driverValue=155.0, attribute='DM_JCM_LKneeTwist__25_KEYED', value=1)
    cmds.setDrivenKeyframe(blend_shape_group, inTangentType='linear', outTangentType='linear', currentDriver=blend_shape_group + '.rShldrBend__CTRLMD_N_YRotate_n40', driverValue=0, attribute='DM_JCM_R_Arm_Back_01', value=0)
    cmds.setDrivenKeyframe(blend_shape_group, inTangentType='linear', outTangentType='linear', currentDriver=blend_shape_group + '.rShldrBend__CTRLMD_N_YRotate_n40', driverValue=1.0, attribute='DM_JCM_R_Arm_Back_01', value=1)
    cmds.setDrivenKeyframe(blend_shape_group, currentDriver=blend_shape_group + '.DM_JCM_RKneeTwist__25_L1_KEYED', driverValue=0, attribute='DM_JCM_R_Knee_Twist_15K', value=0)
    cmds.setDrivenKeyframe(blend_shape_group, currentDriver=blend_shape_group + '.DM_JCM_RKneeTwist__25_L1_KEYED', driverValue=1, attribute='DM_JCM_R_Knee_Twist_15K', value=0)
    cmds.setDrivenKeyframe(blend_shape_group, currentDriver=blend_shape_group + '.DM_JCM_L_Knee_Bend_KEYED', driverValue=0, attribute='DM_JCM_L_Knee_Bend_Bone', value=0)
    cmds.setDrivenKeyframe(blend_shape_group, currentDriver=blend_shape_group + '.DM_JCM_L_Knee_Bend_KEYED', driverValue=1, attribute='DM_JCM_L_Knee_Bend_Bone', value=0)
    cmds.setDrivenKeyframe(blend_shape_group, inTangentType='linear', outTangentType='linear', currentDriver='rForearmTwist.rotateX', driverValue=0, attribute='DM_JCM_RForearmBendTwistKEYED', value=0)
    cmds.setDrivenKeyframe(blend_shape_group, inTangentType='linear', outTangentType='linear', currentDriver='rForearmTwist.rotateX', driverValue=-90.0, attribute='DM_JCM_RForearmBendTwistKEYED', value=1)
    cmds.setDrivenKeyframe(blend_shape_group, currentDriver=blend_shape_group + '.DM_JCM_L_Thigh_Side_85_KEYED', driverValue=0.3529412, attribute='DM_JCM_L_Thigh_Side_85', value=0)
    cmds.setDrivenKeyframe(blend_shape_group, currentDriver=blend_shape_group + '.DM_JCM_L_Thigh_Side_85_KEYED', driverValue=1, attribute='DM_JCM_L_Thigh_Side_85', value=1)
    cmds.setDrivenKeyframe(blend_shape_group, inTangentType='linear', outTangentType='linear', currentDriver='rForearmBend.rotateY', driverValue=0, attribute='DM_JCM_R_Forearm_Bend_KEYED', value=0)
    cmds.setDrivenKeyframe(blend_shape_group, inTangentType='linear', outTangentType='linear', currentDriver='rForearmBend.rotateY', driverValue=135.0, attribute='DM_JCM_R_Forearm_Bend_KEYED', value=1)
    cmds.setDrivenKeyframe(blend_shape_group, currentDriver=blend_shape_group + '.DM_JCM_R_T_Bend_115_Side_85_KEYED', driverValue=0.2608696, attribute='DM_JCM_R_Thigh_Bend_115_Side_85', value=0)
    cmds.setDrivenKeyframe(blend_shape_group, currentDriver=blend_shape_group + '.DM_JCM_R_T_Bend_115_Side_85_KEYED', driverValue=1, attribute='DM_JCM_R_Thigh_Bend_115_Side_85', value=1)
    cmds.setDrivenKeyframe(blend_shape_group, currentDriver=blend_shape_group + '.DM_JCM_LKneeTwist__25_L1_KEYED', driverValue=0, attribute='DM_JCM_L_Knee_Twist_15K', value=0)
    cmds.setDrivenKeyframe(blend_shape_group, currentDriver=blend_shape_group + '.DM_JCM_LKneeTwist__25_L1_KEYED', driverValue=1, attribute='DM_JCM_L_Knee_Twist_15K', value=0)
    cmds.setDrivenKeyframe(blend_shape_group, inTangentType='linear', outTangentType='linear', currentDriver=blend_shape_group + '.DM_JCM_L_Thigh_Bend_115_Side_85_KEYED', driverValue=0, attribute='DM_JCM_L_Thigh_UP_KEYED', value=0)
    cmds.setDrivenKeyframe(blend_shape_group, inTangentType='linear', outTangentType='linear', currentDriver=blend_shape_group + '.DM_JCM_L_Thigh_Bend_115_Side_85_KEYED', driverValue=1.0, attribute='DM_JCM_L_Thigh_UP_KEYED', value=-1)
    cmds.setDrivenKeyframe(blend_shape_group, inTangentType='linear', outTangentType='linear', currentDriver='rForearmBend.rotateY', driverValue=0, attribute='rForearmBend__CTRLMD_N_YRotate_90', value=0)
    cmds.setDrivenKeyframe(blend_shape_group, inTangentType='linear', outTangentType='linear', currentDriver='rForearmBend.rotateY', driverValue=90.0, attribute='rForearmBend__CTRLMD_N_YRotate_90', value=1)
    cmds.setDrivenKeyframe(blend_shape_group, inTangentType='linear', outTangentType='linear', currentDriver='rShldrTwist.rotateX', driverValue=0, attribute='DM_JCM_R_Shoulder_bend_40_twist_80', value=0)
    cmds.setDrivenKeyframe(blend_shape_group, inTangentType='linear', outTangentType='linear', currentDriver='rShldrTwist.rotateX', driverValue=80.0, attribute='DM_JCM_R_Shoulder_bend_40_twist_80', value=1)
    cmds.setDrivenKeyframe(blend_shape_group, inTangentType='linear', outTangentType='linear', currentDriver='lShldrTwist.rotateX', driverValue=0, attribute='DM_JCM_L_Shoulder_bend_40_twist_80', value=0)
    cmds.setDrivenKeyframe(blend_shape_group, inTangentType='linear', outTangentType='linear', currentDriver='lShldrTwist.rotateX', driverValue=80.0, attribute='DM_JCM_L_Shoulder_bend_40_twist_80', value=1)
    cmds.setDrivenKeyframe(blend_shape_group, inTangentType='linear', outTangentType='linear', currentDriver='rShin.rotateY', driverValue=0, attribute='DM_JCM_RKneeTwist__25_L1_KEYED', value=0)
    cmds.setDrivenKeyframe(blend_shape_group, inTangentType='linear', outTangentType='linear', currentDriver='rShin.rotateY', driverValue=25.0, attribute='DM_JCM_RKneeTwist__25_L1_KEYED', value=1)
    cmds.setDrivenKeyframe(blend_shape_group, inTangentType='linear', outTangentType='linear', currentDriver=blend_shape_group + '.DM_JCM_L_Forearm_Bend_KEYED', driverValue=0, attribute='DM_JCM_L_Arm_BendWrinkle_HD_div3', value=0)
    cmds.setDrivenKeyframe(blend_shape_group, inTangentType='linear', outTangentType='linear', currentDriver=blend_shape_group + '.DM_JCM_L_Forearm_Bend_KEYED', driverValue=1.0, attribute='DM_JCM_L_Arm_BendWrinkle_HD_div3', value=1)
    cmds.setDrivenKeyframe(blend_shape_group, inTangentType='linear', outTangentType='linear', currentDriver='lShldrBend.rotateY', driverValue=0, attribute='lShldrBend__CTRLMD_N_YRotate_n110', value=0)
    cmds.setDrivenKeyframe(blend_shape_group, inTangentType='linear', outTangentType='linear', currentDriver='lShldrBend.rotateY', driverValue=-110.0, attribute='lShldrBend__CTRLMD_N_YRotate_n110', value=1)
    cmds.setDrivenKeyframe(blend_shape_group, inTangentType='linear', outTangentType='linear', currentDriver='lCollar.rotateZ', driverValue=0, attribute='lCollar__CTRLMD_N_ZRotate_55', value=0)
    cmds.setDrivenKeyframe(blend_shape_group, inTangentType='linear', outTangentType='linear', currentDriver='lCollar.rotateZ', driverValue=55.0, attribute='lCollar__CTRLMD_N_ZRotate_55', value=1)
    cmds.setDrivenKeyframe(blend_shape_group, inTangentType='linear', outTangentType='linear', currentDriver='lShin.rotateX', driverValue=0, attribute='lShin__CTRLMD_N_XRotate_155', value=0)
    cmds.setDrivenKeyframe(blend_shape_group, inTangentType='linear', outTangentType='linear', currentDriver='lShin.rotateX', driverValue=155.0, attribute='lShin__CTRLMD_N_XRotate_155', value=1)
    cmds.setDrivenKeyframe(blend_shape_group, inTangentType='linear', outTangentType='linear', currentDriver=blend_shape_group + '.DM_JCM_L_Knee_Bend', driverValue=0, attribute='DM_JCM_L_Knee_BendWrinkle_HD', value=0)
    cmds.setDrivenKeyframe(blend_shape_group, inTangentType='linear', outTangentType='linear', currentDriver=blend_shape_group + '.DM_JCM_L_Knee_Bend', driverValue=1.0, attribute='DM_JCM_L_Knee_BendWrinkle_HD', value=1)
    cmds.setDrivenKeyframe(blend_shape_group, inTangentType='linear', outTangentType='linear', currentDriver='lCollar.rotateZ', driverValue=0, attribute='DM_JCM_L_Collar_Bend_55', value=0)
    cmds.setDrivenKeyframe(blend_shape_group, inTangentType='linear', outTangentType='linear', currentDriver='lCollar.rotateZ', driverValue=55.0, attribute='DM_JCM_L_Collar_Bend_55', value=1)
    cmds.setDrivenKeyframe(blend_shape_group, inTangentType='linear', outTangentType='linear', currentDriver=blend_shape_group + '.lCollar__CTRLMD_N_XRotate_n15', driverValue=0, attribute='DM_JCM_L_Forearm_Bend_Fix01', value=0)
    cmds.setDrivenKeyframe(blend_shape_group, inTangentType='linear', outTangentType='linear', currentDriver=blend_shape_group + '.lCollar__CTRLMD_N_XRotate_n15', driverValue=1.0, attribute='DM_JCM_L_Forearm_Bend_Fix01', value=1)
    cmds.setDrivenKeyframe(blend_shape_group, currentDriver=blend_shape_group + '.DM_JCM_R_Knee_Bend_KEYED', driverValue=0.5806452, attribute='DM_JCM_R_Knee_Bend_120', value=0)
    cmds.setDrivenKeyframe(blend_shape_group, currentDriver=blend_shape_group + '.DM_JCM_R_Knee_Bend_KEYED', driverValue=1, attribute='DM_JCM_R_Knee_Bend_120', value=0)
    cmds.setDrivenKeyframe(blend_shape_group, inTangentType='linear', outTangentType='linear', currentDriver=blend_shape_group + '.rShldrBend__CTRLMD_N_YRotate_110', driverValue=0, attribute='DM_JCM_Collar_Bend55_R_ArmFB110B40', value=0)
    cmds.setDrivenKeyframe(blend_shape_group, inTangentType='linear', outTangentType='linear', currentDriver=blend_shape_group + '.rShldrBend__CTRLMD_N_YRotate_110', driverValue=1.0, attribute='DM_JCM_Collar_Bend55_R_ArmFB110B40', value=1)
    cmds.setDrivenKeyframe(blend_shape_group, inTangentType='linear', outTangentType='linear', currentDriver=blend_shape_group + '.DM_JCM_R_Knee_Bend', driverValue=0, attribute='DM_JCM_R_Knee_BendWrinkle_HD', value=0)
    cmds.setDrivenKeyframe(blend_shape_group, inTangentType='linear', outTangentType='linear', currentDriver=blend_shape_group + '.DM_JCM_R_Knee_Bend', driverValue=1.0, attribute='DM_JCM_R_Knee_BendWrinkle_HD', value=1)
    cmds.setDrivenKeyframe(blend_shape_group, inTangentType='linear', outTangentType='linear', currentDriver='lThighBend.rotateX', driverValue=0, attribute='lThighBend__CTRLMD_N_XRotate_n115', value=0)
    cmds.setDrivenKeyframe(blend_shape_group, inTangentType='linear', outTangentType='linear', currentDriver='lThighBend.rotateX', driverValue=-115.0, attribute='lThighBend__CTRLMD_N_XRotate_n115', value=1)
    cmds.setDrivenKeyframe(blend_shape_group, currentDriver=blend_shape_group + '.DM_JCM_L_Forearm_Bend_KEYED', driverValue=1, attribute='DM_JCM_L_ForearmBone0', value=0)
    cmds.setDrivenKeyframe(blend_shape_group, currentDriver=blend_shape_group + '.DM_JCM_L_Forearm_Bend_KEYED', driverValue=1, attribute='DM_JCM_L_ForearmBone0', value=0)
    cmds.setDrivenKeyframe(blend_shape_group, currentDriver=blend_shape_group + '.DM_JCM_L_Arm_Down_KEYED', driverValue=0.625, attribute='DM_JCM_L_Arm_Down', value=0)
    cmds.setDrivenKeyframe(blend_shape_group, currentDriver=blend_shape_group + '.DM_JCM_L_Arm_Down_KEYED', driverValue=1, attribute='DM_JCM_L_Arm_Down', value=1)
    cmds.setDrivenKeyframe(blend_shape_group, inTangentType='linear', outTangentType='linear', currentDriver=blend_shape_group + '.DM_JCM_L_Thigh_UP_KEYED', driverValue=0, attribute='DM_JCM_L_G8F_Abs', value=0)
    cmds.setDrivenKeyframe(blend_shape_group, inTangentType='linear', outTangentType='linear', currentDriver=blend_shape_group + '.DM_JCM_L_Thigh_UP_KEYED', driverValue=1.0, attribute='DM_JCM_L_G8F_Abs', value=1)
    cmds.setDrivenKeyframe(blend_shape_group, currentDriver=blend_shape_group + '.DM_JCM_L_Thigh_UP_KEYED', driverValue=0.4347826, attribute='DM_JCM_L_Thigh_UP', value=0)
    cmds.setDrivenKeyframe(blend_shape_group, currentDriver=blend_shape_group + '.DM_JCM_L_Thigh_UP_KEYED', driverValue=1, attribute='DM_JCM_L_Thigh_UP', value=0.85)
    cmds.setDrivenKeyframe(blend_shape_group, inTangentType='linear', outTangentType='linear', currentDriver=blend_shape_group + '.DM_JCM_L_Thigh_UP_KEYED', driverValue=0, attribute='DM_JCM_L_Thigh_UPWrinkle_HD', value=0)
    cmds.setDrivenKeyframe(blend_shape_group, inTangentType='linear', outTangentType='linear', currentDriver=blend_shape_group + '.DM_JCM_L_Thigh_UP_KEYED', driverValue=1.0, attribute='DM_JCM_L_Thigh_UPWrinkle_HD', value=1)
    cmds.setDrivenKeyframe(blend_shape_group, currentDriver=blend_shape_group + '.DM_JCM_R_Forearm_Bend_KEYED', driverValue=1, attribute='DM_JCM_R_ForearmBone0', value=-1)
    cmds.setDrivenKeyframe(blend_shape_group, currentDriver=blend_shape_group + '.DM_JCM_R_Forearm_Bend_KEYED', driverValue=1, attribute='DM_JCM_R_ForearmBone0', value=-1)
    cmds.setDrivenKeyframe(blend_shape_group, inTangentType='linear', outTangentType='linear', currentDriver=blend_shape_group + '.DM_JCM_L_Arm_Down', driverValue=0, attribute='DM_JCM_L_Arm_Down_HD', value=0)
    cmds.setDrivenKeyframe(blend_shape_group, inTangentType='linear', outTangentType='linear', currentDriver=blend_shape_group + '.DM_JCM_L_Arm_Down', driverValue=1.0, attribute='DM_JCM_L_Arm_Down_HD', value=1)
    cmds.setDrivenKeyframe(blend_shape_group, inTangentType='linear', outTangentType='linear', currentDriver='rShldrBend.rotateZ', driverValue=0, attribute='DM_JCM_R_Arm_Down_KEYED', value=0)
    cmds.setDrivenKeyframe(blend_shape_group, inTangentType='linear', outTangentType='linear', currentDriver='rShldrBend.rotateZ', driverValue=40.0, attribute='DM_JCM_R_Arm_Down_KEYED', value=1)
    cmds.setDrivenKeyframe(blend_shape_group, currentDriver=blend_shape_group + '.DM_JCM_R_Thigh_UP_KEYED', driverValue=0.4347826, attribute='DM_JCM_R_Thigh_UP', value=0)
    cmds.setDrivenKeyframe(blend_shape_group, currentDriver=blend_shape_group + '.DM_JCM_R_Thigh_UP_KEYED', driverValue=1, attribute='DM_JCM_R_Thigh_UP', value=0.85)
    cmds.setDrivenKeyframe(blend_shape_group, inTangentType='linear', outTangentType='linear', currentDriver=blend_shape_group + '.DM_JCM_R_Arm_Down', driverValue=0, attribute='DM_JCM_R_Arm_Down_HD', value=0)
    cmds.setDrivenKeyframe(blend_shape_group, inTangentType='linear', outTangentType='linear', currentDriver=blend_shape_group + '.DM_JCM_R_Arm_Down', driverValue=1.0, attribute='DM_JCM_R_Arm_Down_HD', value=1)
    cmds.setDrivenKeyframe(blend_shape_group, inTangentType='linear', outTangentType='linear', currentDriver=blend_shape_group + '.DM_JCM_R_T_Bend_115_Side_85_KEYED', driverValue=0, attribute='DM_JCM_R_Thigh_UP_KEYED', value=0)
    cmds.setDrivenKeyframe(blend_shape_group, inTangentType='linear', outTangentType='linear', currentDriver=blend_shape_group + '.DM_JCM_R_T_Bend_115_Side_85_KEYED', driverValue=1.0, attribute='DM_JCM_R_Thigh_UP_KEYED', value=-1)
    cmds.setDrivenKeyframe(blend_shape_group, inTangentType='linear', outTangentType='linear', currentDriver=blend_shape_group + '.lThighBend__CTRLMD_N_XRotate_n115', driverValue=0, attribute='DM_JCM_L_Knee_Bend115Side85', value=0)
    cmds.setDrivenKeyframe(blend_shape_group, inTangentType='linear', outTangentType='linear', currentDriver=blend_shape_group + '.lThighBend__CTRLMD_N_XRotate_n115', driverValue=1.0, attribute='DM_JCM_L_Knee_Bend115Side85', value=1)
    cmds.setDrivenKeyframe(blend_shape_group, currentDriver=blend_shape_group + '.DM_JCM_RKneeTwist__25_KEYED', driverValue=0.7741935, attribute='DM_JCM_R_Knee_Bend_Twist__25', value=0)
    cmds.setDrivenKeyframe(blend_shape_group, currentDriver=blend_shape_group + '.DM_JCM_RKneeTwist__25_KEYED', driverValue=1, attribute='DM_JCM_R_Knee_Bend_Twist__25', value=1)
    cmds.setDrivenKeyframe(blend_shape_group, inTangentType='linear', outTangentType='linear', currentDriver='rThighBend.rotateX', driverValue=0, attribute='rThighBend__CTRLMD_N_XRotate_n115', value=0)
    cmds.setDrivenKeyframe(blend_shape_group, inTangentType='linear', outTangentType='linear', currentDriver='rThighBend.rotateX', driverValue=-115.0, attribute='rThighBend__CTRLMD_N_XRotate_n115', value=1)
    cmds.setDrivenKeyframe(blend_shape_group, currentDriver=blend_shape_group + '.DM_JCM_L_Thigh_Bend_115_Side_85_KEYED', driverValue=0, attribute='DM_JCM_L_Thigh_Bend_115_Side_85', value=0)
    cmds.setDrivenKeyframe(blend_shape_group, currentDriver=blend_shape_group + '.DM_JCM_L_Thigh_Bend_115_Side_85_KEYED', driverValue=1, attribute='DM_JCM_L_Thigh_Bend_115_Side_85', value=0.7811159)
    cmds.setDrivenKeyframe(blend_shape_group, inTangentType='linear', outTangentType='linear', currentDriver=blend_shape_group + '.rThighBend__CTRLMD_N_XRotate_n115', driverValue=0, attribute='DM_JCM_R_Knee_Bend115Side85', value=0)
    cmds.setDrivenKeyframe(blend_shape_group, inTangentType='linear', outTangentType='linear', currentDriver=blend_shape_group + '.rThighBend__CTRLMD_N_XRotate_n115', driverValue=1.0, attribute='DM_JCM_R_Knee_Bend115Side85', value=1)
    cmds.setDrivenKeyframe(blend_shape_group, inTangentType='linear', outTangentType='linear', currentDriver='lShldrBend.rotateZ', driverValue=0, attribute='lShldrBend__CTRLMD_N_ZRotate_48', value=0)
    cmds.setDrivenKeyframe(blend_shape_group, inTangentType='linear', outTangentType='linear', currentDriver='lShldrBend.rotateZ', driverValue=48.0, attribute='lShldrBend__CTRLMD_N_ZRotate_48', value=1)
    cmds.setDrivenKeyframe(blend_shape_group, inTangentType='linear', outTangentType='linear', currentDriver='lForearmBend.rotateY', driverValue=0, attribute='DM_JCM_L_Forearm_Bend_KEYED', value=0)
    cmds.setDrivenKeyframe(blend_shape_group, inTangentType='linear', outTangentType='linear', currentDriver='lForearmBend.rotateY', driverValue=-135.0, attribute='DM_JCM_L_Forearm_Bend_KEYED', value=1)
    cmds.setDrivenKeyframe(blend_shape_group, inTangentType='linear', outTangentType='linear', currentDriver='rShldrBend.rotateY', driverValue=0, attribute='rShldrBend__CTRLMD_N_YRotate_110', value=0)
    cmds.setDrivenKeyframe(blend_shape_group, inTangentType='linear', outTangentType='linear', currentDriver='rShldrBend.rotateY', driverValue=110.0, attribute='rShldrBend__CTRLMD_N_YRotate_110', value=1)
    cmds.setDrivenKeyframe(blend_shape_group, inTangentType='linear', outTangentType='linear', currentDriver='rShin.rotateX', driverValue=0, attribute='DM_JCM_RKneeTwist__25_KEYED', value=0)
    cmds.setDrivenKeyframe(blend_shape_group, inTangentType='linear', outTangentType='linear', currentDriver='rShin.rotateX', driverValue=155.0, attribute='DM_JCM_RKneeTwist__25_KEYED', value=1)
    cmds.setDrivenKeyframe(blend_shape_group, inTangentType='linear', outTangentType='linear', currentDriver=blend_shape_group + '.DM_JCM_Rib_Cage_Keyed', driverValue=0, attribute='DM_JCM_Rib_Cage_HD_div3', value=0)
    cmds.setDrivenKeyframe(blend_shape_group, inTangentType='linear', outTangentType='linear', currentDriver=blend_shape_group + '.DM_JCM_Rib_Cage_Keyed', driverValue=1.0, attribute='DM_JCM_Rib_Cage_HD_div3', value=1)
    cmds.setDrivenKeyframe(blend_shape_group, inTangentType='linear', outTangentType='linear', currentDriver=blend_shape_group + '.DM_JCM_R_Forearm_Bend_KEYED', driverValue=0, attribute='DM_JCM_Arm_BendWrinkle_HD_div3', value=0)
    cmds.setDrivenKeyframe(blend_shape_group, inTangentType='linear', outTangentType='linear', currentDriver=blend_shape_group + '.DM_JCM_R_Forearm_Bend_KEYED', driverValue=1.0, attribute='DM_JCM_Arm_BendWrinkle_HD_div3', value=1)
    cmds.setDrivenKeyframe(blend_shape_group, inTangentType='linear', outTangentType='linear', currentDriver=blend_shape_group + '.rCollar__CTRLMD_N_YRotate_10', driverValue=0, attribute='DM_JCM_R_Forearm_Bend_Fix01', value=0)
    cmds.setDrivenKeyframe(blend_shape_group, inTangentType='linear', outTangentType='linear', currentDriver=blend_shape_group + '.rCollar__CTRLMD_N_YRotate_10', driverValue=1.0, attribute='DM_JCM_R_Forearm_Bend_Fix01', value=1)
    cmds.setDrivenKeyframe(blend_shape_group, currentDriver=blend_shape_group + '.DM_JCM_LKneeTwist__25_KEYED', driverValue=0.5806452, attribute='DM_JCM_L_Knee_Bend_Twist__25', value=0)
    cmds.setDrivenKeyframe(blend_shape_group, currentDriver=blend_shape_group + '.DM_JCM_LKneeTwist__25_KEYED', driverValue=1, attribute='DM_JCM_L_Knee_Bend_Twist__25', value=1)
    cmds.setDrivenKeyframe(blend_shape_group, inTangentType='linear', outTangentType='linear', currentDriver=blend_shape_group + '.lShldrBend__CTRLMD_N_YRotate_n110', driverValue=0, attribute='DM_JCM_L_Front_110_Bend_40_KEYED', value=0)
    cmds.setDrivenKeyframe(blend_shape_group, inTangentType='linear', outTangentType='linear', currentDriver=blend_shape_group + '.lShldrBend__CTRLMD_N_YRotate_n110', driverValue=1.0, attribute='DM_JCM_L_Front_110_Bend_40_KEYED', value=1)
    cmds.setDrivenKeyframe(blend_shape_group, currentDriver=blend_shape_group + '.DM_JCM_R_Forearm_Bend_KEYED', driverValue=0.6666667, attribute='DM_JCM_R_Forearm_Bend_135', value=0)
    cmds.setDrivenKeyframe(blend_shape_group, currentDriver=blend_shape_group + '.DM_JCM_R_Forearm_Bend_KEYED', driverValue=1, attribute='DM_JCM_R_Forearm_Bend_135', value=1)
    cmds.setDrivenKeyframe(blend_shape_group, inTangentType='linear', outTangentType='linear', currentDriver='lShldrBend.rotateZ', driverValue=0, attribute='DM_JCM_L_Arm_Up_L2', value=0)
    cmds.setDrivenKeyframe(blend_shape_group, inTangentType='linear', outTangentType='linear', currentDriver='lShldrBend.rotateZ', driverValue=60.0, attribute='DM_JCM_L_Arm_Up_L2', value=1)
    cmds.setDrivenKeyframe(blend_shape_group, inTangentType='linear', outTangentType='linear', currentDriver=blend_shape_group + '.DM_JCM_Rib_Cage_Keyed', driverValue=0, attribute='DM_JCM_Rib_Cage_2_HD_div3', value=0)
    cmds.setDrivenKeyframe(blend_shape_group, inTangentType='linear', outTangentType='linear', currentDriver=blend_shape_group + '.DM_JCM_Rib_Cage_Keyed', driverValue=1.0, attribute='DM_JCM_Rib_Cage_2_HD_div3', value=1)
    cmds.setDrivenKeyframe(blend_shape_group, inTangentType='linear', outTangentType='linear', currentDriver=blend_shape_group + '.rForearmBend__CTRLMD_N_YRotate_90', driverValue=0, attribute='DM_JCM_R_Bicep', value=0)
    cmds.setDrivenKeyframe(blend_shape_group, inTangentType='linear', outTangentType='linear', currentDriver=blend_shape_group + '.rForearmBend__CTRLMD_N_YRotate_90', driverValue=1.0, attribute='DM_JCM_R_Bicep', value=1)
    cmds.setDrivenKeyframe(blend_shape_group, inTangentType='linear', outTangentType='linear', currentDriver='rCollar.rotateZ', driverValue=0, attribute='DM_JCM_R_Collar_Bend_55', value=0)
    cmds.setDrivenKeyframe(blend_shape_group, inTangentType='linear', outTangentType='linear', currentDriver='rCollar.rotateZ', driverValue=-55.0, attribute='DM_JCM_R_Collar_Bend_55', value=1)
    cmds.setDrivenKeyframe(blend_shape_group, inTangentType='linear', outTangentType='linear', currentDriver=blend_shape_group + '.rShin__CTRLMD_N_XRotate_155', driverValue=0, attribute='DM_JCM_R_Knee_BendTw10B155', value=0)
    cmds.setDrivenKeyframe(blend_shape_group, inTangentType='linear', outTangentType='linear', currentDriver=blend_shape_group + '.rShin__CTRLMD_N_XRotate_155', driverValue=1.0, attribute='DM_JCM_R_Knee_BendTw10B155', value=1)
    cmds.setDrivenKeyframe(blend_shape_group, inTangentType='linear', outTangentType='linear', currentDriver=blend_shape_group + '.DM_JCM_R_T_Bend_115_Side_85_KEYED', driverValue=0, attribute='DM_JCM_R_Thigh_Side__85_KEYED', value=0)
    cmds.setDrivenKeyframe(blend_shape_group, inTangentType='linear', outTangentType='linear', currentDriver=blend_shape_group + '.DM_JCM_R_T_Bend_115_Side_85_KEYED', driverValue=1.0, attribute='DM_JCM_R_Thigh_Side__85_KEYED', value=-1)
    cmds.setDrivenKeyframe(blend_shape_group, inTangentType='linear', outTangentType='linear', currentDriver='rShldrBend.rotateY', driverValue=0, attribute='rShldrBend__CTRLMD_N_YRotate_n40', value=0)
    cmds.setDrivenKeyframe(blend_shape_group, inTangentType='linear', outTangentType='linear', currentDriver='rShldrBend.rotateY', driverValue=-40.0, attribute='rShldrBend__CTRLMD_N_YRotate_n40', value=1)
    cmds.setDrivenKeyframe(blend_shape_group, inTangentType='linear', outTangentType='linear', currentDriver='lThighBend.rotateX', driverValue=0, attribute='lThighBend__CTRLMD_N_XRotate_n27_5432', value=0)
    cmds.setDrivenKeyframe(blend_shape_group, inTangentType='linear', outTangentType='linear', currentDriver='lThighBend.rotateX', driverValue=-27.543, attribute='lThighBend__CTRLMD_N_XRotate_n27_5432', value=1)
    cmds.setDrivenKeyframe(blend_shape_group, inTangentType='linear', outTangentType='linear', currentDriver=blend_shape_group + '.lCollar__CTRLMD_N_ZRotate_55', driverValue=0, attribute='DM_JCM_L_Arm_Down_Showder_UP', value=0)
    cmds.setDrivenKeyframe(blend_shape_group, inTangentType='linear', outTangentType='linear', currentDriver=blend_shape_group + '.lCollar__CTRLMD_N_ZRotate_55', driverValue=1.0, attribute='DM_JCM_L_Arm_Down_Showder_UP', value=1)
    cmds.setDrivenKeyframe(blend_shape_group, inTangentType='linear', outTangentType='linear', currentDriver=blend_shape_group + '.lShldrBend__CTRLMD_N_YRotate_40', driverValue=0, attribute='DM_JCM_L_Arm_Back_01', value=0)
    cmds.setDrivenKeyframe(blend_shape_group, inTangentType='linear', outTangentType='linear', currentDriver=blend_shape_group + '.lShldrBend__CTRLMD_N_YRotate_40', driverValue=1.0, attribute='DM_JCM_L_Arm_Back_01', value=1)
    cmds.setDrivenKeyframe(blend_shape_group, inTangentType='linear', outTangentType='linear', currentDriver='chestLower.rotateX', driverValue=0, attribute='DM_JCM_Rib_Cage_Keyed', value=0)
    cmds.setDrivenKeyframe(blend_shape_group, inTangentType='linear', outTangentType='linear', currentDriver='chestLower.rotateX', driverValue=-25.0, attribute='DM_JCM_Rib_Cage_Keyed', value=1)
    cmds.setDrivenKeyframe(blend_shape_group, inTangentType='linear', outTangentType='linear', currentDriver=blend_shape_group + '.lShin__CTRLMD_N_XRotate_155', driverValue=0, attribute='DM_JCM_L_Knee_BendTw10B155', value=0)
    cmds.setDrivenKeyframe(blend_shape_group, inTangentType='linear', outTangentType='linear', currentDriver=blend_shape_group + '.lShin__CTRLMD_N_XRotate_155', driverValue=1.0, attribute='DM_JCM_L_Knee_BendTw10B155', value=1)
    cmds.setDrivenKeyframe(blend_shape_group, inTangentType='linear', outTangentType='linear', currentDriver='lShldrBend.rotateZ', driverValue=0, attribute='DM_JCM_L_Arm_Down_KEYED', value=0)
    cmds.setDrivenKeyframe(blend_shape_group, inTangentType='linear', outTangentType='linear', currentDriver='lShldrBend.rotateZ', driverValue=-40.0, attribute='DM_JCM_L_Arm_Down_KEYED', value=1)
    cmds.setDrivenKeyframe(blend_shape_group, currentDriver=blend_shape_group + '.DM_JCM_L_Knee_Bend_KEYED', driverValue=0.5806452, attribute='DM_JCM_L_Knee_Bend_120', value=0)
    cmds.setDrivenKeyframe(blend_shape_group, currentDriver=blend_shape_group + '.DM_JCM_L_Knee_Bend_KEYED', driverValue=1, attribute='DM_JCM_L_Knee_Bend_120', value=0)
    cmds.setDrivenKeyframe(blend_shape_group, inTangentType='linear', outTangentType='linear', currentDriver=blend_shape_group + '.rShldrBend__CTRLMD_N_YRotate_110', driverValue=0, attribute='DM_JCM_R_Front_110_Bend_40_KEYED', value=0)
    cmds.setDrivenKeyframe(blend_shape_group, inTangentType='linear', outTangentType='linear', currentDriver=blend_shape_group + '.rShldrBend__CTRLMD_N_YRotate_110', driverValue=1.0, attribute='DM_JCM_R_Front_110_Bend_40_KEYED', value=1)
    cmds.setDrivenKeyframe(blend_shape_group, inTangentType='linear', outTangentType='linear', currentDriver=blend_shape_group + '.rThighBend__CTRLMD_N_XRotate_n115', driverValue=0, attribute='DM_JCM_R_T_Bend_115_Side_85_KEYED', value=0)
    cmds.setDrivenKeyframe(blend_shape_group, inTangentType='linear', outTangentType='linear', currentDriver=blend_shape_group + '.rThighBend__CTRLMD_N_XRotate_n115', driverValue=1.0, attribute='DM_JCM_R_T_Bend_115_Side_85_KEYED', value=1)
    cmds.setDrivenKeyframe(blend_shape_group, inTangentType='linear', outTangentType='linear', currentDriver=blend_shape_group + '.rCollar__CTRLMD_N_ZRotate_n55', driverValue=0, attribute='DM_JCM_R_Arm_Down_Showder_UP', value=0)
    cmds.setDrivenKeyframe(blend_shape_group, inTangentType='linear', outTangentType='linear', currentDriver=blend_shape_group + '.rCollar__CTRLMD_N_ZRotate_n55', driverValue=1.0, attribute='DM_JCM_R_Arm_Down_Showder_UP', value=1)
    cmds.setDrivenKeyframe(blend_shape_group, inTangentType='linear', outTangentType='linear', currentDriver='lFoot.rotateX', driverValue=0, attribute='DM_JCM_L_Foot_Down', value=0)
    cmds.setDrivenKeyframe(blend_shape_group, inTangentType='linear', outTangentType='linear', currentDriver='lFoot.rotateX', driverValue=65.0, attribute='DM_JCM_L_Foot_Down', value=1)
    cmds.setDrivenKeyframe(blend_shape_group, currentDriver=blend_shape_group + '.DM_JCM_R_Arm_Down_KEYED', driverValue=0.625, attribute='DM_JCM_R_Arm_Down', value=0)
    cmds.setDrivenKeyframe(blend_shape_group, currentDriver=blend_shape_group + '.DM_JCM_R_Arm_Down_KEYED', driverValue=1, attribute='DM_JCM_R_Arm_Down', value=1)
    cmds.setDrivenKeyframe(blend_shape_group, inTangentType='linear', outTangentType='linear', currentDriver=blend_shape_group + '.lShldrBend__CTRLMD_N_YRotate_n110', driverValue=0, attribute='DM_JCM_Collar_Bend55_L_ArmFB110B40', value=0)
    cmds.setDrivenKeyframe(blend_shape_group, inTangentType='linear', outTangentType='linear', currentDriver=blend_shape_group + '.lShldrBend__CTRLMD_N_YRotate_n110', driverValue=1.0, attribute='DM_JCM_Collar_Bend55_L_ArmFB110B40', value=1)
    cmds.setDrivenKeyframe(blend_shape_group, inTangentType='linear', outTangentType='linear', currentDriver='lShin.rotateX', driverValue=0, attribute='DM_JCM_L_Knee_Bend_KEYED', value=0)
    cmds.setDrivenKeyframe(blend_shape_group, inTangentType='linear', outTangentType='linear', currentDriver='lShin.rotateX', driverValue=155.0, attribute='DM_JCM_L_Knee_Bend_KEYED', value=1)
    cmds.setDrivenKeyframe(blend_shape_group, currentDriver=blend_shape_group + '.DM_JCM_R_Thigh_Side__85_KEYED', driverValue=0.3529412, attribute='DM_JCM_R_Thigh_Side__85', value=0)
    cmds.setDrivenKeyframe(blend_shape_group, currentDriver=blend_shape_group + '.DM_JCM_R_Thigh_Side__85_KEYED', driverValue=1, attribute='DM_JCM_R_Thigh_Side__85', value=1)
    cmds.setDrivenKeyframe(blend_shape_group, currentDriver=blend_shape_group + '.DM_JCM_R_Forearm_Bend_KEYED', driverValue=0.3703704, attribute='DM_JCM_R_ForearmBone135', value=0)
    cmds.setDrivenKeyframe(blend_shape_group, currentDriver=blend_shape_group + '.DM_JCM_R_Forearm_Bend_KEYED', driverValue=1, attribute='DM_JCM_R_ForearmBone135', value=1)
    cmds.setDrivenKeyframe(blend_shape_group, currentDriver=blend_shape_group + '.DM_JCM_R_Knee_Bend_KEYED', driverValue=0, attribute='DM_JCM_R_Knee_Bend_Bone', value=0)
    cmds.setDrivenKeyframe(blend_shape_group, currentDriver=blend_shape_group + '.DM_JCM_R_Knee_Bend_KEYED', driverValue=1, attribute='DM_JCM_R_Knee_Bend_Bone', value=0)
    cmds.setDrivenKeyframe(blend_shape_group, currentDriver=blend_shape_group + '.DM_JCM_R_Thigh_UP_KEYED', driverValue=0, attribute='DM_JCM_R_Leg_Muscle', value=0)
    cmds.setDrivenKeyframe(blend_shape_group, currentDriver=blend_shape_group + '.DM_JCM_R_Thigh_UP_KEYED', driverValue=1, attribute='DM_JCM_R_Leg_Muscle', value=0.5)
    cmds.setDrivenKeyframe(blend_shape_group, inTangentType='linear', outTangentType='linear', currentDriver='rShldrBend.rotateZ', driverValue=0, attribute='DM_JCM_R_Arm_Up_L2', value=0)
    cmds.setDrivenKeyframe(blend_shape_group, inTangentType='linear', outTangentType='linear', currentDriver='rShldrBend.rotateZ', driverValue=-60.0, attribute='DM_JCM_R_Arm_Up_L2', value=1)
    cmds.setDrivenKeyframe(blend_shape_group, inTangentType='linear', outTangentType='linear', currentDriver='rCollar.rotateY', driverValue=0, attribute='rCollar__CTRLMD_N_YRotate_10', value=0)
    cmds.setDrivenKeyframe(blend_shape_group, inTangentType='linear', outTangentType='linear', currentDriver='rCollar.rotateY', driverValue=10.0, attribute='rCollar__CTRLMD_N_YRotate_10', value=1)


def auto_shape_enhancer_g8f_2(blend_shape_group):
    # DM_JCM_Arm_BendWrinkle_HD_div3
    # DM_JCM_Back_Bend01
    # DM_JCM_Back_Bend02
    # DM_JCM_Collar_Bend55_L_ArmFB110B40
    # DM_JCM_Collar_Bend55_R_ArmFB110B40
    # DM_JCM_DM_JCM_LForearmBendTwist
    # DM_JCM_LForearmBendTwistKEYED
    # DM_JCM_LKneeTwist__25_KEYED
    # DM_JCM_LKneeTwist__25_L1_KEYED
    # DM_JCM_L_Arm_Back_01
    # DM_JCM_L_Arm_BendWrinkle_HD_div3
    # DM_JCM_L_Arm_Down
    # DM_JCM_L_Arm_Down_HD
    # DM_JCM_L_Arm_Down_KEYED
    # DM_JCM_L_Arm_Down_Showder_UP

    # DM_JCM_L_Arm_Up_L2
    cmds.setDrivenKeyframe(
        blend_shape_group,
        currentDriver='lShldrBend.rotateZ',
        driverValue=0,
        attribute='DM_JCM_L_Arm_Up_L2',
        value=0
    )
    cmds.setDrivenKeyframe(
        blend_shape_group,
        currentDriver='lShldrBend.rotateZ',
        driverValue=90,
        attribute='DM_JCM_L_Arm_Up_L2',
        value=1
    )

    # DM_JCM_L_Bicep

    # DM_JCM_L_Collar_Bend_55
    cmds.setDrivenKeyframe(
        blend_shape_group,
        currentDriver='lCollar.rotateZ',
        driverValue=0,
        attribute='DM_JCM_L_Collar_Bend_55',
        value=0
    )
    cmds.setDrivenKeyframe(
        blend_shape_group,
        currentDriver='lCollar.rotateZ',
        driverValue=55,
        attribute='DM_JCM_L_Collar_Bend_55',
        value=1
    )
    # DM_JCM_L_Foot_Down
    # DM_JCM_L_ForearmBone0
    # DM_JCM_L_ForearmBone135
    # DM_JCM_L_Forearm_Bend_135
    # DM_JCM_L_Forearm_Bend_Fix01
    # DM_JCM_L_Forearm_Bend_KEYED
    # DM_JCM_L_Front_110_Bend_40
    # DM_JCM_L_Front_110_Bend_40_KEYED
    # DM_JCM_L_G8F_Abs
    # DM_JCM_L_Knee_Bend
    # DM_JCM_L_Knee_Bend115Side85
    # DM_JCM_L_Knee_BendTw10B155
    # DM_JCM_L_Knee_BendWrinkle_HD
    # DM_JCM_L_Knee_Bend_120
    # DM_JCM_L_Knee_Bend_140
    # DM_JCM_L_Knee_Bend_Bone
    # DM_JCM_L_Knee_Bend_KEYED
    # DM_JCM_L_Knee_Bend_Twist__25
    # DM_JCM_L_Knee_Twist_15K
    # DM_JCM_L_Leg_Muscle
    # DM_JCM_L_Shin_UP
    # DM_JCM_L_Shoulder_bend_40_twist_80
    # DM_JCM_L_Shoulder_bend_40_twist__95
    # DM_JCM_L_Thigh_Bend_115_Side_85
    # DM_JCM_L_Thigh_Bend_115_Side_85_KEYED
    # DM_JCM_L_Thigh_Side_85
    # DM_JCM_L_Thigh_Side_85_KEYED
    # DM_JCM_L_Thigh_UP
    # DM_JCM_L_Thigh_UPWrinkle_HD
    # DM_JCM_L_Thigh_UP_KEYED
    # DM_JCM_RForearmBendTwist
    # DM_JCM_RForearmBendTwistKEYED
    # DM_JCM_RKneeTwist__25_KEYED
    # DM_JCM_RKneeTwist__25_L1_KEYED
    # DM_JCM_R_Arm_Back_01
    # DM_JCM_R_Arm_Down
    # DM_JCM_R_Arm_Down_HD
    # DM_JCM_R_Arm_Down_KEYED
    # DM_JCM_R_Arm_Down_Showder_UP

    # DM_JCM_R_Arm_Up_L2
    cmds.setDrivenKeyframe(
        blend_shape_group,
        currentDriver='rShldrBend.rotateZ',
        driverValue=0,
        attribute='DM_JCM_R_Arm_Up_L2',
        value=0
    )
    cmds.setDrivenKeyframe(
        blend_shape_group,
        currentDriver='rShldrBend.rotateZ',
        driverValue=-90,
        attribute='DM_JCM_R_Arm_Up_L2',
        value=1
    )

    # DM_JCM_R_Bicep

    # DM_JCM_R_Collar_Bend_55
    cmds.setDrivenKeyframe(
        blend_shape_group,
        currentDriver='rCollar.rotateZ',
        driverValue=0,
        attribute='DM_JCM_R_Collar_Bend_55',
        value=0
    )
    cmds.setDrivenKeyframe(
        blend_shape_group,
        currentDriver='rCollar.rotateZ',
        driverValue=-55,
        attribute='DM_JCM_R_Collar_Bend_55',
        value=1
    )
    # DM_JCM_R_Foot_Down
    # DM_JCM_R_ForearmBone0
    # DM_JCM_R_ForearmBone135
    # DM_JCM_R_Forearm_Bend_135
    # DM_JCM_R_Forearm_Bend_Fix01
    # DM_JCM_R_Forearm_Bend_KEYED
    # DM_JCM_R_Front_110_Bend_40
    # DM_JCM_R_Front_110_Bend_40_KEYED
    # DM_JCM_R_G8F_Abs
    # DM_JCM_R_Knee_Bend
    # DM_JCM_R_Knee_Bend115Side85
    # DM_JCM_R_Knee_BendTw10B155
    # DM_JCM_R_Knee_BendWrinkle_HD
    # DM_JCM_R_Knee_Bend_120
    # DM_JCM_R_Knee_Bend_140
    # DM_JCM_R_Knee_Bend_Bone
    # DM_JCM_R_Knee_Bend_KEYED
    # DM_JCM_R_Knee_Bend_Twist__25
    # DM_JCM_R_Knee_Twist_15K
    # DM_JCM_R_Leg_Muscle
    # DM_JCM_R_Shin_UP
    # DM_JCM_R_Shoulder_bend_40_twist_80
    # DM_JCM_R_Shoulder_bend_40_twist__95
    # DM_JCM_R_T_Bend_115_Side_85_KEYED
    # DM_JCM_R_Thigh_Bend_115_Side_85
    # DM_JCM_R_Thigh_Side__85
    # DM_JCM_R_Thigh_Side__85_KEYED
    # DM_JCM_R_Thigh_UP
    # DM_JCM_R_Thigh_UPWrinkle_HD
    # DM_JCM_R_Thigh_UP_KEYED
    # DM_JCM_Rib_Cage_2_HD_div3
    # DM_JCM_Rib_Cage_HD_div3
    # DM_JCM_Rib_Cage_Keyed
