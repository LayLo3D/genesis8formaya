import maya.mel as mel
import maya.cmds as cmds


def Floyd8JCMs(character, blendShapeGroup):
    pyBlendShapesName = blendShapeGroup
    if not cmds.objExists('{}.pJCMFloyd8AbdomenLowerBndFwd'.format(pyBlendShapesName)):
        error = character + ' specific JCMs were not created.\n\n' + character + ' JCMs were either not exported out properly or the wrong file was selected for import.'
        cmds.warning(error.replace('\n\n', ' '))
        return error

    # pJCMFloyd8AbdomenLowerBndFwd
    mel.eval('setAttr "abdomenLower.rotateX" 0;')
    mel.eval('setDrivenKeyframe -currentDriver abdomenLower.rotateX {0}.pJCMFloyd8AbdomenLowerBndFwd;'.format(pyBlendShapesName))
    mel.eval('setAttr "abdomenLower.rotateX" 30;')
    mel.eval('setAttr "{0}.pJCMFloyd8AbdomenLowerBndFwd" 1;'.format(pyBlendShapesName))
    mel.eval('setDrivenKeyframe -currentDriver abdomenLower.rotateX {0}.pJCMFloyd8AbdomenLowerBndFwd;'.format(pyBlendShapesName))
    mel.eval('setAttr "abdomenLower.rotateX" 0;')

    # pJCMFloyd8AbdomenUpperBndFwd
    mel.eval('setAttr "abdomenUpper.rotateX" 0;')
    mel.eval('setDrivenKeyframe -currentDriver abdomenUpper.rotateX {0}.pJCMFloyd8AbdomenUpperBndFwd;'.format(
        pyBlendShapesName))
    mel.eval('setAttr "abdomenUpper.rotateX" 40;')
    mel.eval('setAttr "{0}.pJCMFloyd8AbdomenUpperBndFwd" 1;'.format(pyBlendShapesName))
    mel.eval('setDrivenKeyframe -currentDriver abdomenUpper.rotateX {0}.pJCMFloyd8AbdomenUpperBndFwd;'.format(
        pyBlendShapesName))
    mel.eval('setAttr "abdomenUpper.rotateX" 0;')

    # pJCMFloyd8ChestLowerBndFwd
    mel.eval('setAttr "chestLower.rotateX" 0;')
    mel.eval('setDrivenKeyframe -currentDriver chestLower.rotateX {0}.pJCMFloyd8ChestLowerBndFwd;'.format(
        pyBlendShapesName))
    mel.eval('setAttr "chestLower.rotateX" 35;')
    mel.eval('setAttr "{0}.pJCMFloyd8ChestLowerBndFwd" 1;'.format(pyBlendShapesName))
    mel.eval('setDrivenKeyframe -currentDriver chestLower.rotateX {0}.pJCMFloyd8ChestLowerBndFwd;'.format(
        pyBlendShapesName))
    mel.eval('setAttr "chestLower.rotateX" 0;')

    # pJCMFloyd8CollarBndUpL
    mel.eval('setAttr "lCollar.rotateZ" 0;')
    mel.eval(
        'setDrivenKeyframe -currentDriver lCollar.rotateZ {0}.pJCMFloyd8CollarBndUpL;'.format(pyBlendShapesName))
    mel.eval('setAttr "lCollar.rotateZ" 55;')
    mel.eval('setAttr "{0}.pJCMFloyd8CollarBndUpL" 1;'.format(pyBlendShapesName))
    mel.eval(
        'setDrivenKeyframe -currentDriver lCollar.rotateZ {0}.pJCMFloyd8CollarBndUpL;'.format(pyBlendShapesName))
    mel.eval('setAttr "lCollar.rotateZ" 0;')

    # pJCMFloyd8CollarBndUpR
    mel.eval('setAttr "rCollar.rotateZ" 0;')
    mel.eval(
        'setDrivenKeyframe -currentDriver rCollar.rotateZ {0}.pJCMFloyd8CollarBndUpR;'.format(pyBlendShapesName))
    mel.eval('setAttr "rCollar.rotateZ" -55;')
    mel.eval('setAttr "{0}.pJCMFloyd8CollarBndUpR" 1;'.format(pyBlendShapesName))
    mel.eval(
        'setDrivenKeyframe -currentDriver rCollar.rotateZ {0}.pJCMFloyd8CollarBndUpR;'.format(pyBlendShapesName))
    mel.eval('setAttr "rCollar.rotateZ" 0;')

    # pJCMFloyd8FootBndUpL
    mel.eval('setAttr "lFoot.rotateX" 0;')
    mel.eval('setDrivenKeyframe -currentDriver lFoot.rotateX {0}.pJCMFloyd8FootBndUpL;'.format(pyBlendShapesName))
    mel.eval('setAttr "lFoot.rotateX" -45;')
    mel.eval('setAttr "{0}.pJCMFloyd8FootBndUpL" 1;'.format(pyBlendShapesName))
    mel.eval('setDrivenKeyframe -currentDriver lFoot.rotateX {0}.pJCMFloyd8FootBndUpL;'.format(pyBlendShapesName))
    mel.eval('setAttr "lFoot.rotateX" 0;')

    # pJCMFloyd8FootBndUpR
    mel.eval('setAttr "rFoot.rotateX" 0;')
    mel.eval('setDrivenKeyframe -currentDriver rFoot.rotateX {0}.pJCMFloyd8FootBndUpR;'.format(pyBlendShapesName))
    mel.eval('setAttr "rFoot.rotateX" -45;')
    mel.eval('setAttr "{0}.pJCMFloyd8FootBndUpR" 1;'.format(pyBlendShapesName))
    mel.eval('setDrivenKeyframe -currentDriver rFoot.rotateX {0}.pJCMFloyd8FootBndUpR;'.format(pyBlendShapesName))
    mel.eval('setAttr "rFoot.rotateX" 0;')

    # pJCMFloyd8ForearmBndL
    mel.eval('setAttr "lForearmBend.rotateY" 0;')
    mel.eval('setDrivenKeyframe -currentDriver lForearmBend.rotateY {0}.pJCMFloyd8ForearmBndL;'.format(
        pyBlendShapesName))
    mel.eval('setAttr "lForearmBend.rotateY" -135;')
    mel.eval('setAttr "{0}.pJCMFloyd8ForearmBndL" 1;'.format(pyBlendShapesName))
    mel.eval('setDrivenKeyframe -currentDriver lForearmBend.rotateY {0}.pJCMFloyd8ForearmBndL;'.format(
        pyBlendShapesName))
    mel.eval('setAttr "lForearmBend.rotateY" 0;')

    # pJCMFloyd8ForearmBndR
    mel.eval('setAttr "rForearmBend.rotateY" 0;')
    mel.eval('setDrivenKeyframe -currentDriver rForearmBend.rotateY {0}.pJCMFloyd8ForearmBndR;'.format(
        pyBlendShapesName))
    mel.eval('setAttr "rForearmBend.rotateY" 135;')
    mel.eval('setAttr "{0}.pJCMFloyd8ForearmBndR" 1;'.format(pyBlendShapesName))
    mel.eval('setDrivenKeyframe -currentDriver rForearmBend.rotateY {0}.pJCMFloyd8ForearmBndR;'.format(
        pyBlendShapesName))
    mel.eval('setAttr "rForearmBend.rotateY" 0;')

    # pJCMFloyd8HeadBndDown_HDLv3
    mel.eval('setAttr "head.rotateX" 0;')
    mel.eval('setDrivenKeyframe -currentDriver head.rotateX {0}.pJCMFloyd8HeadBndDown_HDLv3;'.format(pyBlendShapesName))
    mel.eval('setAttr "head.rotateX" 25;')
    mel.eval('setAttr "{0}.pJCMFloyd8HeadBndDown_HDLv3" 1;'.format(pyBlendShapesName))
    mel.eval('setDrivenKeyframe -currentDriver head.rotateX {0}.pJCMFloyd8HeadBndDown_HDLv3;'.format(pyBlendShapesName))
    mel.eval('setAttr "head.rotateX" 0;')

    # pJCMFloyd8HeadTwistL_HDLv3
    mel.eval('setAttr "head.rotateY" 0;')
    mel.eval('setDrivenKeyframe -currentDriver head.rotateY {0}.pJCMFloyd8HeadTwistL_HDLv3;'.format(pyBlendShapesName))
    mel.eval('setAttr "head.rotateY" 22;')
    mel.eval('setAttr "{0}.pJCMFloyd8HeadTwistL_HDLv3" 1;'.format(pyBlendShapesName))
    mel.eval('setDrivenKeyframe -currentDriver head.rotateY {0}.pJCMFloyd8HeadTwistL_HDLv3;'.format(pyBlendShapesName))
    mel.eval('setAttr "head.rotateY" 0;')

    # pJCMFloyd8HeadTwistR_HDLv3
    mel.eval('setAttr "head.rotateY" 0;')
    mel.eval('setDrivenKeyframe -currentDriver head.rotateY {0}.pJCMFloyd8HeadTwistR_HDLv3;'.format(
        pyBlendShapesName))
    mel.eval('setAttr "head.rotateY" -22;')
    mel.eval('setAttr "{0}.pJCMFloyd8HeadTwistR_HDLv3" 1;'.format(pyBlendShapesName))
    mel.eval('setDrivenKeyframe -currentDriver head.rotateY {0}.pJCMFloyd8HeadTwistR_HDLv3;'.format(
        pyBlendShapesName))
    mel.eval('setAttr "head.rotateY" 0;')

    # pJCMFloyd8ShinBndBckL_100L
    mel.eval('setAttr "lShin.rotateX" 0;')
    mel.eval(
        'setDrivenKeyframe -currentDriver lShin.rotateX {0}.pJCMFloyd8ShinBndBckL_100L;'.format(pyBlendShapesName))
    mel.eval('setAttr "lShin.rotateX" 100;')
    mel.eval('setAttr "{0}.pJCMFloyd8ShinBndBckL_100L" 1;'.format(pyBlendShapesName))
    mel.eval(
        'setDrivenKeyframe -currentDriver lShin.rotateX {0}.pJCMFloyd8ShinBndBckL_100L;'.format(pyBlendShapesName))
    mel.eval('setAttr "lShin.rotateX" 0;')

    # pJCMFloyd8ShinBndBckL_100R
    mel.eval('setAttr "rShin.rotateX" 0;')
    mel.eval(
        'setDrivenKeyframe -currentDriver rShin.rotateX {0}.pJCMFloyd8ShinBndBckL_100R;'.format(pyBlendShapesName))
    mel.eval('setAttr "rShin.rotateX" 100;')
    mel.eval('setAttr "{0}.pJCMFloyd8ShinBndBckL_100R" 1;'.format(pyBlendShapesName))
    mel.eval(
        'setDrivenKeyframe -currentDriver rShin.rotateX {0}.pJCMFloyd8ShinBndBckL_100R;'.format(pyBlendShapesName))
    mel.eval('setAttr "rShin.rotateX" 0;')

    # pJCMFloyd8ShinBndBckL
    mel.eval('setAttr "lShin.rotateX" 0;')
    mel.eval(
        'setDrivenKeyframe -currentDriver lShin.rotateX {0}.pJCMFloyd8ShinBndBckL;'.format(pyBlendShapesName))
    mel.eval('setAttr "lShin.rotateX" 155;')
    mel.eval('setAttr "{0}.pJCMFloyd8ShinBndBckL" 1;'.format(pyBlendShapesName))
    mel.eval(
        'setDrivenKeyframe -currentDriver lShin.rotateX {0}.pJCMFloyd8ShinBndBckL;'.format(pyBlendShapesName))
    mel.eval('setAttr "lShin.rotateX" 0;')

    # pJCMFloyd8ShinBndBckR
    mel.eval('setAttr "rShin.rotateX" 0;')
    mel.eval(
        'setDrivenKeyframe -currentDriver rShin.rotateX {0}.pJCMFloyd8ShinBndBckR;'.format(pyBlendShapesName))
    mel.eval('setAttr "rShin.rotateX" 155;')
    mel.eval('setAttr "{0}.pJCMFloyd8ShinBndBckR" 1;'.format(pyBlendShapesName))
    mel.eval(
        'setDrivenKeyframe -currentDriver rShin.rotateX {0}.pJCMFloyd8ShinBndBckR;'.format(pyBlendShapesName))
    mel.eval('setAttr "rShin.rotateX" 0;')

    # pJCMFloyd8ShoulderBndUpL
    mel.eval('setAttr "lShldrBend.rotateZ" 0;')
    mel.eval('setDrivenKeyframe -currentDriver lShldrBend.rotateZ {0}.pJCMFloyd8ShoulderBndUpL;'.format(
        pyBlendShapesName))
    mel.eval('setAttr "lShldrBend.rotateZ" 90;')
    mel.eval('setAttr "{0}.pJCMFloyd8ShoulderBndUpL" 1;'.format(pyBlendShapesName))
    mel.eval('setDrivenKeyframe -currentDriver lShldrBend.rotateZ {0}.pJCMFloyd8ShoulderBndUpL;'.format(
        pyBlendShapesName))
    mel.eval('setAttr "lShldrBend.rotateZ" 0;')

    # pJCMFloyd8ShoulderBndUpR
    mel.eval('setAttr "rShldrBend.rotateZ" 0;')
    mel.eval('setDrivenKeyframe -currentDriver rShldrBend.rotateZ {0}.pJCMFloyd8ShoulderBndUpR;'.format(
        pyBlendShapesName))
    mel.eval('setAttr "rShldrBend.rotateZ" -90;')
    mel.eval('setAttr "{0}.pJCMFloyd8ShoulderBndUpR" 1;'.format(pyBlendShapesName))
    mel.eval('setDrivenKeyframe -currentDriver rShldrBend.rotateZ {0}.pJCMFloyd8ShoulderBndUpR;'.format(
        pyBlendShapesName))
    mel.eval('setAttr "rShldrBend.rotateZ" 0;')

    # pJCMFloyd8ShoulderFwdL
    mel.eval('setAttr "lShldrBend.rotateY" 0;')
    mel.eval('setDrivenKeyframe -currentDriver lShldrBend.rotateY {0}.pJCMFloyd8ShoulderFwdL;'.format(
        pyBlendShapesName))
    mel.eval('setAttr "lShldrBend.rotateY" -85;')
    mel.eval('setAttr "{0}.pJCMFloyd8ShoulderFwdL" 1;'.format(pyBlendShapesName))
    mel.eval('setDrivenKeyframe -currentDriver lShldrBend.rotateY {0}.pJCMFloyd8ShoulderFwdL;'.format(
        pyBlendShapesName))
    mel.eval('setAttr "lShldrBend.rotateY" 0;')

    # pJCMFloyd8ShoulderFwdR
    mel.eval('setAttr "rShldrBend.rotateY" 0;')
    mel.eval('setDrivenKeyframe -currentDriver rShldrBend.rotateY {0}.pJCMFloyd8ShoulderFwdR;'.format(pyBlendShapesName))
    mel.eval('setAttr "rShldrBend.rotateY" 85;')
    mel.eval('setAttr "{0}.pJCMFloyd8ShoulderFwdR" 1;'.format(pyBlendShapesName))
    mel.eval('setDrivenKeyframe -currentDriver rShldrBend.rotateY {0}.pJCMFloyd8ShoulderFwdR;'.format(pyBlendShapesName))
    mel.eval('setAttr "rShldrBend.rotateY" 0;')

    # pJCMFloyd8ThighSideSideL
    mel.eval('setAttr "lThighBend.rotateZ" 0;')
    mel.eval('setDrivenKeyframe -currentDriver lThighBend.rotateZ {0}.pJCMFloyd8ThighSideSideL;'.format(
        pyBlendShapesName))
    mel.eval('setAttr "lThighBend.rotateZ" 85;')
    mel.eval('setAttr "{0}.pJCMFloyd8ThighSideSideL" 1;'.format(pyBlendShapesName))
    mel.eval('setDrivenKeyframe -currentDriver lThighBend.rotateZ {0}.pJCMFloyd8ThighSideSideL;'.format(
        pyBlendShapesName))
    mel.eval('setAttr "lThighBend.rotateZ" 0;')

    # pJCMFloyd8ThighSideSideR
    mel.eval('setAttr "rThighBend.rotateZ" 0;')
    mel.eval('setDrivenKeyframe -currentDriver rThighBend.rotateZ {0}.pJCMFloyd8ThighSideSideR;'.format(
        pyBlendShapesName))
    mel.eval('setAttr "rThighBend.rotateZ" -85;')
    mel.eval('setAttr "{0}.pJCMFloyd8ThighSideSideR" 1;'.format(pyBlendShapesName))
    mel.eval('setDrivenKeyframe -currentDriver rThighBend.rotateZ {0}.pJCMFloyd8ThighSideSideR;'.format(
        pyBlendShapesName))
    mel.eval('setAttr "rThighBend.rotateZ" 0;')
