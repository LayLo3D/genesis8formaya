import maya.mel as mel
import maya.cmds as cmds


def Mabel8JCMs(character, blendShapeGroup):
    pyBlendShapesName = blendShapeGroup
    if not cmds.objExists('{}.pJCM_Mabel8_AbdomenLowerForward_35'.format(pyBlendShapesName)):
        error = character + ' specific JCMs were not created.\n\n' + character + ' JCMs were either not exported out properly or the wrong file was selected for import.'
        cmds.warning(error.replace('\n\n', ' '))
        return error

    # pJCM_Mabel8_AbdomenLowerForward_35
    mel.eval('setAttr "abdomenLower.rotateX" 0;')
    mel.eval('setDrivenKeyframe -currentDriver abdomenLower.rotateX {0}.pJCM_Mabel8_AbdomenLowerForward_35;'.format(
        pyBlendShapesName))
    mel.eval('setAttr "abdomenLower.rotateX" 35;')
    mel.eval('setAttr "{0}.pJCM_Mabel8_AbdomenLowerForward_35" 1;'.format(pyBlendShapesName))
    mel.eval('setDrivenKeyframe -currentDriver abdomenLower.rotateX {0}.pJCM_Mabel8_AbdomenLowerForward_35;'.format(
        pyBlendShapesName))
    mel.eval('setAttr "abdomenLower.rotateX" 0;')

    # pJCM_Mabel8_AbdomenLowerSide_15_L
    mel.eval('setAttr "abdomenLower.rotateZ" 0;')
    mel.eval('setDrivenKeyframe -currentDriver abdomenLower.rotateZ {0}.pJCM_Mabel8_AbdomenLowerSide_15_L;'.format(
        pyBlendShapesName))
    mel.eval('setAttr "abdomenLower.rotateZ" -15;')
    mel.eval('setAttr "{0}.pJCM_Mabel8_AbdomenLowerSide_15_L" 1;'.format(pyBlendShapesName))
    mel.eval('setDrivenKeyframe -currentDriver abdomenLower.rotateZ {0}.pJCM_Mabel8_AbdomenLowerSide_15_L;'.format(
        pyBlendShapesName))
    mel.eval('setAttr "abdomenLower.rotateZ" 0;')

    # pJCM_Mabel8_AbdomenLowerSide_15_R
    mel.eval('setAttr "abdomenLower.rotateZ" 0;')
    mel.eval('setDrivenKeyframe -currentDriver abdomenLower.rotateZ {0}.pJCM_Mabel8_AbdomenLowerSide_15_R;'.format(
        pyBlendShapesName))
    mel.eval('setAttr "abdomenLower.rotateZ" 15;')
    mel.eval('setAttr "{0}.pJCM_Mabel8_AbdomenLowerSide_15_R" 1;'.format(pyBlendShapesName))
    mel.eval('setDrivenKeyframe -currentDriver abdomenLower.rotateZ {0}.pJCM_Mabel8_AbdomenLowerSide_15_R;'.format(
        pyBlendShapesName))
    mel.eval('setAttr "abdomenLower.rotateZ" 0;')

    # pJCM_Mabel8_AbdomenUpperForward_40
    mel.eval('setAttr "abdomenUpper.rotateX" 0;')
    mel.eval('setDrivenKeyframe -currentDriver abdomenUpper.rotateX {0}.pJCM_Mabel8_AbdomenUpperForward_40;'.format(
        pyBlendShapesName))
    mel.eval('setAttr "abdomenUpper.rotateX" 40;')
    mel.eval('setAttr "{0}.pJCM_Mabel8_AbdomenUpperForward_40" 1;'.format(pyBlendShapesName))
    mel.eval('setDrivenKeyframe -currentDriver abdomenUpper.rotateX {0}.pJCM_Mabel8_AbdomenUpperForward_40;'.format(
        pyBlendShapesName))
    mel.eval('setAttr "abdomenUpper.rotateX" 0;')

    # pJCM_Mabel8_AbdomenUpperSide_24_L
    mel.eval('setAttr "abdomenUpper.rotateZ" 0;')
    mel.eval('setDrivenKeyframe -currentDriver abdomenUpper.rotateZ {0}.pJCM_Mabel8_AbdomenUpperSide_24_L;'.format(
        pyBlendShapesName))
    mel.eval('setAttr "abdomenUpper.rotateZ" -24;')
    mel.eval('setAttr "{0}.pJCM_Mabel8_AbdomenUpperSide_24_L" 1;'.format(pyBlendShapesName))
    mel.eval('setDrivenKeyframe -currentDriver abdomenUpper.rotateZ {0}.pJCM_Mabel8_AbdomenUpperSide_24_L;'.format(
        pyBlendShapesName))
    mel.eval('setAttr "abdomenUpper.rotateZ" 0;')

    # pJCM_Mabel8_AbdomenUpperSide_24_R
    mel.eval('setAttr "abdomenUpper.rotateZ" 0;')
    mel.eval('setDrivenKeyframe -currentDriver abdomenUpper.rotateZ {0}.pJCM_Mabel8_AbdomenUpperSide_24_R;'.format(
        pyBlendShapesName))
    mel.eval('setAttr "abdomenUpper.rotateZ" 24;')
    mel.eval('setAttr "{0}.pJCM_Mabel8_AbdomenUpperSide_24_R" 1;'.format(pyBlendShapesName))
    mel.eval('setDrivenKeyframe -currentDriver abdomenUpper.rotateZ {0}.pJCM_Mabel8_AbdomenUpperSide_24_R;'.format(
        pyBlendShapesName))
    mel.eval('setAttr "abdomenUpper.rotateZ" 0;')

    # pJCM_Mabel8_ChestLowerForward_35
    mel.eval('setAttr "chestLower.rotateX" 0;')
    mel.eval('setDrivenKeyframe -currentDriver chestLower.rotateX {0}.pJCM_Mabel8_ChestLowerForward_35;'.format(
        pyBlendShapesName))
    mel.eval('setAttr "chestLower.rotateX" 35;')
    mel.eval('setAttr "{0}.pJCM_Mabel8_ChestLowerForward_35" 1;'.format(pyBlendShapesName))
    mel.eval('setDrivenKeyframe -currentDriver chestLower.rotateX {0}.pJCM_Mabel8_ChestLowerForward_35;'.format(
        pyBlendShapesName))
    mel.eval('setAttr "chestLower.rotateX" 0;')

    # pJCM_Mabel8_ChestSide_20_L
    mel.eval('setAttr "chestLower.rotateZ" 0;')
    mel.eval('setDrivenKeyframe -currentDriver chestLower.rotateZ {0}.pJCM_Mabel8_ChestSide_20_L;'.format(
        pyBlendShapesName))
    mel.eval('setAttr "chestLower.rotateZ" -20;')
    mel.eval('setAttr "{0}.pJCM_Mabel8_ChestSide_20_L" 1;'.format(pyBlendShapesName))
    mel.eval('setDrivenKeyframe -currentDriver chestLower.rotateZ {0}.pJCM_Mabel8_ChestSide_20_L;'.format(
        pyBlendShapesName))
    mel.eval('setAttr "chestLower.rotateZ" 0;')

    # pJCM_Mabel8_ChestSide_20_R
    mel.eval('setAttr "chestLower.rotateZ" 0;')
    mel.eval('setDrivenKeyframe -currentDriver chestLower.rotateZ {0}.pJCM_Mabel8_ChestSide_20_R;'.format(
        pyBlendShapesName))
    mel.eval('setAttr "chestLower.rotateZ" 20;')
    mel.eval('setAttr "{0}.pJCM_Mabel8_ChestSide_20_R" 1;'.format(pyBlendShapesName))
    mel.eval('setDrivenKeyframe -currentDriver chestLower.rotateZ {0}.pJCM_Mabel8_ChestSide_20_R;'.format(
        pyBlendShapesName))
    mel.eval('setAttr "chestLower.rotateZ" 0;')

    # pJCM_Mabel8_ChestUpperSide_10_L
    mel.eval('setAttr "chestUpper.rotateZ" 0;')
    mel.eval('setDrivenKeyframe -currentDriver chestUpper.rotateZ {0}.pJCM_Mabel8_ChestUpperSide_10_L;'.format(
        pyBlendShapesName))
    mel.eval('setAttr "chestUpper.rotateZ" -10;')
    mel.eval('setAttr "{0}.pJCM_Mabel8_ChestUpperSide_10_L" 1;'.format(pyBlendShapesName))
    mel.eval('setDrivenKeyframe -currentDriver chestUpper.rotateZ {0}.pJCM_Mabel8_ChestUpperSide_10_L;'.format(
        pyBlendShapesName))
    mel.eval('setAttr "chestUpper.rotateZ" 0;')

    # pJCM_Mabel8_ChestUpperSide_10_R
    mel.eval('setAttr "chestUpper.rotateZ" 0;')
    mel.eval('setDrivenKeyframe -currentDriver chestUpper.rotateZ {0}.pJCM_Mabel8_ChestUpperSide_10_R;'.format(
        pyBlendShapesName))
    mel.eval('setAttr "chestUpper.rotateZ" 10;')
    mel.eval('setAttr "{0}.pJCM_Mabel8_ChestUpperSide_10_R" 1;'.format(pyBlendShapesName))
    mel.eval('setDrivenKeyframe -currentDriver chestUpper.rotateZ {0}.pJCM_Mabel8_ChestUpperSide_10_R;'.format(
        pyBlendShapesName))
    mel.eval('setAttr "chestUpper.rotateZ" 0;')

    # pJCM_Mabel8_CollarUp_55_L
    mel.eval('setAttr "lCollar.rotateZ" 0;')
    mel.eval('setDrivenKeyframe -currentDriver lCollar.rotateZ {0}.pJCM_Mabel8_CollarUp_55_L;'.format(
        pyBlendShapesName))
    mel.eval('setAttr "lCollar.rotateZ" 55;')
    mel.eval('setAttr "{0}.pJCM_Mabel8_CollarUp_55_L" 1;'.format(pyBlendShapesName))
    mel.eval('setDrivenKeyframe -currentDriver lCollar.rotateZ {0}.pJCM_Mabel8_CollarUp_55_L;'.format(
        pyBlendShapesName))
    mel.eval('setAttr "lCollar.rotateZ" 0;')

    # pJCM_Mabel8_CollarUp_55_R
    mel.eval('setAttr "rCollar.rotateZ" 0;')
    mel.eval('setDrivenKeyframe -currentDriver rCollar.rotateZ {0}.pJCM_Mabel8_CollarUp_55_R;'.format(
        pyBlendShapesName))
    mel.eval('setAttr "rCollar.rotateZ" -55;')
    mel.eval('setAttr "{0}.pJCM_Mabel8_CollarUp_55_R" 1;'.format(pyBlendShapesName))
    mel.eval('setDrivenKeyframe -currentDriver rCollar.rotateZ {0}.pJCM_Mabel8_CollarUp_55_R;'.format(
        pyBlendShapesName))
    mel.eval('setAttr "rCollar.rotateZ" 0;')

    # pJCM_Mabel8_ForeArmFwd_75_L
    mel.eval('setAttr "lForearmBend.rotateY" 0;')
    mel.eval('setDrivenKeyframe -currentDriver lForearmBend.rotateY {0}.pJCM_Mabel8_ForeArmFwd_75_L;'.format(
        pyBlendShapesName))
    mel.eval('setAttr "lForearmBend.rotateY" -75;')
    mel.eval('setAttr "{0}.pJCM_Mabel8_ForeArmFwd_75_L" 1;'.format(pyBlendShapesName))
    mel.eval('setDrivenKeyframe -currentDriver lForearmBend.rotateY {0}.pJCM_Mabel8_ForeArmFwd_75_L;'.format(
        pyBlendShapesName))
    mel.eval('setAttr "lForearmBend.rotateY" 0;')

    # pJCM_Mabel8_ForeArmFwd_75_R
    mel.eval('setAttr "rForearmBend.rotateY" 0;')
    mel.eval('setDrivenKeyframe -currentDriver rForearmBend.rotateY {0}.pJCM_Mabel8_ForeArmFwd_75_R;'.format(
        pyBlendShapesName))
    mel.eval('setAttr "rForearmBend.rotateY" 75;')
    mel.eval('setAttr "{0}.pJCM_Mabel8_ForeArmFwd_75_R" 1;'.format(pyBlendShapesName))
    mel.eval('setDrivenKeyframe -currentDriver rForearmBend.rotateY {0}.pJCM_Mabel8_ForeArmFwd_75_R;'.format(
        pyBlendShapesName))
    mel.eval('setAttr "rForearmBend.rotateY" 0;')

    # pJCMMabel8ForearmBndL_HDLv4
    mel.eval('setAttr "lForearmBend.rotateY" 0;')
    mel.eval('setDrivenKeyframe -currentDriver lForearmBend.rotateY {0}.pJCMMabel8ForearmBndL_HDLv4;'.format(
        pyBlendShapesName))
    mel.eval('setAttr "lForearmBend.rotateY" -135;')
    mel.eval('setAttr "{0}.pJCMMabel8ForearmBndL_HDLv4" 1;'.format(pyBlendShapesName))
    mel.eval('setDrivenKeyframe -currentDriver lForearmBend.rotateY {0}.pJCMMabel8ForearmBndL_HDLv4;'.format(
        pyBlendShapesName))
    mel.eval('setAttr "lForearmBend.rotateY" 0;')

    # pJCMMabel8ForearmBndR_HDLv4
    mel.eval('setAttr "rForearmBend.rotateY" 0;')
    mel.eval('setDrivenKeyframe -currentDriver rForearmBend.rotateY {0}.pJCMMabel8ForearmBndR_HDLv4;'.format(
        pyBlendShapesName))
    mel.eval('setAttr "rForearmBend.rotateY" 135;')
    mel.eval('setAttr "{0}.pJCMMabel8ForearmBndR_HDLv4" 1;'.format(pyBlendShapesName))
    mel.eval('setDrivenKeyframe -currentDriver rForearmBend.rotateY {0}.pJCMMabel8ForearmBndR_HDLv4;'.format(
        pyBlendShapesName))
    mel.eval('setAttr "rForearmBend.rotateY" 0;')

    # pJCM_Mabel8_HeadFwd_25
    mel.eval('setAttr "head.rotateX" 0;')
    mel.eval('setDrivenKeyframe -currentDriver head.rotateX {0}.pJCM_Mabel8_HeadFwd_25;'.format(pyBlendShapesName))
    mel.eval('setAttr "head.rotateX" 25;')
    mel.eval('setAttr "{0}.pJCM_Mabel8_HeadFwd_25" 1;'.format(pyBlendShapesName))
    mel.eval('setDrivenKeyframe -currentDriver head.rotateX {0}.pJCM_Mabel8_HeadFwd_25;'.format(pyBlendShapesName))
    mel.eval('setAttr "head.rotateX" 0;')
    
    # pJCM_Mabel8_ShinBend_90_L
    mel.eval('setAttr "lShin.rotateX" 0;')
    mel.eval(
        'setDrivenKeyframe -currentDriver lShin.rotateX {0}.pJCM_Mabel8_ShinBend_90_L;'.format(pyBlendShapesName))
    mel.eval('setAttr "lShin.rotateX" 90;')
    mel.eval('setAttr "{0}.pJCM_Mabel8_ShinBend_90_L" 1;'.format(pyBlendShapesName))
    mel.eval(
        'setDrivenKeyframe -currentDriver lShin.rotateX {0}.pJCM_Mabel8_ShinBend_90_L;'.format(pyBlendShapesName))
    mel.eval('setAttr "lShin.rotateX" 0;')

    # pJCM_Mabel8_ShinBend_90_R
    mel.eval('setAttr "rShin.rotateX" 0;')
    mel.eval(
        'setDrivenKeyframe -currentDriver rShin.rotateX {0}.pJCM_Mabel8_ShinBend_90_R;'.format(pyBlendShapesName))
    mel.eval('setAttr "rShin.rotateX" 90;')
    mel.eval('setAttr "{0}.pJCM_Mabel8_ShinBend_90_R" 1;'.format(pyBlendShapesName))
    mel.eval(
        'setDrivenKeyframe -currentDriver rShin.rotateX {0}.pJCM_Mabel8_ShinBend_90_R;'.format(pyBlendShapesName))
    mel.eval('setAttr "rShin.rotateX" 0;')

    # pJCM_Mabel8_ShinBend_155_L
    mel.eval('setAttr "lShin.rotateX" 90;')
    mel.eval(
        'setDrivenKeyframe -currentDriver lShin.rotateX {0}.pJCM_Mabel8_ShinBend_155_L;'.format(pyBlendShapesName))
    mel.eval('setAttr "lShin.rotateX" 155;')
    mel.eval('setAttr "{0}.pJCM_Mabel8_ShinBend_155_L" 1;'.format(pyBlendShapesName))
    mel.eval(
        'setDrivenKeyframe -currentDriver lShin.rotateX {0}.pJCM_Mabel8_ShinBend_155_L;'.format(pyBlendShapesName))
    mel.eval('setAttr "lShin.rotateX" 0;')

    # pJCM_Mabel8_ShinBend_155_R
    mel.eval('setAttr "rShin.rotateX" 90;')
    mel.eval(
        'setDrivenKeyframe -currentDriver rShin.rotateX {0}.pJCM_Mabel8_ShinBend_155_R;'.format(pyBlendShapesName))
    mel.eval('setAttr "rShin.rotateX" 155;')
    mel.eval('setAttr "{0}.pJCM_Mabel8_ShinBend_155_R" 1;'.format(pyBlendShapesName))
    mel.eval(
        'setDrivenKeyframe -currentDriver rShin.rotateX {0}.pJCM_Mabel8_ShinBend_155_R;'.format(pyBlendShapesName))
    mel.eval('setAttr "rShin.rotateX" 0;')

    # pJCMMabel8ShinBndL_HDLv4
    mel.eval('setAttr "lShin.rotateX" 90;')
    mel.eval(
        'setDrivenKeyframe -currentDriver lShin.rotateX {0}.pJCMMabel8ShinBndL_HDLv4;'.format(pyBlendShapesName))
    mel.eval('setAttr "lShin.rotateX" 155;')
    mel.eval('setAttr "{0}.pJCMMabel8ShinBndL_HDLv4" 1;'.format(pyBlendShapesName))
    mel.eval(
        'setDrivenKeyframe -currentDriver lShin.rotateX {0}.pJCMMabel8ShinBndL_HDLv4;'.format(pyBlendShapesName))
    mel.eval('setAttr "lShin.rotateX" 0;')

    # pJCMMabel8ShinBndR_HDLv4
    mel.eval('setAttr "rShin.rotateX" 90;')
    mel.eval(
        'setDrivenKeyframe -currentDriver rShin.rotateX {0}.pJCMMabel8ShinBndR_HDLv4;'.format(pyBlendShapesName))
    mel.eval('setAttr "rShin.rotateX" 155;')
    mel.eval('setAttr "{0}.pJCMMabel8ShinBndR_HDLv4" 1;'.format(pyBlendShapesName))
    mel.eval(
        'setDrivenKeyframe -currentDriver rShin.rotateX {0}.pJCMMabel8ShinBndR_HDLv4;'.format(pyBlendShapesName))
    mel.eval('setAttr "rShin.rotateX" 0;')

    # pJCM_Mabel8_ShldrUp_90_L
    mel.eval('setAttr "lShldrBend.rotateZ" 0;')
    mel.eval('setDrivenKeyframe -currentDriver lShldrBend.rotateZ {0}.pJCM_Mabel8_ShldrUp_90_L;'.format(
        pyBlendShapesName))
    mel.eval('setAttr "lShldrBend.rotateZ" 90;')
    mel.eval('setAttr "{0}.pJCM_Mabel8_ShldrUp_90_L" 1;'.format(pyBlendShapesName))
    mel.eval('setDrivenKeyframe -currentDriver lShldrBend.rotateZ {0}.pJCM_Mabel8_ShldrUp_90_L;'.format(
        pyBlendShapesName))
    mel.eval('setAttr "lShldrBend.rotateZ" 0;')

    # pJCM_Mabel8_ShldrUp_90_R
    mel.eval('setAttr "rShldrBend.rotateZ" 0;')
    mel.eval('setDrivenKeyframe -currentDriver rShldrBend.rotateZ {0}.pJCM_Mabel8_ShldrUp_90_R;'.format(
        pyBlendShapesName))
    mel.eval('setAttr "rShldrBend.rotateZ" -90;')
    mel.eval('setAttr "{0}.pJCM_Mabel8_ShldrUp_90_R" 1;'.format(pyBlendShapesName))
    mel.eval('setDrivenKeyframe -currentDriver rShldrBend.rotateZ {0}.pJCM_Mabel8_ShldrUp_90_R;'.format(
        pyBlendShapesName))
    mel.eval('setAttr "rShldrBend.rotateZ" 0;')

    # pJCM_Mabel8_ThighFwd_57_L
    mel.eval('setAttr "lThighBend.rotateX" 0;')
    mel.eval('setDrivenKeyframe -currentDriver lThighBend.rotateX {0}.pJCM_Mabel8_ThighFwd_57_L;'.format(
        pyBlendShapesName))
    mel.eval('setAttr "lThighBend.rotateX" -57;')
    mel.eval('setAttr "{0}.pJCM_Mabel8_ThighFwd_57_L" 1;'.format(pyBlendShapesName))
    mel.eval('setDrivenKeyframe -currentDriver lThighBend.rotateX {0}.pJCM_Mabel8_ThighFwd_57_L;'.format(
        pyBlendShapesName))
    mel.eval('setAttr "lThighBend.rotateX" 0;')

    # pJCM_Mabel8_ThighFwd_57_R
    mel.eval('setAttr "rThighBend.rotateX" 0;')
    mel.eval('setDrivenKeyframe -currentDriver rThighBend.rotateX {0}.pJCM_Mabel8_ThighFwd_57_R;'.format(
        pyBlendShapesName))
    mel.eval('setAttr "rThighBend.rotateX" -57;')
    mel.eval('setAttr "{0}.pJCM_Mabel8_ThighFwd_57_R" 1;'.format(pyBlendShapesName))
    mel.eval('setDrivenKeyframe -currentDriver rThighBend.rotateX {0}.pJCM_Mabel8_ThighFwd_57_R;'.format(
        pyBlendShapesName))
    mel.eval('setAttr "rThighBend.rotateX" 0;')

    # pJCM_Mabel8_ThighSide_85_L
    mel.eval('setAttr "lThighBend.rotateZ" 0;')
    mel.eval('setDrivenKeyframe -currentDriver lThighBend.rotateZ {0}.pJCM_Mabel8_ThighSide_85_L;'.format(
        pyBlendShapesName))
    mel.eval('setAttr "lThighBend.rotateZ" 85;')
    mel.eval('setAttr "{0}.pJCM_Mabel8_ThighSide_85_L" 1;'.format(pyBlendShapesName))
    mel.eval('setDrivenKeyframe -currentDriver lThighBend.rotateZ {0}.pJCM_Mabel8_ThighSide_85_L;'.format(
        pyBlendShapesName))
    mel.eval('setAttr "lThighBend.rotateZ" 0;')

    # pJCM_Mabel8_ThighSide_85_R
    mel.eval('setAttr "rThighBend.rotateZ" 0;')
    mel.eval('setDrivenKeyframe -currentDriver rThighBend.rotateZ {0}.pJCM_Mabel8_ThighSide_85_R;'.format(
        pyBlendShapesName))
    mel.eval('setAttr "rThighBend.rotateZ" -85;')
    mel.eval('setAttr "{0}.pJCM_Mabel8_ThighSide_85_R" 1;'.format(pyBlendShapesName))
    mel.eval('setDrivenKeyframe -currentDriver rThighBend.rotateZ {0}.pJCM_Mabel8_ThighSide_85_R;'.format(
        pyBlendShapesName))
    mel.eval('setAttr "rThighBend.rotateZ" 0;')
