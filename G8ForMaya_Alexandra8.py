import maya.mel as mel
import maya.cmds as cmds


def Alexandra8JCMs(character, blendShapeGroup):
    pyBlendShapesName = blendShapeGroup
    if not cmds.objExists('{}.pJCMAlexandra8_AbdomenLowerBndFwd'.format(pyBlendShapesName)):
        error = character + ' specific JCMs were not created.\n\n' + character + ' JCMs were either not exported out properly or the wrong file was selected for import.'
        cmds.warning(error.replace('\n\n', ' '))
        return error

    # pJCMAlexandra8_AbdomenLowerBndFwd
    mel.eval('setAttr "abdomenLower.rotateX" 0;')
    mel.eval('setDrivenKeyframe -currentDriver abdomenLower.rotateX {0}.pJCMAlexandra8_AbdomenLowerBndFwd;'.format(
        pyBlendShapesName))
    mel.eval('setAttr "abdomenLower.rotateX" 30;')
    mel.eval('setAttr "{0}.pJCMAlexandra8_AbdomenLowerBndFwd" 1;'.format(pyBlendShapesName))
    mel.eval('setDrivenKeyframe -currentDriver abdomenLower.rotateX {0}.pJCMAlexandra8_AbdomenLowerBndFwd;'.format(
        pyBlendShapesName))
    mel.eval('setAttr "abdomenLower.rotateX" 0;')

    # pJCMAlexandra8_AbdomenUpperBndFwd
    mel.eval('setAttr "abdomenUpper.rotateX" 0;')
    mel.eval('setDrivenKeyframe -currentDriver abdomenUpper.rotateX {0}.pJCMAlexandra8_AbdomenUpperBndFwd;'.format(
        pyBlendShapesName))
    mel.eval('setAttr "abdomenUpper.rotateX" 40;')
    mel.eval('setAttr "{0}.pJCMAlexandra8_AbdomenUpperBndFwd" 1;'.format(pyBlendShapesName))
    mel.eval('setDrivenKeyframe -currentDriver abdomenUpper.rotateX {0}.pJCMAlexandra8_AbdomenUpperBndFwd;'.format(
        pyBlendShapesName))
    mel.eval('setAttr "abdomenUpper.rotateX" 0;')

    # pJCMAlexandra8_CollarBndUpL
    mel.eval('setAttr "lCollar.rotateZ" 0;')
    mel.eval('setDrivenKeyframe -currentDriver lCollar.rotateZ {0}.pJCMAlexandra8_CollarBndUpL;'.format(
        pyBlendShapesName))
    mel.eval('setAttr "lCollar.rotateZ" 55;')
    mel.eval('setAttr "{0}.pJCMAlexandra8_CollarBndUpL" 1;'.format(pyBlendShapesName))
    mel.eval('setDrivenKeyframe -currentDriver lCollar.rotateZ {0}.pJCMAlexandra8_CollarBndUpL;'.format(
        pyBlendShapesName))
    mel.eval('setAttr "lCollar.rotateZ" 0;')

    # pJCMAlexandra8_CollarBndUpR
    mel.eval('setAttr "rCollar.rotateZ" 0;')
    mel.eval('setDrivenKeyframe -currentDriver rCollar.rotateZ {0}.pJCMAlexandra8_CollarBndUpR;'.format(
        pyBlendShapesName))
    mel.eval('setAttr "rCollar.rotateZ" -55;')
    mel.eval('setAttr "{0}.pJCMAlexandra8_CollarBndUpR" 1;'.format(pyBlendShapesName))
    mel.eval('setDrivenKeyframe -currentDriver rCollar.rotateZ {0}.pJCMAlexandra8_CollarBndUpR;'.format(
        pyBlendShapesName))
    mel.eval('setAttr "rCollar.rotateZ" 0;')

    # pJCMAlexandra8_FootBndUpL
    mel.eval('setAttr "lFoot.rotateX" 0;')
    mel.eval(
        'setDrivenKeyframe -currentDriver lFoot.rotateX {0}.pJCMAlexandra8_FootBndUpL;'.format(pyBlendShapesName))
    mel.eval('setAttr "lFoot.rotateX" -45;')
    mel.eval('setAttr "{0}.pJCMAlexandra8_FootBndUpL" 1;'.format(pyBlendShapesName))
    mel.eval(
        'setDrivenKeyframe -currentDriver lFoot.rotateX {0}.pJCMAlexandra8_FootBndUpL;'.format(pyBlendShapesName))
    mel.eval('setAttr "lFoot.rotateX" 0;')

    # pJCMAlexandra8_FootBndUpR
    mel.eval('setAttr "rFoot.rotateX" 0;')
    mel.eval(
        'setDrivenKeyframe -currentDriver rFoot.rotateX {0}.pJCMAlexandra8_FootBndUpR;'.format(pyBlendShapesName))
    mel.eval('setAttr "rFoot.rotateX" -45;')
    mel.eval('setAttr "{0}.pJCMAlexandra8_FootBndUpR" 1;'.format(pyBlendShapesName))
    mel.eval(
        'setDrivenKeyframe -currentDriver rFoot.rotateX {0}.pJCMAlexandra8_FootBndUpR;'.format(pyBlendShapesName))
    mel.eval('setAttr "rFoot.rotateX" 0;')

    # pJCMAlexandra8_ForearmBnd_n90L
    mel.eval('setAttr "lForearmBend.rotateY" 0;')
    mel.eval('setDrivenKeyframe -currentDriver lForearmBend.rotateY {0}.pJCMAlexandra8_ForearmBnd_n90L;'.format(
        pyBlendShapesName))
    mel.eval('setAttr "lForearmBend.rotateY" -90;')
    mel.eval('setAttr "{0}.pJCMAlexandra8_ForearmBnd_n90L" 1;'.format(pyBlendShapesName))
    mel.eval('setDrivenKeyframe -currentDriver lForearmBend.rotateY {0}.pJCMAlexandra8_ForearmBnd_n90L;'.format(
        pyBlendShapesName))
    mel.eval('setAttr "lForearmBend.rotateY" 0;')

    #  pJCMAlexandra8_ForearmBnd_n90R
    mel.eval('setAttr "rForearmBend.rotateY" 0;')
    mel.eval('setDrivenKeyframe -currentDriver rForearmBend.rotateY {0}.pJCMAlexandra8_ForearmBnd_n90R;'.format(
        pyBlendShapesName))
    mel.eval('setAttr "rForearmBend.rotateY" 90;')
    mel.eval('setAttr "{0}.pJCMAlexandra8_ForearmBnd_n90R" 1;'.format(pyBlendShapesName))
    mel.eval('setDrivenKeyframe -currentDriver rForearmBend.rotateY {0}.pJCMAlexandra8_ForearmBnd_n90R;'.format(
        pyBlendShapesName))
    mel.eval('setAttr "rForearmBend.rotateY" 0;')

    # pJCMAlexandra8_ForearmBndL
    mel.eval('setAttr "lForearmBend.rotateY" 0;')
    mel.eval('setDrivenKeyframe -currentDriver lForearmBend.rotateY {0}.pJCMAlexandra8_ForearmBndL;'.format(
        pyBlendShapesName))
    mel.eval('setAttr "lForearmBend.rotateY" -135;')
    mel.eval('setAttr "{0}.pJCMAlexandra8_ForearmBndL" 1;'.format(pyBlendShapesName))
    mel.eval('setDrivenKeyframe -currentDriver lForearmBend.rotateY {0}.pJCMAlexandra8_ForearmBndL;'.format(
        pyBlendShapesName))
    mel.eval('setAttr "lForearmBend.rotateY" 0;')

    # pJCMAlexandra8_ForearmBndR
    mel.eval('setAttr "rForearmBend.rotateY" 0;')
    mel.eval('setDrivenKeyframe -currentDriver rForearmBend.rotateY {0}.pJCMAlexandra8_ForearmBndR;'.format(
        pyBlendShapesName))
    mel.eval('setAttr "rForearmBend.rotateY" 135;')
    mel.eval('setAttr "{0}.pJCMAlexandra8_ForearmBndR" 1;'.format(pyBlendShapesName))
    mel.eval('setDrivenKeyframe -currentDriver rForearmBend.rotateY {0}.pJCMAlexandra8_ForearmBndR;'.format(
        pyBlendShapesName))
    mel.eval('setAttr "rForearmBend.rotateY" 0;')

    # pJCMAlexandra8_HeadBndDn_HDLv3
    mel.eval('setAttr "head.rotateX" 0;')
    mel.eval('setDrivenKeyframe -currentDriver head.rotateX {0}.pJCMAlexandra8_HeadBndDn_HDLv3;'.format(
        pyBlendShapesName))
    mel.eval('setAttr "head.rotateX" 25;')
    mel.eval('setAttr "{0}.pJCMAlexandra8_HeadBndDn_HDLv3" 1;'.format(pyBlendShapesName))
    mel.eval('setDrivenKeyframe -currentDriver head.rotateX {0}.pJCMAlexandra8_HeadBndDn_HDLv3;'.format(
        pyBlendShapesName))
    mel.eval('setAttr "head.rotateX" 0;')

    # pJCMAlexandra8_NeckUpperTwist_HDLv3L
    mel.eval('setAttr "neckUpper.rotateY" 0;')
    mel.eval('setDrivenKeyframe -currentDriver neckUpper.rotateY {0}.pJCMAlexandra8_NeckUpperTwist_HDLv3L;'.format(
        pyBlendShapesName))
    mel.eval('setAttr "neckUpper.rotateY" 22;')
    mel.eval('setAttr "{0}.pJCMAlexandra8_NeckUpperTwist_HDLv3L" 1;'.format(pyBlendShapesName))
    mel.eval('setDrivenKeyframe -currentDriver neckUpper.rotateY {0}.pJCMAlexandra8_NeckUpperTwist_HDLv3L;'.format(
        pyBlendShapesName))
    mel.eval('setAttr "neckUpper.rotateY" 0;')

    # pJCMAlexandra8_NeckUpperTwist_HDLv3R
    mel.eval('setAttr "neckUpper.rotateY" 0;')
    mel.eval('setDrivenKeyframe -currentDriver neckUpper.rotateY {0}.pJCMAlexandra8_NeckUpperTwist_HDLv3R;'.format(
        pyBlendShapesName))
    mel.eval('setAttr "neckUpper.rotateY" -22;')
    mel.eval('setAttr "{0}.pJCMAlexandra8_NeckUpperTwist_HDLv3R" 1;'.format(pyBlendShapesName))
    mel.eval('setDrivenKeyframe -currentDriver neckUpper.rotateY {0}.pJCMAlexandra8_NeckUpperTwist_HDLv3R;'.format(
        pyBlendShapesName))
    mel.eval('setAttr "neckUpper.rotateY" 0;')

    # pJCMAlexandra8_ShinBndBck_90L
    mel.eval('setAttr "lShin.rotateX" 0;')
    mel.eval(
        'setDrivenKeyframe -currentDriver lShin.rotateX {0}.pJCMAlexandra8_ShinBndBck_90L;'.format(
            pyBlendShapesName))
    mel.eval('setAttr "lShin.rotateX" 90;')
    mel.eval('setAttr "{0}.pJCMAlexandra8_ShinBndBck_90L" 1;'.format(pyBlendShapesName))
    mel.eval(
        'setDrivenKeyframe -currentDriver lShin.rotateX {0}.pJCMAlexandra8_ShinBndBck_90L;'.format(
            pyBlendShapesName))
    mel.eval('setAttr "lShin.rotateX" 0;')

    # pJCMAlexandra8_ShinBndBck_90R
    mel.eval('setAttr "rShin.rotateX" 0;')
    mel.eval(
        'setDrivenKeyframe -currentDriver rShin.rotateX {0}.pJCMAlexandra8_ShinBndBck_90R;'.format(
            pyBlendShapesName))
    mel.eval('setAttr "rShin.rotateX" 90;')
    mel.eval('setAttr "{0}.pJCMAlexandra8_ShinBndBck_90R" 1;'.format(pyBlendShapesName))
    mel.eval(
        'setDrivenKeyframe -currentDriver rShin.rotateX {0}.pJCMAlexandra8_ShinBndBck_90R;'.format(
            pyBlendShapesName))
    mel.eval('setAttr "rShin.rotateX" 0;')

    # pJCMAlexandra8_ShinBndBckL
    mel.eval('setAttr "lShin.rotateX" 0;')
    mel.eval(
        'setDrivenKeyframe -currentDriver lShin.rotateX {0}.pJCMAlexandra8_ShinBndBckL;'.format(pyBlendShapesName))
    mel.eval('setAttr "lShin.rotateX" 155;')
    mel.eval('setAttr "{0}.pJCMAlexandra8_ShinBndBckL" 1;'.format(pyBlendShapesName))
    mel.eval(
        'setDrivenKeyframe -currentDriver lShin.rotateX {0}.pJCMAlexandra8_ShinBndBckL;'.format(pyBlendShapesName))
    mel.eval('setAttr "lShin.rotateX" 0;')

    # pJCMAlexandra8_ShinBndBckR
    mel.eval('setAttr "rShin.rotateX" 0;')
    mel.eval(
        'setDrivenKeyframe -currentDriver rShin.rotateX {0}.pJCMAlexandra8_ShinBndBckR;'.format(pyBlendShapesName))
    mel.eval('setAttr "rShin.rotateX" 155;')
    mel.eval('setAttr "{0}.pJCMAlexandra8_ShinBndBckR" 1;'.format(pyBlendShapesName))
    mel.eval(
        'setDrivenKeyframe -currentDriver rShin.rotateX {0}.pJCMAlexandra8_ShinBndBckR;'.format(pyBlendShapesName))
    mel.eval('setAttr "rShin.rotateX" 0;')

    # pJCMAlexandra8_ShoulderBndDn_n15_HDLv3L
    mel.eval('setAttr "lShldrBend.rotateZ" 0;')
    mel.eval('setDrivenKeyframe -currentDriver lShldrBend.rotateZ {0}.pJCMAlexandra8_ShoulderBndDn_n15_HDLv3L;'.format(
        pyBlendShapesName))
    mel.eval('setAttr "lShldrBend.rotateZ" -15;')
    mel.eval('setAttr "{0}.pJCMAlexandra8_ShoulderBndDn_n15_HDLv3L" 1;'.format(pyBlendShapesName))
    mel.eval('setDrivenKeyframe -currentDriver lShldrBend.rotateZ {0}.pJCMAlexandra8_ShoulderBndDn_n15_HDLv3L;'.format(
        pyBlendShapesName))
    mel.eval('setAttr "lShldrBend.rotateZ" 0;')

    # pJCMAlexandra8_ShoulderBndDn_15_HDLv3R
    mel.eval('setAttr "rShldrBend.rotateZ" 0;')
    mel.eval('setDrivenKeyframe -currentDriver rShldrBend.rotateZ {0}.pJCMAlexandra8_ShoulderBndDn_15_HDLv3R;'.format(
        pyBlendShapesName))
    mel.eval('setAttr "rShldrBend.rotateZ" 15;')
    mel.eval('setAttr "{0}.pJCMAlexandra8_ShoulderBndDn_15_HDLv3R" 1;'.format(pyBlendShapesName))
    mel.eval('setDrivenKeyframe -currentDriver rShldrBend.rotateZ {0}.pJCMAlexandra8_ShoulderBndDn_15_HDLv3R;'.format(
        pyBlendShapesName))
    mel.eval('setAttr "rShldrBend.rotateZ" 0;')

    # pJCMAlexandra8_ShoulderBndUpL
    mel.eval('setAttr "lShldrBend.rotateZ" 0;')
    mel.eval('setDrivenKeyframe -currentDriver lShldrBend.rotateZ {0}.pJCMAlexandra8_ShoulderBndUpL;'.format(
        pyBlendShapesName))
    mel.eval('setAttr "lShldrBend.rotateZ" 90;')
    mel.eval('setAttr "{0}.pJCMAlexandra8_ShoulderBndUpL" 1;'.format(pyBlendShapesName))
    mel.eval('setDrivenKeyframe -currentDriver lShldrBend.rotateZ {0}.pJCMAlexandra8_ShoulderBndUpL;'.format(
        pyBlendShapesName))
    mel.eval('setAttr "lShldrBend.rotateZ" 0;')

    # pJCMAlexandra8_ShoulderBndUpR
    mel.eval('setAttr "rShldrBend.rotateZ" 0;')
    mel.eval('setDrivenKeyframe -currentDriver rShldrBend.rotateZ {0}.pJCMAlexandra8_ShoulderBndUpR;'.format(
        pyBlendShapesName))
    mel.eval('setAttr "rShldrBend.rotateZ" -90;')
    mel.eval('setAttr "{0}.pJCMAlexandra8_ShoulderBndUpR" 1;'.format(pyBlendShapesName))
    mel.eval('setDrivenKeyframe -currentDriver rShldrBend.rotateZ {0}.pJCMAlexandra8_ShoulderBndUpR;'.format(
        pyBlendShapesName))
    mel.eval('setAttr "rShldrBend.rotateZ" 0;')

    # pJCMAlexandra8_ShoulderFwd_n110L
    mel.eval('setAttr "lShldrBend.rotateY" 0;')
    mel.eval('setDrivenKeyframe -currentDriver lShldrBend.rotateY {0}.pJCMAlexandra8_ShoulderFwd_n110L;'.format(
        pyBlendShapesName))
    mel.eval('setAttr "lShldrBend.rotateY" -90;')
    mel.eval('setAttr "{0}.pJCMAlexandra8_ShoulderFwd_n110L" 1;'.format(pyBlendShapesName))
    mel.eval('setDrivenKeyframe -currentDriver lShldrBend.rotateY {0}.pJCMAlexandra8_ShoulderFwd_n110L;'.format(
        pyBlendShapesName))
    mel.eval('setAttr "lShldrBend.rotateY" 0;')

    # pJCMAlexandra8_ShoulderFwd_n80L
    mel.eval('setAttr "lShldrBend.rotateY" 0;')
    mel.eval('setDrivenKeyframe -currentDriver lShldrBend.rotateY {0}.pJCMAlexandra8_ShoulderFwd_n80L;'.format(
        pyBlendShapesName))
    mel.eval('setAttr "lShldrBend.rotateY" -90;')
    mel.eval('setAttr "{0}.pJCMAlexandra8_ShoulderFwd_n80L" 1;'.format(pyBlendShapesName))
    mel.eval('setDrivenKeyframe -currentDriver lShldrBend.rotateY {0}.pJCMAlexandra8_ShoulderFwd_n80L;'.format(
        pyBlendShapesName))
    mel.eval('setAttr "lShldrBend.rotateY" 0;')

    # pJCMAlexandra8_ShoulderFwd_110R
    mel.eval('setAttr "rShldrBend.rotateY" 0;')
    mel.eval('setDrivenKeyframe -currentDriver rShldrBend.rotateY {0}.pJCMAlexandra8_ShoulderFwd_110R;'.format(
        pyBlendShapesName))
    mel.eval('setAttr "rShldrBend.rotateY" 110;')
    mel.eval('setAttr "{0}.pJCMAlexandra8_ShoulderFwd_110R" 1;'.format(pyBlendShapesName))
    mel.eval('setDrivenKeyframe -currentDriver rShldrBend.rotateY {0}.pJCMAlexandra8_ShoulderFwd_110R;'.format(
        pyBlendShapesName))
    mel.eval('setAttr "rShldrBend.rotateY" 0;')

    # pJCMAlexandra8_ShoulderFwd_80R
    mel.eval('setAttr "rShldrBend.rotateY" 0;')
    mel.eval('setDrivenKeyframe -currentDriver rShldrBend.rotateY {0}.pJCMAlexandra8_ShoulderFwd_80R;'.format(
        pyBlendShapesName))
    mel.eval('setAttr "rShldrBend.rotateY" 110;')
    mel.eval('setAttr "{0}.pJCMAlexandra8_ShoulderFwd_80R" 1;'.format(pyBlendShapesName))
    mel.eval('setDrivenKeyframe -currentDriver rShldrBend.rotateY {0}.pJCMAlexandra8_ShoulderFwd_80R;'.format(
        pyBlendShapesName))
    mel.eval('setAttr "rShldrBend.rotateY" 0;')

    # pJCMAlexandra8_ThighSideSideL
    mel.eval('setAttr "lThighBend.rotateZ" 0;')
    mel.eval('setDrivenKeyframe -currentDriver lThighBend.rotateZ {0}.pJCMAlexandra8_ThighSideSideL;'.format(
        pyBlendShapesName))
    mel.eval('setAttr "lThighBend.rotateZ" 85;')
    mel.eval('setAttr "{0}.pJCMAlexandra8_ThighSideSideL" 1;'.format(pyBlendShapesName))
    mel.eval('setDrivenKeyframe -currentDriver lThighBend.rotateZ {0}.pJCMAlexandra8_ThighSideSideL;'.format(
        pyBlendShapesName))
    mel.eval('setAttr "lThighBend.rotateZ" 0;')

    # pJCMAlexandra8_ThighSideSideR
    mel.eval('setAttr "rThighBend.rotateZ" 0;')
    mel.eval('setDrivenKeyframe -currentDriver rThighBend.rotateZ {0}.pJCMAlexandra8_ThighSideSideR;'.format(
        pyBlendShapesName))
    mel.eval('setAttr "rThighBend.rotateZ" -85;')
    mel.eval('setAttr "{0}.pJCMAlexandra8_ThighSideSideR" 1;'.format(pyBlendShapesName))
    mel.eval('setDrivenKeyframe -currentDriver rThighBend.rotateZ {0}.pJCMAlexandra8_ThighSideSideR;'.format(
        pyBlendShapesName))
    mel.eval('setAttr "rThighBend.rotateZ" 0;')
