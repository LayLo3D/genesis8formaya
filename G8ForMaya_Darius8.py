import maya.mel as mel
import maya.cmds as cmds


def Darius8JCMs(character, blendShapeGroup):
    pyBlendShapesName = blendShapeGroup
    if not cmds.objExists('{}.pJCMDarius8_AbdomenLowerFwd'.format(pyBlendShapesName)):
        error = character + ' specific JCMs were not created.\n\n' + character + ' JCMs were either not exported out properly or the wrong file was selected for import.'
        cmds.warning(error.replace('\n\n', ' '))
        return error

    # pJCMDarius8_AbdomenLowerFwd
    mel.eval('setAttr "abdomenLower.rotateX" 0;')
    mel.eval('setDrivenKeyframe -currentDriver abdomenLower.rotateX {0}.pJCMDarius8_AbdomenLowerFwd;'.format(pyBlendShapesName))
    mel.eval('setAttr "abdomenLower.rotateX" 30;')
    mel.eval('setAttr "{0}.pJCMDarius8_AbdomenLowerFwd" 1;'.format(pyBlendShapesName))
    mel.eval('setDrivenKeyframe -currentDriver abdomenLower.rotateX {0}.pJCMDarius8_AbdomenLowerFwd;'.format(pyBlendShapesName))
    mel.eval('setAttr "abdomenLower.rotateX" 0;')

    # pJCMDarius8_AbdomenUpperFwd
    mel.eval('setAttr "abdomenUpper.rotateX" 0;')
    mel.eval('setDrivenKeyframe -currentDriver abdomenUpper.rotateX {0}.pJCMDarius8_AbdomenUpperFwd;'.format(
        pyBlendShapesName))
    mel.eval('setAttr "abdomenUpper.rotateX" 40;')
    mel.eval('setAttr "{0}.pJCMDarius8_AbdomenUpperFwd" 1;'.format(pyBlendShapesName))
    mel.eval('setDrivenKeyframe -currentDriver abdomenUpper.rotateX {0}.pJCMDarius8_AbdomenUpperFwd;'.format(
        pyBlendShapesName))
    mel.eval('setAttr "abdomenUpper.rotateX" 0;')

    # pJCMDarius8_ChestLowerFwd
    mel.eval('setAttr "chestLower.rotateX" 0;')
    mel.eval('setDrivenKeyframe -currentDriver chestLower.rotateX {0}.pJCMDarius8_ChestLowerFwd;'.format(
        pyBlendShapesName))
    mel.eval('setAttr "chestLower.rotateX" 35;')
    mel.eval('setAttr "{0}.pJCMDarius8_ChestLowerFwd" 1;'.format(pyBlendShapesName))
    mel.eval('setDrivenKeyframe -currentDriver chestLower.rotateX {0}.pJCMDarius8_ChestLowerFwd;'.format(
        pyBlendShapesName))
    mel.eval('setAttr "chestLower.rotateX" 0;')

    # pJCMDarius8_CollarBendUpL
    mel.eval('setAttr "lCollar.rotateZ" 0;')
    mel.eval(
        'setDrivenKeyframe -currentDriver lCollar.rotateZ {0}.pJCMDarius8_CollarBendUpL;'.format(pyBlendShapesName))
    mel.eval('setAttr "lCollar.rotateZ" 55;')
    mel.eval('setAttr "{0}.pJCMDarius8_CollarBendUpL" 1;'.format(pyBlendShapesName))
    mel.eval(
        'setDrivenKeyframe -currentDriver lCollar.rotateZ {0}.pJCMDarius8_CollarBendUpL;'.format(pyBlendShapesName))
    mel.eval('setAttr "lCollar.rotateZ" 0;')

    # pJCMDarius8_CollarBendUpR
    mel.eval('setAttr "rCollar.rotateZ" 0;')
    mel.eval(
        'setDrivenKeyframe -currentDriver rCollar.rotateZ {0}.pJCMDarius8_CollarBendUpR;'.format(pyBlendShapesName))
    mel.eval('setAttr "rCollar.rotateZ" -55;')
    mel.eval('setAttr "{0}.pJCMDarius8_CollarBendUpR" 1;'.format(pyBlendShapesName))
    mel.eval(
        'setDrivenKeyframe -currentDriver rCollar.rotateZ {0}.pJCMDarius8_CollarBendUpR;'.format(pyBlendShapesName))
    mel.eval('setAttr "rCollar.rotateZ" 0;')

    # pJCMDarius8_CollarBndDnL
    mel.eval('setAttr "lCollar.rotateZ" 0;')
    mel.eval(
        'setDrivenKeyframe -currentDriver lCollar.rotateZ {0}.pJCMDarius8_CollarBndDnL;'.format(pyBlendShapesName))
    mel.eval('setAttr "lCollar.rotateZ" -10;')
    mel.eval('setAttr "{0}.pJCMDarius8_CollarBndDnL" 1;'.format(pyBlendShapesName))
    mel.eval(
        'setDrivenKeyframe -currentDriver lCollar.rotateZ {0}.pJCMDarius8_CollarBndDnL;'.format(pyBlendShapesName))
    mel.eval('setAttr "lCollar.rotateZ" 0;')

    # pJCMDarius8_CollarBndDnR
    mel.eval('setAttr "rCollar.rotateZ" 0;')
    mel.eval(
        'setDrivenKeyframe -currentDriver rCollar.rotateZ {0}.pJCMDarius8_CollarBndDnR;'.format(pyBlendShapesName))
    mel.eval('setAttr "rCollar.rotateZ" -55;')
    mel.eval('setAttr "{0}.pJCMDarius8_CollarBndDnR" 1;'.format(pyBlendShapesName))
    mel.eval(
        'setDrivenKeyframe -currentDriver rCollar.rotateZ {0}.pJCMDarius8_CollarBndDnR;'.format(pyBlendShapesName))
    mel.eval('setAttr "rCollar.rotateZ" 0;')

    # pJCMDarius8_CollarFwdL
    mel.eval('setAttr "lCollar.rotateY" 0;')
    mel.eval(
        'setDrivenKeyframe -currentDriver lCollar.rotateY {0}.pJCMDarius8_CollarFwdL;'.format(pyBlendShapesName))
    mel.eval('setAttr "lCollar.rotateY" -26;')
    mel.eval('setAttr "{0}.pJCMDarius8_CollarFwdL" 1;'.format(pyBlendShapesName))
    mel.eval(
        'setDrivenKeyframe -currentDriver lCollar.rotateY {0}.pJCMDarius8_CollarFwdL;'.format(pyBlendShapesName))
    mel.eval('setAttr "lCollar.rotateY" 0;')

    # pJCMDarius8_CollarFwdR
    mel.eval('setAttr "rCollar.rotateY" 0;')
    mel.eval(
        'setDrivenKeyframe -currentDriver rCollar.rotateY {0}.pJCMDarius8_CollarFwdR;'.format(pyBlendShapesName))
    mel.eval('setAttr "rCollar.rotateY" 26;')
    mel.eval('setAttr "{0}.pJCMDarius8_CollarFwdR" 1;'.format(pyBlendShapesName))
    mel.eval(
        'setDrivenKeyframe -currentDriver rCollar.rotateY {0}.pJCMDarius8_CollarFwdR;'.format(pyBlendShapesName))
    mel.eval('setAttr "rCollar.rotateY" 0;')

    # pJCMDarius8_CollarTwistL
    mel.eval('setAttr "lCollar.rotateX" 0;')
    mel.eval('setDrivenKeyframe -currentDriver lCollar.rotateX {0}.pJCMDarius8_CollarTwistL;'.format(pyBlendShapesName))
    mel.eval('setAttr "lCollar.rotateX" -30;')
    mel.eval('setAttr "{0}.pJCMDarius8_CollarTwistL" 1;'.format(pyBlendShapesName))
    mel.eval('setDrivenKeyframe -currentDriver lCollar.rotateX {0}.pJCMDarius8_CollarTwistL;'.format(pyBlendShapesName))
    mel.eval('setAttr "lCollar.rotateX" 0;')

    # pJCMDarius8_CollarTwistR
    mel.eval('setAttr "rCollar.rotateX" 0;')
    mel.eval('setDrivenKeyframe -currentDriver rCollar.rotateX {0}.pJCMDarius8_CollarTwistR;'.format(pyBlendShapesName))
    mel.eval('setAttr "rCollar.rotateX" -30;')
    mel.eval('setAttr "{0}.pJCMDarius8_CollarTwistR" 1;'.format(pyBlendShapesName))
    mel.eval('setDrivenKeyframe -currentDriver rCollar.rotateX {0}.pJCMDarius8_CollarTwistR;'.format(pyBlendShapesName))
    mel.eval('setAttr "rCollar.rotateX" 0;')

    # pJCMDarius8_FootBndUpL
    mel.eval('setAttr "lFoot.rotateX" 0;')
    mel.eval('setDrivenKeyframe -currentDriver lFoot.rotateX {0}.pJCMDarius8_FootBndUpL;'.format(pyBlendShapesName))
    mel.eval('setAttr "lFoot.rotateX" -45;')
    mel.eval('setAttr "{0}.pJCMDarius8_FootBndUpL" 1;'.format(pyBlendShapesName))
    mel.eval('setDrivenKeyframe -currentDriver lFoot.rotateX {0}.pJCMDarius8_FootBndUpL;'.format(pyBlendShapesName))
    mel.eval('setAttr "lFoot.rotateX" 0;')

    # pJCMDarius8_FootBndUpR
    mel.eval('setAttr "rFoot.rotateX" 0;')
    mel.eval('setDrivenKeyframe -currentDriver rFoot.rotateX {0}.pJCMDarius8_FootBndUpR;'.format(pyBlendShapesName))
    mel.eval('setAttr "rFoot.rotateX" -45;')
    mel.eval('setAttr "{0}.pJCMDarius8_FootBndUpR" 1;'.format(pyBlendShapesName))
    mel.eval('setDrivenKeyframe -currentDriver rFoot.rotateX {0}.pJCMDarius8_FootBndUpR;'.format(pyBlendShapesName))
    mel.eval('setAttr "rFoot.rotateX" 0;')

    # pJCMDarius8_ForearmBnd_75L
    mel.eval('setAttr "lForearmBend.rotateY" 0;')
    mel.eval('setDrivenKeyframe -currentDriver lForearmBend.rotateY {0}.pJCMDarius8_ForearmBnd_75L;'.format(
        pyBlendShapesName))
    mel.eval('setAttr "lForearmBend.rotateY" -75;')
    mel.eval('setAttr "{0}.pJCMDarius8_ForearmBnd_75L" 1;'.format(pyBlendShapesName))
    mel.eval('setDrivenKeyframe -currentDriver lForearmBend.rotateY {0}.pJCMDarius8_ForearmBnd_75L;'.format(
        pyBlendShapesName))
    mel.eval('setAttr "lForearmBend.rotateY" 0;')

    # pJCMDarius8_ForearmBnd_n75R
    mel.eval('setAttr "rForearmBend.rotateY" 0;')
    mel.eval('setDrivenKeyframe -currentDriver rForearmBend.rotateY {0}.pJCMDarius8_ForearmBnd_n75R;'.format(
        pyBlendShapesName))
    mel.eval('setAttr "rForearmBend.rotateY" 75;')
    mel.eval('setAttr "{0}.pJCMDarius8_ForearmBnd_n75R" 1;'.format(pyBlendShapesName))
    mel.eval('setDrivenKeyframe -currentDriver rForearmBend.rotateY {0}.pJCMDarius8_ForearmBnd_n75R;'.format(
        pyBlendShapesName))
    mel.eval('setAttr "rForearmBend.rotateY" 0;')

    # pJCMDarius8_ForearmBndL
    mel.eval('setAttr "lForearmBend.rotateY" 0;')
    mel.eval('setDrivenKeyframe -currentDriver lForearmBend.rotateY {0}.pJCMDarius8_ForearmBndL;'.format(
        pyBlendShapesName))
    mel.eval('setAttr "lForearmBend.rotateY" -135;')
    mel.eval('setAttr "{0}.pJCMDarius8_ForearmBndL" 1;'.format(pyBlendShapesName))
    mel.eval('setDrivenKeyframe -currentDriver lForearmBend.rotateY {0}.pJCMDarius8_ForearmBndL;'.format(
        pyBlendShapesName))
    mel.eval('setAttr "lForearmBend.rotateY" 0;')

    # pJCMDarius8_ForearmBndR
    mel.eval('setAttr "rForearmBend.rotateY" 0;')
    mel.eval('setDrivenKeyframe -currentDriver rForearmBend.rotateY {0}.pJCMDarius8_ForearmBndR;'.format(
        pyBlendShapesName))
    mel.eval('setAttr "rForearmBend.rotateY" 135;')
    mel.eval('setAttr "{0}.pJCMDarius8_ForearmBndR" 1;'.format(pyBlendShapesName))
    mel.eval('setDrivenKeyframe -currentDriver rForearmBend.rotateY {0}.pJCMDarius8_ForearmBndR;'.format(
        pyBlendShapesName))
    mel.eval('setAttr "rForearmBend.rotateY" 0;')

    # pJCMDarius8_HeadBndBck_HDLv3
    mel.eval('setAttr "head.rotateX" -5;')
    mel.eval(
        'setDrivenKeyframe -currentDriver head.rotateX {0}.pJCMDarius8_HeadBndBck_HDLv3;'.format(pyBlendShapesName))
    mel.eval('setAttr "head.rotateX" -30;')
    mel.eval('setAttr "{0}.pJCMDarius8_HeadBndBck_HDLv3" 0.9;'.format(pyBlendShapesName))
    mel.eval(
        'setDrivenKeyframe -currentDriver head.rotateX {0}.pJCMDarius8_HeadBndBck_HDLv3;'.format(pyBlendShapesName))
    mel.eval('setAttr "head.rotateX" 0;')

    # pJCMDarius8_HeadBndDn_HDLv3
    mel.eval('setAttr "head.rotateX" 0;')
    mel.eval('setDrivenKeyframe -currentDriver head.rotateX {0}.pJCMDarius8_HeadBndDn_HDLv3;'.format(pyBlendShapesName))
    mel.eval('setAttr "head.rotateX" 25;')
    mel.eval('setAttr "{0}.pJCMDarius8_HeadBndDn_HDLv3" 1;'.format(pyBlendShapesName))
    mel.eval('setDrivenKeyframe -currentDriver head.rotateX {0}.pJCMDarius8_HeadBndDn_HDLv3;'.format(pyBlendShapesName))
    mel.eval('setAttr "head.rotateX" 0;')

    # pJCMDarius8_NeckLowerSideSideL
    mel.eval('setAttr "neckLower.rotateZ" 0;')
    mel.eval('setDrivenKeyframe -currentDriver neckLower.rotateZ {0}.pJCMDarius8_NeckLowerSideSideL;'.format(pyBlendShapesName))
    mel.eval('setAttr "neckLower.rotateZ" -20;')
    mel.eval('setAttr "{0}.pJCMDarius8_NeckLowerSideSideL" 1;'.format(pyBlendShapesName))
    mel.eval('setDrivenKeyframe -currentDriver neckLower.rotateZ {0}.pJCMDarius8_NeckLowerSideSideL;'.format(pyBlendShapesName))
    mel.eval('setAttr "neckLower.rotateZ" 0;')

    # pJCMDarius8_NeckLowerSideSideR
    mel.eval('setAttr "neckLower.rotateZ" 0;')
    mel.eval('setDrivenKeyframe -currentDriver neckLower.rotateZ {0}.pJCMDarius8_NeckLowerSideSideR;'.format(pyBlendShapesName))
    mel.eval('setAttr "neckLower.rotateZ" 20;')
    mel.eval('setAttr "{0}.pJCMDarius8_NeckLowerSideSideR" 1;'.format(pyBlendShapesName))
    mel.eval('setDrivenKeyframe -currentDriver neckLower.rotateZ {0}.pJCMDarius8_NeckLowerSideSideR;'.format(pyBlendShapesName))
    mel.eval('setAttr "neckLower.rotateZ" 0;')

    # pJCMDarius8_NeckUpperTwistL_HDLv3
    mel.eval('setAttr "neckUpper.rotateY" 0;')
    mel.eval('setDrivenKeyframe -currentDriver neckUpper.rotateY {0}.pJCMDarius8_NeckUpperTwistL_HDLv3;'.format(pyBlendShapesName))
    mel.eval('setAttr "neckUpper.rotateY" 22;')
    mel.eval('setAttr "{0}.pJCMDarius8_NeckUpperTwistL_HDLv3" 1;'.format(pyBlendShapesName))
    mel.eval('setDrivenKeyframe -currentDriver neckUpper.rotateY {0}.pJCMDarius8_NeckUpperTwistL_HDLv3;'.format(pyBlendShapesName))
    mel.eval('setAttr "neckUpper.rotateY" 0;')

    # pJCMDarius8_NeckUpperTwistR_HDLv3
    mel.eval('setAttr "neckUpper.rotateY" 0;')
    mel.eval('setDrivenKeyframe -currentDriver neckUpper.rotateY {0}.pJCMDarius8_NeckUpperTwistR_HDLv3;'.format(
        pyBlendShapesName))
    mel.eval('setAttr "neckUpper.rotateY" -22;')
    mel.eval('setAttr "{0}.pJCMDarius8_NeckUpperTwistR_HDLv3" 1;'.format(pyBlendShapesName))
    mel.eval('setDrivenKeyframe -currentDriver neckUpper.rotateY {0}.pJCMDarius8_NeckUpperTwistR_HDLv3;'.format(
        pyBlendShapesName))
    mel.eval('setAttr "neckUpper.rotateY" 0;')

    # pJCMDarius8_ShinBndBck_95L
    mel.eval('setAttr "lShin.rotateX" 0;')
    mel.eval(
        'setDrivenKeyframe -currentDriver lShin.rotateX {0}.pJCMDarius8_ShinBndBck_95L;'.format(pyBlendShapesName))
    mel.eval('setAttr "lShin.rotateX" 95;')
    mel.eval('setAttr "{0}.pJCMDarius8_ShinBndBck_95L" 1;'.format(pyBlendShapesName))
    mel.eval(
        'setDrivenKeyframe -currentDriver lShin.rotateX {0}.pJCMDarius8_ShinBndBck_95L;'.format(pyBlendShapesName))
    mel.eval('setAttr "lShin.rotateX" 0;')

    # pJCMDarius8_ShinBndBck_95R
    mel.eval('setAttr "rShin.rotateX" 0;')
    mel.eval(
        'setDrivenKeyframe -currentDriver rShin.rotateX {0}.pJCMDarius8_ShinBndBck_95R;'.format(pyBlendShapesName))
    mel.eval('setAttr "rShin.rotateX" 95;')
    mel.eval('setAttr "{0}.pJCMDarius8_ShinBndBck_95R" 1;'.format(pyBlendShapesName))
    mel.eval(
        'setDrivenKeyframe -currentDriver rShin.rotateX {0}.pJCMDarius8_ShinBndBck_95R;'.format(pyBlendShapesName))
    mel.eval('setAttr "rShin.rotateX" 0;')

    # pJCMDarius8_ShinBndBckL
    mel.eval('setAttr "lShin.rotateX" 0;')
    mel.eval(
        'setDrivenKeyframe -currentDriver lShin.rotateX {0}.pJCMDarius8_ShinBndBckL;'.format(pyBlendShapesName))
    mel.eval('setAttr "lShin.rotateX" 155;')
    mel.eval('setAttr "{0}.pJCMDarius8_ShinBndBckL" 1;'.format(pyBlendShapesName))
    mel.eval(
        'setDrivenKeyframe -currentDriver lShin.rotateX {0}.pJCMDarius8_ShinBndBckL;'.format(pyBlendShapesName))
    mel.eval('setAttr "lShin.rotateX" 0;')

    # pJCMDarius8_ShinBndBckR
    mel.eval('setAttr "rShin.rotateX" 0;')
    mel.eval(
        'setDrivenKeyframe -currentDriver rShin.rotateX {0}.pJCMDarius8_ShinBndBckR;'.format(pyBlendShapesName))
    mel.eval('setAttr "rShin.rotateX" 155;')
    mel.eval('setAttr "{0}.pJCMDarius8_ShinBndBckR" 1;'.format(pyBlendShapesName))
    mel.eval(
        'setDrivenKeyframe -currentDriver rShin.rotateX {0}.pJCMDarius8_ShinBndBckR;'.format(pyBlendShapesName))
    mel.eval('setAttr "rShin.rotateX" 0;')

    # pJCMDarius8_ShoulderBndDn_n30L_HDLv4
    mel.eval('setAttr "lShldrBend.rotateZ" 0;')
    mel.eval('setDrivenKeyframe -currentDriver lShldrBend.rotateZ {0}.pJCMDarius8_ShoulderBndDn_n30L_HDLv4;'.format(
        pyBlendShapesName))
    mel.eval('setAttr "lShldrBend.rotateZ" -30;')
    mel.eval('setAttr "{0}.pJCMDarius8_ShoulderBndDn_n30L_HDLv4" 1;'.format(pyBlendShapesName))
    mel.eval('setDrivenKeyframe -currentDriver lShldrBend.rotateZ {0}.pJCMDarius8_ShoulderBndDn_n30L_HDLv4;'.format(
        pyBlendShapesName))
    mel.eval('setAttr "lShldrBend.rotateZ" 0;')

    # pJCMDarius8_ShoulderBndDn_30R_HDLv4
    mel.eval('setAttr "rShldrBend.rotateZ" 0;')
    mel.eval('setDrivenKeyframe -currentDriver rShldrBend.rotateZ {0}.pJCMDarius8_ShoulderBndDn_30R_HDLv4;'.format(
        pyBlendShapesName))
    mel.eval('setAttr "rShldrBend.rotateZ" 30;')
    mel.eval('setAttr "{0}.pJCMDarius8_ShoulderBndDn_30R_HDLv4" 1;'.format(pyBlendShapesName))
    mel.eval('setDrivenKeyframe -currentDriver rShldrBend.rotateZ {0}.pJCMDarius8_ShoulderBndDn_30R_HDLv4;'.format(
        pyBlendShapesName))
    mel.eval('setAttr "rShldrBend.rotateZ" 0;')

    # pJCMDarius8_ShoulderBndDnL
    mel.eval('setAttr "lShldrBend.rotateZ" 0;')
    mel.eval('setDrivenKeyframe -currentDriver lShldrBend.rotateZ {0}.pJCMDarius8_ShoulderBndDnL;'.format(
        pyBlendShapesName))
    mel.eval('setAttr "lShldrBend.rotateZ" -40;')
    mel.eval('setAttr "{0}.pJCMDarius8_ShoulderBndDnL" 1;'.format(pyBlendShapesName))
    mel.eval('setDrivenKeyframe -currentDriver lShldrBend.rotateZ {0}.pJCMDarius8_ShoulderBndDnL;'.format(
        pyBlendShapesName))
    mel.eval('setAttr "lShldrBend.rotateZ" 0;')

    # pJCMDarius8_ShoulderBndDnR
    mel.eval('setAttr "rShldrBend.rotateZ" 0;')
    mel.eval('setDrivenKeyframe -currentDriver rShldrBend.rotateZ {0}.pJCMDarius8_ShoulderBndDnR;'.format(
        pyBlendShapesName))
    mel.eval('setAttr "rShldrBend.rotateZ" 40;')
    mel.eval('setAttr "{0}.pJCMDarius8_ShoulderBndDnR" 1;'.format(pyBlendShapesName))
    mel.eval('setDrivenKeyframe -currentDriver rShldrBend.rotateZ {0}.pJCMDarius8_ShoulderBndDnR;'.format(
        pyBlendShapesName))
    mel.eval('setAttr "rShldrBend.rotateZ" 0;')

    # pJCMDarius8_ShoulderFwd_n95L
    mel.eval('setAttr "lShldrBend.rotateY" 0;')
    mel.eval('setDrivenKeyframe -currentDriver lShldrBend.rotateY {0}.pJCMDarius8_ShoulderFwd_n95L;'.format(
        pyBlendShapesName))
    mel.eval('setAttr "lShldrBend.rotateY" -95;')
    mel.eval('setAttr "{0}.pJCMDarius8_ShoulderFwd_n95L" 1;'.format(pyBlendShapesName))
    mel.eval('setDrivenKeyframe -currentDriver lShldrBend.rotateY {0}.pJCMDarius8_ShoulderFwd_n95L;'.format(
        pyBlendShapesName))
    mel.eval('setAttr "lShldrBend.rotateY" 0;')

    # pJCMDarius8_ShoulderFwd_95R
    mel.eval('setAttr "rShldrBend.rotateY" 0;')
    mel.eval('setDrivenKeyframe -currentDriver rShldrBend.rotateY {0}.pJCMDarius8_ShoulderFwd_95R;'.format(pyBlendShapesName))
    mel.eval('setAttr "rShldrBend.rotateY" 95;')
    mel.eval('setAttr "{0}.pJCMDarius8_ShoulderFwd_95R" 1;'.format(pyBlendShapesName))
    mel.eval('setDrivenKeyframe -currentDriver rShldrBend.rotateY {0}.pJCMDarius8_ShoulderFwd_95R;'.format(pyBlendShapesName))
    mel.eval('setAttr "rShldrBend.rotateY" 0;')

    # pJCMDarius8_ThighForward_n85L
    mel.eval('setAttr "lThighBend.rotateX" 0;')
    mel.eval('setDrivenKeyframe -currentDriver lThighBend.rotateX {0}.pJCMDarius8_ThighForward_n85L;'.format(
        pyBlendShapesName))
    mel.eval('setAttr "lThighBend.rotateX" -85;')
    mel.eval('setAttr "{0}.pJCMDarius8_ThighForward_n85L" 1;'.format(pyBlendShapesName))
    mel.eval('setDrivenKeyframe -currentDriver lThighBend.rotateX {0}.pJCMDarius8_ThighForward_n85L;'.format(
        pyBlendShapesName))
    mel.eval('setAttr "lThighBend.rotateX" 0;')

    # pJCMDarius8_ThighForward_n85R
    mel.eval('setAttr "rThighBend.rotateX" 0;')
    mel.eval('setDrivenKeyframe -currentDriver rThighBend.rotateX {0}.pJCMDarius8_ThighForward_n85R;'.format(
        pyBlendShapesName))
    mel.eval('setAttr "rThighBend.rotateX" -85;')
    mel.eval('setAttr "{0}.pJCMDarius8_ThighForward_n85R" 1;'.format(pyBlendShapesName))
    mel.eval('setDrivenKeyframe -currentDriver rThighBend.rotateX {0}.pJCMDarius8_ThighForward_n85R;'.format(
        pyBlendShapesName))
    mel.eval('setAttr "rThighBend.rotateX" 0;')

    # pJCMDarius8_ThighSideSideL
    mel.eval('setAttr "lThighBend.rotateZ" 0;')
    mel.eval('setDrivenKeyframe -currentDriver lThighBend.rotateZ {0}.pJCMDarius8_ThighSideSideL;'.format(
        pyBlendShapesName))
    mel.eval('setAttr "lThighBend.rotateZ" 85;')
    mel.eval('setAttr "{0}.pJCMDarius8_ThighSideSideL" 1;'.format(pyBlendShapesName))
    mel.eval('setDrivenKeyframe -currentDriver lThighBend.rotateZ {0}.pJCMDarius8_ThighSideSideL;'.format(
        pyBlendShapesName))
    mel.eval('setAttr "lThighBend.rotateZ" 0;')

    # pJCMDarius8_ThighSideSideR
    mel.eval('setAttr "rThighBend.rotateZ" 0;')
    mel.eval('setDrivenKeyframe -currentDriver rThighBend.rotateZ {0}.pJCMDarius8_ThighSideSideR;'.format(
        pyBlendShapesName))
    mel.eval('setAttr "rThighBend.rotateZ" -85;')
    mel.eval('setAttr "{0}.pJCMDarius8_ThighSideSideR" 1;'.format(pyBlendShapesName))
    mel.eval('setDrivenKeyframe -currentDriver rThighBend.rotateZ {0}.pJCMDarius8_ThighSideSideR;'.format(
        pyBlendShapesName))
    mel.eval('setAttr "rThighBend.rotateZ" 0;')
