import maya.mel as mel
import maya.cmds as cmds


def ToonDwayne8JCMs(character, blendShapeGroup):
    pyBlendShapesName = blendShapeGroup
    if not cmds.objExists('{}.pJCMToonDwayne8_NeckLower_Bend_n25_NeckUpper_Bend_n27_Twist_n22'.format(pyBlendShapesName)):
        error = character + ' specific JCMs were not created.\n\n' + character + ' JCMs were either not exported out properly or the wrong file was selected for import.'
        cmds.warning(error.replace('\n\n', ' '))
        return error

    # pJCMToonDwayne8_NeckLower_Bend_n25_NeckUpper_Bend_n27_Twist_22
    cmds.expression(
        s='if ((neckUpper.rotateX * -0.0370370) * (neckLower.rotateX * -0.040) * (neckUpper.rotateY * 0.0454546) > 0)\n\t{0}.pJCMToonDwayne8_NeckLower_Bend_n25_NeckUpper_Bend_n27_Twist_22 = (neckUpper.rotateX * -0.0370370) * (neckLower.rotateX * -0.040) * (neckUpper.rotateY * 0.0454546);\nelse\n\t{0}.pJCMToonDwayne8_NeckLower_Bend_n25_NeckUpper_Bend_n27_Twist_22 = 0'.format(
            pyBlendShapesName), o='{0}'.format(pyBlendShapesName), n='pJCMToonDwayne8_NeckLower_Bend_n25_NeckUpper_Bend_n27_Twist_22_exp', ae=1,
        uc='all')

    # pJCMToonDwayne8_NeckLower_Bend_n25_NeckUpper_Bend_n27_Twist_n22
    cmds.expression(
        s='if ((neckUpper.rotateX * -0.0370370) * (neckLower.rotateX * -0.040) * (neckUpper.rotateY * -0.0454546) > 0)\n\t{0}.pJCMToonDwayne8_NeckLower_Bend_n25_NeckUpper_Bend_n27_Twist_n22 = (neckUpper.rotateX * -0.0370370) * (neckLower.rotateX * -0.040) * (neckUpper.rotateY * -0.0454546);\nelse\n\t{0}.pJCMToonDwayne8_NeckLower_Bend_n25_NeckUpper_Bend_n27_Twist_n22 = 0'.format(
            pyBlendShapesName), o='{0}'.format(pyBlendShapesName),
        n='pJCMToonDwayne8_NeckLower_Bend_n25_NeckUpper_Bend_n27_Twist_n22_exp', ae=1,
        uc='all')
    
    # pJCMToonDwayne8Abdomen2Fwd_40
    mel.eval('select {}'.format(pyBlendShapesName))
    mel.eval('setDrivenKeyframe -itt linear -ott linear -at pJCMToonDwayne8Abdomen2Fwd_40 -v 0 -cd Genesis8Male_Group|hip|abdomenLower|abdomenUpper.rotateX -dv 0;')
    mel.eval('setDrivenKeyframe -itt linear -ott linear -at pJCMToonDwayne8Abdomen2Fwd_40 -v 1 -cd Genesis8Male_Group|hip|abdomenLower|abdomenUpper.rotateX -dv 40;')
    # pJCMToonDwayne8Abdomen2Side_24_L
    mel.eval('select {}'.format(pyBlendShapesName))
    mel.eval('setDrivenKeyframe -itt linear -ott linear -at pJCMToonDwayne8Abdomen2Side_24_L -v 0 -cd Genesis8Male_Group|hip|abdomenLower|abdomenUpper.rotateZ -dv 0;')
    mel.eval('setDrivenKeyframe -itt linear -ott linear -at pJCMToonDwayne8Abdomen2Side_24_L -v 1 -cd Genesis8Male_Group|hip|abdomenLower|abdomenUpper.rotateZ -dv -24;')
    # pJCMToonDwayne8Abdomen2Side_24_L
    mel.eval('select {}'.format(pyBlendShapesName))     
    mel.eval('setDrivenKeyframe -itt linear -ott linear -at pJCMToonDwayne8Abdomen2Side_24_R -v 0 -cd Genesis8Male_Group|hip|abdomenLower|abdomenUpper.rotateZ -dv 0;')
    mel.eval('setDrivenKeyframe -itt linear -ott linear -at pJCMToonDwayne8Abdomen2Side_24_R -v 1 -cd Genesis8Male_Group|hip|abdomenLower|abdomenUpper.rotateZ -dv 24;')
    # pJCMToonDwayne8AbdomenFwd_35
    mel.eval('select {}'.format(pyBlendShapesName))
    mel.eval('setDrivenKeyframe -itt linear -ott linear -at pJCMToonDwayne8AbdomenFwd_35 -v 0 -cd Genesis8Male_Group|hip|abdomenLower.rotateX -dv 0;')
    mel.eval('setDrivenKeyframe -itt linear -ott linear -at pJCMToonDwayne8AbdomenFwd_35 -v 1 -cd Genesis8Male_Group|hip|abdomenLower.rotateX -dv 35;')

    # pJCMToonDwayne8ChestSide_20_R
    mel.eval('select {}'.format(pyBlendShapesName))
    mel.eval('setDrivenKeyframe -itt linear -ott linear -at pJCMToonDwayne8ChestSide_20_R -v 0 -cd Genesis8Male_Group|hip|abdomenLower|abdomenUpper|chestLower.rotateZ -dv 0;')
    mel.eval('setDrivenKeyframe -itt linear -ott linear -at pJCMToonDwayne8ChestSide_20_R -v 1 -cd Genesis8Male_Group|hip|abdomenLower|abdomenUpper|chestLower.rotateZ -dv 20;')
    # pJCMToonDwayne8ChestSide_20_L    
    mel.eval('select {}'.format(pyBlendShapesName))
    mel.eval('setDrivenKeyframe -itt linear -ott linear -at pJCMToonDwayne8ChestSide_20_L -v 0 -cd Genesis8Male_Group|hip|abdomenLower|abdomenUpper|chestLower.rotateZ -dv 0;')
    mel.eval('setDrivenKeyframe -itt linear -ott linear -at pJCMToonDwayne8ChestSide_20_L -v 1 -cd Genesis8Male_Group|hip|abdomenLower|abdomenUpper|chestLower.rotateZ -dv -20;')
    # pJCMToonDwayne8ChestFwd_35
    mel.eval('select {}'.format(pyBlendShapesName))
    mel.eval('setDrivenKeyframe -itt linear -ott linear -at pJCMToonDwayne8ChestFwd_35 -v 0 -cd Genesis8Male_Group|hip|abdomenLower|abdomenUpper|chestLower.rotateX -dv 0;')
    mel.eval('setDrivenKeyframe -itt linear -ott linear -at pJCMToonDwayne8ChestFwd_35 -v 1 -cd Genesis8Male_Group|hip|abdomenLower|abdomenUpper|chestLower.rotateX -dv 35;')
    mel.eval('select {}'.format(pyBlendShapesName))

    mel.eval('select {}'.format(pyBlendShapesName))
    mel.eval('setDrivenKeyframe -itt linear -ott linear -at pJCMToonDwayne8Collar_Fwd_n26_R -v 0 -cd Genesis8Male_Group|hip|abdomenLower|abdomenUpper|chestLower|chestUpper|rCollar.rotateY -dv 0;')
    mel.eval('setDrivenKeyframe -itt linear -ott linear -at pJCMToonDwayne8Collar_Fwd_n26_R -v 1 -cd Genesis8Male_Group|hip|abdomenLower|abdomenUpper|chestLower|chestUpper|rCollar.rotateY -dv 26;')
    mel.eval('select {}'.format(pyBlendShapesName))
    mel.eval('setDrivenKeyframe -itt linear -ott linear -at pJCMToonDwayne8Collar_Fwd_n26_L -v 0 -cd Genesis8Male_Group|hip|abdomenLower|abdomenUpper|chestLower|chestUpper|lCollar.rotateY -dv 0;')
    mel.eval('setDrivenKeyframe -itt linear -ott linear -at pJCMToonDwayne8Collar_Fwd_n26_L -v 1 -cd Genesis8Male_Group|hip|abdomenLower|abdomenUpper|chestLower|chestUpper|lCollar.rotateY -dv -26;')
    mel.eval('select {}'.format(pyBlendShapesName))
    mel.eval('setDrivenKeyframe -itt linear -ott linear -at pJCMToonDwayne8CollarUp_55_R -v 0 -cd Genesis8Male_Group|hip|abdomenLower|abdomenUpper|chestLower|chestUpper|rCollar.rotateZ -dv 0;')
    mel.eval('setDrivenKeyframe -itt linear -ott linear -at pJCMToonDwayne8CollarUp_55_R -v 1 -cd Genesis8Male_Group|hip|abdomenLower|abdomenUpper|chestLower|chestUpper|rCollar.rotateZ -dv -55;')
    mel.eval('select {}'.format(pyBlendShapesName))
    mel.eval('setDrivenKeyframe -itt linear -ott linear -at pJCMToonDwayne8CollarUp_55_L -v 0 -cd Genesis8Male_Group|hip|abdomenLower|abdomenUpper|chestLower|chestUpper|lCollar.rotateZ -dv 0;')
    mel.eval('setDrivenKeyframe -itt linear -ott linear -at pJCMToonDwayne8CollarUp_55_L -v 1 -cd Genesis8Male_Group|hip|abdomenLower|abdomenUpper|chestLower|chestUpper|lCollar.rotateZ -dv 55;')
    # mel.eval('select {}'.format(pyBlendShapesName))
    # mel.eval('setDrivenKeyframe -itt linear -ott linear -at Genesis8Male__pJCMCollarTwist_p30_R -v 0 -cd Genesis8Male_Group|hip|abdomenLower|abdomenUpper|chestLower|chestUpper|rCollar.rotateX -dv 0;')
    # mel.eval('setDrivenKeyframe -itt linear -ott linear -at Genesis8Male__pJCMCollarTwist_p30_R -v 1 -cd Genesis8Male_Group|hip|abdomenLower|abdomenUpper|chestLower|chestUpper|rCollar.rotateX -dv 30;')
    # mel.eval('select {}'.format(pyBlendShapesName))
    # mel.eval('setDrivenKeyframe -itt linear -ott linear -at Genesis8Male__pJCMCollarTwist_p30_L -v 0 -cd Genesis8Male_Group|hip|abdomenLower|abdomenUpper|chestLower|chestUpper|lCollar.rotateX -dv 0;')
    # mel.eval('setDrivenKeyframe -itt linear -ott linear -at Genesis8Male__pJCMCollarTwist_p30_L -v 1 -cd Genesis8Male_Group|hip|abdomenLower|abdomenUpper|chestLower|chestUpper|lCollar.rotateX -dv 30;')
    mel.eval('select {}'.format(pyBlendShapesName))
    mel.eval('setDrivenKeyframe -itt linear -ott linear -at pJCMToonDwayne8CollarTwist_n30_R -v 0 -cd Genesis8Male_Group|hip|abdomenLower|abdomenUpper|chestLower|chestUpper|rCollar.rotateX -dv 0;')
    mel.eval('setDrivenKeyframe -itt linear -ott linear -at pJCMToonDwayne8CollarTwist_n30_R -v 1 -cd Genesis8Male_Group|hip|abdomenLower|abdomenUpper|chestLower|chestUpper|rCollar.rotateX -dv -30;')
    mel.eval('select {}'.format(pyBlendShapesName))
    mel.eval('setDrivenKeyframe -itt linear -ott linear -at pJCMToonDwayne8CollarTwist_n30_L -v 0 -cd Genesis8Male_Group|hip|abdomenLower|abdomenUpper|chestLower|chestUpper|lCollar.rotateX -dv 0;')
    mel.eval('setDrivenKeyframe -itt linear -ott linear -at pJCMToonDwayne8CollarTwist_n30_L -v 1 -cd Genesis8Male_Group|hip|abdomenLower|abdomenUpper|chestLower|chestUpper|lCollar.rotateX -dv -30;')

    mel.eval('setDrivenKeyframe -itt linear -ott linear -at pJCMToonDwayne8FootUp_40_R -v 0 -cd Genesis8Male_Group|hip|pelvis|rThighBend|rThighTwist|rShin|rFoot.rotateX -dv 0;')
    mel.eval('setDrivenKeyframe -itt linear -ott linear -at pJCMToonDwayne8FootUp_40_R -v 1 -cd Genesis8Male_Group|hip|pelvis|rThighBend|rThighTwist|rShin|rFoot.rotateX -dv -40;')
    mel.eval('select {}'.format(pyBlendShapesName))
    mel.eval('setDrivenKeyframe -itt linear -ott linear -at pJCMToonDwayne8FootUp_40_L -v 0 -cd Genesis8Male_Group|hip|pelvis|lThighBend|lThighTwist|lShin|lFoot.rotateX -dv 0;')
    mel.eval('setDrivenKeyframe -itt linear -ott linear -at pJCMToonDwayne8FootUp_40_L -v 1 -cd Genesis8Male_Group|hip|pelvis|lThighBend|lThighTwist|lShin|lFoot.rotateX -dv -40;')
    mel.eval('select {}'.format(pyBlendShapesName))
    mel.eval('setDrivenKeyframe -itt linear -ott linear -at pJCMToonDwayne8FootDwn_75_R -v 0 -cd Genesis8Male_Group|hip|pelvis|rThighBend|rThighTwist|rShin|rFoot.rotateX -dv 0;')
    mel.eval('setDrivenKeyframe -itt linear -ott linear -at pJCMToonDwayne8FootDwn_75_R -v 1 -cd Genesis8Male_Group|hip|pelvis|rThighBend|rThighTwist|rShin|rFoot.rotateX -dv 75;')
    mel.eval('select {}'.format(pyBlendShapesName))
    mel.eval('setDrivenKeyframe -itt linear -ott linear -at pJCMToonDwayne8FootDwn_75_L -v 0 -cd Genesis8Male_Group|hip|pelvis|lThighBend|lThighTwist|lShin|lFoot.rotateX -dv 0;')
    mel.eval('setDrivenKeyframe -itt linear -ott linear -at pJCMToonDwayne8FootDwn_75_L -v 1 -cd Genesis8Male_Group|hip|pelvis|lThighBend|lThighTwist|lShin|lFoot.rotateX -dv 75;')

    mel.eval('select {}'.format(pyBlendShapesName))
    mel.eval('setDrivenKeyframe -itt linear -ott linear -at pJCMToonDwayne8HandDwn_70_R -v 0 -cd Genesis8Male_Group|hip|abdomenLower|abdomenUpper|chestLower|chestUpper|rCollar|rShldrBend|rShldrTwist|rForearmBend|rForearmTwist|rHand.rotateZ -dv 0;')
    mel.eval('setDrivenKeyframe -itt linear -ott linear -at pJCMToonDwayne8HandDwn_70_R -v 1 -cd Genesis8Male_Group|hip|abdomenLower|abdomenUpper|chestLower|chestUpper|rCollar|rShldrBend|rShldrTwist|rForearmBend|rForearmTwist|rHand.rotateZ -dv 70;')
    mel.eval('select {}'.format(pyBlendShapesName))
    mel.eval('setDrivenKeyframe -itt linear -ott linear -at pJCMToonDwayne8HandDwn_70_L -v 0 -cd Genesis8Male_Group|hip|abdomenLower|abdomenUpper|chestLower|chestUpper|lCollar|lShldrBend|lShldrTwist|lForearmBend|lForearmTwist|lHand.rotateZ -dv 0;')
    mel.eval('setDrivenKeyframe -itt linear -ott linear -at pJCMToonDwayne8HandDwn_70_L -v 1 -cd Genesis8Male_Group|hip|abdomenLower|abdomenUpper|chestLower|chestUpper|lCollar|lShldrBend|lShldrTwist|lForearmBend|lForearmTwist|lHand.rotateZ -dv -70;')
    mel.eval('select {}'.format(pyBlendShapesName))
    mel.eval('setDrivenKeyframe -itt linear -ott linear -at pJCMToonDwayne8HandSide_Side_30_R -v 0 -cd Genesis8Male_Group|hip|abdomenLower|abdomenUpper|chestLower|chestUpper|rCollar|rShldrBend|rShldrTwist|rForearmBend|rForearmTwist|rHand.rotateY -dv 0;')
    mel.eval('setDrivenKeyframe -itt linear -ott linear -at pJCMToonDwayne8HandSide_Side_30_R -v 1 -cd Genesis8Male_Group|hip|abdomenLower|abdomenUpper|chestLower|chestUpper|rCollar|rShldrBend|rShldrTwist|rForearmBend|rForearmTwist|rHand.rotateY -dv -30;')
    mel.eval('select {}'.format(pyBlendShapesName))
    mel.eval('setDrivenKeyframe -itt linear -ott linear -at pJCMToonDwayne8HandSide_Side_30_L -v 0 -cd Genesis8Male_Group|hip|abdomenLower|abdomenUpper|chestLower|chestUpper|lCollar|lShldrBend|lShldrTwist|lForearmBend|lForearmTwist|lHand.rotateY -dv 0;')
    mel.eval('setDrivenKeyframe -itt linear -ott linear -at pJCMToonDwayne8HandSide_Side_30_L -v 1 -cd Genesis8Male_Group|hip|abdomenLower|abdomenUpper|chestLower|chestUpper|lCollar|lShldrBend|lShldrTwist|lForearmBend|lForearmTwist|lHand.rotateY -dv 30;')
    mel.eval('select {}'.format(pyBlendShapesName))
    mel.eval('setDrivenKeyframe -itt linear -ott linear -at pJCMToonDwayne8HandSide_Side_n28_R -v 0 -cd Genesis8Male_Group|hip|abdomenLower|abdomenUpper|chestLower|chestUpper|rCollar|rShldrBend|rShldrTwist|rForearmBend|rForearmTwist|rHand.rotateY -dv 0;')
    mel.eval('setDrivenKeyframe -itt linear -ott linear -at pJCMToonDwayne8HandSide_Side_n28_R -v 1 -cd Genesis8Male_Group|hip|abdomenLower|abdomenUpper|chestLower|chestUpper|rCollar|rShldrBend|rShldrTwist|rForearmBend|rForearmTwist|rHand.rotateY -dv 28;')
    mel.eval('select {}'.format(pyBlendShapesName))
    mel.eval('setDrivenKeyframe -itt linear -ott linear -at pJCMToonDwayne8HandSide_Side_n28_L -v 0 -cd Genesis8Male_Group|hip|abdomenLower|abdomenUpper|chestLower|chestUpper|lCollar|lShldrBend|lShldrTwist|lForearmBend|lForearmTwist|lHand.rotateY -dv 0;')
    mel.eval('setDrivenKeyframe -itt linear -ott linear -at pJCMToonDwayne8HandSide_Side_n28_L -v 1 -cd Genesis8Male_Group|hip|abdomenLower|abdomenUpper|chestLower|chestUpper|lCollar|lShldrBend|lShldrTwist|lForearmBend|lForearmTwist|lHand.rotateY -dv -28;')
    mel.eval('select {}'.format(pyBlendShapesName))
    mel.eval('setDrivenKeyframe -itt linear -ott linear -at pJCMToonDwayne8HandUp_80_R -v 0 -cd Genesis8Male_Group|hip|abdomenLower|abdomenUpper|chestLower|chestUpper|rCollar|rShldrBend|rShldrTwist|rForearmBend|rForearmTwist|rHand.rotateZ -dv 0;')
    mel.eval('setDrivenKeyframe -itt linear -ott linear -at pJCMToonDwayne8HandUp_80_R -v 1 -cd Genesis8Male_Group|hip|abdomenLower|abdomenUpper|chestLower|chestUpper|rCollar|rShldrBend|rShldrTwist|rForearmBend|rForearmTwist|rHand.rotateZ -dv -80;')
    mel.eval('select {}'.format(pyBlendShapesName))
    mel.eval('setDrivenKeyframe -itt linear -ott linear -at pJCMToonDwayne8HandUp_80_L -v 0 -cd Genesis8Male_Group|hip|abdomenLower|abdomenUpper|chestLower|chestUpper|lCollar|lShldrBend|lShldrTwist|lForearmBend|lForearmTwist|lHand.rotateZ -dv 0;')
    mel.eval('setDrivenKeyframe -itt linear -ott linear -at pJCMToonDwayne8HandUp_80_L -v 1 -cd Genesis8Male_Group|hip|abdomenLower|abdomenUpper|chestLower|chestUpper|lCollar|lShldrBend|lShldrTwist|lForearmBend|lForearmTwist|lHand.rotateZ -dv 80;')

    mel.eval('select {}'.format(pyBlendShapesName))
    mel.eval('setDrivenKeyframe -itt linear -ott linear -at pJCMToonDwayne8HeadFwd_25 -v 0 -cd Genesis8Male_Group|hip|abdomenLower|abdomenUpper|chestLower|chestUpper|neckLower|neckUpper|head.rotateX -dv 0;')
    mel.eval('setDrivenKeyframe -itt linear -ott linear -at pJCMToonDwayne8HeadFwd_25 -v 1 -cd Genesis8Male_Group|hip|abdomenLower|abdomenUpper|chestLower|chestUpper|neckLower|neckUpper|head.rotateX -dv 25;')
    mel.eval('select {}'.format(pyBlendShapesName))
    mel.eval('setDrivenKeyframe -itt linear -ott linear -at pJCMToonDwayne8HeadBack_27 -v 0 -cd Genesis8Male_Group|hip|abdomenLower|abdomenUpper|chestLower|chestUpper|neckLower|neckUpper|head.rotateX -dv 0;')
    mel.eval('setDrivenKeyframe -itt linear -ott linear -at pJCMToonDwayne8HeadBack_27 -v 1 -cd Genesis8Male_Group|hip|abdomenLower|abdomenUpper|chestLower|chestUpper|neckLower|neckUpper|head.rotateX -dv -27;')

    mel.eval('select {}'.format(pyBlendShapesName))
    mel.eval('setDrivenKeyframe -itt linear -ott linear -at pJCMToonDwayne8LowerNeckTwist_22_R -v 0 -cd Genesis8Male_Group|hip|abdomenLower|abdomenUpper|chestLower|chestUpper|neckLower.rotateY -dv 0;')
    mel.eval('setDrivenKeyframe -itt linear -ott linear -at pJCMToonDwayne8LowerNeckTwist_22_R -v 1 -cd Genesis8Male_Group|hip|abdomenLower|abdomenUpper|chestLower|chestUpper|neckLower.rotateY -dv -22;')
    mel.eval('select {}'.format(pyBlendShapesName))
    mel.eval('setDrivenKeyframe -itt linear -ott linear -at pJCMToonDwayne8LowerNeckTwist_22_L -v 0 -cd Genesis8Male_Group|hip|abdomenLower|abdomenUpper|chestLower|chestUpper|neckLower.rotateY -dv 0;')
    mel.eval('setDrivenKeyframe -itt linear -ott linear -at pJCMToonDwayne8LowerNeckTwist_22_L -v 1 -cd Genesis8Male_Group|hip|abdomenLower|abdomenUpper|chestLower|chestUpper|neckLower.rotateY -dv 22;')
    mel.eval('select {}'.format(pyBlendShapesName))
    mel.eval('setDrivenKeyframe -itt linear -ott linear -at pJCMToonDwayne8NeckBack_27 -v 0 -cd Genesis8Male_Group|hip|abdomenLower|abdomenUpper|chestLower|chestUpper|neckLower|neckUpper.rotateX -dv 0;')
    mel.eval('setDrivenKeyframe -itt linear -ott linear -at pJCMToonDwayne8NeckBack_27 -v 1 -cd Genesis8Male_Group|hip|abdomenLower|abdomenUpper|chestLower|chestUpper|neckLower|neckUpper.rotateX -dv -54.0541;')
    mel.eval('setDrivenKeyframe -itt linear -ott linear -at pJCMToonDwayne8NeckBack_27 -v 0 -cd Genesis8Male_Group|hip|abdomenLower|abdomenUpper|chestLower|chestUpper|neckLower.rotateX -dv 0;')
    mel.eval('setDrivenKeyframe -itt linear -ott linear -at pJCMToonDwayne8NeckBack_27 -v 1 -cd Genesis8Male_Group|hip|abdomenLower|abdomenUpper|chestLower|chestUpper|neckLower.rotateX -dv -50;')
    cmds.rename('blendWeighted1', 'pJCMToonDwayne8NeckBack_27_exp')
    mel.eval('select {}'.format(pyBlendShapesName))
    mel.eval('setDrivenKeyframe -itt linear -ott linear -at pJCMToonDwayne8NeckLowerSide_40_R -v 0 -cd Genesis8Male_Group|hip|abdomenLower|abdomenUpper|chestLower|chestUpper|neckLower.rotateZ -dv 0;')
    mel.eval('setDrivenKeyframe -itt linear -ott linear -at pJCMToonDwayne8NeckLowerSide_40_R -v 1 -cd Genesis8Male_Group|hip|abdomenLower|abdomenUpper|chestLower|chestUpper|neckLower.rotateZ -dv 30;')
    mel.eval('select {}'.format(pyBlendShapesName))
    mel.eval('setDrivenKeyframe -itt linear -ott linear -at pJCMToonDwayne8NeckLowerSide_40_L -v 0 -cd Genesis8Male_Group|hip|abdomenLower|abdomenUpper|chestLower|chestUpper|neckLower.rotateZ -dv 0;')
    mel.eval('setDrivenKeyframe -itt linear -ott linear -at pJCMToonDwayne8NeckLowerSide_40_L -v 1 -cd Genesis8Male_Group|hip|abdomenLower|abdomenUpper|chestLower|chestUpper|neckLower.rotateZ -dv -30;')

    mel.eval('select {}'.format(pyBlendShapesName))
    mel.eval('setDrivenKeyframe -at pJCMToonDwayne8ShinBend_90_R -v 0 -cd Genesis8Male_Group|hip|pelvis|rThighBend|rThighTwist|rShin.rotateX -dv 0;')
    mel.eval('setDrivenKeyframe -at pJCMToonDwayne8ShinBend_90_R -v 1 -cd Genesis8Male_Group|hip|pelvis|rThighBend|rThighTwist|rShin.rotateX -dv 90;')
    mel.eval('select {}'.format(pyBlendShapesName))
    mel.eval('setDrivenKeyframe -at pJCMToonDwayne8ShinBend_90_L -v 0 -cd Genesis8Male_Group|hip|pelvis|lThighBend|lThighTwist|lShin.rotateX -dv 0;')
    mel.eval('setDrivenKeyframe -at pJCMToonDwayne8ShinBend_90_L -v 1 -cd Genesis8Male_Group|hip|pelvis|lThighBend|lThighTwist|lShin.rotateX -dv 90;')

    mel.eval('select {}'.format(pyBlendShapesName))
    mel.eval('setDrivenKeyframe -itt linear -ott linear -at pJCMToonDwayne8ShldrUp_90_R -v 0 -cd Genesis8Male_Group|hip|abdomenLower|abdomenUpper|chestLower|chestUpper|rCollar|rShldrBend.rotateZ -dv 0;')
    mel.eval('setDrivenKeyframe -itt linear -ott linear -at pJCMToonDwayne8ShldrUp_90_R -v 1 -cd Genesis8Male_Group|hip|abdomenLower|abdomenUpper|chestLower|chestUpper|rCollar|rShldrBend.rotateZ -dv -90;')
    mel.eval('select {}'.format(pyBlendShapesName))
    mel.eval('setDrivenKeyframe -itt linear -ott linear -at pJCMToonDwayne8ShldrUp_90_L -v 0 -cd Genesis8Male_Group|hip|abdomenLower|abdomenUpper|chestLower|chestUpper|lCollar|lShldrBend.rotateZ -dv 0;')
    mel.eval('setDrivenKeyframe -itt linear -ott linear -at pJCMToonDwayne8ShldrUp_90_L -v 1 -cd Genesis8Male_Group|hip|abdomenLower|abdomenUpper|chestLower|chestUpper|lCollar|lShldrBend.rotateZ -dv 90;')
    mel.eval('select {}'.format(pyBlendShapesName))
    mel.eval('setDrivenKeyframe -itt linear -ott linear -at pJCMToonDwayne8ShldrFwd_110_R -v 0 -cd Genesis8Male_Group|hip|abdomenLower|abdomenUpper|chestLower|chestUpper|rCollar|rShldrBend.rotateY -dv 0;')
    mel.eval('setDrivenKeyframe -itt linear -ott linear -at pJCMToonDwayne8ShldrFwd_110_R -v 1 -cd Genesis8Male_Group|hip|abdomenLower|abdomenUpper|chestLower|chestUpper|rCollar|rShldrBend.rotateY -dv 110;')
    mel.eval('select {}'.format(pyBlendShapesName))
    mel.eval('setDrivenKeyframe -itt linear -ott linear -at pJCMToonDwayne8ShldrFwd_110_L -v 0 -cd Genesis8Male_Group|hip|abdomenLower|abdomenUpper|chestLower|chestUpper|lCollar|lShldrBend.rotateY -dv 0;')
    mel.eval('setDrivenKeyframe -itt linear -ott linear -at pJCMToonDwayne8ShldrFwd_110_L -v 1 -cd Genesis8Male_Group|hip|abdomenLower|abdomenUpper|chestLower|chestUpper|lCollar|lShldrBend.rotateY -dv -110;')
    mel.eval('select {}'.format(pyBlendShapesName))
    mel.eval('setDrivenKeyframe -itt linear -ott linear -at pJCMToonDwayne8Shldr_Dwn_40_R -v 0 -cd Genesis8Male_Group|hip|abdomenLower|abdomenUpper|chestLower|chestUpper|rCollar|rShldrBend.rotateZ -dv 0;')
    mel.eval('setDrivenKeyframe -itt linear -ott linear -at pJCMToonDwayne8Shldr_Dwn_40_R -v 1 -cd Genesis8Male_Group|hip|abdomenLower|abdomenUpper|chestLower|chestUpper|rCollar|rShldrBend.rotateZ -dv 40;')
    mel.eval('select {}'.format(pyBlendShapesName))
    mel.eval('setDrivenKeyframe -itt linear -ott linear -at pJCMToonDwayne8Shldr_Dwn_40_L -v 0 -cd Genesis8Male_Group|hip|abdomenLower|abdomenUpper|chestLower|chestUpper|lCollar|lShldrBend.rotateZ -dv 0;')
    mel.eval('setDrivenKeyframe -itt linear -ott linear -at pJCMToonDwayne8Shldr_Dwn_40_L -v 1 -cd Genesis8Male_Group|hip|abdomenLower|abdomenUpper|chestLower|chestUpper|lCollar|lShldrBend.rotateZ -dv -40;')

    # pJCMToonDwayne8Shoulder_Bend_90_Front-Back_n60_L
    cmds.expression(
        s='if ((lShldrBend.rotateY * -0.0166667) * (lShldrBend.rotateZ * 0.0111111) > 0)\n\t{0}.pJCMToonDwayne8Shoulder_Bend_90_Front_Back_n60_L = (lShldrBend.rotateY * -0.0166667) * (lShldrBend.rotateZ * 0.0111111);\nelse\n\t{0}.pJCMToonDwayne8Shoulder_Bend_90_Front_Back_n60_L = 0'.format(
            pyBlendShapesName), o='{0}'.format(pyBlendShapesName),
        n='pJCMToonDwayne8Shoulder_Bend_90_Front_Back_n60_L_exp', ae=1,
        uc='all')

    # pJCMToonDwayne8Shoulder_Bend_n90_Front-Back_60_R
    cmds.expression(
        s='if ((rShldrBend.rotateY * 0.0166667) * (rShldrBend.rotateZ * -0.0111111) > 0)\n\t{0}.pJCMToonDwayne8Shoulder_Bend_n90_Front_Back_60_R = (rShldrBend.rotateY * 0.0166667) * (rShldrBend.rotateZ * -0.0111111);\nelse\n\t{0}.pJCMToonDwayne8Shoulder_Bend_n90_Front_Back_60_R = 0'.format(
            pyBlendShapesName), o='{0}'.format(pyBlendShapesName),
        n='pJCMToonDwayne8Shoulder_Bend_n90_Front_Back_60_R_exp', ae=1,
        uc='all')

    # pJCMToonDwayne8ShoulderFront-Back_40_Bend_90_L
    cmds.expression(
        s='if ((lShldrBend.rotateY * 0.0250) * (lShldrBend.rotateZ * 0.0111111) > 0)\n\t{0}.pJCMToonDwayne8ShoulderFront_Back_40_Bend_90_L = (lShldrBend.rotateY * 0.0250) * (lShldrBend.rotateZ * 0.0111111);\nelse\n\t{0}.pJCMToonDwayne8ShoulderFront_Back_40_Bend_90_L = 0'.format(
            pyBlendShapesName), o='{0}'.format(pyBlendShapesName),
        n='pJCMToonDwayne8ShoulderFront_Back_40_Bend_90_L_exp', ae=1,
        uc='all')

    # pJCMToonDwayne8ShoulderFront-Back_40_Bend_90_R
    cmds.expression(
        s='if ((rShldrBend.rotateY * -0.0250) * (rShldrBend.rotateZ * -0.0111111) > 0)\n\t{0}.pJCMToonDwayne8ShoulderFront_Back_40_Bend_90_R = (rShldrBend.rotateY * -0.0250) * (rShldrBend.rotateZ * -0.0111111);\nelse\n\t{0}.pJCMToonDwayne8ShoulderFront_Back_40_Bend_90_R = 0'.format(
            pyBlendShapesName), o='{0}'.format(pyBlendShapesName),
        n='pJCMToonDwayne8ShoulderFront_Back_40_Bend_90_R_exp', ae=1,
        uc='all')

    mel.eval('select {}'.format(pyBlendShapesName))
    mel.eval('setDrivenKeyframe -itt linear -ott linear -at pJCMToonDwayne8ShoulderFront_Back_40_R -v 0 -cd Genesis8Male_Group|hip|abdomenLower|abdomenUpper|chestLower|chestUpper|rCollar|rShldrBend.rotateY -dv 0;')
    mel.eval('setDrivenKeyframe -itt linear -ott linear -at pJCMToonDwayne8ShoulderFront_Back_40_R -v 1 -cd Genesis8Male_Group|hip|abdomenLower|abdomenUpper|chestLower|chestUpper|rCollar|rShldrBend.rotateY -dv -40;')
    mel.eval('select {}'.format(pyBlendShapesName))
    mel.eval('setDrivenKeyframe -itt linear -ott linear -at pJCMToonDwayne8ShoulderFront_Back_40_L -v 0 -cd Genesis8Male_Group|hip|abdomenLower|abdomenUpper|chestLower|chestUpper|lCollar|lShldrBend.rotateY -dv 0;')
    mel.eval('setDrivenKeyframe -itt linear -ott linear -at pJCMToonDwayne8ShoulderFront_Back_40_L -v 1 -cd Genesis8Male_Group|hip|abdomenLower|abdomenUpper|chestLower|chestUpper|lCollar|lShldrBend.rotateY -dv 40;')

    mel.eval('select {}'.format(pyBlendShapesName))
    mel.eval('setDrivenKeyframe -itt linear -ott linear -at pJCMToonDwayne8ThighSide_85_R -v 0 -cd Genesis8Male_Group|hip|pelvis|rThighBend.rotateZ -dv 0;')
    mel.eval('setDrivenKeyframe -itt linear -ott linear -at pJCMToonDwayne8ThighSide_85_R -v 1 -cd Genesis8Male_Group|hip|pelvis|rThighBend.rotateZ -dv -85;')
    mel.eval('select {}'.format(pyBlendShapesName))
    mel.eval('setDrivenKeyframe -itt linear -ott linear -at pJCMToonDwayne8ThighSide_85_L -v 0 -cd Genesis8Male_Group|hip|pelvis|lThighBend.rotateZ -dv 0;')
    mel.eval('setDrivenKeyframe -itt linear -ott linear -at pJCMToonDwayne8ThighSide_85_L -v 1 -cd Genesis8Male_Group|hip|pelvis|lThighBend.rotateZ -dv 85;')
    mel.eval('select {}'.format(pyBlendShapesName))
    mel.eval('setDrivenKeyframe -at pJCMToonDwayne8ThighFwd_57_R -v 1 -cd Genesis8Male_Group|hip|pelvis|rThighBend.rotateX -dv -57;')
    mel.eval('setDrivenKeyframe -at pJCMToonDwayne8ThighFwd_57_R -v 0 -cd Genesis8Male_Group|hip|pelvis|rThighBend.rotateX -dv 0;')
    mel.eval('select {}'.format(pyBlendShapesName))
    mel.eval('setDrivenKeyframe -at pJCMToonDwayne8ThighFwd_57_L -v 1 -cd Genesis8Male_Group|hip|pelvis|lThighBend.rotateX -dv -57;')
    mel.eval('setDrivenKeyframe -at pJCMToonDwayne8ThighFwd_57_L -v 0 -cd Genesis8Male_Group|hip|pelvis|lThighBend.rotateX -dv 0;')
    mel.eval('select {}'.format(pyBlendShapesName))
    mel.eval('setDrivenKeyframe -at pJCMToonDwayne8ThighFwd_115_R -v 1 -cd Genesis8Male_Group|hip|pelvis|rThighBend.rotateX -dv -115;')
    mel.eval('setDrivenKeyframe -at pJCMToonDwayne8ThighFwd_115_R -v 0 -cd Genesis8Male_Group|hip|pelvis|rThighBend.rotateX -dv -57;')
    mel.eval('select {}'.format(pyBlendShapesName))
    mel.eval('setDrivenKeyframe -at pJCMToonDwayne8ThighFwd_115_L -v 1 -cd Genesis8Male_Group|hip|pelvis|lThighBend.rotateX -dv -115;')
    mel.eval('setDrivenKeyframe -at pJCMToonDwayne8ThighFwd_115_L -v 0 -cd Genesis8Male_Group|hip|pelvis|lThighBend.rotateX -dv -57;')
    mel.eval('select {}'.format(pyBlendShapesName))
    mel.eval('setDrivenKeyframe -itt linear -ott linear -at pJCMToonDwayne8ThighBack_35_R -v 0 -cd Genesis8Male_Group|hip|pelvis|rThighBend.rotateX -dv 0;')
    mel.eval('setDrivenKeyframe -itt linear -ott linear -at pJCMToonDwayne8ThighBack_35_R -v 1 -cd Genesis8Male_Group|hip|pelvis|rThighBend.rotateX -dv 35;')
    mel.eval('select {}'.format(pyBlendShapesName))
    mel.eval('setDrivenKeyframe -itt linear -ott linear -at pJCMToonDwayne8ThighBack_35_L -v 0 -cd Genesis8Male_Group|hip|pelvis|lThighBend.rotateX -dv 0;')
    mel.eval('setDrivenKeyframe -itt linear -ott linear -at pJCMToonDwayne8ThighBack_35_L -v 1 -cd Genesis8Male_Group|hip|pelvis|lThighBend.rotateX -dv 35;')

    # pJCMToonDwayne8UpperNeck_Bend_n27_Twist_22
    cmds.expression(
        s='if ((neckUpper.rotateX * -0.0370370) * (neckUpper.rotateY * 0.0454546) > 0)\n\t{0}.pJCMToonDwayne8UpperNeck_Bend_n27_Twist_22 = (neckUpper.rotateX * -0.0370370) * (neckUpper.rotateY * 0.0454546);\nelse\n\t{0}.pJCMToonDwayne8UpperNeck_Bend_n27_Twist_22 = 0'.format(
            pyBlendShapesName), o='{0}'.format(pyBlendShapesName),
        n='pJCMToonDwayne8UpperNeck_Bend_n27_Twist_22_exp', ae=1,
        uc='all')
    
    # pJCMToonDwayne8UpperNeck_Bend_n27_Twist_n22
    cmds.expression(
        s='if ((neckUpper.rotateX * -0.0370370) * (neckUpper.rotateY * -0.0454546) > 0)\n\t{0}.pJCMToonDwayne8UpperNeck_Bend_n27_Twist_n22 = (neckUpper.rotateX * -0.0370370) * (neckUpper.rotateY * -0.0454546);\nelse\n\t{0}.pJCMToonDwayne8UpperNeck_Bend_n27_Twist_n22 = 0'.format(
            pyBlendShapesName), o='{0}'.format(pyBlendShapesName),
        n='pJCMToonDwayne8UpperNeck_Bend_n27_Twist_n22_exp', ae=1,
        uc='all')
