import maya.mel as mel
import maya.cmds as cmds


def Charlotte8JCMs(character, blendShapeGroup):
    pyBlendShapesName = blendShapeGroup
    if not cmds.objExists('{}.pJCMCharlotte8_AbdomenUpperBndFwd'.format(pyBlendShapesName)):
        error = character + ' specific JCMs were not created.\n\n' + character + ' JCMs were either not exported out properly or the wrong file was selected for import.'
        cmds.warning(error.replace('\n\n', ' '))
        return error

    # pJCMCharlotte8_AbdomenLowerBndFwd
    mel.eval('setAttr "abdomenLower.rotateX" 0;')
    mel.eval('setDrivenKeyframe -currentDriver abdomenLower.rotateX {0}.pJCMCharlotte8_AbdomenLowerBndFwd;'.format(
        pyBlendShapesName))
    mel.eval('setAttr "abdomenLower.rotateX" 30;')
    mel.eval('setAttr "{0}.pJCMCharlotte8_AbdomenLowerBndFwd" 1;'.format(pyBlendShapesName))
    mel.eval('setDrivenKeyframe -currentDriver abdomenLower.rotateX {0}.pJCMCharlotte8_AbdomenLowerBndFwd;'.format(
        pyBlendShapesName))
    mel.eval('setAttr "abdomenLower.rotateX" 0;')

    # pJCMCharlotte8_AbdomenUpperBndFwd
    mel.eval('setAttr "abdomenUpper.rotateX" 0;')
    mel.eval('setDrivenKeyframe -currentDriver abdomenUpper.rotateX {0}.pJCMCharlotte8_AbdomenUpperBndFwd;'.format(
        pyBlendShapesName))
    mel.eval('setAttr "abdomenUpper.rotateX" 40;')
    mel.eval('setAttr "{0}.pJCMCharlotte8_AbdomenUpperBndFwd" 1;'.format(pyBlendShapesName))
    mel.eval('setDrivenKeyframe -currentDriver abdomenUpper.rotateX {0}.pJCMCharlotte8_AbdomenUpperBndFwd;'.format(
        pyBlendShapesName))
    mel.eval('setAttr "abdomenUpper.rotateX" 0;')

    # pJCMCharlotte8_CollarBndUpL
    mel.eval('setAttr "lCollar.rotateZ" 0;')
    mel.eval('setDrivenKeyframe -currentDriver lCollar.rotateZ {0}.pJCMCharlotte8_CollarBndUpL;'.format(
        pyBlendShapesName))
    mel.eval('setAttr "lCollar.rotateZ" 55;')
    mel.eval('setAttr "{0}.pJCMCharlotte8_CollarBndUpL" 1;'.format(pyBlendShapesName))
    mel.eval('setDrivenKeyframe -currentDriver lCollar.rotateZ {0}.pJCMCharlotte8_CollarBndUpL;'.format(
        pyBlendShapesName))
    mel.eval('setAttr "lCollar.rotateZ" 0;')

    # pJCMCharlotte8_CollarBndUpR
    mel.eval('setAttr "rCollar.rotateZ" 0;')
    mel.eval('setDrivenKeyframe -currentDriver rCollar.rotateZ {0}.pJCMCharlotte8_CollarBndUpR;'.format(
        pyBlendShapesName))
    mel.eval('setAttr "rCollar.rotateZ" -55;')
    mel.eval('setAttr "{0}.pJCMCharlotte8_CollarBndUpR" 1;'.format(pyBlendShapesName))
    mel.eval('setDrivenKeyframe -currentDriver rCollar.rotateZ {0}.pJCMCharlotte8_CollarBndUpR;'.format(
        pyBlendShapesName))
    mel.eval('setAttr "rCollar.rotateZ" 0;')

    # pJCMCharlotte8_FootBndUpL
    mel.eval('setAttr "lFoot.rotateX" 0;')
    mel.eval('setDrivenKeyframe -currentDriver lFoot.rotateX {0}.pJCMCharlotte8_FootBndUpL;'.format(pyBlendShapesName))
    mel.eval('setAttr "lFoot.rotateX" -45;')
    mel.eval('setAttr "{0}.pJCMCharlotte8_FootBndUpL" 1;'.format(pyBlendShapesName))
    mel.eval('setDrivenKeyframe -currentDriver lFoot.rotateX {0}.pJCMCharlotte8_FootBndUpL;'.format(pyBlendShapesName))
    mel.eval('setAttr "lFoot.rotateX" 0;')

    # pJCMCharlotte8_FootBndUpR
    mel.eval('setAttr "rFoot.rotateX" 0;')
    mel.eval('setDrivenKeyframe -currentDriver rFoot.rotateX {0}.pJCMCharlotte8_FootBndUpR;'.format(pyBlendShapesName))
    mel.eval('setAttr "rFoot.rotateX" -45;')
    mel.eval('setAttr "{0}.pJCMCharlotte8_FootBndUpR" 1;'.format(pyBlendShapesName))
    mel.eval('setDrivenKeyframe -currentDriver rFoot.rotateX {0}.pJCMCharlotte8_FootBndUpR;'.format(pyBlendShapesName))
    mel.eval('setAttr "rFoot.rotateX" 0;')

    # pJCMCharlotte8_ForearmBndL
    mel.eval('setAttr "lForearmBend.rotateY" 0;')
    mel.eval('setDrivenKeyframe -currentDriver lForearmBend.rotateY {0}.pJCMCharlotte8_ForearmBndL;'.format(
        pyBlendShapesName))
    mel.eval('setAttr "lForearmBend.rotateY" -135;')
    mel.eval('setAttr "{0}.pJCMCharlotte8_ForearmBndL" 1;'.format(pyBlendShapesName))
    mel.eval('setDrivenKeyframe -currentDriver lForearmBend.rotateY {0}.pJCMCharlotte8_ForearmBndL;'.format(
        pyBlendShapesName))
    mel.eval('setAttr "lForearmBend.rotateY" 0;')

    # pJCMCharlotte8_ForearmBndR
    mel.eval('setAttr "rForearmBend.rotateY" 0;')
    mel.eval('setDrivenKeyframe -currentDriver rForearmBend.rotateY {0}.pJCMCharlotte8_ForearmBndR;'.format(
        pyBlendShapesName))
    mel.eval('setAttr "rForearmBend.rotateY" 135;')
    mel.eval('setAttr "{0}.pJCMCharlotte8_ForearmBndR" 1;'.format(pyBlendShapesName))
    mel.eval('setDrivenKeyframe -currentDriver rForearmBend.rotateY {0}.pJCMCharlotte8_ForearmBndR;'.format(
        pyBlendShapesName))
    mel.eval('setAttr "rForearmBend.rotateY" 0;')

    # pJCMCharlotte8_HeadBndDn_HDLv3
    mel.eval('setAttr "head.rotateX" 0;')
    mel.eval('setDrivenKeyframe -currentDriver head.rotateX {0}.pJCMCharlotte8_HeadBndDn_HDLv3;'.format(pyBlendShapesName))
    mel.eval('setAttr "head.rotateX" 25;')
    mel.eval('setAttr "{0}.pJCMCharlotte8_HeadBndDn_HDLv3" 1;'.format(pyBlendShapesName))
    mel.eval('setDrivenKeyframe -currentDriver head.rotateX {0}.pJCMCharlotte8_HeadBndDn_HDLv3;'.format(pyBlendShapesName))
    mel.eval('setAttr "head.rotateX" 0;')

    # pJCMCharlotte8_NeckUpperTwistL_HDLv3
    mel.eval('setAttr "neckUpper.rotateY" 0;')
    mel.eval('setDrivenKeyframe -currentDriver neckUpper.rotateY {0}.pJCMCharlotte8_NeckUpperTwistL_HDLv3;'.format(pyBlendShapesName))
    mel.eval('setAttr "neckUpper.rotateY" 22;')
    mel.eval('setAttr "{0}.pJCMCharlotte8_NeckUpperTwistL_HDLv3" 1;'.format(pyBlendShapesName))
    mel.eval('setDrivenKeyframe -currentDriver neckUpper.rotateY {0}.pJCMCharlotte8_NeckUpperTwistL_HDLv3;'.format(pyBlendShapesName))
    mel.eval('setAttr "neckUpper.rotateY" 0;')

    # pJCMCharlotte8_NeckUpperTwistR_HDLv3
    mel.eval('setAttr "neckUpper.rotateY" 0;')
    mel.eval('setDrivenKeyframe -currentDriver neckUpper.rotateY {0}.pJCMCharlotte8_NeckUpperTwistR_HDLv3;'.format(
        pyBlendShapesName))
    mel.eval('setAttr "neckUpper.rotateY" -22;')
    mel.eval('setAttr "{0}.pJCMCharlotte8_NeckUpperTwistR_HDLv3" 1;'.format(pyBlendShapesName))
    mel.eval('setDrivenKeyframe -currentDriver neckUpper.rotateY {0}.pJCMCharlotte8_NeckUpperTwistR_HDLv3;'.format(
        pyBlendShapesName))
    mel.eval('setAttr "neckUpper.rotateY" 0;')
    
    # pJCMCharlotte8_ShinBndBck_90L
    mel.eval('setAttr "lShin.rotateX" 0;')
    mel.eval(
        'setDrivenKeyframe -currentDriver lShin.rotateX {0}.pJCMCharlotte8_ShinBndBck_90L;'.format(pyBlendShapesName))
    mel.eval('setAttr "lShin.rotateX" 90;')
    mel.eval('setAttr "{0}.pJCMCharlotte8_ShinBndBck_90L" 1;'.format(pyBlendShapesName))
    mel.eval(
        'setDrivenKeyframe -currentDriver lShin.rotateX {0}.pJCMCharlotte8_ShinBndBck_90L;'.format(pyBlendShapesName))
    mel.eval('setAttr "lShin.rotateX" 0;')

    # pJCMCharlotte8_ShinBndBck_90R
    mel.eval('setAttr "rShin.rotateX" 0;')
    mel.eval(
        'setDrivenKeyframe -currentDriver rShin.rotateX {0}.pJCMCharlotte8_ShinBndBck_90R;'.format(pyBlendShapesName))
    mel.eval('setAttr "rShin.rotateX" 90;')
    mel.eval('setAttr "{0}.pJCMCharlotte8_ShinBndBck_90R" 1;'.format(pyBlendShapesName))
    mel.eval(
        'setDrivenKeyframe -currentDriver rShin.rotateX {0}.pJCMCharlotte8_ShinBndBck_90R;'.format(pyBlendShapesName))
    mel.eval('setAttr "rShin.rotateX" 0;')

    # pJCMCharlotte8_ShinBndBckL
    mel.eval('setAttr "lShin.rotateX" 0;')
    mel.eval(
        'setDrivenKeyframe -currentDriver lShin.rotateX {0}.pJCMCharlotte8_ShinBndBckL;'.format(pyBlendShapesName))
    mel.eval('setAttr "lShin.rotateX" 155;')
    mel.eval('setAttr "{0}.pJCMCharlotte8_ShinBndBckL" 1;'.format(pyBlendShapesName))
    mel.eval(
        'setDrivenKeyframe -currentDriver lShin.rotateX {0}.pJCMCharlotte8_ShinBndBckL;'.format(pyBlendShapesName))
    mel.eval('setAttr "lShin.rotateX" 0;')

    # pJCMCharlotte8_ShinBndBckR
    mel.eval('setAttr "rShin.rotateX" 0;')
    mel.eval(
        'setDrivenKeyframe -currentDriver rShin.rotateX {0}.pJCMCharlotte8_ShinBndBckR;'.format(pyBlendShapesName))
    mel.eval('setAttr "rShin.rotateX" 155;')
    mel.eval('setAttr "{0}.pJCMCharlotte8_ShinBndBckR" 1;'.format(pyBlendShapesName))
    mel.eval(
        'setDrivenKeyframe -currentDriver rShin.rotateX {0}.pJCMCharlotte8_ShinBndBckR;'.format(pyBlendShapesName))
    mel.eval('setAttr "rShin.rotateX" 0;')

    # pJCMCharlotte8_ShoulderBndDn_n30L
    mel.eval('setAttr "lShldrBend.rotateZ" 0;')
    mel.eval('setDrivenKeyframe -currentDriver lShldrBend.rotateZ {0}.pJCMCharlotte8_ShoulderBndDn_n30L;'.format(
        pyBlendShapesName))
    mel.eval('setAttr "lShldrBend.rotateZ" -30;')
    mel.eval('setAttr "{0}.pJCMCharlotte8_ShoulderBndDn_n30L" 1;'.format(pyBlendShapesName))
    mel.eval('setDrivenKeyframe -currentDriver lShldrBend.rotateZ {0}.pJCMCharlotte8_ShoulderBndDn_n30L;'.format(
        pyBlendShapesName))
    mel.eval('setAttr "lShldrBend.rotateZ" 0;')

    # pJCMCharlotte8_ShoulderBndDn_n35L_HDLv3
    mel.eval('setAttr "lShldrBend.rotateZ" 0;')
    mel.eval('setDrivenKeyframe -currentDriver lShldrBend.rotateZ {0}.pJCMCharlotte8_ShoulderBndDn_n35L_HDLv3;'.format(
        pyBlendShapesName))
    mel.eval('setAttr "lShldrBend.rotateZ" -35;')
    mel.eval('setAttr "{0}.pJCMCharlotte8_ShoulderBndDn_n35L_HDLv3" 1;'.format(pyBlendShapesName))
    mel.eval('setDrivenKeyframe -currentDriver lShldrBend.rotateZ {0}.pJCMCharlotte8_ShoulderBndDn_n35L_HDLv3;'.format(
        pyBlendShapesName))
    mel.eval('setAttr "lShldrBend.rotateZ" 0;')

    # pJCMCharlotte8_ShoulderBndDn_30R
    mel.eval('setAttr "rShldrBend.rotateZ" 0;')
    mel.eval('setDrivenKeyframe -currentDriver rShldrBend.rotateZ {0}.pJCMCharlotte8_ShoulderBndDn_30R;'.format(
        pyBlendShapesName))
    mel.eval('setAttr "rShldrBend.rotateZ" 30;')
    mel.eval('setAttr "{0}.pJCMCharlotte8_ShoulderBndDn_30R" 1;'.format(pyBlendShapesName))
    mel.eval('setDrivenKeyframe -currentDriver rShldrBend.rotateZ {0}.pJCMCharlotte8_ShoulderBndDn_30R;'.format(
        pyBlendShapesName))
    mel.eval('setAttr "rShldrBend.rotateZ" 0;')

    # pJCMCharlotte8_ShoulderBndDn_35R_HDLv3
    mel.eval('setAttr "rShldrBend.rotateZ" 0;')
    mel.eval('setDrivenKeyframe -currentDriver rShldrBend.rotateZ {0}.pJCMCharlotte8_ShoulderBndDn_35R_HDLv3;'.format(
        pyBlendShapesName))
    mel.eval('setAttr "rShldrBend.rotateZ" 35;')
    mel.eval('setAttr "{0}.pJCMCharlotte8_ShoulderBndDn_35R_HDLv3" 1;'.format(pyBlendShapesName))
    mel.eval('setDrivenKeyframe -currentDriver rShldrBend.rotateZ {0}.pJCMCharlotte8_ShoulderBndDn_35R_HDLv3;'.format(
        pyBlendShapesName))
    mel.eval('setAttr "rShldrBend.rotateZ" 0;')

    # pJCMCharlotte8_ShoulderBndUpL
    mel.eval('setAttr "lShldrBend.rotateZ" 0;')
    mel.eval('setDrivenKeyframe -currentDriver lShldrBend.rotateZ {0}.pJCMCharlotte8_ShoulderBndUpL;'.format(
        pyBlendShapesName))
    mel.eval('setAttr "lShldrBend.rotateZ" 90;')
    mel.eval('setAttr "{0}.pJCMCharlotte8_ShoulderBndUpL" 1;'.format(pyBlendShapesName))
    mel.eval('setDrivenKeyframe -currentDriver lShldrBend.rotateZ {0}.pJCMCharlotte8_ShoulderBndUpL;'.format(
        pyBlendShapesName))
    mel.eval('setAttr "lShldrBend.rotateZ" 0;')

    # pJCMCharlotte8_ShoulderBndUpR
    mel.eval('setAttr "rShldrBend.rotateZ" 0;')
    mel.eval('setDrivenKeyframe -currentDriver rShldrBend.rotateZ {0}.pJCMCharlotte8_ShoulderBndUpR;'.format(
        pyBlendShapesName))
    mel.eval('setAttr "rShldrBend.rotateZ" -90;')
    mel.eval('setAttr "{0}.pJCMCharlotte8_ShoulderBndUpR" 1;'.format(pyBlendShapesName))
    mel.eval('setDrivenKeyframe -currentDriver rShldrBend.rotateZ {0}.pJCMCharlotte8_ShoulderBndUpR;'.format(
        pyBlendShapesName))
    mel.eval('setAttr "rShldrBend.rotateZ" 0;')

    # pJCMCharlotte8_ShoulderFwd_n90L
    mel.eval('setAttr "lShldrBend.rotateY" 0;')
    mel.eval('setDrivenKeyframe -currentDriver lShldrBend.rotateY {0}.pJCMCharlotte8_ShoulderFwd_n90L;'.format(
        pyBlendShapesName))
    mel.eval('setAttr "lShldrBend.rotateY" -90;')
    mel.eval('setAttr "{0}.pJCMCharlotte8_ShoulderFwd_n90L" 1;'.format(pyBlendShapesName))
    mel.eval('setDrivenKeyframe -currentDriver lShldrBend.rotateY {0}.pJCMCharlotte8_ShoulderFwd_n90L;'.format(
        pyBlendShapesName))
    mel.eval('setAttr "lShldrBend.rotateY" 0;')

    # pJCMCharlotte8_ShoulderFwd_90R
    mel.eval('setAttr "rShldrBend.rotateY" 0;')
    mel.eval('setDrivenKeyframe -currentDriver rShldrBend.rotateY {0}.pJCMCharlotte8_ShoulderFwd_90R;'.format(
        pyBlendShapesName))
    mel.eval('setAttr "rShldrBend.rotateY" 90;')
    mel.eval('setAttr "{0}.pJCMCharlotte8_ShoulderFwd_90R" 1;'.format(pyBlendShapesName))
    mel.eval('setDrivenKeyframe -currentDriver rShldrBend.rotateY {0}.pJCMCharlotte8_ShoulderFwd_90R;'.format(
        pyBlendShapesName))
    mel.eval('setAttr "rShldrBend.rotateY" 0;')
    
    # pJCMCharlotte8_ThighSideSideL
    mel.eval('setAttr "lThighBend.rotateZ" 0;')
    mel.eval('setDrivenKeyframe -currentDriver lThighBend.rotateZ {0}.pJCMCharlotte8_ThighSideSideL;'.format(
        pyBlendShapesName))
    mel.eval('setAttr "lThighBend.rotateZ" 85;')
    mel.eval('setAttr "{0}.pJCMCharlotte8_ThighSideSideL" 1;'.format(pyBlendShapesName))
    mel.eval('setDrivenKeyframe -currentDriver lThighBend.rotateZ {0}.pJCMCharlotte8_ThighSideSideL;'.format(
        pyBlendShapesName))
    mel.eval('setAttr "lThighBend.rotateZ" 0;')

    # pJCMCharlotte8_ThighSideSideR
    mel.eval('setAttr "rThighBend.rotateZ" 0;')
    mel.eval('setDrivenKeyframe -currentDriver rThighBend.rotateZ {0}.pJCMCharlotte8_ThighSideSideR;'.format(
        pyBlendShapesName))
    mel.eval('setAttr "rThighBend.rotateZ" -85;')
    mel.eval('setAttr "{0}.pJCMCharlotte8_ThighSideSideR" 1;'.format(pyBlendShapesName))
    mel.eval('setDrivenKeyframe -currentDriver rThighBend.rotateZ {0}.pJCMCharlotte8_ThighSideSideR;'.format(
        pyBlendShapesName))
    mel.eval('setAttr "rThighBend.rotateZ" 0;')
